default = {
	#navy = {
		#male = {
		#}
		#female = {
		#}
	#}

	#army = {
		#male = {
		#}
		#female = {
		#}
	#}

	male = {
		"gfx/leaders/leader_unknown.dds"
	}
	female = {
		"gfx/leaders/leader_unknown.dds"
	}
}

continent = {
	name = europe
	army = {
		male = {
			"gfx/leaders/Europe/Portrait_Europe_Generic_land_1.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_land_2.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_land_3.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_land_4.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_land_5.dds"
			"gfx/leaders/Europe/portrait_europe_generic_land_13.dds"
		}
	}

	navy = {
		male = {
			"gfx/leaders/Europe/Portrait_Europe_Generic_navy_1.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_navy_2.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_navy_3.dds"
		}
	}

	political = {
		communism = {
				male = {
					"gfx/leaders/Europe/Portrait_Europe_Generic_1.dds"
			}
		}
		democratic = {
				male = {
					"gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
			}
		}
		fascism = {
				male = {
					"gfx/leaders/Europe/Portrait_Europe_Generic_3.dds"
			}
		}
		neutrality = {
				male = {
					"gfx/leaders/Europe/Portrait_Europe_Generic_3.dds"
			}
		}
	}
}

GER = {
	army = {
		male = {
			"gfx/leaders/GER/Portrait_Germany_Generic_land_1.dds"
			"gfx/leaders/GER/Portrait_Germany_Generic_land_2.dds"
			"gfx/leaders/GER/Portrait_Germany_Generic_land_3.dds"
			"gfx/leaders/GER/Portrait_Germany_Generic_land_4.dds"
			"gfx/leaders/GER/Portrait_Germany_Generic_land_5.dds"
		}
	}

	navy = {
		male = {
			"gfx/leaders/GER/Portrait_Germany_Generic_navy_1.dds"
			"gfx/leaders/GER/Portrait_Germany_Generic_navy_2.dds"
			"gfx/leaders/GER/Portrait_Germany_Generic_navy_3.dds"
		}
	}
}

ENG = {
	army = {
		male = {
			"gfx/leaders/ENG/Portrait_Britain_Generic_land_1.dds"
			"gfx/leaders/ENG/Portrait_Britain_Generic_land_2.dds"
			"gfx/leaders/ENG/Portrait_Britain_Generic_land_3.dds"
			"gfx/leaders/ENG/Portrait_Britain_Generic_land_4.dds"
			"gfx/leaders/ENG/Portrait_Britain_Generic_land_5.dds"
		}
	}

	navy = {
		male = {
			"gfx/leaders/ENG/Portrait_Britain_Generic_navy_1.dds"
			"gfx/leaders/ENG/Portrait_Britain_Generic_navy_2.dds"
			"gfx/leaders/ENG/Portrait_Britain_Generic_navy_3.dds"
		}
	}
}

FRA = {
	army = {
		male = {
			"gfx/leaders/FRA/Portrait_France_Generic_land_1.dds"
			"gfx/leaders/FRA/Portrait_France_Generic_land_2.dds"
			"gfx/leaders/FRA/Portrait_France_Generic_land_3.dds"
			"gfx/leaders/FRA/Portrait_France_Generic_land_4.dds"
			"gfx/leaders/FRA/Portrait_France_Generic_land_5.dds"
		}
	}

	navy = {
		male = {
			"gfx/leaders/FRA/Portrait_France_Generic_navy_1.dds"
			"gfx/leaders/FRA/Portrait_France_Generic_navy_2.dds"
			"gfx/leaders/FRA/Portrait_France_Generic_navy_3.dds"
		}
	}
}

ITA = {
	army = {
		male = {
			"gfx/leaders/ITA/Portrait_Italy_Generic_land_1.dds"
			"gfx/leaders/ITA/Portrait_Italy_Generic_land_2.dds"
			"gfx/leaders/ITA/Portrait_Italy_Generic_land_3.dds"
		}
	}

	navy = {
		male = {
			"gfx/leaders/ITA/Portrait_Italy_Generic_navy_1.dds"
			"gfx/leaders/ITA/Portrait_Italy_Generic_navy_2.dds"
			"gfx/leaders/ITA/Portrait_Italy_Generic_navy_3.dds"
		}
	}
}

SOV = {
	army = {
		male = {
			"gfx/leaders/SOV/Portrait_Soviet_Generic_land_1.dds"
			"gfx/leaders/SOV/Portrait_Soviet_Generic_land_2.dds"
			"gfx/leaders/SOV/Portrait_Soviet_Generic_land_3.dds"
			"gfx/leaders/SOV/Portrait_Soviet_Generic_land_4.dds"
			"gfx/leaders/SOV/Portrait_Soviet_Generic_land_5.dds"
			"gfx/leaders/SOV/portrait_soviet_pdxcon_magic_winner.dds"
		}
	}

	navy = {
		male = {
			"gfx/leaders/SOV/Portrait_Soviet_Generic_navy_1.dds"
			"gfx/leaders/SOV/Portrait_Soviet_Generic_navy_2.dds"
			"gfx/leaders/SOV/Portrait_Soviet_Generic_navy_3.dds"
		}
	}
}

USA = {
	army = {
		male = {
			"gfx/leaders/USA/Portrait_USA_Generic_land_1.dds"
			"gfx/leaders/USA/Portrait_USA_Generic_land_2.dds"
			"gfx/leaders/USA/Portrait_USA_Generic_land_3.dds"
			"gfx/leaders/USA/Portrait_USA_Generic_land_4.dds"
			"gfx/leaders/USA/Portrait_USA_Generic_land_5.dds"
		}
	}

	navy = {
		male = {
			"gfx/leaders/USA/Portrait_USA_Generic_navy_1.dds"
			"gfx/leaders/USA/Portrait_USA_Generic_navy_2.dds"
			"gfx/leaders/USA/Portrait_USA_Generic_navy_3.dds"
		}
	}
}

SPR = {
	army = {
		male = {
			"gfx/leaders/Europe/Portrait_Europe_Generic_land_1.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_land_2.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_land_3.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_land_4.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_land_5.dds"
		}
	}

	navy = {
		male = {
			"gfx/leaders/Europe/Portrait_Europe_Generic_navy_1.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_navy_2.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_navy_3.dds"
		}
	}
}

HOL = {
	army = {
		male = {
			"gfx/leaders/Europe/Portrait_Europe_Generic_land_1.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_land_2.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_land_3.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_land_4.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_land_5.dds"
			"gfx/leaders/Europe/portrait_europe_generic_land_13.dds"
		}
	}

	navy = {
		male = {
			"gfx/leaders/Europe/Portrait_Europe_Generic_navy_1.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_navy_2.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_navy_3.dds"
		}
	}

}
