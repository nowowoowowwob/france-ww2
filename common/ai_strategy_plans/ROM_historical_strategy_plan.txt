ROM_historical = {
	name = "Romanian historical plan"
	desc = ""

	allowed = {
		original_tag = ROM
		has_dlc = "Death or Dishonor"
	}
	enable = {
		OR = {
			is_historical_focus_on = yes
			has_country_flag = ROM_AI_RANDOM_FASCIST_HISTORICAL
			has_game_rule = {
				rule = ROM_ai_behavior
				option = FASCIST_HISTORICAL
			}
		}
	}
	abort = {

	}

	ai_national_focuses = {
		ROM_king_michaels_coup #ASAP

		ROM_preserve_greater_romania						#35
		ROM_army_maneuvers								#35
		ROM_expand_the_air_force							#30
		ROM_civil_works										#45
		ROM_agrarian_reform								#45
		ROM_local_development								#35
		ROM_army_war_college								#35
		ROM_danubian_transport_network					#35
		ROM_malaxa										#45
		ROM_institute_royal_dictatorship					#60
		ROM_revise_the_constitution						#70
		ROM_flexible_foreign_policy						#35
		ROM_air_superiority								#35
		ROM_trade_treaty_with_germany						#35
		ROM_iar_80										#35
		ROM_cas											#35
		ROM_royal_guards_divisions						#35
		ROM_the_zb_53										#30
		ROM_vanatori_de_munte								#70
		ROM_hunedoara_steel_works							#60
		ROM_appoint_german_friendly_government			#45
		ROM_invite_german_advisors						#70
		ROM_iron_guard									#35
		ROM_force_abdication								#70
		ROM_the_royal_foundation							#70
		ROM_join_axis										#10
		ROM_the_armored_division							#35
		ROM_expand_ploiesti_oil_production				#45
		ROM_german_romanian_oil_exploitation_company		#45
		ROM_expand_the_university_of_bucharest			#70
		ROM_acquire_modern_tanks							#35
		ROM_invest_in_the_iar								#70
		ROM_artillery_modernization							#35
		ROM_mobile_tank_destroyers						#35
		ROM_mountain_artillery							#35
		ROM_national_defense_industry						#60

		ROM_expand_the_marine_regiment					#70
		ROM_fortify_the_borders							#30

		ROM_the_maresal									#35
		ROM_exploit_the_baita_mines						#70
		ROM_a_deal_with_the_devil							#35
		ROM_form_peasant_militias							#45

		ROM_king_michaels_coup							#45
		ROM_appoint_allied_friendly_government			#45
		ROM_basing_rights_for_soviet_union					#45
		ROM_join_comintern									#45
	}

	research = {
		industry = 50.0
		infantry_tech = 15.0
		artillery = 8.0
		support_tech = 6.5
	}

	ideas = {

	}

	traits = {
		captain_of_industry = 5
		war_industrialist = 5
	}

	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}

	focus_factors = {

	}

}

