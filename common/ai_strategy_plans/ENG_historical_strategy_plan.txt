ENG_historical_plan = {
	name = "United Kingdom historical plan"
	desc = "Historical behavior for United Kingdom"

	allowed = {
		original_tag = ENG
	}
	enable = {
		OR = {
			AND = {
				is_historical_focus_on = yes
				OR = {
					not = { has_dlc = "Man the Guns" }
					has_game_rule = {
						rule = ENG_ai_behavior
						option = DEFAULT
					}
				}
			}
			has_country_flag = ENG_AI_DEMOCRATIC_HISTORICAL
			has_game_rule = {
				rule = ENG_ai_behavior
				option = DEMOCRATIC_HISTORICAL
			}
		}
	}

	ai_national_focuses = {
		limited_rearmament_focus
		shadow_scheme_focus
		uk_industrial_focus
		general_rearmament_focus
		royal_ordinance_focus
		uk_extra_tech_slot
		uk_empire_focus
		uk_colonial_focus
		uk_commonwealth_focus
		uk_canada_focus
		uk_australia_focus
		uk_new_zealand_focus
		uk_india_focus
		uk_south_africa_focus
		ENG_imperial_conference
		uk_small_arms_focus
		air_defense_focus
		radar_focus
		air_rearmament_focus
		ENG_steady_as_she_goes
		ENG_home_defence
		ENG_prepare_for_the_inevitable
		ENG_issue_gasmasks
		fighter_command_focus
		bomber_command_focus
		coastal_command_focus
		aircraft_production_focus
		allies_ENG_spitfire
		ENG_buy_the_cmp_truck
		ENG_cruiser_tanks
		allies_ENG_found_joint_intelligence_committee
		allies_middle_eastern_policy
		allies_enforce_petroleum_quota_iraq
		allies_ban_golden_square_iraq
		uk_mediterranean_focus
		ENG_british_commonwealth_air_training_plan
		uk_rock_focus
		uk_malta_focus
		ENG_military_training_act
		crypto_bomb_focus
		uk_balkan_strategy
		uk_sanction_italy_focus
		ENG_embargo_germany #1940.5.10
		tizard_mission_focus
		maud_focus
		ENG_discovery_of_nuclear_fission
		ENG_a_comet_strikes
		allies_prepare_for_african_campaign
		allies_operation_compass
		uk_protect_suez
		allies_operation_sabine
		uk_scandinavian_focus
		allies_el_alamein_fortifications
		ENG_chiefs_of_staff_committee #1941.6.22
		allies_operation_brevity_battleaxe
		allies_operation_crusader
		allies_ultimatum_to_iran
		allies_operation_countenance
		allies_depose_the_shah
		allies_ENG_supermarine_spiteful
		ENG_tube_alloys_project
		allies_cairo_fallback_line
		uk_amphibious_focus
		ENG_special_air_service
		UK_secret_focus #1942.11.19
	}

	research = {

	}

	ideas = {
		home_defence_spirit = 1
	}

	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}

}
