NDefines.NGraphics.UNITS_ICONS_DISTANCE_CUTOFF = 1800
NDefines.NGraphics.UNITS_DISTANCE_CUTOFF = 180
NDefines.NGraphics.VICTORY_POINTS_DISTANCE_CUTOFF = {500, 750, 1250}
NDefines.NGraphics.VICTORY_POINT_MAP_ICON_TEXT_CUTOFF = {350, 600, 1100}
NDefines.NGraphics.CAPITAL_ICON_CUTOFF = 2000
NDefines.NGraphics.STATE_BORDER_FADE_NEAR = 900
NDefines.NGraphics.STATE_BORDER_FADE_FAR = 1100
NDefines.NGraphics.BORDER_WIDTH = 0.8
NDefines.NInterface.SLOW_INTERFACE_THRESHOLD = 15000
