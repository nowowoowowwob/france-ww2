NDefines.NGame.MAP_SCALE_PIXEL_TO_KM = 4                            --180 km, 296 pixels: Just air
NDefines.NGame.LAG_DAYS_FOR_LOWER_SPEED = 25 --from 10
NDefines.NGame.LAG_DAYS_FOR_PAUSE = 50 --from 25
NDefines.NFocus.FOCUS_POINT_DAYS = 1
NDefines.NFocus.MAX_SAVED_FOCUS_PROGRESS = 20

------------------------------------------ Buildings ----------------------------------------
NDefines.NBuildings.RADAR_RANGE_MAX = 300
NDefines.NBuildings.MAX_BUILDING_LEVELS = 200
NDefines.NBuildings.MAX_SHARED_SLOTS = 200
NDefines.NBuildings.ANTI_AIR_SUPERIORITY_MULT = 20.0	-- How much air superiority reduction to the enemy does our AA guns? Normally each building level = -1 reduction. With this multiplier, from 5
NDefines.NBuildings.OWNER_CHANGE_EXTRA_SHARED_SLOTS_FACTOR = 1

------------------------------------------ Country ------------------------------------------
-- NDefines.NCountry.BASE_MAX_COMMAND_POWER = 200.0 --Now in vanilla
NDefines.NCountry.STARTING_COMMAND_POWER = 100.0
NDefines.NCountry.BASE_MOBILIZATION_SPEED = 0.02
NDefines.NCountry.POLITICAL_POWER_CAP = 5000.0
NDefines.NCountry.POPULATION_YEARLY_GROWTH_BASE = 0 --No new people, so less calculations
NDefines.NCountry.REINFORCEMENT_EQUIPMENT_DELIVERY_SPEED = 0.6	-- Modifier for army equipment reinforcement speed, from 0.3
NDefines.NCountry.REINFORCEMENT_MANPOWER_DELIVERY_SPEED = 15	-- Modifier for army manpower reinforcement delivery speed (travel time), from 10
NDefines.NCountry.BASE_COMMAND_POWER_GAIN = 0.2 -- from 0.006
NDefines.NCountry.MAX_HEROES_BEING_KILLED_WAR_SUPPORT_IMPACT = -0.2 -- from -0.3
NDefines.NCountry.MAX_CONVOYS_BEING_RAIDED_WAR_SUPPORT_IMPACT = -0.3 -- from -0.5
NDefines.NCountry.BOMBING_WEEKLY_WAR_SUPPORT_PENALTY_DECAY = 0.005 -- from 0.001
NDefines.NCountry.CONVOYS_BEING_RAIDED_WEEKLY_WAR_SUPPORT_PENALTY_DECAY = 0.005 -- from 0.003

NDefines.NTechnology.BASE_RESEARCH_POINTS_SAVED = 60.0

------------------------------------------ Military -----------------------------------------
NDefines.NMilitary.LAND_SPEED_MODIFIER = 0.13                       --Land movement speed. from 0.05
NDefines.NMilitary.RIVER_CROSSING_PENALTY_LARGE = -0.4              --Bridges, from -0.6
NDefines.NMilitary.RIVER_CROSSING_SPEED_PENALTY_LARGE = -0.4              --Bridges, from -0.5
NDefines.NMilitary.MAX_DIVISION_BRIGADE_WIDTH = 4
NDefines.NMilitary.MAX_DIVISION_BRIGADE_HEIGHT = 3
NDefines.NMilitary.MAX_DIVISION_SUPPORT_WIDTH = 2
NDefines.NMilitary.MAX_DIVISION_SUPPORT_HEIGHT = 4
-- NDefines.NMilitary.BASE_COMBAT_WIDTH = 40 --Now in terrain
-- NDefines.NMilitary.ADDITIONAL_COMBAT_WIDTH = 20

NDefines.NMilitary.FIELD_MARSHAL_DIVISIONS_CAP = 36
NDefines.NMilitary.UNIT_LEADER_MODIFIER_COOLDOWN_ON_GROUP_CHANGE = 1 --1 day to regain general/field marshal bonus, for AI sake not 0

NDefines.NMilitary.BASE_DIVISION_BRIGADE_GROUP_COST = 5
NDefines.NMilitary.BASE_DIVISION_BRIGADE_CHANGE_COST = 1
NDefines.NMilitary.BASE_DIVISION_SUPPORT_SLOT_COST = 5

NDefines.NMilitary.MAX_ARMY_EXPERIENCE = 999
NDefines.NMilitary.MAX_NAVY_EXPERIENCE = 999
NDefines.NMilitary.MAX_AIR_EXPERIENCE = 999
NDefines.NMilitary.COMBAT_MINIMUM_TIME = 2

--JFU/GDU defines

NDefines.NMilitary.MULTIPLE_COMBATS_PENALTY = -0.35
NDefines.NMilitary.UNIT_EXPERIENCE_PER_COMBAT_HOUR = 0.0003 -- from 0.0001
NDefines.NMilitary.SLOWEST_SPEED = 3 -- from 4
NDefines.NMilitary.EXPERIENCE_LOSS_FACTOR = 0.90 -- from 1
NDefines.NMilitary.SPEED_REINFORCEMENT_BONUS = 0.02 -- from 0.01
NDefines.NMilitary.ENCIRCLED_PENALTY = -0.35 -- from 0.3
NDefines.NMilitary.PLAYER_ORDER_PLANNING_DECAY = 0
NDefines.NMilitary.ENEMY_AIR_SUPERIORITY_IMPACT = -0.275
NDefines.NMilitary.ENEMY_AIR_SUPERIORITY_DEFENSE = 0.75	       -- more AA attack will approach this amount of help (diminishing returns)

-------------------------------------------- Air --------------------------------------------
NDefines.NAir.COMBAT_DAMAGE_SCALE = 0.05
NDefines.NAir.AIR_DEPLOYMENT_DAYS = 1
NDefines.NAir.AIR_WING_MAX_SIZE = 1600
NDefines.NAir.ANTI_AIR_MAXIMUM_DAMAGE_REDUCTION_FACTOR = 0.6 -- .75 Maximum damage reduction factor applied to incoming air attacks against units with AA.
NDefines.NAir.DISRUPTION_FACTOR = 7  -- (4 -> 7) with decent radar coverage equal amounts of fighters vs naval bombers will disrupt almost all naval bombers if not escorted, with low detection very few bombers are intercepted still
NDefines.NAir.ESCORT_FACTOR = 4 -- (2 -> 3) to make sure that escorted planes are still capable of bombing, with equal escorts/interceptors most of bombers get through Keep in mind that these values will also affect how cas/tac/strat bombers work, they make escorting planes much more important (which imo is 100% fine)
NDefines.NAir.AIR_WING_XP_LOSS_WHEN_KILLED = 100
NDefines.NAir.DETECT_CHANCE_FROM_AIRCRAFTS_EFFECTIVE_COUNT = 1000
NDefines.NAir.COMBAT_MULTIPLANE_CAP = 2

--------------------------------------- Production -----------------------------------------
--NDefines.NProduction.BASE_FACTORY_START_EFFICIENCY_FACTOR = 10 -- from 10
NDefines.NProduction.BASE_FACTORY_MAX_EFFICIENCY_FACTOR = 50 -- from 50
NDefines.NProduction.BASE_FACTORY_SPEED_MIL = 2.50 -- from 4.5, factory output
NDefines.NProduction.BASE_FACTORY_EFFICIENCY_GAIN = 0.6 -- from 1
NDefines.NProduction.BASE_FACTORY_EFFICIENCY_VARIANT_CHANGE_FACTOR = 80 -- from 90
NDefines.NProduction.BASE_FACTORY_EFFICIENCY_FAMILY_CHANGE_FACTOR = 60 -- from 70
NDefines.BASE_FACTORY_EFFICIENCY_PARENT_CHANGE_FACTOR = 25 -- from 30
NDefines.BASE_FACTORY_EFFICIENCY_ARCHETYPE_CHANGE_FACTOR = 15 -- from 20

NDefines.NProduction.BASE_LICENSE_IC_COST = 0.5 -- from 1
NDefines.NProduction.LICENSE_EQUIPMENT_BASE_SPEED = -0.15 -- from -0.25
NDefines.NProduction.LICENSE_EQUIPMENT_UPGRADE_XP_FACTOR = 1.5 -- from 2

------------------------------------------ Diplo -------------------------------------------

--0 volunteers, only through spirits
NDefines.NDiplomacy.VOLUNTEERS_PER_TARGET_PROVINCE = 0			-- Each province owned by the target country contributes this amount of volunteers to the limit.
NDefines.NDiplomacy.VOLUNTEERS_PER_COUNTRY_ARMY = 0				-- Each army unit owned by the source country contributes this amount of volunteers to the limit.
NDefines.NDiplomacy.VOLUNTEERS_RETURN_EQUIPMENT = 1				-- Returning volunteers keep this much equipment
NDefines.NDiplomacy.VOLUNTEERS_TRANSFER_SPEED = 5					-- days to transfer a unit to another nation
NDefines.NDiplomacy.VOLUNTEERS_DIVISIONS_REQUIRED = 1				-- This many divisons are required for the country to be able to send volunteers.
NDefines.NDiplomacy.TENSION_VOLUNTEER_FORCE_DIVISION = 0			-- Amount of tension generated for each sent division
NDefines.NDiplomacy.TENSION_DECAY = 0.05 -- Monthly tension decay
NDefines.NDiplomacy.TENSION_GUARANTEE = 0 -- Guarantee doesnt subtract tension

------------------------------------------ Country ------------------------------------------
NDefines.NCountry.AIR_VOLUNTEER_PLANES_LIMIT = 0 -- Ratio for volunteer planes available for sending in relation to sender air force
NDefines.NCountry.AIR_VOLUNTEER_BASES_CAPACITY_LIMIT = 0 -- Ratio for volunteer planes available for sending in relation to receiver air base capacity
NDefines.NAI.DIPLOMACY_ACCEPT_VOLUNTEERS_BASE = 1000
NDefines.NAI.DIPLOMACY_ACCEPT_ATTACHE_BASE = 1000
NDefines.NAI.MAIN_ENEMY_FRONT_IMPORTANCE = 8

------------------------------------------ Supply -------------------------------------------
NDefines.NSupply.CAPITAL_SUPPLY_BASE = 7.5 -- Bit higher capital supply
NDefines.NSupply.SUPPLY_FLOW_DROP_REDUCTION_AT_MAX_INFRA = 0.25 -- Getting this big a free supply boost from building max infra is retarded. 0.30>0.25
NDefines.NSupply.SUPPLY_FLOW_PENALTY_CROSSING_RIVERS = 0 -- Rivers have fucking railway bridges. There should not be a debuff for rivers.
NDefines.NSupply.SUPPLY_HUB_FULL_MOTORIZATION_BONUS = 3 --from 2.2
NDefines.NSupply.NODE_STARTING_PENALTY_PER_PROVINCE = 0.60 -- Cause custom map we need to make this worse (0.70 > 0.60)
NDefines.NSupply.NODE_ADDED_PENALTY_PER_PROVINCE = 0.80 -- Cause custom map we need to make this worse (0.70 > 0.80)
-- NDefines.NSupply.RIVER_RAILWAY_LEVEL = 0 -- DOESN'T WORK BECAUSE MINIMUM OF 1. DUMB. Cause we have a lot of rivers this needs to be 0
NDefines.NSupply.CAPITAL_SUPPLY_CIVILIAN_FACTORIES = 0.2 -- Cause we have a lot more factories less supply
NDefines.NSupply.CAPITAL_SUPPLY_MILITARY_FACTORIES = 0.4 -- Cause we have a lot more factories less supply

---------------------------------------- Characters ----------------------------------------
NDefines.NCharacter.DEFAULT_PP_COST_FOR_MILITARY_ADVISOR = 100 --From 50
NDefines.NCharacter.ADVISOR_PROMOTION_COST = 500 --Stop AI from promoting advisors

---------------------------------------- Resistance ----------------------------------------
NDefines.NResistance.RESISTANCE_GROWTH_BASE = 0.3 --from 0.2, subjects lower this
NDefines.NResistance.RESISTANCE_TARGET_BASE = 50 --from 35, subjects lower this
