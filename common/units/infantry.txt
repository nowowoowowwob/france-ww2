# options for map_icon_category:
# For land units: infantry,armored,other
# For sea units: ship,transport,uboat

sub_units = {
	bicycle_battalion = {
		sprite = bicycle
		map_icon_category = infantry

		priority = 600
		ai_priority = 150
		active = no

		type = {
			infantry
		}

		group = infantry

		categories = {
			category_front_line
			category_light_infantry
			category_all_infantry
			category_army
		}

		combat_width = 1.5

		#Size Definitions
		max_strength = 15
		max_organisation = 23
		default_morale = 0.3
		maximum_speed = 0.8
		manpower = 1000

		#Misc Abilities
		training_time = 90
		suppression = 1
		weight = 0.5

		supply_consumption = 0.07

		need = {
			infantry_equipment = 100
			support_equipment = 10
		}

		desert = {
			movement = 0.2
		}

		forest = {
			movement = 0.10
		}
		hills = {
			movement = 0.1
		}
		mountain = {
			movement = 0.1
		}
		marsh = {
			movement = 0.05
		}
		plains = {
			movement = 0.30
		}
		urban = {
			movement = 0.2
		}
		river = {
			movement = -0.05
		}
		amphibious = {
			movement = -0.05
		}
	}
	infantry = {
		sprite = infantry
		map_icon_category = infantry

		priority = 600
		ai_priority = 200
		active = no

		type = {
			infantry
		}

		group = infantry

		categories = {
			category_front_line
			category_light_infantry
			category_all_infantry
			category_army
		}

		combat_width = 1.5

		#Size Definitions
		max_strength = 25
		max_organisation = 40
		default_morale = 0.3
		manpower = 1000

		#Misc Abilities
		training_time = 90
		suppression = 1
		weight = 0.5

		supply_consumption = 0.06

		need = {
			infantry_equipment = 90
		}
	}

	marine = {
		sprite = infantry
		map_icon_category = infantry
		special_forces = yes
		marines = yes

		priority = 601
		ai_priority = 200
		active = no

		type = {
			infantry
		}

		group = infantry

		categories = {
			category_front_line
			category_light_infantry
			category_all_infantry
			category_army
			category_special_forces
		}

		combat_width = 1.5

		#Size Definitions
		max_strength = 25
		max_organisation = 45
		default_morale = 0.4
		manpower = 1000

		#Misc Abilities
		training_time = 120
		suppression = 1
		weight = 0.5
		supply_consumption = 0.06
		breakthrough = 0.20

		need = {
			infantry_equipment = 200
		}

		marsh = {
			attack = 0.3
		}
		river = {
			attack = 0.4
		}
		amphibious = {
			attack = 0.5
		}
	}

	mountaineers = {
		sprite = infantry
		map_icon_category = infantry
		special_forces = yes
		mountaineers = yes

		priority = 601
		ai_priority = 200
		active = no

		type = {
			infantry
		}

		group = infantry

		categories = {
			category_front_line
			category_light_infantry
			category_all_infantry
			category_army
			category_special_forces
		}

		combat_width = 1.5

		#Size Definitions
		max_strength = 25
		max_organisation = 45
		default_morale = 0.4
		manpower = 1000

		#Misc Abilities
		training_time = 120
		suppression = 1
		weight = 0.5

		supply_consumption = 0.07
		breakthrough = 0.20

		need = {
			infantry_equipment = 200
		}

		hills = {
			attack = 0.2
			defence = 0.1
			movement = 0.1
		}
		mountain = {
			attack = 0.3
			defence = 0.2
			movement = 0.2
		}
	}

	paratrooper = {
		sprite = infantry
		map_icon_category = infantry
		special_forces = yes

		priority = 601
		ai_priority = 2
		active = no

		type = {
			infantry
		}

		group = infantry

		categories = {
			category_front_line
			category_light_infantry
			category_all_infantry
			category_army
			category_special_forces
		}

		combat_width = 1.5

		#Size Definitions
		max_strength = 25
		max_organisation = 45
		default_morale = 0.5
		manpower = 1000

		#Misc Abilities
		training_time = 150
		suppression = 1
		weight = 0.4
		supply_consumption = 0.03

		can_be_parachuted = yes

		need = {
			infantry_equipment = 425
		}

		desert = {
			movement = 0.4
			defence = 0.3
			attack = 0.2
		}

		amphibious = {
			attack = 0.2
		}
		marsh = {
			attack = 0.15
		}
		river = {
			attack = 0.25
		}
		jungle = {
			attack = 0.10
		}
	}


	motorized = {
		sprite = motorized
		map_icon_category = infantry

		priority = 599
		ai_priority = 200
		active = no

		type = {
			motorized
		}

		group = mobile

		categories = {
			category_front_line
			category_all_infantry
			category_army
		}

		combat_width = 1.5

		#Size Definitions
		max_strength = 25
		max_organisation = 31
		default_morale = 0.30
		manpower = 1200

		#Misc Abilities
		training_time = 90
		suppression = 1
		weight = 0.75
		supply_consumption = 0.12

		# this is what moves us and sets speed
		transport = motorized_equipment

		need = {
			infantry_equipment = 100
			motorized_equipment = 50
		}

		forest = {
			attack = -0.1
			movement = -0.35
		}
		mountain = {
			attack = -0.1
			defence = -0.1
			movement = -0.2
		}
		marsh = {
			attack = -0.1
			movement = -0.3
		}
		urban = {
			attack = -0.1
		}
		river = {
			attack = -0.1
			movement = -0.1
		}
		amphibious = {
			attack = -0.2
		}
	}

	mechanized = {
		abbreviation = "MEC"
		sprite = mechanized
		map_icon_category = infantry

		priority = 610
		ai_priority = 200
		active = yes

		type = {
			mechanized
		}

		group = mobile

		categories = {
			category_front_line
			category_all_infantry
			category_army
		}

		combat_width = 1.5

		#Offensive Abilities
		soft_attack = 0.2
		hard_attack = 3.5

		#Size Definitions
		max_strength = 38
		max_organisation = 36
		default_morale = 0.3
		manpower = 1200

		#Misc Abilities
		training_time = 120
		suppression = 1
		weight = 1

		supply_consumption = 0.160

		# needed since we give so much bonus to infantry even with no mech equipment
		essential = {
			infantry_equipment
			mechanized_equipment
		}

		# this is what moves us and sets speed
		transport = mechanized_equipment

		need = {
			mechanized_equipment = 50
			infantry_equipment = 100
		}

		forest = {
			attack = -0.2
			movement = -0.35
		}
		mountain = {
			attack = -0.1
			defence = -0.1
			movement = -0.2
		}
		marsh = {
			attack = -0.1
		}
		urban = {
			attack = -0.2
			defence = -0.05
		}
		river = {
			attack = -0.2
			movement = -0.2
		}
		amphibious = {
			attack = -0.4
		}

		hardness = 0.2
	}

	fake_intel_unit = {
		sprite = infantry
		map_icon_category = infantry

		priority = 0
		ai_priority = 0
		active = no

		type = {
			infantry
		}

		group = infantry

		categories = {
			category_front_line
			category_light_infantry
			category_all_infantry
			category_army
		}

		combat_width = 1

		#Size Definitions
		max_strength = 1
		max_organisation = 100
		default_morale = 0.3
		manpower = 0

		#Misc Abilities
		training_time = 90
		suppression = 1
		weight = 0.5

		supply_consumption = 0.0

		need = {
			infantry_equipment = 1
		}
	}


	penal_battalion = {
		abbreviation = "PEN"
		sprite = infantry
		map_icon_category = infantry

		priority = 400 #600
		ai_priority = 150 #200
		active = no

		type = {
			infantry
		}

		group = infantry

		categories = {
			category_front_line
			category_light_infantry
			category_all_infantry
			category_army
		}

		combat_width = 1.5

		#Size Definitions
		max_strength = 15 #25 (HP)
		max_organisation = 50 #40 -> Lower HP but higher Org: They should keep fighting longer
		default_morale = 0.4 #0.3 (Recovery) -> Easy to get more punishees
		manpower = 850 #1000 -> Usually no more than 850

		#Misc Abilities
		training_time = 50 #90 -> Already trained
		suppression = 0.5 #1.5 -> Not for suppression roles
		weight = 0.5

		supply_consumption = 0.06 #0.07 -> Don't need that much supply, and they have lower HP

		need = {
			infantry_equipment = 85 #100 -> Same ratio as regular Infantry
		}
	}
}
