﻿##### HUNGARY NAME LISTS #####
### REGULAR DESTROYER NAMES###
HUN_DD_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_DESTROYERS

	for_countries = { HUN }

	type = ship
	ship_types = { ship_hull_light destroyer }

	fallback_name = "Romboló %d"

	unique = {
		 "Tátra" "Lika" "Csepel" "Balaton" "Csikós" "Reka" "Dinara" "Pandúr" "Turul" "Ulan" "Huszár" "Hajdú" "Krokodil" "Triton" "Pingvin" "Csuka" "Balin"
		 "Süllő" "Harcsa" "Cápa" "Rája" "Pandúr" "Villám" "Kuruc" "Kaméleon" "Héja" "Vércse" "Sólyom" "Sas" "Albatrosz" "Vihar" "Mennydörgés" "Vulkán" "Szalamandra" "Farkas" "Rozmár"
	}
}

### LIGHT CRUISER NAMES###
HUN_CL_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_CL

	for_countries = { HUN }

	type = ship
	ship_types = { ship_hull_cruiser light_cruiser }

	fallback_name = "Könnyűcirkáló %d"

	unique = {
		"Esztergom" "Székelyudvarhely" "Visegrád" "Nógrád" "Komárom" "Hajdú" " Csongrád" "Eger" "Veszprém" "Szigetvár" "Ungvár" "Eperjes" "Zsolna" "Szolnok"
		"Tatbánya" " Tokaj" "Marosvásárhely" "Nándorfehérvár" "Brassó" "Temesvár" "Zombor" "Villány" "Kecskemét" "Novara" "Zenta"
	}
}

### HEAVY CRUISER NAMES###
HUN_CA_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_CA

	for_countries = { HUN }

	type = ship
	ship_types = { ship_hull_cruiser heavy_cruiser }

	fallback_name = "Nehézcirkálo %d"

	unique = {
		"Budapest" "Gyôr" "Szeged" "Arad" "Pécs" "Miskolc" "Kolozsvár" "Újvidék" " Sopron" "Szombathely" "Pozsony" "Kassa" "Besztercebánya"
	}
}


### BATTLESHIP NAMES ###
HUN_BB_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_BB

	for_countries = { HUN }

	type = ship
	ship_types = { ship_hull_heavy battleship }


	fallback_name = "Csatahajó %d"

	unique = {
		 "Szent István" "Szent László" "Hunyadi" "Hunyadi Mátyás" "Árpád" "Nagy Lajos" "Zsigmond" "IV. Béla"
	}
}

### BATTLECRUISER NAMES ###
HUN_BC_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_BC

	for_countries = { HUN }

	type = ship
	ship_types = { ship_hull_heavy battle_cruiser }


	fallback_name = "Csatacirkáló %d"

	unique = {
		 "Kossuth" "Széchenyi" "Thököly Imre" "Rákóczi" "Bethlen" "Zrínyi"
	}
}

### AIRCRAFT CARRIER NAMES ###
HUN_CV_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_CARRIERS

	for_countries = { HUN }

	type = ship
	ship_types = { ship_hull_carrier carrier }


	fallback_name = "Repülögép-hordozó %d"

	unique = {
		 "Mars" "Pannónia" "Erdély" "Felvidék" "Vajdaság" "Alföld" "Kárpátalja" "Carpathia"
	}
}

### SUBMARINES ###
HUN_SS_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_SUBMARINES

	for_countries = { HUN }

	type = ship
	ship_types = { ship_hull_submarine submarine }


	fallback_name = "U-%d"

	ordered = {
		1 = {"U-%d"}
		2 = {"U-%d"}
		3 = {"U-%d"}
		4 = {"U-%d"}
		5 = {"U-%d"}
		6 = {"U-%d"}
		7 = {"U-%d"}
		8 = {"U-%d"}
		9 = {"U-%d"}
		10 = {"U-%d"}
		11 = {"U-%d"}
		12 = {"U-%d"}
		13 = {"U-%d"}
		14 = {"U-%d"}
		15 = {"U-%d"}
		16 = {"U-%d"}
		17 = {"U-%d"}
		18 = {"U-%d"}
		19 = {"U-%d"}
		20 = {"U-%d"}
		21 = {"U-%d"}
		22 = {"U-%d"}
		23 = {"U-%d"}
		24 = {"U-%d"}
		25 = {"U-%d"}
	}
}

### THEME: HUNGARIAN RIVERS ###
HUN_THEME_RIVERS = {
	name = NAME_THEME_RIVERS

	for_countries = { HUN }

	type = ship

	fallback_name = "Hadihajó-%d"

	unique = {
		"Duna" "Tisza" "Sajó" "Rába" "Körös" "Zagyva" "Drava" "Zala" "Hornád" "Marcal" "Tarna" "Bodrog" "Galga" "Lajta" "Ipoly" Szamos" Mura" "Maros" "Hernád" "Rábca" "Kapos" "Kraszna" "Berettyó"
	}
}

### THEME: HUNGARIAN BATTLES ###
HUN_THEME_BATTLES = {
	name = NAME_THEME_BATTLES

	for_countries = { HUN }

	type = ship

	fallback_name = "Hadihajó-%d"

	unique = {
		"Asiago" "Brenta" "Buda" "Caporettó" "Cserhalom" "Ticonderoga" "Debrecen" "Doberdó" "Dubica" "Dürnkurt" "Eger" "Enyicke" "Eperjes" "Galambóc" "Gorlice"
		"Gvozd" "Györke" "Győr" "Gyula" "Hadad" "Hetény" "Hommonna" "Isaszeg" "Isonzó" "Kahlenberg" "Kenyérmező" "Kroissenbrunn" "Ménfő" "Mogyoród" "Moldvabánya"
		"Muhi" "Munkács" "Nagyharsány" "Nándorfehérvár" "Nikápoly" "Ortigar" "Kápolna" "Palást" "Pákozd" "Párkány" "Piave" "Posada" "Pozsony" "Rigómező" "Rozgony" "Segesvár"
		"Szalánkemén" "Szászfenes" "Szentandrás" "Szigetvár" "Szikszó" "Tápióbicske" "Temesvár" "Vác" "Várna" "Vászló" "Zenta" "Zimony"
	}
}

### THEME: HUNGARIAN LEADERS ###
HUN_THEME_LEADERS = {
	name = NAME_THEME_LEADERS

	for_countries = { HUN }

	type = ship

	fallback_name = "Hadihajó-%d"

	unique = {
		"Álmos" "Előd" "Ond" "Kond" "Tas" "Huba" "Töhötöm" "Gyula" "Harka" "Kende" "Apor" "Bogát" "Bulcsú" "Jelek" "Jutocsa" "Kál" "Koppány" "Kurszán" "Lél" "Levedi"
		"Prokuj" "Súr" "Szabolcs" "Taksony" "Tarkacsu" "Temecsü" "Tétény" "Teveli" "Tonzuba" "Urkund" "Ügyek" "Tar" "Zolta" "Zombor"
	}
}

### THEME: HUNGARIAN KINGS ###
HUN_THEME_KINGS = {
	name = NAME_THEME_KINGS

	for_countries = { HUN }

	type = ship

	fallback_name = "Hadihajó-%d"

	unique = {
		"Szent István" "Velencei Péter" "Aba Sámuel" "Magnus Géza" "Szent László" "Könyves Kálmán" "IV Béla" "Kun László" "Velencei András" "Cseh Vencel" "Bajor Ottó" "Károly Róbert" "Nagy Lajos" "Luxemburgi Zsigmond"
        "Habsburg Albert" "Várnai Ulászló" "Hunyadi Mátyás" "Dobzse László" "Szapolyai János" "János Zsigmond"
	}
}

### THEME: HUNGARIAN GENERALS ###
HUN_THEME_GENERALS = {
	name = NAME_THEME_GENERALS

	for_countries = { HUN }

	type = ship

	fallback_name = "Hadihajó-%d"

	unique = {
		"Aulich Lajos" "Damjanich János" "Dessewffy Arisztrid" "Kiss Ernő" "Knézicz Károly" "Láhner György" "Lázár Vilmos" "Leiningen-Westerburg Károly" "Nagysándor József" "Poeltenberg Ernő" "Schweidel József" "Török Ignác" "Vécsey Károly" "Görgei Artúr"
        "Bem József" "Hunyadi János" "Bottyán János" "Zrínyi Miklós" "Esze Tamás" "Dózsa György" "Ocskay László" "Asbóth Lajos" "Báthori István" "Kinizsi Pál" "Hadik András" "Hadik Károly József" "Kőszegi Henrik" "Nádasdy Ferenc" "Tomori Pál" "Filippo di Stephano Scolari"
        "Zrínyi Ilona" "Zrínyi Péter" "Jiskra János" "Dobó István" "Esterházy Miklós József" "Festetics József"	"Perczel Mór"
	}
}

### THEME: HUNGARIAN MYTHOLOGY ###
HUN_THEME_MYTHOLOGY = {
	name = NAME_THEME_MYTHOLOGY

	for_countries = { HUN }

	type = ship

	fallback_name = "Hadihajó-%d"

	unique = {
		"Csodaszarvas" "Griff" "Lidérc" "Sárkány" "Turul" "Hunor" "Magor" "Álmos" "Emese" "Dula" "Garabonciás" "Göncöl" "Kalamóna" "Lúdvérc"
	}
}
