sub_units = {

	amphibious_armor = {
		sprite = amphibious_armor
		map_icon_category = armored
		priority = 2501
		ai_priority = 2000
		active = yes
		special_forces = yes
		marines = yes
		affects_speed = no
		type = {
			armor
			support
		}

		group = support

		categories = {
			category_tanks
			category_front_line
			category_all_armor
			category_army
		}


		combat_width = 1

		need = {
			amphibious_tank_chassis = 50
		}
		manpower = 500
		max_organisation = 10
		default_morale = 0.3
		training_time = 180
		max_strength = 2
		weight = 1
		supply_consumption = 0.2

		river = {
			attack = 0.30
			movement = 0.2
		}
		amphibious = {
			attack = 0.50
			movement = 0.2
		}

		#support unit nerf
		soft_attack = -0.15
		hard_attack = -0.15
		breakthrough = -0.6
		defense = -0.5
		ap_attack = -0.4
		armor_value = -0.4
	}

	amphibious_light_armor = {
		abbreviation = "LAM"
		sprite = amphibious_armor
		map_icon_category = armored
		priority = 2501
		ai_priority = 2000
		active = yes
		special_forces = yes
		marines = yes

		affects_speed = no
		type = {
			armor
			support
		}

		group = support

		categories = {
			category_tanks
			category_front_line
			category_all_armor
			category_army
		}

		#support unit nerf
		soft_attack = -0.15
		hard_attack = -0.15
		breakthrough = -0.6
		defense = -0.5
		ap_attack = -0.4
		armor_value = -0.4


		combat_width = 1

		need = {
			light_tank_amphibious_chassis = 50
		}
		manpower = 500
		max_organisation = 10
		default_morale = 0.3
		training_time = 180
		max_strength = 2
		weight = 1
		supply_consumption = 0.2

		river = {
			attack = 0.15
			movement = 0.1
		}
		amphibious = {
			attack = 0.25
			movement = 0.1
		}
	}

	amphibious_medium_armor = {
		abbreviation = "MAM"
		sprite = amphibious_armor
		map_icon_category = armored
		priority = 2501
		ai_priority = 2000
		active = yes
		special_forces = yes
		marines = yes
		type = {
			armor
			support
		}

		group = support

		categories = {
			category_tanks
			category_front_line
			category_all_armor
			category_army
		}

		#support unit nerf
		soft_attack = -0.15
		hard_attack = -0.15
		breakthrough = -0.6
		defense = -0.5
		ap_attack = -0.4
		armor_value = -0.4


		combat_width = 1

		need = {
			medium_tank_amphibious_chassis = 50
		}
		manpower = 500
		max_organisation = 10
		default_morale = 0.3
		training_time = 180
		max_strength = 2
		weight = 1
		supply_consumption = 0.2

		river = {
			attack = 0.15
			movement = 0.1
		}
		amphibious = {
			attack = 0.25
			movement = 0.1
		}
	}

	amphibious_heavy_armor = {
		abbreviation = "HAM"
		sprite = amphibious_armor
		map_icon_category = armored
		priority = 2501
		ai_priority = 2000
		active = yes
		special_forces = yes
		marines = yes
		type = {
			armor
			support
		}

		group = support

		categories = {
			category_tanks
			category_front_line
			category_all_armor
			category_army
		}

		#support unit nerf
		soft_attack = -0.15
		hard_attack = -0.15
		breakthrough = -0.6
		defense = -0.5
		ap_attack = -0.4
		armor_value = -0.4


		combat_width = 1

		need = {
			heavy_tank_amphibious_chassis = 50
		}
		manpower = 500
		max_organisation = 10
		default_morale = 0.3
		training_time = 180
		max_strength = 2
		weight = 1
		supply_consumption = 0.2

		river = {
			attack = 0.15
			movement = 0.1
		}
		amphibious = {
			attack = 0.25
			movement = 0.1
		}
	}
}
