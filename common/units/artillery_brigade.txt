sub_units = {

	artillery_brigade = {
		sprite = artillery
		map_icon_category = infantry
		priority = 1198
		ai_priority = 100
		active = yes

		type = {
			infantry
			artillery
		}

		group = infantry

		categories = {
			category_army
			category_line_artillery
			category_artillery
		}

		combat_width = 1.5

		manpower = 250
		need = {
			artillery_equipment = 12
		}

		max_strength = 0.6
		max_organisation = 5
		default_morale = 0.1
		training_time = 50
		weight = 0.2
		supply_consumption = 0.04

		forest = {
			attack = -0.2
			movement = -0.2
		}

		hills = {
			movement = -0.05
		}

		mountain = {
			movement = -0.2
		}

		marsh = {
			attack = -0.2
			movement = -0.3
		}

		fort = {
			attack = 0.1
		}

		river = {
			attack = -0.2
			movement = -0.2
		}

		amphibious = {
			attack = -0.3
		}
	}

	paradrop_artillery_brigade = {
		sprite = artillery
		map_icon_category = infantry
		priority = 1198
		ai_priority = 100
		active = yes
		special_forces = yes

		type = {
			infantry
			artillery
		}

		group = infantry

		categories = {
			category_army
			category_line_artillery
			category_artillery
		}

		combat_width = 1.5

		manpower = 250
		need = {
			artillery_equipment = 12
		}
		can_be_parachuted = yes

		max_strength = 0.6
		max_organisation = 5
		default_morale = 0.1
		training_time = 50
		weight = 0.2
		supply_consumption = 0.04
		soft_attack = -0.2

		forest = {
			attack = -0.2
			movement = -0.2
		}

		hills = {
			movement = -0.05
		}

		mountain = {
			movement = -0.2
		}

		marsh = {
			attack = -0.2
			movement = -0.3
		}

		fort = {
			attack = 0.1
		}

		river = {
			attack = -0.2
			movement = -0.2
		}

		amphibious = {
			attack = -0.3
		}
	}

	mot_artillery_brigade = {
		sprite = artillery
		map_icon_category = infantry
		priority = 1198
		ai_priority = 100
		active = yes

		type = {
			motorized
			artillery
		}

		group = mobile

		categories = {
			category_army
			category_line_artillery
			category_artillery
		}

		combat_width = 1.5
		transport = motorized_equipment
		manpower = 500
		need = {
			artillery_equipment = 12
			motorized_equipment = 20
		}

		max_strength = 0.6
		max_organisation = 5
		default_morale = 0.1
		training_time = 50
		weight = 0.5
		supply_consumption = 0.08

		forest = {
			attack = -0.2
			movement = -0.2
		}

		hills = {
			movement = -0.05
		}

		mountain = {
			movement = -0.2
		}

		marsh = {
			attack = -0.2
			movement = -0.3
		}

		fort = {
			attack = 0.1
		}

		river = {
			attack = -0.2
			movement = -0.2
		}

		amphibious = {
			attack = -0.3
		}
	}

	rocket_artillery_brigade = {
		sprite = "artillery"
		map_icon_category = infantry
		priority = 1199
		ai_priority = 100
		active = yes

		type = {
			infantry
			artillery
		}

		group = infantry

		categories = {
			category_army
			category_line_artillery
			category_artillery
		}

		combat_width = 1.5

		need = {
			rocket_artillery_equipment = 12
		}

		manpower = 500
		max_organisation = 5
		default_morale = 0.1
		max_strength = 0.6
		training_time = 120
		weight = 0.5

		supply_consumption = 0.06

		forest = {
			attack = -0.2
			movement = -0.2
		}

		hills = {
			movement = -0.05
		}

		mountain = {
			movement = -0.2
		}

		marsh = {
			attack = -0.2
			movement = -0.3
		}

		fort = {
			attack = 0.1
		}

		river = {
			attack = -0.2
			movement = -0.2
		}

		amphibious = {
			attack = -0.4
		}
	}

	mot_rocket_artillery_brigade = {
		sprite = "artillery"
		map_icon_category = infantry
		priority = 1199
		ai_priority = 100
		active = yes

		type = {
			motorized
			artillery
		}

		group = mobile

		categories = {
			category_army
			category_line_artillery
			category_artillery
		}

		combat_width = 1.5
		transport = motorized_equipment
		need = {
			rocket_artillery_equipment = 12
			motorized_equipment = 25
		}

		manpower = 500
		max_organisation = 5
		default_morale = 0.1
		max_strength = 0.6
		training_time = 50
		weight = 0.3

		supply_consumption = 0.08

		forest = {
			attack = -0.2
			movement = -0.5
		}

		hills = {
			movement = -0.05
		}

		mountain = {
			movement = -0.2
		}

		marsh = {
			attack = -0.2
			movement = -0.3
		}

		fort = {
			attack = 0.1
		}

		river = {
			attack = -0.2
			movement = -0.2
		}

		amphibious = {
			attack = -0.4
		}
	}

	motorized_rocket_brigade = {
		sprite = motorized
		map_icon_category = infantry
		priority = 1199
		ai_priority = 100
		active = no

		type = {
			artillery
			motorized
			rocket
		}

		group = mobile

		categories = {
			category_army
			category_line_artillery
			category_artillery
		}

		combat_width = 1.5

		need = {
			motorized_rocket_equipment = 12
			motorized_equipment = 15
		}

		manpower = 500
		max_organisation = 5
		default_morale = 0.1
		max_strength = 0.6
		training_time = 80
		weight = 0.3


		supply_consumption = 0.10

		forest = {
			attack = -0.05
			defence = 0.15
			movement = -0.5
		}

		hills = {
			attack = 0.15
			defence = 0.15
			movement = -0.05
		}

		mountain = {
			movement = -0.2
		}

		plains = {
			attack = 0.15
			defence = 0.15
		}

		desert = {
			attack = 0.15
			defence = 0.15
		}

		marsh = {
			attack = -0.2
			movement = -0.3
		}

		fort = {
			attack = 0.1
		}

		river = {
			attack = -0.2
			movement = -0.2
		}

		amphibious = {
			attack = -0.4
		}
	}
}
