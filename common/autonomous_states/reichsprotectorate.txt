autonomy_state = {
	id = autonomy_reichsprotectorate

	default = yes
	is_puppet = yes

	use_overlord_color = yes

	min_freedom_level = 0.5

	manpower_influence = 0.7

	rule = {
		can_not_declare_war = yes
		can_decline_call_to_war = no
		can_be_spymaster = no
		contributes_operatives = no
		can_create_collaboration_government = no
	}

	modifier = {
		autonomy_manpower_share = 0.6
		can_master_build_for_us = 1
		conscription_factor = -0.25
		extra_trade_to_overlord_factor = 0.6
		overlord_trade_cost_factor = -0.35
		cic_to_overlord_factor = 0.20
		mic_to_overlord_factor = 0.20
		license_subject_master_purchase_cost = -1
		autonomy_gain_global_factor = -0.4
		surrender_limit = 0.3
		resistance_growth = -0.27 #Base is 150% of vanilla, 150-40,5 = ~110%
		resistance_target = -0.15
	}

	allowed = {
		has_government = fascism
	}

	ai_subject_wants_higher = {
		factor = 1.0
	}

	ai_overlord_wants_lower = {
		factor = 1.0
	}

	ai_overlord_wants_garrison = {
		always = no
	}

	can_take_level = {
		#trigger here
	}

	can_lose_level = {
		#trigger here
	}
}
