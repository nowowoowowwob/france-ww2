autonomy_state = {
	id = autonomy_satellite

	min_freedom_level = 0.80

	manpower_influence = 0.4

	use_overlord_color = yes

	rule = {
		can_not_declare_war = yes
		can_decline_call_to_war = no
	}

	modifier = {
		autonomy_manpower_share = 0.4
		extra_trade_to_overlord_factor = 0.4
		overlord_trade_cost_factor = -0.25
		cic_to_overlord_factor = 0.10
		mic_to_overlord_factor = 0.10
		license_subject_master_purchase_cost = -1
		autonomy_gain_global_factor = -0.3
		surrender_limit = 0.2
		resistance_growth = -0.30 #Base is 150% of vanilla, 150-45 = 105%
		resistance_target = -0.20
	}

	allowed = {
		has_government = fascism
	}

	ai_subject_wants_higher = {
		factor = 1.0
	}

	ai_overlord_wants_lower = {
		factor = 1.0
	}

	ai_overlord_wants_garrison = {
		always = no
	}

	can_take_level = {
		#trigger here
	}

	can_lose_level = {
		#trigger here
	}
}
