
clear_hungarian_president_flags = {
	clr_country_flag = HUN_daranyi_president
	clr_country_flag = HUN_imredy_president
	clr_country_flag = HUN_teleki_president
	clr_country_flag = HUN_bethlen_president
	clr_country_flag = HUN_bardossy_president
	clr_country_flag = HUN_kallay_president
	clr_country_flag = HUN_president_kept
}

clear_horthy_inheritance = {
	clr_country_flag = HUN_istvan_horthy_successor
	clr_country_flag = HUN_istvan_horthy_jr_successor
	clr_country_flag = HUN_victor_emmanuel_successor
	clr_country_flag = HUN_wilhelm_hohenzollern_successor
	clr_country_flag = HUN_otto_habsburg_successor
}

disable_elections_for_current_governing_party = {
	if = {
		limit = { has_government = neutrality }
		set_politics = {
			ruling_party = neutrality
			elections_allowed = no
		}
	}
	if = {
		limit = { has_government = democratic }
		set_politics = {
			ruling_party = democratic
			elections_allowed = no
		}
	}
	if = {
		limit = { has_government = communism }
		set_politics = {
			ruling_party = communism
			elections_allowed = no
		}
	}
	if = {
		limit = { has_government = fascism }
		set_politics = {
			ruling_party = fascism
			elections_allowed = no
		}
	}
}

clear_regional_election_flags = {
	clr_country_flag = HUN_election_AUS_conservatives
	clr_country_flag = HUN_election_AUS_socialists
	clr_country_flag = HUN_election_AUS_independence
	clr_country_flag = HUN_election_AUS_liberals

	clr_country_flag = HUN_election_HUN_conservatives
	clr_country_flag = HUN_election_HUN_socialists
	clr_country_flag = HUN_election_HUN_independence
	clr_country_flag = HUN_election_HUN_liberals

	clr_country_flag = HUN_election_SLO_conservatives
	clr_country_flag = HUN_election_SLO_socialists
	clr_country_flag = HUN_election_SLO_independence
	clr_country_flag = HUN_election_SLO_liberals

	clr_country_flag = HUN_election_BOH_conservatives
	clr_country_flag = HUN_election_BOH_socialists
	clr_country_flag = HUN_election_BOH_independence
	clr_country_flag = HUN_election_BOH_liberals

	clr_country_flag = HUN_election_GAL_conservatives
	clr_country_flag = HUN_election_GAL_socialists
	clr_country_flag = HUN_election_GAL_independence
	clr_country_flag = HUN_election_GAL_liberals

	clr_country_flag = HUN_election_TRA_conservatives
	clr_country_flag = HUN_election_TRA_socialists
	clr_country_flag = HUN_election_TRA_independence
	clr_country_flag = HUN_election_TRA_liberals

	clr_country_flag = HUN_election_CRO_conservatives
	clr_country_flag = HUN_election_CRO_socialists
	clr_country_flag = HUN_election_CRO_independence
	clr_country_flag = HUN_election_CRO_liberals

	clr_country_flag = HUN_election_CAR_conservatives
	clr_country_flag = HUN_election_CAR_socialists
	clr_country_flag = HUN_election_CAR_independence
	clr_country_flag = HUN_election_CAR_liberals
}

set_root_government_to_ideology = {
	if = {
		limit = { has_government = neutrality }
		ROOT = {
			set_politics = {
				ruling_party = neutrality
				elections_allowed = yes
			}
		}
	}
	if = {
		limit = { has_government = democratic }
		ROOT = {
			set_politics = {
				ruling_party = democratic
				elections_allowed = yes
			}
		}
	}
	if = {
		limit = { has_government = fascism }
		ROOT = {
			set_politics = {
				ruling_party = fascism
				elections_allowed = no
			}
		}
	}
	if = {
		limit = { has_government = communism }
		ROOT = {
			set_politics = {
				ruling_party = communism
				elections_allowed = no
			}
		}
	}
}

get_all_cores_HUN = {
	every_state = {
		limit = {
			is_core_of = HUN
			NOT = { is_owned_by = HUN }
		}
		OWNER = {
			if = {
				limit = { NOT = { has_country_flag = HUN_lost_territory_after_revision } }
				set_country_flag = HUN_lost_territory_after_revision
				add_opinion_modifier = { target = HUN modifier = HUN_lost_territory_after_war }
				add_opinion_modifier = { target = ENG modifier = HUN_lost_territory_after_war }
				add_opinion_modifier = { target = USA modifier = HUN_lost_territory_after_war }
				add_opinion_modifier = { target = SOV modifier = HUN_lost_territory_after_war }
			}
		}
		HUN = {
			transfer_state = PREV
		}
	}
}

HUN_king_on_the_throne_effects = {
	if = {
		limit = { has_idea = kingdom_without_a_king }
		remove_ideas = kingdom_without_a_king
	}
	set_country_flag = HUN_has_king
}

HUN_remove_press_lawsuits = {
	if = {
		limit = { has_idea = press_lawsuits_neutrality }
		remove_ideas = press_lawsuits_neutrality
	}
	if = {
		limit = { has_idea = press_lawsuits_communism }
		remove_ideas = press_lawsuits_communism
	}
	if = {
		limit = { has_idea = press_lawsuits_democratic }
		remove_ideas = press_lawsuits_democratic
	}
	if = {
		limit = { has_idea = press_lawsuits_fascism }
		remove_ideas = press_lawsuits_fascism
	}
}

HUN_remove_trianon_restrictions = {
	if = {
		limit = { has_idea = trianon_restrictions_bled_agreement }
		remove_ideas = trianon_restrictions_bled_agreement
	}
	if = {
		limit = { has_idea = trianon_restrictions }
		remove_ideas = trianon_restrictions
	}
	if = {
		limit = { has_idea = HUN_secret_rearmament }
		remove_ideas = HUN_secret_rearmament
	}
}