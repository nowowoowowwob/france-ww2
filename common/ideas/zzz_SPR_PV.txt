ideas = {
	country = {
		SOV_volunteer_support_scw = {
			picture = TODO

			available = {
				SPR_scw_in_progress = yes
			}

			modifier = {
				send_volunteers_tension = -1
				send_volunteer_divisions_required = -1
				send_volunteer_size = 2
				experience_gain_army_factor = -0.75
				experience_gain_army_unit_factor = 0.25
				experience_loss_factor = -0.25
			}

			rule = {
				can_send_volunteers = yes
			}
		}
		SOV_air_volunteer_support_scw = {
			picture = TODO

			available = {
				SPR_scw_in_progress = yes
			}

			modifier = {
				send_volunteers_tension = -1
				air_volunteer_cap = 250
				air_ace_generation_chance_factor = 2
				experience_gain_air_factor = -1
			}

			rule = {
				can_send_volunteers = yes
			}
		}
		ITA_volunteer_support_scw = {
			picture = TODO

			available = {
				SPR_scw_in_progress = yes
			}

			modifier = {
				send_volunteers_tension = -1
				send_volunteer_divisions_required = -1
				send_volunteer_size = 10
				experience_gain_army_factor = -0.75
				experience_gain_army_unit_factor = 0.25
				experience_loss_factor = -0.25
			}

			rule = {
				can_send_volunteers = yes
			}
		}
		ITA_air_volunteer_support_scw = {
			picture = TODO

			available = {
				SPR_scw_in_progress = yes
			}

			modifier = {
				send_volunteers_tension = -1
				air_volunteer_cap = 400
				air_ace_generation_chance_factor = 2
				experience_gain_air_factor = -1
			}

			rule = {
				can_send_volunteers = yes
			}
		}
		GER_volunteer_support_scw = {
			picture = TODO

			available = {
				SPR_scw_in_progress = yes
			}

			modifier = {
				send_volunteers_tension = -1
				send_volunteer_divisions_required = -1
				send_volunteer_size = 2
				experience_gain_army_factor = -0.75
				experience_gain_army_unit_factor = 0.25
				experience_loss_factor = -0.25
			}

			rule = {
				can_send_volunteers = yes
			}
		}
		GER_air_volunteer_support_scw = {
			picture = TODO

			available = {
				SPR_scw_in_progress = yes
			}

			modifier = {
				send_volunteers_tension = -1
				air_volunteer_cap = 400
				air_ace_generation_chance_factor = 2
				experience_gain_air_factor = -1
			}

			rule = {
				can_send_volunteers = yes
			}
		}
    }
}
