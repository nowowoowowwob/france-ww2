ideas = {
	country = {
		generic_defeatism_1 = {
			allowed = { always = no }
			removal_cost = -1

			modifier = {
				army_morale_factor = -0.02
				war_support_weekly = -0.001
				conscription_factor = -0.05
				training_time_factor = 0.05
				resistance_growth = 0.025
				resistance_target = 0.025
			}
		}
		generic_defeatism_2 = {
			allowed = { always = no }
			removal_cost = -1

			modifier = {
				army_morale_factor = -0.04
				war_support_weekly = -0.002
				conscription_factor = -0.1
				training_time_factor = 0.10
				resistance_growth = 0.05
				resistance_target = 0.0375
				army_org_factor = -0.03
			}
		}
		generic_defeatism_3 = {
			allowed = { always = no }
			removal_cost = -1

			modifier = {
				army_morale_factor = -0.06
				war_support_weekly = -0.003
				conscription_factor = -0.15
				training_time_factor = 0.15
				resistance_growth = 0.10
				resistance_target = 0.0525
				army_org_factor = -0.06
				land_reinforce_rate = -0.02
			}
		}
		generic_defeatism_4 = {
			allowed = { always = no }
			removal_cost = -1

			modifier = {
				army_morale_factor = -0.08
				war_support_weekly = -0.004
				conscription_factor = -0.20
				training_time_factor = 0.20
				resistance_growth = 0.20
				resistance_target = 0.0850
				army_org_factor = -0.09
				land_reinforce_rate = -0.03
			}
		}
		generic_defeatism_5 = {
			allowed = { always = no }
			removal_cost = -1

			modifier = {
				army_morale_factor = -0.10
				war_support_weekly = -0.005
				conscription_factor = -0.25
				training_time_factor = 0.25
				resistance_growth = 0.30
				resistance_target = 0.1250
				army_org_factor = -0.12
				land_reinforce_rate = -0.04
			}
		}
	}
}
