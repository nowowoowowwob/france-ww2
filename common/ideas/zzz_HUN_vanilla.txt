ideas = {
    industrial_concern = {

        HUN_mavag = {

            picture = generic_industrial_concern_1

            allowed = {
                original_tag = HUN
            }

            cost = 150
            removal_cost = 10

            research_bonus = {
                industry = 0.15
            }

            traits = {
                industrial_concern
            }
        }
    }

    materiel_manufacturer = {

        designer = yes

        HUN_femaru_fegyver_es_gepgyar = {

            picture = generic_infantry_equipment_manufacturer_2

            allowed = {
                original_tag = HUN
            }

            cost = 100
            removal_cost = 0

            research_bonus = {
                infantry_weapons = 0.10
            }

            traits = {
                infantry_equipment_manufacturer
            }
        }
    }
}
