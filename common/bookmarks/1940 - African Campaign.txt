bookmarks = {
	bookmark = {
		name = "FALL_GELB_NAME"
		desc = "FALL_GELB_DESC"
		date = 1940.5.10.1
		picture = "GFX_select_date_1940"
		default_country = "GER"
		default = yes

		"GER"={
			history = "GER_FALL_GELB_DESC"
			ideology = fascism
			ideas = {
				sour_loser
				GER_heer
			}
			focuses = {
				GER_anschluss
				GER_eastern_front
				GER_danzigorwar
			}
		}
		"ITA"={
			history = "ITA_FALL_GELB_DESC"
			ideology = fascism
			ideas = {
				vittoria_mutilata
				victor_emmanuel
				ITA_army
			}
			focuses = {
				ITA_legacyofrome
				ITA_italianfascism
				ITA_establish_montenegro
			}
		}
		"USA"={
			history = "USA_FALL_GELB_DESC"
			ideology = democratic
			ideas = {
				home_of_the_free
				great_depression
				undisturbed_isolation
			}
			focuses = {
				USA_war_plan_black
			}
		}
		"ENG"={
			history = "ENG_FALL_GELB_DESC"
			ideology = democratic
			ideas = {
				stiff_upper_lip
				ENG_the_war_to_end_all_wars
				george_v
			}
			focuses = {
				tizard_mission_focus
				allies_ENG_lend_lease_act
			}
		}
		"FRA"={
			history = "FRA_FALL_GELB_DESC"
			ideology = democratic
			ideas = {
				FRA_victors_of_wwi
				FRA_disjointed_government
				FRA_protected_by_the_maginot_line
			}
			focuses = {
				FRA_form_the_popular_front
				FRA_revive_the_national_bloc
			}
		}

		"SOV"={
			history = "SOV_FALL_GELB_DESC"
			ideology = communism
			ideas = {
				trotskyite_plot_nsb
				SOV_second_five_year_plan_fake_1936_ns
				SOV_politicized_military
			}
			focuses = {
				SOV_behead_the_snake
				SOV_the_supreme_soviet
			}
		}

		"---"={
			history = "OTHER_FALL_GELB_DESC"
		}

		"CAN"={
			minor = yes
			history = "CAN_FALL_GELB_DESC"
			ideology = democratic
			ideas = {
				CAN_great_depression_1
				CAN_conscription_crisis
			}
			focuses = {

			}
		}
		"HUN"={
			minor = yes
			history = "HUN_FALL_GELB_DESC"
			ideology = neutrality
			ideas = {
				HUN_treaty_of_triannon
			}
			focuses = {
				HUN_secret_rearmament_focus
				HUN_demand_transylvania_focus
			}
		}

		"ROM"={
			minor = yes
			history = "ROM_FALL_GELB_DESC"
			ideology = neutrality
			ideas = {
				ROM_king_carol_ii_hedonist
				neutrality_idea
			}
			focuses = {
				ROM_institute_royal_dictatorship
				ROM_preserve_greater_romania
			}
		}

		"BUL"={
			minor = yes
			history = "ROM_FALL_GELB_DESC"
			ideology = neutrality
			ideas = {
				BUL_second_national_catastrophe
				BUL_army_restrictions
				BUL_imro_01
			}
			focuses = {
				BUL_the_fatherland_front
				BUL_restore_the_bulgarian_patriarchate
				BUL_prussia_of_the_balkans
			}
		}

		"SPR"={
			minor = yes
			history = "ROM_FALL_GELB_DESC"
			ideology = democratic
			ideas = {
				SPR_military_disloyalty
				SPA_carlism_1
				SPR_political_violence
			}
			focuses = {
				SPA_caudillo_of_spain
				SPA_no_compromise_on_carlist_ideals
				SPR_anarchism_knows_no_borders
			}
		}

		effect = {
			randomize_weather = 22345
		}
	}
}
