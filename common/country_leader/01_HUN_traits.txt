leader_traits = {

	#Political leader traits

	centrist_monarch = {
		random = no
		drift_defence_factor = 0.4

		ai_will_do = {
			factor = 1
		}
	}

	anti_fascist = {
		random = no
		fascism_acceptance = -50
		fascism_drift = -0.01

		ai_will_do = {
			factor = 1
		}
	}

	national_leader = {
		random = no
		stability_factor = 0.10
		war_support_factor = 0.10

		ai_will_do = {
			factor = 1
		}
	}

	foreign_leader = {
		random = no
		stability_factor = -0.10

		ai_will_do = {
			factor = 1
		}
	}

	former_ace_pilot = {
		random = no
		experience_gain_air = 0.05

		ai_will_do = {
			factor = 1
		}
	}

	italian_royalty = {
		random = no
		stability_factor = 0.02
		experience_gain_navy_factor = 0.05
		ai_strategy = {
			type = befriend
			id = "ITA"
			value = 200
		}

		ai_will_do = {
			factor = 1
		}
	}

	german_royalty = {
		random = no
		stability_factor = 0.02
		political_power_factor = 0.1
		ai_strategy = {
			type = befriend
			id = "GER"
			value = 200
		}

		ai_will_do = {
			factor = 1
		}
	}

	reformcommunist = {
		random = no
		political_advisor_cost_factor = -0.10
		trade_laws_cost_factor = -0.10
		economy_cost_factor = -0.10
		compliance_growth = 0.03

		ai_will_do = {
			factor = 1
		}
	}

	multi_national = {
		random = no
		stability_factor = 0.05
		compliance_growth = 0.10

		ai_will_do = {
			factor = 1
		}
	}

	hungarizmus = {
		random = no
		compliance_growth = 0.05

		ai_will_do = {
			factor = 1
		}
	}

	governor = {
		random = no
		political_advisor_cost_factor = -0.10
		stability_factor = 0.05

		ai_will_do = {
			factor = 1
		}
	}


	jet_scientist = {
		random = no
		sprite = 14
		experience_gain_air = 0.10

		ai_will_do = {
			factor = 1
		}
	}
}
