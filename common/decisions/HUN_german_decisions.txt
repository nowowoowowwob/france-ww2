
foreign_politics = {

	GER_access_to_poland_through_hungary = {

		icon = infiltrate_state

		available = {
			is_neighbor_of = HUN
			POL = { is_neighbor_of = HUN }
		}

		cost = 10
		fire_only_once = yes

		allowed = {
			original_tag = GER
		}

		visible = {
			is_neighbor_of = HUN
			NOT = { has_war_with = HUN }
			POL = {
				has_war_with = GER
				NOT = { has_war_with = HUN }
				NOT = { is_in_faction_with = HUN }
				is_neighbor_of = HUN
			}
		}
		ai_will_do = {
			factor = 1
		}
		complete_effect = {
			HUN = { country_event = { id = hungary_flavor.137 } }
		}
	}
}