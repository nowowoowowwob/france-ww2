technologies = {

	early_fighter = {

		enable_equipments = {
			fighter_equipment_0
		}

		path = {
			leads_to_tech = fighter1
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = CAS1
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = naval_bomber1
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1933
		folder = {
			name = air_techs_folder
			position = { x = 2 y = 2 }
		}

		categories = {
			light_air
			light_fighter
			air_equipment
		}

		sub_technologies = {
			cv_early_fighter
		}

		ai_will_do = {
			factor = 1
		}
	}

	cv_early_fighter = {

		enable_equipments = {
			cv_fighter_equipment_0
		}

		research_cost = 1.0 #Carrier Variant
		start_year = 1933

		categories = {
			naval_air
			air_equipment
		}

		ai_will_do = {
			factor = 1.5
		}
	}

	fighter1 = {

		enable_equipments = {
			fighter_equipment_1
		}

		path = {
			leads_to_tech = fighter2
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1936

		folder = {
			name = air_techs_folder
			position = { x = 2 y = 4 }
		}

		categories = {
			light_air
			light_fighter
			air_equipment
		}

		sub_technologies = {
			cv_fighter1
		}

		ai_will_do = {
			factor = 4
		}
	}

	cv_fighter1 = {

		enable_equipments = {
			cv_fighter_equipment_1
		}

		research_cost = 1 #Carrier Variant
		start_year = 1936

		categories = {
			naval_air
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	fighter2 = {

		enable_equipments = {
			fighter_equipment_2
		}

		path = {
			leads_to_tech = fighter3
			research_cost_coeff = 1
		}


		research_cost = 2
		start_year = 1938
		folder = {
			name = air_techs_folder
			position = { x = 2 y = 6 }
		}

		categories = {
			light_air
			light_fighter
			air_equipment
		}

		sub_technologies = {
			cv_fighter2
		}

		ai_will_do = {
			factor = 4
		}

		on_research_complete = {
			if = {
				limit = { original_tag = ENG }

				create_equipment_variant = {
					name = "Supermarine Spitfire"
					type = fighter_equipment_2
					upgrades = {
						plane_gun_upgrade = 3
						plane_range_upgrade = 0
						plane_engine_upgrade = 3
						plane_reliability_upgrade = 2
					}
				}
				add_equipment_production = {
					equipment = {
						type = fighter_equipment_2
						creator = "ENG"
						version_name = "Supermarine Spitfire"
					}
					requested_factories = 15
					progress = 0
					efficiency = 35
				}
			}
		}
	}

	cv_fighter2 = {

		enable_equipments = {
			cv_fighter_equipment_2
		}

		research_cost = 1.0 #Carrier Variant
		start_year = 1938

		categories = {
			naval_air
			air_equipment
		}

		ai_will_do = {
			factor = 1

		}
	}

	fighter3 = {

		enable_equipments = {
			fighter_equipment_3
		}

		path = {
			leads_to_tech = fighter4
			research_cost_coeff = 1
		}

		path = {
			leads_to_tech = jet_fighter1
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1940

		folder = {
			name = air_techs_folder
			position = { x = 2 y = 8 }
		}

		categories = {
			light_air
			light_fighter
			air_equipment
		}

		sub_technologies = {
			cv_fighter3
		}

		ai_will_do = {
			factor = 4
		}
	}

	cv_fighter3 = {

		enable_equipments = {
			cv_fighter_equipment_3
		}

		research_cost = 1.0 #Carrier Variant
		start_year = 1940

		categories = {
			naval_air
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	fighter4 = {

		enable_equipments = {
			fighter_equipment_4
		}

		path = {
			leads_to_tech = fighter5
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1942

		folder = {
			name = air_techs_folder
			position = { x = 0 y = 10 }
		}

		categories = {
			light_air
			light_fighter
			air_equipment
		}

		sub_technologies = {
			cv_fighter4
		}

		ai_will_do = {
			factor = 4
		}
	}

	cv_fighter4 = {

		enable_equipments = {
			cv_fighter_equipment_4
		}

		research_cost = 1.0 #Carrier Variant
		start_year = 1942

		categories = {
			naval_air
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	fighter5 = {

		enable_equipments = {
			fighter_equipment_5
		}

		path = {
			leads_to_tech = fighter6
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1944

		folder = {
			name = air_techs_folder
			position = { x = 0 y = 12 }
		}

		categories = {
			light_air
			light_fighter
			air_equipment
		}

		sub_technologies = {
			cv_fighter5
		}

		ai_will_do = {
			factor = 4
		}
	}

	cv_fighter5 = {

		enable_equipments = {
			cv_fighter_equipment_5
		}

		research_cost = 1.0 #Carrier Variant
		start_year = 1944

		categories = {
			naval_air
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	fighter6 = {

		enable_equipments = {
			fighter_equipment_6
		}

		research_cost = 2
		start_year = 1946

		folder = {
			name = air_techs_folder
			position = { x = 0 y = 14 }
		}

		categories = {
			light_air
			light_fighter
			air_equipment
		}

		sub_technologies = {
			cv_fighter6
		}

		ai_will_do = {
			factor = 4
		}
	}

	cv_fighter6 = {

		enable_equipments = {
			cv_fighter_equipment_6
		}

		research_cost = 1.0 #Carrier Variant
		start_year = 1946

		categories = {
			naval_air
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	jet_fighter1 = {

		enable_equipments = {
				jet_fighter_equipment_1
		}

		path = {
			leads_to_tech = jet_fighter2
			research_cost_coeff = 1
		}

		dependencies = {
			jet_engines = 1
		}

		research_cost = 2
		start_year = 1942

		folder = {
			name = air_techs_folder
			position = { x = 4 y = 10 }
		}

		categories = {
			light_air
			jet_technology
		}

		ai_will_do = {
			factor = 1
		}
	}

	jet_fighter2 = {

		enable_equipments = {
			jet_fighter_equipment_2
		}

		path = {
			leads_to_tech = jet_fighter3
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1944

		folder = {
			name = air_techs_folder
			position = { x = 4 y = 12 }
		}

		categories = {
			light_air
			jet_technology
		}

		ai_will_do = {
			factor = 1
		}
	}

	jet_fighter3 = {

		enable_equipments = {
			jet_fighter_equipment_3
		}

		research_cost = 2
		start_year = 1946

		folder = {
			name = air_techs_folder
			position = { x = 4 y = 14 }
		}

		categories = {
			light_air
			jet_technology
		}

		ai_will_do = {
			factor = 1
		}
	}


	CAS1 = {

		enable_equipments = {
			CAS_equipment_1
		}

		path = {
			leads_to_tech = CAS2
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1936

		folder = {
			name = air_techs_folder
			position = { x = -2 y = 4 }
		}

		sub_technologies = {
			cv_CAS1
		}

		categories = {
			light_air
			cas_bomber
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	cv_CAS1 = {

		enable_equipments = {
			cv_CAS_equipment_1
		}

		research_cost = 1.0 #Carrier Variant
		start_year = 1936

		categories = {
			naval_air
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	CAS2 = {

		enable_equipments = {
			CAS_equipment_2
		}

		path = {
			leads_to_tech = CAS3
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1938

		folder = {
			name = air_techs_folder
			position = { x = -2 y = 6 }
		}

		sub_technologies = {
			cv_CAS2
		}

		categories = {
			light_air
			cas_bomber
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	cv_CAS2 = {

		enable_equipments = {
			cv_CAS_equipment_2
		}

		research_cost = 1.0 #Carrier Variant
		start_year = 1938

		categories = {
			naval_air
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	CAS3 = {

		enable_equipments = {
			CAS_equipment_3
		}

		path = {
			leads_to_tech = CAS4
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1940
		folder = {
			name = air_techs_folder
			position = { x = -2 y = 8 }
		}

		sub_technologies = {
			cv_CAS3
		}

		categories = {
			light_air
			cas_bomber
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	cv_CAS3 = {

		enable_equipments = {
			cv_CAS_equipment_3
		}

		research_cost = 1.0 #Carrier Variant
		start_year = 1940

		categories = {
			naval_air
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	CAS4 = {

		enable_equipments = {
			CAS_equipment_4
		}

		research_cost = 2
		start_year = 1942
		folder = {
			name = air_techs_folder
			position = { x = -4 y = 10 }
		}

		path = {
			leads_to_tech = CAS5
			research_cost_coeff = 1
		}

		sub_technologies = {
			cv_CAS4
		}

		categories = {
			light_air
			cas_bomber
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	cv_CAS4 = {

		enable_equipments = {
			cv_CAS_equipment_4
		}

		research_cost = 1.0 #Carrier Variant
		start_year = 1942

		categories = {
			naval_air
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	CAS5 = {

		enable_equipments = {
			CAS_equipment_5
		}

		research_cost = 2
		start_year = 1944
		folder = {
			name = air_techs_folder
			position = { x = -4 y = 12 }
		}

		path = {
			leads_to_tech = CAS6
			research_cost_coeff = 1
		}

		sub_technologies = {
			cv_CAS5
		}

		categories = {
			light_air
			cas_bomber
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	cv_CAS5 = {

		enable_equipments = {
			cv_CAS_equipment_5
		}

		research_cost = 1.0 #Carrier Variant
		start_year = 1944

		categories = {
			naval_air
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	CAS6 = {

		enable_equipments = {
			CAS_equipment_6
		}

		research_cost = 2
		start_year = 1946
		folder = {
			name = air_techs_folder
			position = { x = -4 y = 14 }
		}

		sub_technologies = {
			cv_CAS6
		}

		categories = {
			light_air
			cas_bomber
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	cv_CAS6 = {

		enable_equipments = {
			cv_CAS_equipment_6
		}

		research_cost = 1.0 #Carrier Variant
		start_year = 1946

		categories = {
			naval_air
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	naval_bomber1 = {

		enable_equipments = {
			nav_bomber_equipment_1
		}

		path = {
			leads_to_tech = naval_bomber2
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1936
		folder = {
			name = air_techs_folder
			position = { x = 6 y = 4 }
		}

		sub_technologies = {
			cv_naval_bomber1
		}

		categories = {
			naval_air
			naval_bomber
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	cv_naval_bomber1 = {

		enable_equipments = {
			cv_nav_bomber_equipment_1
		}

		research_cost = 1.0 #Carrier Variant
		start_year = 1936

		categories = {
			naval_air
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	naval_bomber2 = {

		enable_equipments = {
			nav_bomber_equipment_2
		}

		path = {
			leads_to_tech = naval_bomber3
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1938
		folder = {
			name = air_techs_folder
			position = { x = 6 y = 6 }
		}

		sub_technologies = {
			cv_naval_bomber2
		}

		categories = {
			naval_air
			air_equipment
			naval_bomber
		}

		ai_will_do = {
			factor = 1.5
		}
	}

	cv_naval_bomber2 = {

		enable_equipments = {
			cv_nav_bomber_equipment_2
		}

		research_cost = 1.0 #Carrier Variant
		start_year = 1938

		categories = {
			naval_air
			air_equipment
		}

		ai_will_do = {
			factor = 1.5 #if you have carriers, you definitely want these!
		}
	}

	naval_bomber3 = {

		enable_equipments = {
			nav_bomber_equipment_3
		}

		path = {
			leads_to_tech = naval_bomber4
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1940
		folder = {
			name = air_techs_folder
			position = { x = 6 y = 8 }
		}

		sub_technologies = {
			cv_naval_bomber3
		}

		categories = {
			naval_air
			naval_bomber
			air_equipment
		}

		ai_will_do = {
			factor = 1.5
		}
	}

	cv_naval_bomber3 = {

		enable_equipments = {
			cv_nav_bomber_equipment_3
		}

		research_cost = 1.0 #Carrier Variant
		start_year = 1940

		categories = {
			naval_air
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	naval_bomber4 = {

		enable_equipments = {
			nav_bomber_equipment_4
		}

		path = {
			leads_to_tech = naval_bomber5
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1942
		folder = {
			name = air_techs_folder
			position = { x = 8 y = 10 }
		}

		sub_technologies = {
			cv_naval_bomber4
		}

		categories = {
			naval_air
			naval_bomber
			air_equipment
		}

		ai_will_do = {
			factor = 1.5
		}
	}

	cv_naval_bomber4 = {

		enable_equipments = {
			cv_nav_bomber_equipment_4
		}

		research_cost = 1.0 #Carrier Variant
		start_year = 1942

		categories = {
			naval_air
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	naval_bomber5 = {

		enable_equipments = {
			nav_bomber_equipment_5
		}

		path = {
			leads_to_tech = naval_bomber6
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1944
		folder = {
			name = air_techs_folder
			position = { x = 8 y = 12 }
		}

		sub_technologies = {
			cv_naval_bomber5
		}

		categories = {
			naval_air
			naval_bomber
			air_equipment
		}

		ai_will_do = {
			factor = 1.5
		}
	}

	cv_naval_bomber5 = {

		enable_equipments = {
			cv_nav_bomber_equipment_5
		}

		research_cost = 1.0 #Carrier Variant
		start_year = 1944

		categories = {
			naval_air
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	naval_bomber6 = {

		enable_equipments = {
			nav_bomber_equipment_6
		}

		research_cost = 2
		start_year = 1946
		folder = {
			name = air_techs_folder
			position = { x = 8 y = 14 }
		}

		sub_technologies = {
			cv_naval_bomber6
		}

		categories = {
			naval_air
			naval_bomber
			air_equipment
		}

		ai_will_do = {
			factor = 1.5
		}
	}

	cv_naval_bomber6 = {

		enable_equipments = {
			cv_nav_bomber_equipment_6
		}

		research_cost = 1.0 #Carrier Variant
		start_year = 1946

		categories = {
			naval_air
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	early_bomber = {

		enable_equipments = {
			tac_bomber_equipment_0
		}

		path = {
			leads_to_tech = heavy_fighter1
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = tactical_bomber1
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1933
		folder = {
			name = air_techs_folder
			position = { x = 4 y = 2 }
		}

		categories = {
			medium_air
			tactical_bomber
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	heavy_fighter1 = {

		enable_equipments = {
			heavy_fighter_equipment_1
		}

		path = {
			leads_to_tech = heavy_fighter2
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1936
		folder = {
			name = air_techs_folder
			position = { x = 0 y = 4 }
		}

		categories = {
			medium_air
			air_equipment
			cat_heavy_fighter
		}

		ai_will_do = {
			factor = 1
		}
	}

	heavy_fighter2 = {

		enable_equipments = {
			heavy_fighter_equipment_2
		}

		path = {
			leads_to_tech = heavy_fighter3
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1938
		folder = {
			name = air_techs_folder
			position = { x = 0 y = 6 }
		}

		categories = {
			medium_air
			air_equipment
			cat_heavy_fighter
		}

		ai_will_do = {
			factor = 1
		}
	}

	heavy_fighter3 = {

		enable_equipments = {
			heavy_fighter_equipment_3
		}

		path = {
			leads_to_tech = heavy_fighter4
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1940
		folder = {
			name = air_techs_folder
			position = { x = 0 y = 8 }
		}

		categories = {
			medium_air
			air_equipment
			cat_heavy_fighter
		}

		ai_will_do = {
			factor = 1
		}
	}

	heavy_fighter4 = {

		enable_equipments = {
			heavy_fighter_equipment_4
		}

		path = {
			leads_to_tech = heavy_fighter5
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1942
		folder = {
			name = air_techs_folder
			position = { x = -2 y = 10 }
		}

		categories = {
			medium_air
			air_equipment
			cat_heavy_fighter
		}

		ai_will_do = {
			factor = 1
		}
	}

	heavy_fighter5 = {

		enable_equipments = {
			heavy_fighter_equipment_5
		}

		path = {
			leads_to_tech = heavy_fighter6
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1944
		folder = {
			name = air_techs_folder
			position = { x = -2 y = 12 }
		}

		categories = {
			medium_air
			air_equipment
			cat_heavy_fighter
		}

		ai_will_do = {
			factor = 1
		}
	}

	heavy_fighter6 = {

		enable_equipments = {
			heavy_fighter_equipment_6
		}

		research_cost = 2
		start_year = 1946
		folder = {
			name = air_techs_folder
			position = { x = -2 y = 14 }
		}

		categories = {
			medium_air
			air_equipment
			cat_heavy_fighter
		}

		ai_will_do = {
			factor = 1
		}
	}

	tactical_bomber1 = {

		enable_equipments = {
			tac_bomber_equipment_1
		}

		path = {
			leads_to_tech = tactical_bomber2
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1936

		folder = {
			name = air_techs_folder
			position = { x = 4 y = 4 }
		}

		categories = {
			medium_air
			tactical_bomber
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	tactical_bomber2 = {

		enable_equipments = {
			tac_bomber_equipment_2
		}

		path = {
			leads_to_tech = tactical_bomber3
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1938
		folder = {
			name = air_techs_folder
			position = { x = 4 y = 6 }
		}

		categories = {
			medium_air
			tactical_bomber
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	tactical_bomber3 = {

		enable_equipments = {
			tac_bomber_equipment_3
		}

		path = {
			leads_to_tech = jet_tactical_bomber1
			research_cost_coeff = 1
		}

		path = {
			leads_to_tech = tactical_bomber4
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1940
		folder = {
			name = air_techs_folder
			position = { x = 4 y = 8 }
		}

		categories = {
			medium_air
			tactical_bomber
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	tactical_bomber4 = {

		enable_equipments = {
			tac_bomber_equipment_4
		}

		path = {
			leads_to_tech = tactical_bomber5
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1942
		folder = {
			name = air_techs_folder
			position = { x = 6 y = 10 }
		}

		categories = {
			medium_air
			tactical_bomber
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	tactical_bomber5 = {

		enable_equipments = {
			tac_bomber_equipment_5
		}

		path = {
			leads_to_tech = tactical_bomber6
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1944
		folder = {
			name = air_techs_folder
			position = { x = 6 y = 12 }
		}

		categories = {
			medium_air
			tactical_bomber
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	tactical_bomber6 = {

		enable_equipments = {
			tac_bomber_equipment_6
		}

		research_cost = 2
		start_year = 1946
		folder = {
			name = air_techs_folder
			position = { x = 6 y = 14 }
		}

		categories = {
			medium_air
			tactical_bomber
			air_equipment
		}

		ai_will_do = {
			factor = 1
		}
	}

	jet_tactical_bomber1 = {

		enable_equipments = {
			jet_tac_bomber_equipment_1
		}

		path = {
			leads_to_tech = jet_tactical_bomber2
			research_cost_coeff = 1
		}

		dependencies = {
			jet_engines = 1
		}

		research_cost = 2
		start_year = 1942

		folder = {
			name = air_techs_folder
			position = { x = 2 y = 10 }
		}

		categories = {
			medium_air
			jet_technology
			tactical_bomber
		}

		ai_will_do = {
			factor = 1
		}
	}

	jet_tactical_bomber2 = {

		enable_equipments = {
			jet_tac_bomber_equipment_2
		}

		path = {
			leads_to_tech = jet_tactical_bomber3
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1944
		folder = {
			name = air_techs_folder
			position = { x = 2 y = 12 }
		}

		categories = {
			medium_air
			jet_technology
			tactical_bomber
		}

		ai_will_do = {
			factor = 1
		}
	}

	jet_tactical_bomber3 = {

		enable_equipments = {
			jet_tac_bomber_equipment_3
		}

		research_cost = 2
		start_year = 1946
		folder = {
			name = air_techs_folder
			position = { x = 2 y = 14 }
		}

		categories = {
			medium_air
			jet_technology
			tactical_bomber
		}

		ai_will_do = {
			factor = 1
		}
	}


	strategic_bomber1 = {

		enable_equipments = {
			strat_bomber_equipment_1
		}

		path = {
			leads_to_tech = strategic_bomber2
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1936
		folder = {
			name = air_techs_folder
			position = { x = 2 y = 4 }
		}

		categories = {
			heavy_air
			air_equipment
			cat_strategic_bomber
		}

		ai_will_do = {
			factor = 1

		}
	}

	strategic_bomber2 = {

		enable_equipments = {
			strat_bomber_equipment_2
		}

		path = {
			leads_to_tech = strategic_bomber3
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1938
		folder = {
			name = air_techs_folder
			position = { x = 2 y = 6 }
		}

		categories = {
			heavy_air
			air_equipment
			cat_strategic_bomber
		}

		ai_will_do = {
			factor = 1

		}
	}

	strategic_bomber3 = {

		enable_equipments = {
			strat_bomber_equipment_3
		}

		path = {
			leads_to_tech = strategic_bomber4
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1940
		folder = {
			name = air_techs_folder
			position = { x = 2 y = 8 }
		}

		categories = {
			heavy_air
			air_equipment
			cat_strategic_bomber
		}

		ai_will_do = {
			factor = 1

		}
	}

	strategic_bomber4 = {

		enable_equipments = {
			strat_bomber_equipment_4
		}

		path = {
			leads_to_tech = strategic_bomber5
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1942
		folder = {
			name = air_techs_folder
			position = { x = 4 y = 10 }
		}

		categories = {
			heavy_air
			air_equipment
			cat_strategic_bomber
		}

		ai_will_do = {
			factor = 1

		}
	}

	strategic_bomber5 = {

		enable_equipments = {
			strat_bomber_equipment_5
		}

		path = {
			leads_to_tech = strategic_bomber6
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1944
		folder = {
			name = air_techs_folder
			position = { x = 4 y = 12 }
		}

		categories = {
			heavy_air
			air_equipment
			cat_strategic_bomber
		}

		ai_will_do = {
			factor = 1

		}
	}

	strategic_bomber6 = {

		enable_equipments = {
			strat_bomber_equipment_6
		}

		research_cost = 2
		start_year = 1946
		folder = {
			name = air_techs_folder
			position = { x = 4 y = 14 }
		}

		categories = {
			heavy_air
			air_equipment
			cat_strategic_bomber
		}

		ai_will_do = {
			factor = 1

		}
	}

	scout_plane1 = {

		enable_equipments = {
			scout_plane_equipment_1
		}

		path = {
			leads_to_tech = scout_plane2
			research_cost_coeff = 1
		}
		allow_branch = { has_dlc = "La Resistance" }
		research_cost = 2
		start_year = 1936
		folder = {
			name = air_techs_folder
			position = { x = 12 y = 4 }
		}

		categories = {
			medium_air
			air_equipment
			cat_scout_plane
		}

		ai_will_do = {
			factor = 1
		}
	}

	scout_plane2 = {

		enable_equipments = {
			scout_plane_equipment_2
		}

		research_cost = 2
		start_year = 1940
		folder = {
			name = air_techs_folder
			position = { x = 12 y = 6 }
		}

		categories = {
			medium_air
			air_equipment
			cat_scout_plane
		}

		ai_will_do = {
			factor = 1
		}
	}

	suicide_craft = {

		enable_equipments = {
			rocket_suicide_equipment_1
		}

		research_cost = 1.5 #Carrier Variant
		start_year = 1944

		# only from focus!!! #####
		allow = {
			always = no
		}

		#folder = {
		#	name = air_techs_folder
		#	position = { x = -2 y = 8 }
		#}

		ai_will_do = {
			factor = 0
		}
		################
	}
}
