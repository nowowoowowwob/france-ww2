﻿###########################
# D-Day scenario Battle Reports
###########################

add_namespace = battle_reports

#
country_event = {
	id = battle_reports.1
	title = battle_reports.1.t
	desc = battle_reports.1.d
	#picture = GFX_report_event_hitler_handshake

	is_triggered_only = yes

	option = {
		name = battle_reports.1.a
	}
}
