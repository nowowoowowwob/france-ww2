﻿###########################
# French Events
###########################

add_namespace = france

# Maginot Line Extension
country_event = {
	id = france.2
	title = france.2.t
	desc = france.2.d
	picture = GFX_report_event_bunker_01

	is_triggered_only = yes

	option = {
		name = france.2.a
		379 = {
			add_building_construction = {
				type = bunker
				level = 2
				instant_build = yes
				province = {
					all_provinces = yes
					limit_to_border = yes
				}
			}
		}
		184 = {
			add_building_construction = {
				type = bunker
				level = 2
				instant_build = yes
				province = {
					all_provinces = yes
					limit_to_border = yes
				}
			}
		}
		364 = {
			add_building_construction = {
				type = bunker
				level = 2
				instant_build = yes
				province = {
					all_provinces = yes
					limit_to_border = yes
				}
			}
		}
		347 = {
			add_building_construction = {
				type = bunker
				level = 2
				instant_build = yes
				province = {
					all_provinces = yes
					limit_to_border = yes
				}
			}
		}
	}
}

# Alpine Line Extension
country_event = {
	id = france.3
	title = france.3.t
	desc = france.3.d
	picture = GFX_report_event_bunker_01

	is_triggered_only = yes

	option = {
		name = france.3.a
		508 = {
			add_building_construction = {
				type = bunker
				level = 2
				instant_build = yes
				province = {
					all_provinces = yes
					limit_to_border = yes
				}
			}
		}
		547 = {
			add_building_construction = {
				type = bunker
				level = 2
				instant_build = yes
				province = {
					all_provinces = yes
					limit_to_border = yes
				}
			}
		}
		515 = {
			add_building_construction = {
				type = bunker
				level = 2
				instant_build = yes
				province = {
					all_provinces = yes
					limit_to_border = yes
				}
			}
		}
		265 = {
			add_building_construction = {
				type = bunker
				level = 2
				instant_build = yes
				province = {
					all_provinces = yes
					limit_to_border = yes
				}
			}
		}
		496 = {
			add_building_construction = {
				type = bunker
				level = 2
				instant_build = yes
				province = {
					all_provinces = yes
					limit_to_border = yes
				}
			}
		}
	}
}
# The Battle is Lost
country_event = {
	id = france.10
	title = france.10.t
	desc = france.10.d
	picture = GFX_report_event_german_parade_paris

	fire_only_once = yes
	mean_time_to_happen = { days = 1 }
	trigger = {
		tag = FRA
		has_war_with = GER
		412 = { is_controlled_by = GER }
		is_in_faction_with = ENG
		ENG = { has_war_with = GER }
		has_capitulated = yes
	}

	immediate = {
		set_global_flag = achievement_france_surrender
		set_global_flag = fall_of_france
		GER = { country_event = france.14 }
	}

	option = { # Become Vichy
		name = france.10.a
		trigger = {
			is_ai = no
			is_mp = no
		}
		set_country_flag = become_vichy
		save_global_event_target_as = vichy_france
		GER = { country_event = france.14 }
		custom_effect_tooltip = france.10.a_tt
		# Switches to Vichy if GER accepts
	}
	option = { # Become Free French
		name = france.10.b
		ai_chance = {
			factor = 85
		}
		save_global_event_target_as = free_france
		GER = { country_event = france.14 }
		custom_effect_tooltip = france.10.b_tt
	}
	option = { # Ask for British Union
		name = france.10.c
		trigger = {
			is_mp = no
		}
		ai_chance = {
			factor = 15
			modifier = {
				factor = 0
				is_historical_focus_on = yes
			}
		}
		ENG = {
			country_event = france.11
			effect_tooltip = {
				annex_country = { target = FRA transfer_troops = yes }
			}
		}
		custom_effect_tooltip = GAME_OVER_TT
	}
}

# France suggests a Union (Britain)
country_event = {
	id = france.11
	title = france.11.t
	desc = france.11.d
	picture = GFX_report_event_degaulle_churchill

	is_triggered_only = yes

	option = { # Refuse Union
		name = france.11.a
		ai_chance = {
			factor = 10
		}
		FRA = {
			country_event = { hours = 6 id = france.13 }
		}
	}
	option = { # Agree to Union
		name = france.11.b
		ai_chance = {
			factor = 90
		}
		FRA = {
			country_event = { hours = 6 id = france.12 }
		}
	}
}

# Britain agrees to Union (France)
country_event = {
	id = france.12
	title = france.12.t
	desc = france.12.d
	picture = GFX_report_event_chamberlain_announce

	is_triggered_only = yes

	option = {
		name = france.12.a
		# Franco-British Union created
		set_global_flag = fall_of_france
		set_global_flag = flag_franco_british_union
		hidden_effect = {
			every_state = {
				limit = {
					is_core_of = FRA
				}
				ENG = {
					add_state_core = PREV
				}
			}
		}
		FRA = {
			every_unit_leader = {
				set_unit_leader_flag = flag_former_french_general
				set_nationality = ENG
			}
		}
		ENG = {
			annex_country = { target = FRA transfer_troops = yes }
		}
		custom_effect_tooltip = FRA_franco_british_cores
		hidden_effect = {
			ENG = { set_cosmetic_tag = FRA_UK }
			GER = {
				country_event = { hours = 6 id = news.35 }
			}
		}
	}
}

# Britain refuses Union (France)
country_event = {
	id = france.13
	title = france.13.t
	desc = france.13.d
	picture = GFX_report_event_pierre_laval

	is_triggered_only = yes

	option = { # Become Vichy
		name = france.13.a
		trigger = { is_ai = no }
		set_country_flag = become_vichy
		set_global_flag = fall_of_france
		save_global_event_target_as = vichy_france
		GER = { country_event = france.14 }
		custom_effect_tooltip = france.10.a_tt
		# Switches to Vichy if GER accepts
	}
	option = { # Become Free France
		name = france.13.b
		GER = { country_event = france.14 }
		save_global_event_target_as = free_france
		custom_effect_tooltip = france.10.b_tt
		set_global_flag = fall_of_france
	}
	option = { # decide to fight on
		name = france.10.e
		ai_chance = {
			factor = 15
			modifier = {
				factor = 0
				is_historical_focus_on = yes
			}
		}
		trigger = { has_war_support > 0.7 }
		add_stability = -0.05
		custom_effect_tooltip = france_continue_the_fight_tt
	}
}

# France wants Armistice (Germany)
country_event = {
	id = france.14
	title = france.14.t
	desc = france.14.d
	picture = GFX_report_event_german_parade_paris

	is_triggered_only = yes

	option = { # Agree
		name = france.14.a
		ai_chance = {
			factor = 100
		}
		# German Occupation
		GER = {
			transfer_state = 73
			transfer_state = 401
			transfer_state = 684
			transfer_state = 761
		}
		every_state = {
			limit = { is_core_of = FRA }
			add_core_of = GFR
			add_core_of = EFR
		}
		GFR = {
			transfer_state = 152
			transfer_state = 166
			transfer_state = 168
			transfer_state = 175
			transfer_state = 184
			transfer_state = 189
			transfer_state = 207
			transfer_state = 245
			transfer_state = 258
			transfer_state = 271
			transfer_state = 295
			transfer_state = 364
			transfer_state = 374
			transfer_state = 375
			transfer_state = 377
			transfer_state = 379
			transfer_state = 398
			transfer_state = 403
			transfer_state = 404
			transfer_state = 406
			transfer_state = 409
			transfer_state = 412
			transfer_state = 418
			transfer_state = 419
			transfer_state = 419
			transfer_state = 430
			transfer_state = 434
			transfer_state = 437
			transfer_state = 439
			transfer_state = 444
			transfer_state = 446
			transfer_state = 451
			transfer_state = 454
			transfer_state = 479
			transfer_state = 487
			transfer_state = 502

			transfer_state = 761
			transfer_state = 65
			transfer_state = 54
			transfer_state = 22
			transfer_state = 26
			transfer_state = 18
			transfer_state = 2
			transfer_state = 17
			transfer_state = 23
			transfer_state = 795
			transfer_state = 800
			transfer_state = 791
			transfer_state = 30
			transfer_state = 77
			transfer_state = 180
			transfer_state = 132
			transfer_state = 260
			transfer_state = 28
			transfer_state = 171
			transfer_state = 151
			transfer_state = 36
			transfer_state = 531
			transfer_state = 159
			transfer_state = 254
			transfer_state = 38
			transfer_state = 790
			transfer_state = 43
			transfer_state = 13
			transfer_state = 167
			transfer_state = 114
			transfer_state = 792
			transfer_state = 31
			transfer_state = 665
			transfer_state = 45
			transfer_state = 75
			transfer_state = 153
			transfer_state = 63
			transfer_state = 1113
		}
		GER = {
			if = {
				limit = { GFR = { NOT = { is_subject_of = GER } } }
				set_autonomy = { target = GFR autonomy_state = autonomy_reichskommissariat }
			}
		}
		if = {
			limit = { GER = { has_war_with = ENG } }
			GFR = { add_to_war = { targeted_alliance = GER enemy = ENG hostility_reason = asked_to_join } }
		}

		FRA = {
			destroy_ships = {
				type = ship_hull_light
				count = all
			}
			destroy_ships = {
				type = ship_hull_cruiser
				count = all
			}
			destroy_ships = {
				type = ship_hull_heavy
				count = all
			}
			destroy_ships = {
				type = ship_hull_carrier
				count = all
			}
			destroy_ships = {
				type = ship_hull_submarine
				count = all
			}
			start_civil_war = {
				ideology = fascism
				size = 0.9
				capital = 158
				states = {20 34 35 39 42 44 47 50 51 53 57 58 59 71 72 81 86 89 90 94 95 96 100 100 119 138 145 148 155 158 161 201 237 265 346 389 457 469 475 496 498 508 512 515 534 535 538 547 577 585 587 588 594 621 633 662 680 687 688 700 716 718 721 722 723 725 729 734 736 743 756 759 760 789 }
			}

			set_stability = 0.5
			set_war_support = 0.5
			remove_ideas = { FRA_political_violence }
			remove_ideas = { FRA_worker_shortage }
			remove_ideas = { FRA_full_employment }
			remove_ideas = { FRA_inefficient_economy_2 }
			remove_ideas = { FRA_inefficient_economy_1 }
			##add checks if general exists
			if = {
				limit = {
					has_unit_leader = 202
				}
				remove_unit_leader = 202
			}
			if = {
				limit = {
					has_unit_leader = 203
				}
				remove_unit_leader = 203
			}
			if = {
				limit = {
					has_unit_leader = 204
				}
				remove_unit_leader = 204
			}
			if = {
				limit = {
					has_unit_leader = 206
				}
				remove_unit_leader = 206
			}
			if = {
				limit = {
					has_unit_leader = 208
				}
				remove_unit_leader = 208
			}
			if = {
				limit = {
					has_unit_leader = 209
				}
				remove_unit_leader = 209
			}
			if = {
				limit = {
					has_unit_leader = 213
				}
				remove_unit_leader = 213
			}

			add_ideas = FRA_FREE_army_4
			add_ideas = FRA_exile_government_4
			set_cosmetic_tag = FRA_FREE
			# create_corps_commander = {
			# 	name = "Charles De Gaulle"
			# 	picture = "Portrait_France_Charles_De_Gaulle.dds"
			# 	traits = { panzer_leader harsh_leader organizer }
			# 	skill = 4
			# 	attack_skill = 4
			# 	defense_skill = 3
			# 	planning_skill = 3
			# 	logistics_skill = 4
			# }
			create_corps_commander = {
				name = "Marie-Pierre Koenig"
				picture = "Portrait_France_Marie_Pierre_Koenig.dds"
				traits = { trickster organizer infantry_leader }
				skill = 2
				attack_skill = 2
				defense_skill = 3
				planning_skill = 3
				logistics_skill = 2
			}
			# create_corps_commander = {
			# 	name = "Edgard de Larminat"
			# 	picture = "Edgard_de_Larminat.dds"
			# 	traits = { desert_fox infantry_leader harsh_leader exiled_leader }
			# 	skill = 3
			# 	attack_skill = 3
			# 	defense_skill = 2
			# 	planning_skill = 1
			# 	logistics_skill = 3
			# }
			create_navy_leader = {
				name = "Émile Muselier"
				picture = "Portrait_France_Emile_Muselier.dds"
				traits = { spotter }
				skill = 3
				attack_skill = 2
				defense_skill = 2
				maneuvering_skill = 3
				coordination_skill = 3
			}

			set_country_flag = free_france
			transfer_state = 770
			set_capital = { state = 770 }
		}

		# Italian Occupation Zone (if they did well)
		if = {
			limit = {
				ITA = {
					is_in_faction_with = GER
					has_war_with = FRA
				}
			}
			ITA = { transfer_state = 508 }
		}

		random_other_country = {
			limit = { original_tag = FRA has_government = fascism }
			EFR = { annex_country = { target = PREV transfer_troops = no } }
		}

		EFR = {
			set_capital = { state = 158 }
			add_ideas = idea_EFR_neutrality
			diplomatic_relation = { country = ENG relation = non_aggression_pact }
			diplomatic_relation = { country = USA relation = non_aggression_pact }
			diplomatic_relation = { country = FRA relation = non_aggression_pact }
			diplomatic_relation = { country = CAN relation = non_aggression_pact }
			diplomatic_relation = { country = GER relation = non_aggression_pact }
			create_corps_commander = {
				name = "Henri Dentz"
				picture = "Portrait_France_Generic_land_3.dds" ##looks like the generic portrait is really him
				traits = { }
				skill = 1
				attack_skill = 1
				defense_skill = 2
				planning_skill = 1
				logistics_skill = 1
			}
			every_state = {
				limit = {
					NOT = {
						is_core_of = FRA
					}
					is_controlled_by = EFR
				}
				add_claim_by = FRA
			}
			401 = {
				remove_core_of = EFR
			}

			set_variable = { FRA_infantry_equipment = num_equipment@infantry_equipment }
			set_variable = { FRA_support_equipment = num_equipment@support_equipment }
			set_variable = { FRA_motorized_equipment = num_equipment@motorized_equipment }
			set_variable = { FRA_mechanized_equipment = num_equipment@mechanized_equipment }
			set_variable = { FRA_amphibious_mechanized_equipment = num_equipment@amphibious_mechanized_equipment }
			set_variable = { FRA_armored_car_equipment = num_equipment@armored_car_equipment }
			set_variable = { FRA_artillery_equipment = num_equipment@artillery_equipment }
			set_variable = { FRA_rocket_artillery_equipment = num_equipment@rocket_artillery_equipment }
			set_variable = { FRA_motorized_rocket_equipment = num_equipment@motorized_rocket_equipment }
			set_variable = { FRA_anti_air_equipment = num_equipment@anti_air_equipment }
			set_variable = { FRA_anti_tank_equipment = num_equipment@anti_tank_equipment }
			set_variable = { FRA_light_tank_chassis = num_equipment@light_tank_chassis }
			set_variable = { FRA_light_tank_destroyer_chassis = num_equipment@light_tank_destroyer_chassis }
			set_variable = { FRA_light_tank_aa_chassis = num_equipment@light_tank_aa_chassis }
			set_variable = { FRA_light_tank_artillery_chassis = num_equipment@light_tank_artillery_chassis }
			set_variable = { FRA_medium_tank_chassis = num_equipment@medium_tank_chassis }
			set_variable = { FRA_medium_tank_destroyer_chassis = num_equipment@medium_tank_destroyer_chassis }
			set_variable = { FRA_medium_tank_aa_chassis = num_equipment@medium_tank_aa_chassis }
			set_variable = { FRA_medium_tank_artillery_chassis = num_equipment@medium_tank_artillery_chassis }
			set_variable = { FRA_heavy_tank_chassis = num_equipment@heavy_tank_chassis }
			set_variable = { FRA_heavy_tank_destroyer_chassis = num_equipment@heavy_tank_destroyer_chassis }
			set_variable = { FRA_heavy_tank_aa_chassis = num_equipment@heavy_tank_aa_chassis }
			set_variable = { FRA_heavy_tank_artillery_chassis = num_equipment@heavy_tank_artillery_chassis }
			set_variable = { FRA_super_heavy_tank_chassis = num_equipment@super_heavy_tank_chassis }
			set_variable = { FRA_super_heavy_tank_destroyer_chassis = num_equipment@super_heavy_tank_destroyer_chassis }
			set_variable = { FRA_super_heavy_tank_aa_chassis = num_equipment@super_heavy_tank_aa_chassis }
			set_variable = { FRA_super_heavy_tank_artillery_chassis = num_equipment@super_heavy_tank_artillery_chassis }
			set_variable = { FRA_amphibious_tank_chassis = num_equipment@amphibious_tank_chassis }

			subtract_from_variable = { FRA_infantry_equipment = 200 }
			subtract_from_variable = { FRA_support_equipment = 200 }
			subtract_from_variable = { FRA_motorized_equipment = 200 }
			subtract_from_variable = { FRA_mechanized_equipment = 200 }
			subtract_from_variable = { FRA_amphibious_mechanized_equipment = 200 }
			subtract_from_variable = { FRA_armored_car_equipment = 200 }
			subtract_from_variable = { FRA_artillery_equipment = 200 }
			subtract_from_variable = { FRA_rocket_artillery_equipment = 200 }
			subtract_from_variable = { FRA_motorized_rocket_equipment = 200 }
			subtract_from_variable = { FRA_anti_air_equipment = 200 }
			subtract_from_variable = { FRA_anti_tank_equipment = 200 }
			subtract_from_variable = { FRA_light_tank_chassis = 200 }
			subtract_from_variable = { FRA_light_tank_destroyer_chassis = 200 }
			subtract_from_variable = { FRA_light_tank_aa_chassis = 200 }
			subtract_from_variable = { FRA_light_tank_artillery_chassis = 200 }
			subtract_from_variable = { FRA_medium_tank_chassis = 200 }
			subtract_from_variable = { FRA_medium_tank_destroyer_chassis = 200 }
			subtract_from_variable = { FRA_medium_tank_aa_chassis = 200 }
			subtract_from_variable = { FRA_medium_tank_artillery_chassis = 200 }
			subtract_from_variable = { FRA_heavy_tank_chassis = 200 }
			subtract_from_variable = { FRA_heavy_tank_destroyer_chassis = 200 }
			subtract_from_variable = { FRA_heavy_tank_aa_chassis = 200 }
			subtract_from_variable = { FRA_heavy_tank_artillery_chassis = 200 }
			subtract_from_variable = { FRA_super_heavy_tank_chassis = 200 }
			subtract_from_variable = { FRA_super_heavy_tank_destroyer_chassis = 200 }
			subtract_from_variable = { FRA_super_heavy_tank_aa_chassis = 200 }
			subtract_from_variable = { FRA_super_heavy_tank_artillery_chassis = 200 }
			subtract_from_variable = { FRA_amphibious_tank_chassis = 200 }

			clamp_variable = { var = FRA_infantry_equipment min = 0 }
			clamp_variable = { var = FRA_support_equipment min = 0 }
			clamp_variable = { var = FRA_motorized_equipment min = 0 }
			clamp_variable = { var = FRA_mechanized_equipment min = 0 }
			clamp_variable = { var = FRA_amphibious_mechanized_equipment min = 0 }
			clamp_variable = { var = FRA_armored_car_equipment min = 0 }
			clamp_variable = { var = FRA_artillery_equipment min = 0 }
			clamp_variable = { var = FRA_rocket_artillery_equipment min = 0 }
			clamp_variable = { var = FRA_motorized_rocket_equipment min = 0 }
			clamp_variable = { var = FRA_anti_air_equipment min = 0 }
			clamp_variable = { var = FRA_anti_tank_equipment min = 0 }
			clamp_variable = { var = FRA_light_tank_chassis min = 0 }
			clamp_variable = { var = FRA_light_tank_destroyer_chassis min = 0 }
			clamp_variable = { var = FRA_light_tank_aa_chassis min = 0 }
			clamp_variable = { var = FRA_light_tank_artillery_chassis min = 0 }
			clamp_variable = { var = FRA_medium_tank_chassis min = 0 }
			clamp_variable = { var = FRA_medium_tank_destroyer_chassis min = 0 }
			clamp_variable = { var = FRA_medium_tank_aa_chassis min = 0 }
			clamp_variable = { var = FRA_medium_tank_artillery_chassis min = 0 }
			clamp_variable = { var = FRA_heavy_tank_chassis min = 0 }
			clamp_variable = { var = FRA_heavy_tank_destroyer_chassis min = 0 }
			clamp_variable = { var = FRA_heavy_tank_aa_chassis min = 0 }
			clamp_variable = { var = FRA_heavy_tank_artillery_chassis min = 0 }
			clamp_variable = { var = FRA_super_heavy_tank_chassis min = 0 }
			clamp_variable = { var = FRA_super_heavy_tank_destroyer_chassis min = 0 }
			clamp_variable = { var = FRA_super_heavy_tank_aa_chassis min = 0 }
			clamp_variable = { var = FRA_super_heavy_tank_artillery_chassis min = 0 }
			clamp_variable = { var = FRA_amphibious_tank_chassis min = 0 }

			send_equipment = { target = GER type = infantry_equipment amount = var:FRA_infantry_equipment }
			send_equipment = { target = GER type = support_equipment amount = var:FRA_support_equipment }
			send_equipment = { target = GER type = motorized_equipment amount = var:FRA_motorized_equipment }
			send_equipment = { target = GER type = mechanized_equipment amount = var:FRA_mechanized_equipment }
			send_equipment = { target = GER type = amphibious_mechanized_equipment amount = var:FRA_amphibious_mechanized_equipment }
			send_equipment = { target = GER type = armored_car_equipment amount = var:FRA_armored_car_equipment }
			send_equipment = { target = GER type = artillery_equipment amount = var:FRA_artillery_equipment }
			send_equipment = { target = GER type = rocket_artillery_equipment amount = var:FRA_rocket_artillery_equipment }
			send_equipment = { target = GER type = motorized_rocket_equipment amount = var:FRA_motorized_rocket_equipment }
			send_equipment = { target = GER type = anti_air_equipment amount = var:FRA_anti_air_equipment }
			send_equipment = { target = GER type = anti_tank_equipment amount = var:FRA_anti_tank_equipment }
			send_equipment = { target = GER type = light_tank_chassis amount = var:FRA_light_tank_chassis }
			send_equipment = { target = GER type = light_tank_destroyer_chassis amount = var:FRA_light_tank_destroyer_chassis }
			send_equipment = { target = GER type = light_tank_aa_chassis amount = var:FRA_light_tank_aa_chassis }
			send_equipment = { target = GER type = light_tank_artillery_chassis amount = var:FRA_light_tank_artillery_chassis }
			send_equipment = { target = GER type = medium_tank_chassis amount = var:FRA_medium_tank_chassis }
			send_equipment = { target = GER type = medium_tank_destroyer_chassis amount = var:FRA_medium_tank_destroyer_chassis }
			send_equipment = { target = GER type = medium_tank_aa_chassis amount = var:FRA_medium_tank_aa_chassis }
			send_equipment = { target = GER type = medium_tank_artillery_chassis amount = var:FRA_medium_tank_artillery_chassis }
			send_equipment = { target = GER type = heavy_tank_chassis amount = var:FRA_heavy_tank_chassis }
			send_equipment = { target = GER type = heavy_tank_destroyer_chassis amount = var:FRA_heavy_tank_destroyer_chassis }
			send_equipment = { target = GER type = heavy_tank_aa_chassis amount = var:FRA_heavy_tank_aa_chassis }
			send_equipment = { target = GER type = heavy_tank_artillery_chassis amount = var:FRA_heavy_tank_artillery_chassis }
			send_equipment = { target = GER type = super_heavy_tank_chassis amount = var:FRA_super_heavy_tank_chassis }
			send_equipment = { target = GER type = super_heavy_tank_destroyer_chassis amount = var:FRA_super_heavy_tank_destroyer_chassis }
			send_equipment = { target = GER type = super_heavy_tank_aa_chassis amount = var:FRA_super_heavy_tank_aa_chassis }
			send_equipment = { target = GER type = super_heavy_tank_artillery_chassis amount = var:FRA_super_heavy_tank_artillery_chassis }
			send_equipment = { target = GER type = amphibious_tank_chassis amount = var:FRA_amphibious_tank_chassis }

			if = {
				limit = { FRA = { has_country_flag = become_vichy } }
				change_tag_from = FRA
			}

			if = {
				limit = { 20 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 20
			}
			if = {
				limit = { 34 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 34
			}
			if = {
				limit = { 35 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 35
			}
			if = {
				limit = { 39 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 39
			}
			if = {
				limit = { 42 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 42
			}
			if = {
				limit = { 44 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 44
			}
			if = {
				limit = { 47 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 47
			}
			if = {
				limit = { 50 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 50
			}
			if = {
				limit = { 51 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 51
			}
			if = {
				limit = { 53 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 53
			}
			if = {
				limit = { 57 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 57
			}
			if = {
				limit = { 58 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 58
			}
			if = {
				limit = { 59 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 59
			}
			if = {
				limit = { 71 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 71
			}
			if = {
				limit = { 72 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 72
			}
			if = {
				limit = { 81 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 81
			}
			if = {
				limit = { 86 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 86
			}
			if = {
				limit = { 89 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 89
			}
			if = {
				limit = { 90 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 90
			}
			if = {
				limit = { 94 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 94
			}
			if = {
				limit = { 95 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 95
			}
			if = {
				limit = { 96 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 96
			}
			if = {
				limit = { 100 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 100
			}
			if = {
				limit = { 100 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 100
			}
			if = {
				limit = { 119 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 119
			}
			if = {
				limit = { 138 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 138
			}
			if = {
				limit = { 145 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 145
			}
			if = {
				limit = { 148 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 148
			}
			if = {
				limit = { 155 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 155
			}
			if = {
				limit = { 158 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 158
			}
			if = {
				limit = { 161 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 161
			}
			if = {
				limit = { 201 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 201
			}
			if = {
				limit = { 237 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 237
			}
			if = {
				limit = { 265 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 265
			}
			if = {
				limit = { 346 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 346
			}
			if = {
				limit = { 389 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 389
			}
			if = {
				limit = { 457 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 457
			}
			if = {
				limit = { 469 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 469
			}
			if = {
				limit = { 475 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 475
			}
			if = {
				limit = { 496 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 496
			}
			if = {
				limit = { 498 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 498
			}
			if = {
				limit = { 508 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 508
			}
			if = {
				limit = { 512 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 512
			}
			if = {
				limit = { 515 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 515
			}
			if = {
				limit = { 534 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 534
			}
			if = {
				limit = { 535 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 535
			}
			if = {
				limit = { 538 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 538
			}
			if = {
				limit = { 547 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 547
			}
			if = {
				limit = { 577 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 577
			}
			if = {
				limit = { 585 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 585
			}
			if = {
				limit = { 587 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 587
			}
			if = {
				limit = { 588 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 588
			}
			if = {
				limit = { 594 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 594
			}
			if = {
				limit = { 621 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 621
			}
			if = {
				limit = { 633 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 633
			}
			if = {
				limit = { 662 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 662
			}
			if = {
				limit = { 680 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 680
			}
			if = {
				limit = { 687 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 687
			}
			if = {
				limit = { 688 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 688
			}
			if = {
				limit = { 700 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 700
			}
			if = {
				limit = { 716 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 716
			}
			if = {
				limit = { 718 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 718
			}
			if = {
				limit = { 721 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 721
			}
			if = {
				limit = { 722 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 722
			}
			if = {
				limit = { 723 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 723
			}
			if = {
				limit = { 725 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 725
			}
			if = {
				limit = { 729 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 729
			}
			if = {
				limit = { 734 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 734
			}
			if = {
				limit = { 736 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 736
			}
			if = {
				limit = { 743 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 743
			}
			if = {
				limit = { 756 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 756
			}
			if = {
				limit = { 759 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 759
			}
			if = {
				limit = { 760 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 760
			}
			if = {
				limit = { 789 = { NOT = { is_owned_by = EFR } } }
				transfer_state = 789
			}

		}
		country_event = { hours = 6 id = news.34 }
		hidden_effect =  {
			FRA = { news_event = { id = france.20 days = 1 } }
		}
	}
}

# Germany refuses armistice! (France)
country_event = {
	id = france.15
	title = france.15.t
	desc = france.15.d
	picture = GFX_report_event_french_resistance_01

	is_triggered_only = yes

	option = {
		name = france.15.a
	}
}

#Capital captured by non french faction
country_event = {
	id = france.16
	title = france.16.t
	desc = france.16.d
	picture = GFX_report_event_french_resistance_02

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = france.16.a
		remove_ideas = FRA_victors_of_wwi
		remove_ideas = FRA_disjointed_government
		remove_ideas = FRA_protected_by_the_maginot_line
	}
}

# Invitation to French Faction
country_event = {
	id = france.17
	title = france.17.t
	desc = france.17.d
	picture = GFX_report_event_worried_french

	is_triggered_only = yes

	option = {
		name = france.17.a
		ai_chance = { factor = 90 }
		FRA = {
			country_event = france.18
			add_to_faction = ROOT
		}
		add_ai_strategy = {
			type = alliance
			id = "FRA"
			value = 200
		}
	}

	option = {
		name = france.17.b
		ai_chance = { factor = 10 }
		FRA = { country_event = france.19 }
	}
}

# [Country] Accepts
country_event = {
	id = france.18
	title = france.18.t
	desc = france.18.d
	picture = GFX_report_event_soldiers_in_france

	is_triggered_only = yes

	option = {
		name = france.18.a
		effect_tooltip = {
			add_to_faction = FROM
		}
	}
}

# [Country] Refuses
country_event = {
	id = france.19
	title = france.19.t
	desc = france.19.d
	picture = GFX_report_event_finnish_letter

	is_triggered_only = yes

	option = {
		name = france.19.a
	}
}

# De Gaulle becomes leader
country_event = {
	id = france.20
	title = france.20.t
	desc = france.20.d
	picture = GFX_report_event_degaulle_churchill

	is_triggered_only = yes

	option = {
		custom_effect_tooltip = france.20.a_tt
		set_country_flag = france_de_gaulle
		hidden_effect =  {
			create_country_leader = {
				name = "Charles de Gaulle"
				desc = "POLITICS_CHARLES_DE_GAULLE_DESC"
				picture = "Portrait_France_Charles_De_Gaulle.dds"
				expire = "1965.1.1"
				ideology = conservatism
				traits = {
					nationalist_symbol exiled
				}
			}
		}

		name = france.20.a
	}
}

# British abandon yugoslavia
country_event = {
	id = france.25
	title = france.25.t
	desc = france.25.d
	picture = GFX_report_event_hitler_croatia_handshake

	is_triggered_only = yes
	option = { # Follow Britain's lead
		name = france.25.a
		ai_chance = {
			factor = 90
			modifier = {
				factor = 0
				OR = {
					is_in_faction_with = YUG
					has_war_with = GER
				}
			}
		}
		if = {
			limit = {
				has_guaranteed = YUG
			}
			diplomatic_relation = {
   				country = YUG
   				relation = guarantee
   				active = no
  			}
		}
		if = {
			limit = {
				ITA = {
					OR = {
						is_in_faction_with = GER
						exists = no
					}
				}
			}
			YUG = {
				add_opinion_modifier = { target = FRA modifier = western_betrayal }
				country_event = { id = yugoslavia.5 hours = 6 }
			}
		}
	}
	option = { # Support the Yugoslavs alone
		name = france.25.b
		trigger = {
			is_mp = no
		}
		ai_chance = {
			factor = 10
			modifier = {
				factor = 0
				is_in_faction_with = GER
			}
		}
		if = {
			limit = {
				NOT = {
					has_war_with = GER
				}
			}
			add_political_power = -200
			add_opinion_modifier = { target = GER modifier = condemn_aggression }
		}
		YUG = {
			country_event = { id = yugoslavia.4 days = 1 }
			add_opinion_modifier = { target = FRA modifier = offered_support }
		}
		GER = {
			country_event = { id = germany.97 hours = 6 } #Generate wargoal
			effect_tooltip = {
				create_wargoal = {
					type = take_state_focus
					target = YUG
					generator = { 480 }
				}
			}
		}
		if = {
			limit = {
				is_in_faction_with = ENG
			}
			if = {
				limit = { ENG = { is_faction_leader = yes } }
				ENG = {
					country_event = { id = britain.15 hours = 6 }
					remove_from_faction = FRA
				}
				else_if = {
					limit = {
						is_faction_leader = yes
					}
					remove_from_faction = ENG
				}
			}
		}
		if = {
			limit = { is_in_faction_with = GER }
			GER = {
				remove_from_faction = FRA
			}
		}
		if = {
			limit = {
				is_in_faction = yes
				is_faction_leader = yes
				NOT = { is_in_faction_with = ENG }
				NOT = { is_in_faction_with = GER }
			}
			add_to_faction = YUG
			YUG = {
				add_ai_strategy = {
					type = alliance
					id = "FRA"
					value = 200
				}
			}
		}
		if = {
			limit = {
				OR = {
					is_in_faction = no
					is_faction_leader = no
					is_in_faction_with = ENG
					is_in_faction_with = GER
				}
			}
			create_faction = french_alliance
			add_to_faction = YUG
			YUG = {
				add_ai_strategy = {
					type = alliance
					id = "FRA"
					value = 200
				}
			}
		}
		add_stability = 0.05
	}
}

# British support yugoslavia
country_event = {
	id = france.26
	title = france.26.t
	desc = france.26.d
	picture = GFX_report_event_hitler_croatia_handshake

	is_triggered_only = yes

	option = { #stand together
		name = france.26.a
		ai_chance = {
			factor = 90
			modifier = {
				factor = 0
				is_in_faction_with = GER
			}
		}
		if = {
			limit = {
				NOT = {
					has_guaranteed = YUG
				}
			}
			give_guarantee = YUG
		}
		if = {
			limit = { is_in_faction = no }
			ENG = { add_to_faction = FRA }
			FRA = {
				add_ai_strategy = {
					type = alliance
					id = "ENG"
					value = 200
				}
			}
		}
		add_opinion_modifier = { target = GER modifier = condemn_aggression }
		YUG = {
			country_event = { id = yugoslavia.3 days = 1 }
			add_opinion_modifier = { target = FRA modifier = offered_support }
		}
		GER = {
			country_event = { id = germany.93 hours = 6 } #Generate wargoal
			effect_tooltip = {
				create_wargoal = {
					type = take_state_focus
					target = YUG
					generator = { 480 }
				}
			}
		}
	}

	#Abandon
	option = {
		name = france.26.b
		trigger = { NOT = { is_in_faction_with = ENG } }
		ai_chance = { factor = 10 }
		set_global_flag = FRA_abandoned_yugoslavia
		if = {
			limit = {
				has_guaranteed = YUG
			}
			diplomatic_relation = {
   				country = YUG
   				relation = guarantee
   				active = no
  			}
		}
		YUG = {
			country_event = { id = yugoslavia.3 hours = 6 }
			add_opinion_modifier = { target = FRA modifier = western_betrayal }
		}
	}
}

# France - Syria defects to us
country_event = {
	id = france.28
	title = france.28.t
	desc = france.28.d
	picture = GFX_report_event_worried_french

	is_triggered_only	= yes

	option = {
		name = france.28.a
	}

}

# France - Britain accepts Ally request
country_event = {
	id = france.29
	title = france.29.t
	desc = france.29.d
	picture = GFX_report_event_generic_sign_treaty2

	is_triggered_only = yes

	option = {
		name = france.29.a
	}
}

# France - Britain denies Ally request
country_event = {
	id = france.30
	title = france.30.t
	desc = france.30.d
	picture = GFX_report_event_chamberlain

	is_triggered_only = yes

	option = {
		name = france.30.a
	}
}
