﻿###########################
# D-Day scenario base
###########################

add_namespace = pv_defeatism

country_event = {
	id = pv_defeatism.1
	title = pv_defeatism.1.t
	desc = pv_defeatism.1.d
	#picture = GFX_report_event_hitler_handshake

	is_triggered_only = yes

	option = {
		name = pv_defeatism.1.a
		set_variable = { country_defeatism = 0 }
		FRA = { set_variable = { country_defeatism = 1 } }
	}
}

country_event = {
	id = pv_defeatism.2
	title = pv_defeatism.2.t
	desc = pv_defeatism.2.d
	#picture = GFX_report_event_hitler_handshake

	is_triggered_only = yes

	option = {
		name = pv_defeatism.2.a
		add_ideas = generic_defeatism_1
		add_ideas = generic_defeatism_2
		add_ideas = generic_defeatism_3
		add_ideas = generic_defeatism_4
		add_ideas = generic_defeatism_5

		# remove_dynamic_modifier = {
		# 	modifier = blowing_bridge_dynamic_modifier
		# }
		# ROOT = { remove_dynamic_modifier = { modifier = blowing_bridge_dynamic_modifier } }
		# remove_dynamic_modifier = { modifier = repairing_bridge_dynamic_modifier }
		# remove_dynamic_modifier = {
		# 	modifier = repairing_bridge_dynamic_modifier
		# }
	}
}

add_namespace = dday_province_modifiers

state_event = {
	id = dday_province_modifiers.1
	is_triggered_only = yes
	hidden = yes
	option = {
		remove_allies_movement_operation = yes
	}
}
state_event = {
	id = dday_province_modifiers.2
	is_triggered_only = yes
	hidden = yes
	option = {
		remove_axis_movement_operation = yes
	}
}
state_event = {
	id = dday_province_modifiers.3
	is_triggered_only = yes
	hidden = yes
	option = {
		remove_allies_attack_operation = yes
	}
}
state_event = {
	id = dday_province_modifiers.4
	is_triggered_only = yes
	hidden = yes
	option = {
		remove_axis_attack_operation = yes
	}
}
state_event = {
	id = dday_province_modifiers.5
	is_triggered_only = yes
	hidden = yes
	option = {
		remove_allies_armour_attack_operation = yes
	}
}
state_event = {
	id = dday_province_modifiers.6
	is_triggered_only = yes
	hidden = yes
	option = {
		remove_axis_armour_attack_operation = yes
	}
}

add_namespace = dday_base

# Loading USA troops
country_event = {
	id = dday_base.1
	title = dday_base.1.t
	desc = dday_base.1.d
	#picture = GFX_report_event_hitler_handshake

	is_triggered_only = yes

	option = {
		name = dday_base.1.a
		load_oob = "_OOB_USA_1944_dday"
	}
}

# Loading UK troops
country_event = {
	id = dday_base.2
	title = dday_base.2.t
	desc = dday_base.2.d
	#picture = GFX_report_event_hitler_handshake

	is_triggered_only = yes

	option = {
		name = dday_base.2.a
		load_oob = "_OOB_ENG_1944_dday"
	}
}

# Loading GER troops
country_event = {
	id = dday_base.3
	title = dday_base.3.t
	desc = dday_base.3.d
	#picture = GFX_report_event_hitler_handshake

	is_triggered_only = yes

	option = {
		name = dday_base.3.a
		load_oob = "_OOB_GER_1944_dday"
	}
}

# Operation Dragoon
country_event = {
	id = dday_base.4
	title = dday_base.4.t
	desc = dday_base.4.d
	#picture = GFX_report_event_hitler_handshake

	is_triggered_only = yes

	option = {
		name = dday_base.4.a
		ai_chance = { base = 0 }
		load_oob = "_OOB_Allies_1944_dragoon"
	}

	option = {
		name = dday_base.4.b
		ai_chance = { base = 100 }
		IIF = { load_oob = "_OOB_Allies_1944_dragoon" }
	}
}

# Politics Saver
country_event = {
	id = dday_base.5
	hidden = yes
	is_triggered_only = yes
	immediate = { reset_government = yes }

	option = { }
}
