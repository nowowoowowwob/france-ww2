﻿###########################
# D-Day scenario Global news
###########################

add_namespace = global_news

# V-1 Rocket
news_event = {
	id = global_news.1
	title = global_news.1.t
	desc = global_news.1.d
	picture = GFX_event_v1

	is_triggered_only = yes
	major = yes
	trigger = {
		has_country_flag = wants_medium_news_events
	}

	option = {
		name = global_news.1.a
	}
}

# Saipan
news_event = {
	id = global_news.2
	title = global_news.2.t
	desc = global_news.2.d
	picture = GFX_event_saipan

	is_triggered_only = yes
	major = yes
	trigger = {
		has_country_flag = wants_minor_news_events
	}

	option = {
		name = global_news.2.a
	}
}

# Iceland independence
news_event = {
	id = global_news.3
	title = global_news.3.t
	desc = global_news.3.d
	picture = GFX_event_iceland

	is_triggered_only = yes
	major = yes
	trigger = {
		has_country_flag = wants_medium_news_events
	}

	option = {
		name = global_news.3.a
	}
}

# Operation Baghration
news_event = {
	id = global_news.4
	title = global_news.4.t
	desc = global_news.4.d
	picture = GFX_event_bagration

	is_triggered_only = yes
	major = yes
	trigger = {
		has_country_flag = wants_major_news_events
	}

	option = {
		name = global_news.4.a
	}
}

# Resignation of Hideki Tojo
news_event = {
	id = global_news.5
	title = global_news.5.t
	desc = global_news.5.d
	picture = GFX_event_hideki_tojo_resigns

	is_triggered_only = yes
	major = yes

	option = {
		name = global_news.5.a
		trigger = {
			has_country_flag = wants_medium_news_events
		}
	}
}

#July 20th Plot
news_event = {
	id = global_news.6
	title = global_news.6.t
	desc = global_news.6.d
	picture = x

	is_triggered_only = yes
	major = yes
	trigger = {
		has_country_flag = wants_major_news_events
	}

	option = {
		name = global_news.6.a
		trigger = { is_in_faction_with = ENG }
	}
	option = {
		name = global_news.6.b
		trigger = { NOT = { is_in_faction_with = ENG } }
	}
}

#Guam
news_event = {
	id = global_news.7
	title = global_news.7.t
	desc = global_news.7.d
	picture = x

	is_triggered_only = yes
	major = yes
	trigger = {
		has_country_flag = wants_minor_news_events
	}

	option = {
		name = global_news.7.a
		trigger = { is_in_faction_with = ENG }
	}
	option = {
		name = global_news.7.b
		trigger = { NOT = { is_in_faction_with = ENG } }
	}
}

# McNair
news_event = {
	id = global_news.8
	title = global_news.8.t
	desc = global_news.8.d
	picture = x

	is_triggered_only = yes
	major = yes

	trigger = {
	    has_country_flag = wants_medium_news_events
	}

	option = {
		name = global_news.8.a
	}
}

# Tannenberg Line
news_event = {
	id = global_news.9
	title = global_news.9.t
	desc = global_news.9.d
	picture = x

	is_triggered_only = yes
	major = yes

	trigger = {
	    has_country_flag = wants_major_news_events
	}

	option = {
		name = global_news.9.a
		trigger = { is_in_faction_with = ENG }
	}
	option = {
		name = global_news.9.b
		trigger = { NOT = { is_in_faction_with = ENG } }
	}
}

# Storm damages Mulberry Harbour
news_event = {
	id = global_news.10
	title = global_news.10.t
	desc = global_news.10.d
	picture = GFX_event_mulberry_harbour

	is_triggered_only = yes
	major = yes

	trigger = {
	    has_country_flag = wants_major_news_events
	}

	option = {
		name = global_news.10.a
		trigger = { is_in_faction_with = ENG }
	}
	option = {
		name = global_news.10.b
		trigger = { NOT = { is_in_faction_with = ENG } }
	}
}

# The Warsaw Uprising
news_event = {
	id = global_news.11
	title = global_news.11.t
	desc = global_news.11.d
	picture = x

	is_triggered_only = yes
	major = yes

	trigger = {
	    has_country_flag = wants_major_news_events
	}

	option = {
		name = global_news.11.a
		trigger = { is_in_faction_with = ENG }
	}
	option = {
		name = global_news.11.b
		trigger = { NOT = { is_in_faction_with = ENG } }
	}
}

# Turkey Severs Relations with Germany
news_event = {
	id = global_news.12
	title = global_news.12.t
	desc = global_news.12.d
	picture = x

	is_triggered_only = yes
	major = yes

	trigger = {
	    has_country_flag = wants_minor_news_events
	}

	option = {
		name = global_news.12.a
		trigger = { is_in_faction_with = ENG }
	}
	option = {
		name = global_news.12.b
		trigger = { NOT = { is_in_faction_with = ENG } }
	}
}

# Black Baron Dead!
news_event = {
	id = global_news.13
	title = global_news.13.t
	desc = global_news.13.d
	picture = x

	is_triggered_only = yes
	major = yes

	trigger = {
	    has_country_flag = wants_minor_news_events
	}

	option = {
		name = global_news.13.a
	}
}

# King Michael’s Coup
news_event = {
	id = global_news.14
	title = global_news.14.t
	desc = global_news.14.d
	picture = GFX_event_king_michael_coup

	is_triggered_only = yes
	major = yes

	trigger = {
	    has_country_flag = wants_major_news_events
	}

	option = {
		name = global_news.14.a
		trigger = { is_in_faction_with = ENG }
	}
	option = {
		name = global_news.14.b
		trigger = { NOT = { is_in_faction_with = ENG } }
	}
}

# Anne Frank discovered and deported
news_event = {
	id = global_news.15
	title = global_news.15.t
	desc = global_news.15.d
	picture = x

	is_triggered_only = yes
	major = yes

	trigger = {
	    has_country_flag = wants_minor_news_events
	}

	option = {
		name = global_news.15.a
		trigger = { is_in_faction_with = ENG }
	}
	option = {
		name = global_news.15.b
		trigger = { NOT = { is_in_faction_with = ENG } }
	}
}

# The Wola Massacre
news_event = {
	id = global_news.16
	title = global_news.16.t
	desc = global_news.16.d
	picture = x

	is_triggered_only = yes
	major = yes

	trigger = {
	    has_country_flag = wants_medium_news_events
	}

	option = {
		name = global_news.16.a
		trigger = { is_in_faction_with = ENG }
	}
	option = {
		name = global_news.16.b
		trigger = { NOT = { is_in_faction_with = ENG } }
	}
}

# Red Ball Express
news_event = {
	id = global_news.17
	title = global_news.17.t
	desc = global_news.17.d
	picture = x

	is_triggered_only = yes
	major = yes

	trigger = {
	    has_country_flag = wants_medium_news_events
	}

	option = {
		name = global_news.17.a
		trigger = { is_in_faction_with = ENG }
	}
	option = {
		name = global_news.17.b
		trigger = { NOT = { is_in_faction_with = ENG } }
	}
}

# Slovak National Uprising Begins
news_event = {
	id = global_news.18
	title = global_news.18.t
	desc = global_news.18.d
	picture = x

	is_triggered_only = yes
	major = yes

	trigger = {
	    has_country_flag = wants_major_news_events
	}

	option = {
		name = global_news.18.a
		trigger = { is_in_faction_with = ENG }
	}
	option = {
		name = global_news.18.b
		trigger = { NOT = { is_in_faction_with = ENG } }
	}
}

# Operation Dragoon
news_event = {
	id = global_news.19
	title = global_news.19.t
	desc = global_news.19.d
	picture = x

	is_triggered_only = yes
	major = yes

	trigger = {
	    has_country_flag = wants_major_news_events
	}

	option = {
		name = global_news.19.a
		trigger = { is_in_faction_with = ENG }
	}
	option = {
		name = global_news.19.b
		trigger = { NOT = { is_in_faction_with = ENG } }
	}
}

# The Dutch Hongerwinter
news_event = {
	id = global_news.20
	title = global_news.20.t
	desc = global_news.20.d
	picture = x

	is_triggered_only = yes
	major = yes

	trigger = {
	    has_country_flag = wants_medium_news_events
	}

	option = {
		name = global_news.20.a
		trigger = { is_in_faction_with = ENG }
	}
	option = {
		name = global_news.20.b
		trigger = { NOT = { is_in_faction_with = ENG } }
	}
}

# Bagryanov Government in Bulgaria resigns
news_event = {
	id = global_news.21
	title = global_news.21.t
	desc = global_news.21.d
	picture = x

	is_triggered_only = yes
	major = yes

	trigger = {
	    has_country_flag = wants_medium_news_events
	}

	option = {
		name = global_news.21.a
		trigger = { is_in_faction_with = ENG }
	}
	option = {
		name = global_news.21.b
		trigger = { NOT = { is_in_faction_with = ENG } }
	}
}

# Benelux Customs Union founded
news_event = {
	id = global_news.22
	title = global_news.22.t
	desc = global_news.22.d
	picture = x

	is_triggered_only = yes
	major = yes

	trigger = {
	    has_country_flag = wants_minor_news_events
	}

	option = {
		name = global_news.22.a
		trigger = { is_in_faction_with = ENG }
	}
	option = {
		name = global_news.22.b
		trigger = { NOT = { is_in_faction_with = ENG } }
	}
}

# First V-2 strike on London
news_event = {
	id = global_news.23
	title = global_news.23.t
	desc = global_news.23.d
	picture = x

	is_triggered_only = yes
	major = yes

	trigger = {
	    has_country_flag = wants_medium_news_events
	}

	option = {
		name = global_news.23.a
		trigger = { is_in_faction_with = ENG }
	}
	option = {
		name = global_news.23.b
		trigger = { NOT = { is_in_faction_with = ENG } }
	}
}

# Operation Market Garden
news_event = {
	id = global_news.24
	title = global_news.24.t
	desc = global_news.24.d
	picture = x

	is_triggered_only = yes
	major = yes

	trigger = {
	    has_country_flag = wants_major_news_events
	}

	option = {
		name = global_news.24.a
		trigger = { is_in_faction_with = ENG }
	}
	option = {
		name = global_news.24.b
		trigger = { NOT = { is_in_faction_with = ENG } }
	}
}

# Tannu Tuva annexation
news_event = {
	id = global_news.25
	title = global_news.25.t
	desc = global_news.25.d
	picture = GFX_news_event_006

	is_triggered_only = yes
	major = yes

	trigger = {
	    has_country_flag = wants_medium_news_events
	}

	option = {
		name = global_news.25.a
	}
}

# Annexation of Carpatho Ukraine
news_event = {
	id = global_news.26
	title = global_news.26.t
	desc = global_news.26.desc
	#picture = TODO

	is_triggered_only = yes
	major = yes

	trigger = {
	    has_country_flag = wants_major_news_events
	}

	option = {
		name = global_news.26.a
		trigger = { is_in_faction_with = GER }
	}

	option = {
		name = global_news.26.a
		trigger = { NOT = { is_in_faction_with = GER } }
	}
}

# July 22 Athens Protest
#TODO fix date
news_event = {
	id = global_news.93
	title = global_news.93.t
	desc = global_news.93.d
	picture = GFX_news_event_athens_protest

	# fire_only_once = yes
	is_triggered_only = yes

	# trigger = {
	# 	NOT = { date > 1944.1.1 }
	# 	tag = ROOT
	# 	date > 1943.7.21
	# }

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = global_news.93.a
	}
}

# Boris III Dead
news_event = {
	id = global_news.94
	title = global_news.94.t
	desc = global_news.94.d
	picture = GFX_news_event_boris_dead

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.94.a
	}
}

# Gran Sasso Raid
# TODO trigger
news_event = {
	id = global_news.95
	title = global_news.95.t
	desc = global_news.95.d
	picture = GFX_news_event_gran_sasso_raid

	# fire_only_once = yes

	# trigger = {
	# 	tag = ROOT
	# 	NOT = { date > 1944.1.1 }
	# 	date > 1943.9.12
	# }

	is_triggered_only = yes

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = global_news.95.a
	}
}

# Italian Social Republic Founded!
news_event = {
	id = global_news.96
	title = global_news.96.t
	desc = global_news.96.d
	picture = GFX_news_event_italian_social_republic

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.96.a
	}
}

# Italy Surrenders!
news_event = {
	id = global_news.97
	title = global_news.97.t
	desc = global_news.97.d
	picture = GFX_news_event_italy_surrenders

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.97.a
	}
}

#TODO trigger
# Lebanon Independence Day
news_event = {
	id = global_news.98
	title = global_news.98.t
	desc = global_news.98.d
	picture = GFX_news_event_lebanon_indep

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.98.a
	}
}

#TODO trigger
# Moscow Declarations
news_event = {
	id = global_news.99
	title = global_news.99.t
	desc = global_news.99.d
	picture = GFX_news_event_moscow_declarations

	# fire_only_once = yes

	# trigger = {
	# 	tag = ROOT
	# 	NOT = { date > 1944.1.1 }
	# 	date > 1943.11.11
	# }

	is_triggered_only = yes

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = global_news.99.a
	}
}

# Mussolini Falls From Power
news_event = {
	id = global_news.100
	title = global_news.100.t
	desc = global_news.100.d
	picture = GFX_news_event_mussolini_arrested

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.100.a
	}
}

# TODO trigger
# Ploesti Bombed
news_event = {
	id = global_news.101
	title = global_news.101.t
	desc = global_news.101.d
	picture = GFX_news_event_ploesti_bombed

	is_triggered_only = yes
	# fire_only_once = yes

	# trigger = {
	# 	tag = ROOT
	# 	NOT = { date > 1944.1.1 }
	# 	date > 1943.8.1
	# }

	# mean_time_to_happen = {
	# 	days = 1
	# }

	option = {
		name = global_news.101.a
	}
}

# TODO trigger
# Vatican Bombed
news_event = {
	id = global_news.102
	title = global_news.102.t
	desc = global_news.102.d
	picture = GFX_news_event_vatican_bombed

	is_triggered_only = yes
	# fire_only_once = yes

	# trigger = {
	# 	tag = ROOT
	# 	NOT = { date > 1944.1.1 }
	# 	date > 1943.7.16
	# }

	# mean_time_to_happen = {
	# 	days = 1
	# }

	option = {
		name = global_news.102.a
	}
}

#
news_event = {
	id = global_news.103
	title = global_news.103.t
	desc = global_news.103.d
	picture = GFX_news_event_

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.103.a
	}
}

# Siege of Leningrad Lifted
news_event = {
	id = global_news.106
	title = global_news.106.t
	desc = global_news.106.d
	picture = GFX_news_event_leningrad_siege

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.106.a
	}
}

# Operation Margarethe
news_event = {
	id = global_news.107
	title = global_news.107.t
	desc = global_news.107.d
	picture = GFX_news_event_margarethe

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.107.a
	}
}

# East Prussia Falls
news_event = {
	id = global_news.108
	title = global_news.108.t
	desc = global_news.108.d
	picture = GFX_news_event_east_prussia

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.108.a
	}
}

# Plot on Hitler Fails
news_event = {
	id = global_news.109
	title = global_news.109.t
	desc = global_news.109.d
	picture = GFX_news_event_july_20_plot

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.109.a
	}
}

# Hitler Dead!
news_event = {
	id = global_news.110
	title = global_news.110.t
	desc = global_news.110.d
	picture = GFX_news_event_hitler_dead

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.110.a
	}
}

# Mussolini's Last Speech
news_event = {
	id = global_news.112
	title = global_news.112.t
	desc = global_news.112.d
	picture = GFX_news_event_mussolini_speech

	fire_only_once = yes

	trigger = {
		tag = ROOT
		date > 1944.12.15
		NOT = { date > 1945.1.1 }
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = global_news.112.a
	}
}

# Horthy Negotiates with Soviets
news_event = {
	id = global_news.113
	title = global_news.113.t
	desc = global_news.113.d
	picture = GFX_news_event_horthy_ceasefire

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.113.a
	}
}

# Warsaw Uprising
news_event = {
	id = global_news.114
	title = global_news.114.t
	desc = global_news.114.d
	picture = GFX_news_event_warsaw_uprising

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.114.a
	}
}

# Horthy Removed From Power
news_event = {
	id = global_news.115
	title = global_news.115.t
	desc = global_news.115.d
	picture = GFX_news_event_operation_panzerfaust

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.115.a
	}
}

# Michael Wittmann Dies
news_event = {
	id = global_news.116
	title = global_news.116.t
	desc = global_news.116.d
	picture = GFX_news_event_wittmann_dead

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.116.a
	}
}

# TODO trigger
# Mussolini Dead
news_event = {
	id = global_news.117
	title = global_news.117.t
	desc = global_news.117.d
	picture = GFX_news_event_mussolini_dead

	major = yes
	is_triggered_only = yes

	# fire_only_once = yes

	# trigger = {
	# 	date > 1944.4.4
	# 	NOT = { ITA = { controls_state = 159 } }
	# 	has_global_flag	= RSI_Founded
	# }

	# mean_time_to_happen = {
	# 	days = 1
	# }

	option = {
		name = global_news.117.a
		ITA = {
			add_stability = -0.15
			# Junio becomes the new Fascist leader of Germany
			kill_country_leader = yes
			create_country_leader = {
				name = "Junio Valerio Borghese"
				desc = "POLITICS_JUNIO_VALERIO_BORGHESE_DESC"
				picture = "Portrait_Italy_Juno.dds"
				expire = "1965.1.1"
				ideology = fascism_ideology
				traits = {
					#
				}
			}
		}
	}
}

# Tanu Tuva Annexed
news_event = {
	id = global_news.118
	title = global_news.118.t
	desc = global_news.118.d
	picture = GFX_news_event_tuva

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.118.a
	}
}

# Risto Ryti Resigns
news_event = {
	id = global_news.119
	title = global_news.119.t
	desc = global_news.119.d
	picture = GFX_news_event_ryti_resigns

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.119.a
	}
}

# Lappland War
news_event = {
	id = global_news.120
	title = global_news.120.t
	desc = global_news.120.d
	picture = GFX_news_event_lappland_war

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.120.a
	}
}

# Moscow Armistice
news_event = {
	id = global_news.121
	title = global_news.121.t
	desc = global_news.121.d
	picture = GFX_news_event_moscow_armistice

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.121.a
	}
}

# Hitler Flys
news_event = {
	id = global_news.122
	title = global_news.122.t
	desc = global_news.122.d
	picture = GFX_news_event_hitler_flys

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.122.a
	}
}

# FDR Dead
news_event = {
	id = global_news.123
	title = global_news.123.t
	desc = global_news.123.d
	picture = GFX_news_event_fdr_dead

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.123.a
	}
}

# King Michael's Coup
news_event = {
	id = global_news.124
	title = global_news.124.t
	desc = global_news.124.d
	picture = GFX_news_event_michael_coup

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.124.a
	}
}
#mussolini captured
#need gfx
news_event = {
	id = global_news.125
	title = global_news.125.t
	desc = global_news.125.d

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.126.a
	}
}
#mussolini saved
#needs gfx
news_event = {
	id = global_news.126
	title = global_news.126.t
	desc = global_news.126.d

	major = yes

	is_triggered_only = yes

	option = {
		name = global_news.126.a
	}
}

# Iraqi civil war
news_event = {
	id = global_news.127
	title = global_news.127.t
	desc = global_news.127.d
	picture = x

	is_triggered_only = yes
	major = yes

	trigger = {
	    has_country_flag = wants_major_news_events
	}

	option = {
		name = global_news.127.a
		trigger = { is_in_faction_with = GER }
	}
	option = {
		name = global_news.127.b
		trigger = { NOT = { is_in_faction_with = GER } }
	}
}

# Iraqi uprising (no civil war)
news_event = {
	id = global_news.128
	title = global_news.128.t
	desc = global_news.128.d
	picture = x

	is_triggered_only = yes
	major = yes

	trigger = {
	    has_country_flag = wants_major_news_events
	}

	option = {
		name = global_news.128.a
		trigger = { is_in_faction_with = GER }
	}
	option = {
		name = global_news.128.b
		trigger = { NOT = { is_in_faction_with = GER } }
	}
}

# Iraq joins axis
news_event = {
	id = global_news.129
	title = global_news.129.t
	desc = global_news.129.d
	picture = x

	is_triggered_only = yes
	major = yes

	trigger = {
	    has_country_flag = wants_major_news_events
	}

	option = {
		name = global_news.129.a
		trigger = { is_in_faction_with = GER }
	}
	option = {
		name = global_news.129.b
		trigger = { NOT = { is_in_faction_with = GER } }
	}
}