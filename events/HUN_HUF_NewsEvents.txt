﻿###########################
# Hungarian News Events
###########################

add_namespace = hun_news

# The partition of Yugoslavia
news_event = {
	id = hun_news.1
	title = hun_news.1.t
	desc = hun_news.1.d
	picture = GFX_news_event_009

	major = yes

	is_triggered_only = yes

	immediate = {
		hidden_effect = {
			set_country_flag = HUN_end_of_yugoslavian_partition
			clr_country_flag = HUN_is_partitioning_YUG
			if = {
				limit = {
					YUG = { exists = no }
					tag = HUN
				}
				annex_country = { target = YUG transfer_troops = no }
			}
		}
	}

	#The new borders look nice.
	option = {
		name = hun_news.1.a
		trigger = {
			OR = {
				tag = HUN
				is_in_faction_with = HUN
				has_country_flag = HUN_received_territory_during_partition
				AND = {
					tag = SER
					is_puppet_of = HUN
				}
				AND = {
					tag = BUL
					owns_state = 106 	#Macedonia
				}
				AND = {
					tag = ALB
					owns_state = 105	#Montenegro
				}
				AND = {
					tag = ITA
					OR = {
						owns_state = 105	#Montenegro
						owns_state = 815	#Ljubljana
						owns_state = 103	#Dalmatia
					}
				}
				AND = {
					tag = AUS
					owns_state = 102	#Slovenia
				}
			}
		}
		if = {
			limit = { tag = HUN }
			every_country = {
				limit = { has_country_flag = HUN_received_territory_during_partition5 }
				add_opinion_modifier = { target = ROOT modifier = HUN_given_territory5 }
			}
			every_country = {
				limit = { has_country_flag = HUN_received_territory_during_partition4 }
				add_opinion_modifier = { target = ROOT modifier = HUN_given_territory4 }
			}
			every_country = {
				limit = { has_country_flag = HUN_received_territory_during_partition3 }
				add_opinion_modifier = { target = ROOT modifier = HUN_given_territory3 }
			}
			every_country = {
				limit = { has_country_flag = HUN_received_territory_during_partition2 }
				add_opinion_modifier = { target = ROOT modifier = HUN_given_territory2 }
			}
			every_country = {
				limit = { has_country_flag = HUN_received_territory_during_partition1 }
				add_opinion_modifier = { target = ROOT modifier = HUN_given_territory1 }
			}
		}
	}
	#The peace of Europe is in danger!
	option = {
		name = hun_news.1.b
		trigger = {
			NOT = { is_in_faction_with = HUN }
			OR = {
				AND = {
					ENG = {
						is_faction_leader = yes
						has_government = democratic
					}
					is_in_faction_with = ENG
				}
				tag = YUG
				tag = ROM
				tag = GRE
			}
		}
	}
	#Amazing, how quickly countries appear, and vanish.
	option = {
		name = hun_news.1.c
		trigger = {
			NOT = {
				tag = HUN
				is_puppet_of = HUN
				is_in_faction_with = HUN
				has_country_flag = HUN_received_territory_during_partition
			}
		}
	}
}

# [From.GetAdjective] decree against [HUN.GetAdjective] expansion
news_event = {
	id = hun_news.5
	title = hun_news.5.t
	desc = hun_news.5.d
	picture = GFX_news_event_generic_sign_treaty3

	is_triggered_only = yes

	major = yes

	#Let them try
	option = {
		name = hun_news.5.a
		trigger = {
			OR = {
				tag = HUN
				is_in_faction_with = HUN
			}
		}
	}

	#We must not condone the violation of the peace of Europe
	option = {
		name = hun_news.5.b
		trigger = {
			OR = {
				tag = FROM
				is_in_faction_with = FROM
			}
		}
	}

	#Will this be enough to stop them?
	option = {
		name = hun_news.5.c
		trigger = {
			NOT = {
				tag = HUN
				is_in_faction_with = HUN
				tag = FROM
				is_in_faction_with = FROM
			}
		}
	}
}

# [From.Name] goes to war to stop [HUN.GetAdjective] expansion
news_event = {
	id = hun_news.6
	title = hun_news.6.t
	desc = hun_news.6.d
	picture = GFX_news_event_056

	is_triggered_only = yes

	major = yes

	#We will welcome them with machine guns.
	option = {
		name = hun_news.6.a
		trigger = {
			OR = {
				tag = HUN
				is_in_faction_with = HUN
			}
		}
	}

	#To arms!
	option = {
		name = hun_news.6.b
		trigger = {
			OR = {
				tag = FROM
				is_in_faction_with = FROM
			}
		}
	}

	#War it is then.
	option = {
		name = hun_news.6.c
		trigger = {
			NOT = {
				tag = HUN
				is_in_faction_with = HUN
				tag = FROM
				is_in_faction_with = FROM
			}
		}
	}
}

# Return of the Habsburgs
news_event = {
	id = hun_news.7
	title = hun_news.7.t
	desc = hun_news.7.d
	picture = GFX_news_event_generic_rally_3

	is_triggered_only = yes

	major = yes

	#Long live the new monarch!
	option = {
		name = hun_news.7.a
		trigger = {
			OR = {
				tag = HUN
				is_in_faction_with = HUN
			}
		}
	}

	#That sounds worrying
	option = {
		name = hun_news.7.b
		trigger = {
			OR = {
				tag = ENG
				is_in_faction_with = ENG
				tag = FRA
				is_in_faction_with = FRA
				tag = GER
				is_in_faction_with = GER
			}
		}
	}

	#The Habsburgs are still alive? Curious.
	option = {
		name = hun_news.7.c
		trigger = {
			NOT = {
				tag = HUN
				is_in_faction_with = HUN
				tag = ENG
				is_in_faction_with = ENG
				tag = FRA
				is_in_faction_with = FRA
				tag = GER
				is_in_faction_with = GER
			}
		}
	}
}

# The Adriatic Initiative
news_event = {
	id = hun_news.8
	title = hun_news.8.t
	desc = hun_news.8.d
	picture = GFX_news_event_020

	is_triggered_only = yes

	major = yes

	#Let us hope for long lasting cooperation.
	option = {
		name = hun_news.8.a
		trigger = {
			OR = {
				tag = HUN
				is_in_faction_with = HUN
				tag = ITA
				is_in_faction_with = ITA
			}
		}
	}

	#Where is the Adriatic Sea again?
	option = {
		name = hun_news.8.b
		trigger = {
			NOT = {
				tag = HUN
				is_in_faction_with = HUN
				tag = ITA
				is_in_faction_with = ITA
			}
			NOT = {
				owns_state = 117	#Camponia
				owns_state = 156	#Calabria
				owns_state = 157	#Abruzzo
				owns_state = 161	#Emilia Romagna
				owns_state = 160	#Veneto
				owns_state = 163	#Zara
				owns_state = 103	#Dalmatia
				owns_state = 105	#Montenegro
				owns_state = 44		#Albania
				owns_state = 805	#Northern Epirus
				owns_state = 185	#Epirus
				owns_state = 156	#Calabria
				owns_state = 822	#Ulcinj
				owns_state = 818	#Kotor
				owns_state = 823	#Pag
				owns_state = 811	#Fiume
				owns_state = 736	#Istria
				owns_state = 819	#Primorska
				owns_state = 825	#Trieste
			}
		}
	}

	#This sounds worrying
	option = {
		name = hun_news.8.c
		trigger = {
			NOT = {
				tag = HUN
				is_in_faction_with = HUN
				tag = ITA
				is_in_faction_with = ITA
			}
			OR = {
				owns_state = 117	#Camponia
				owns_state = 156	#Calabria
				owns_state = 157	#Abruzzo
				owns_state = 161	#Emilia Romagna
				owns_state = 160	#Veneto
				owns_state = 163	#Zara
				owns_state = 103	#Dalmatia
				owns_state = 105	#Montenegro
				owns_state = 44		#Albania
				owns_state = 805	#Northern Epirus
				owns_state = 185	#Epirus
				owns_state = 156	#Calabria
				owns_state = 822	#Ulcinj
				owns_state = 818	#Kotor
				owns_state = 823	#Pag
				owns_state = 811	#Fiume
				owns_state = 736	#Istria
				owns_state = 819	#Primorska
				owns_state = 825	#Trieste
			}
		}
	}
}

# The Hungarian-Slovakian Small War
news_event = {
	id = hun_news.9
	title = hun_news.9.t
	desc = {
		text = hun_news.9.d_war
		trigger = {
			OR = {
				HUN = { has_war_with = CZE }
				HUN = { has_war_with = SLO }
			}
		}
	}
	desc = {
		text = hun_news.9.d_peace
		trigger = {
			NOT = {
				HUN = { has_war_with = CZE }
				HUN = { has_war_with = SLO }
			}
		}
	}
	picture = GFX_news_event_056

	major = yes

	is_triggered_only = yes

	#We will triumph!
	option = {
		name = hun_news.9.a
		trigger = {
			OR = {
				tag = HUN
				tag = SLO
				tag = CZE
			}
		}
	}
	#Conflict just won't stop in Eastern Europe
	option = {
		name = hun_news.9.b
		trigger = {
			NOT = {
				tag = HUN
				tag = SLO
				tag = CZE
			}
		}
	}
}


# King Carl Wilhelm I
news_event = {
	id = hun_news.16
	title = hun_news.16.t
	desc = hun_news.16.d
	picture = GFX_news_event_royal_visit

	is_triggered_only = yes

	major = no

	#Long live the new monarch!
	option = {
		name = hun_news.16.a
	}
}

# King Miklós I Horthy
news_event = {
	id = hun_news.17
	title = hun_news.17.t
	desc = hun_news.17.d
	picture = GFX_news_event_royal_visit

	is_triggered_only = yes

	major = no

	#Long live the new monarch!
	option = {
		name = hun_news.17.a
	}
}

# Béla Kun returns
news_event = {
	id = hun_news.18
	title = hun_news.18.t
	desc = hun_news.18.d
	picture = GFX_news_event_generic_rally2

	is_triggered_only = yes

	major = no

	#Huzzah!
	option = {
		name = hun_news.18.a
	}
}

# Underground communism spreading
news_event = {
	id = hun_news.19
	title = hun_news.19.t
	desc = hun_news.19.d
	picture = GFX_news_event_024

	is_triggered_only = yes

	major = no

	#The red menace knows no borders.
	option = {
		name = hun_news.19.a
	}
}

# [HUN.GetAdjective] ambitions
news_event = {
	id = hun_news.20
	title = hun_news.20.t
	desc = hun_news.20.d
	picture = GFX_news_event_military_planning

	is_triggered_only = yes

	major = no

	#We must prepare to make a stand against them.
	option = {
		name = hun_news.20.a

		add_ai_strategy = {
			type = contain
			id = "HUN"
			value = 200
		}
	}
}

# The Charles Line
news_event = {
	id = hun_news.21
	title = hun_news.21.t
	desc = hun_news.21.d
	picture = GFX_news_event_HUN_charles_line

	is_triggered_only = yes

	major = no

	#An accomplishment we must take pride in.
	option = {
		name = hun_news.21.a
	}
}

# 100th Anniversary of the 48' Revolution
news_event = {
	id = hun_news.22
	title = hun_news.22.t
	desc = hun_news.22.d
	picture = GFX_news_event_generic_rally

	is_triggered_only = yes

	trigger = {
		tag = HUN
		is_subject = no
	}

	major = no

	#We broke the chains then, and we must never wear them again!
	option = {
		name = hun_news.22.a
		add_war_support = 0.01
		add_stability = 0.01
	}
}

# Gyula Gömbös dies
news_event = {
	id = hun_news.26
	title = hun_news.26.t
	desc = hun_news.26.d
	picture = GFX_news_event_generic_funeral

	is_triggered_only = yes

	major = no

	#The nation mourns.
	option = {
		name = hun_news.26.a
	}
}

# Slovakia returned to Hungary
news_event = {
	id = hun_news.27
	title = hun_news.27.t
	desc = hun_news.27.d
	picture = GFX_news_event_009

	is_triggered_only = yes

	major = yes

	#An important step towards revising Trianon.
	option = {
		name = hun_news.27.a
		trigger = {
			OR = {
				tag = HUN
				is_in_faction_with = HUN
			}
		}
	}

	#We had no choice but to accept.
	option = {
		name = hun_news.27.b
		trigger = {
			tag = CZE
		}
	}

	#Surely ALL of Slovakia isn't Hungarian?
	option = {
		name = hun_news.27.c
		trigger = {
			NOT = {
				tag = HUN
				is_in_faction_with = HUN
				tag = CZE
			}
		}
	}
}

# Transylvania returned to Hungary
news_event = {
	id = hun_news.28
	title = hun_news.28.t
	desc = hun_news.28.d
	picture = GFX_news_event_009

	is_triggered_only = yes

	major = yes

	#This revision was long overdue.
	option = {
		name = hun_news.28.a
		trigger = {
			OR = {
				tag = HUN
				is_in_faction_with = HUN
			}
		}
	}

	#A temporary setback to be sure.
	option = {
		name = hun_news.28.b
		trigger = {
			tag = ROM
		}
	}

	#They may be going a bit too far with this.
	option = {
		name = hun_news.28.c
		trigger = {
			NOT = {
				tag = HUN
				is_in_faction_with = HUN
				tag = ROM
			}
		}
	}
}

# Construction of the Árpád Line
news_event = {
	id = hun_news.33
	title = hun_news.33.t
	desc = hun_news.33.d
	picture = GFX_news_event_HUN_arpad_line

	is_triggered_only = yes

	major = no

	#We should be safe now.
	option = {
		name = hun_news.33.a
	}
}

# Romania Finishes the Charles Line
news_event = {
	id = hun_news.34
	title = hun_news.34.t
	desc = hun_news.34.d
	picture = GFX_news_event_HUN_charles_line

	is_triggered_only = yes

	major = no

	#Time will tell whether it's worth anything.
	option = {
		name = hun_news.34.a
	}
}

# [FROM.GetName] backs out from the Treaty of Trianon
news_event = {
	id = hun_news.42
	title = hun_news.42.t
	desc = hun_news.42.d
	picture = GFX_news_event_generic_read_write

	is_triggered_only = yes

	major = no

	#Something we can agree on.
	option = {
		name = hun_news.42.a
	}
}

# [FROM.GetName] reinforces the Treaty of Trianon
news_event = {
	id = hun_news.43
	title = hun_news.43.t
	desc = hun_news.43.d
	picture = GFX_news_event_generic_read_write

	is_triggered_only = yes

	major = no

	#They can try.
	option = {
		name = hun_news.43.a
	}
}

# The Treaty of Oradea
news_event = {
	id = hun_news.46
	title = hun_news.46.t
	desc = {
		text = hun_news.46.d_full
		trigger = { HUN = { owns_state = 84 } }	#South Transylvania
	}
	desc = {
		text = hun_news.46.d_partial
		trigger = { NOT = { HUN = { owns_state = 84 } }	}	#South Transylvania
	}
	picture = GFX_news_event_generic_sign_treaty2

	is_triggered_only = yes

	major = yes

	#And no fighting was involved? Curious.
	option = {
		name = hun_news.46.a
		trigger = {
			NOT = {
				tag = POL
				tag = HUN
				tag = ROM
				is_in_faction_with = POL
			}
		}
	}

	#All made possible by the peaceful cooperation of our alliance.
	option = {
		name = hun_news.46.b
		trigger = {
			OR = {
				tag = POL
				AND = {
					is_in_faction_with = POL
					NOT = { tag = HUN }
					NOT = { tag = ROM }
				}
			}
		}
	}

	#A diplomatic success for our country.
	option = {
		name = hun_news.46.c
		trigger = {
			tag = HUN
		}
	}

	#A sacrifice for peace.
	option = {
		name = hun_news.46.e
		trigger = {
			tag = ROM
		}
	}
}

# The Belfast Conference
news_event = {
	id = hun_news.47
	title = hun_news.47.t
	desc = hun_news.47.d
	picture = GFX_news_event_009

	is_triggered_only = yes

	major = yes

	#Now all we need is to end this war.
	option = {
		name = hun_news.47.a
		trigger = {
			tag = HUN
		}
	}

	#Hungary will be a valuable ally after the war.
	option = {
		name = hun_news.47.b
		trigger = {
			OR = {
				tag = SOV
				tag = ENG
				tag = USA
			}
		}
	}

	#They can't expect to gain anything from this.
	option = {
		name = hun_news.47.c
		trigger = {
			tag = GER
		}
	}

	#Surely they don't intend to violate OUR borders?!
	option = {
		name = hun_news.47.e
		trigger = {
			NOT = {
				tag = SOV
				tag = ENG
				tag = GER
				tag = USA
				tag = HUN
			}
			any_neighbor_country = { tag = HUN }
		}
	}

	#We shall see what this means after the war.
	option = {
		name = hun_news.47.f
		trigger = {
			NOT = {
				tag = SOV
				tag = ENG
				tag = GER
				tag = USA
				tag = HUN
				any_neighbor_country = { tag = HUN }
			}
		}
	}
}

# Hungarian Revision Realized
news_event = {
	id = hun_news.48
	title = hun_news.48.t
	desc = hun_news.48.d
	picture = GFX_news_event_019

	is_triggered_only = yes

	major = yes

	#A reward well deserved.
	option = {
		name = hun_news.48.a
		trigger = {
			tag = HUN
		}
	}

	#Better a restored Hungary, than a preserved Nazi Germany.
	option = {
		name = hun_news.48.b
		trigger = {
			OR = {
				tag = SOV
				tag = ENG
				tag = USA
			}
		}
	}

	#This decision is an affront to our sovereignity.
	option = {
		name = hun_news.48.e
		trigger = {
			NOT = {
				tag = SOV
				tag = ENG
				tag = USA
				tag = HUN
			}
			has_country_flag = HUN_lost_territory_after_revision
		}
	}

	#Here they go redrawing the borders of Europe again...
	option = {
		name = hun_news.48.f
		trigger = {
			NOT = {
				tag = SOV
				tag = ENG
				tag = USA
				tag = HUN
				has_country_flag = HUN_lost_territory_after_revision
			}
		}
	}
}

# Hungarian Investment Programs
news_event = {
	id = hun_news.49
	title = hun_news.49.t
	desc = hun_news.49.d
	picture = GFX_news_event_HUN_allied_investment

	is_triggered_only = yes

	major = no

	# An investment in our allies is an investment in our alliance.
	option = {
		name = hun_news.49.a
		trigger = {
			tag = HUN
		}
	}

	# More capital in our country? By all means.
	option = {
		name = hun_news.49.b
		trigger = {
			NOT = { tag = HUN }
		}
	}
}