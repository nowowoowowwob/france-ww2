add_namespace = dday_spr

#FROM sends equipment support
country_event = {
    id = dday_spr.1
    title = dday_spr.1.t
    desc = dday_spr.1.d
    # picture =

    is_triggered_only = yes

    option = {
        name = dday_spr.1.a
        effect_tooltip = {
            add_equipment_to_stockpile = { type = infantry_equipment amount = 8000 producer = FROM }
        }
    }
}

#FROM sends artillery support
country_event = {
    id = dday_spr.2
    title = dday_spr.2.t
    desc = dday_spr.2.d
    # picture =

    is_triggered_only = yes

    option = {
        name = dday_spr.2.a
        effect_tooltip = {
            add_equipment_to_stockpile = { type = artillery_equipment amount = 2000 producer = FROM }
        }
    }
}

#FROM sends light tank support
country_event = {
    id = dday_spr.3
    title = dday_spr.3.t
    desc = dday_spr.3.d
    # picture =

    is_triggered_only = yes

    option = {
        name = dday_spr.3.a
        effect_tooltip = {
            add_equipment_to_stockpile = { type = light_tank_chassis amount = 200 producer = FROM }
        }
    }
}
