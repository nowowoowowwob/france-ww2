﻿###########################
# D-Day scenario Country Events
###########################

add_namespace = germany_country_event

# Maus
country_event = {
	id = germany_country_event.1
	title = germany_country_event.1.t
	desc = germany_country_event.1.d
	#picture = GFX_report_event_hitler_handshake

	is_triggered_only = yes

	option = {
		name = germany_country_event.1.a
		add_ideas = super_heavy_tank_designs

		hidden_effect = {
			#TODO SUPERHEAVY
			load_oob = GER_maus
		}
	}
}

add_namespace = armour_indecision

# Armour indecision, choose Hitler's action
country_event = {
	id = armour_indecision.0
	title = armour_indecision.0.t
	desc = armour_indecision.0.d

	is_triggered_only = yes
	fire_only_once = yes

	option = {
		name = armour_indecision.0.a
		remove_ideas = armour_indecision
		add_ideas = armour_indecision_A
		ai_chance = { factor = 100 }
	}

	option = {
		name = armour_indecision.0.b
		remove_ideas = armour_indecision
		add_ideas = armour_indecision_B
		ai_chance = { factor = 0 }
	}
}

#Choose who (if) wakes Hitler
country_event = {
	id = armour_indecision.1
	title = armour_indecision.1.t
	desc = armour_indecision.1.d

	is_triggered_only = yes

	immediate = {
		set_global_flag = D_Day_has_happened
	}

	#Von Rundstedt
	option = {
		name = armour_indecision.1.a
		ai_chance = {
			factor = 0
			modifier = {
				add = 25
				is_historical_focus_on = no
			}
		}
		custom_effect_tooltip = von_rundstedt_tt
		hidden_effect = {
			random_list = {
				65 = { country_event = { id = armour_indecision.2 } }
				20 = { country_event = { id = armour_indecision.3 } }
				15 = { country_event = { id = armour_indecision.4 } }
			}
		}
	}

	#Goebbels
	option = {
		name = armour_indecision.1.b
		ai_chance = {
			factor = 0
			modifier = {
				add = 25
				is_historical_focus_on = no
			}
		}
		custom_effect_tooltip = goebbels_tt
		hidden_effect = {
			random_list = {
				65 = { country_event = { id = armour_indecision.5 } }
				20 = { country_event = { id = armour_indecision.6 } }
				15 = { country_event = { id = armour_indecision.7 } }
			}
		}
	}

	#Nobody
	option = {
		name = armour_indecision.1.c
		ai_chance = {
			factor = 100
			modifier = {
				add = -50
				is_historical_focus_on = no
			}
		}
		complete_national_focus = GER_morning_of_6_june
		custom_effect_tooltip = dont_wake_tt
	}
}

#Von Rundstedt succeeds
country_event = {
	id = armour_indecision.2
	title = armour_indecision.2.t
	desc = armour_indecision.2.d

	is_triggered_only = yes

	option = {
		name = armour_indecision.2.a
		country_event = armour_indecision.0
		complete_national_focus = GER_morning_of_6_june
	}
}

#Von Rundstedt kinda succeeds
country_event = {
	id = armour_indecision.3
	title = armour_indecision.3.t
	desc = armour_indecision.3.d

	is_triggered_only = yes

	option = {
		name = armour_indecision.3.a
		country_event = armour_indecision.0
		complete_national_focus = GER_morning_of_6_june

		# TODO Demote Von Rundstedt
	}
}

#Von Rundstedt fails
country_event = {
	id = armour_indecision.4
	title = armour_indecision.4.t
	desc = armour_indecision.4.d

	is_triggered_only = yes

	option = {
		name = armour_indecision.4.a
		complete_national_focus = GER_morning_of_6_june

		# TODO Demote Von Rundstedt
	}
}

#Goebbels succeeds
country_event = {
	id = armour_indecision.5
	title = armour_indecision.5.t
	desc = armour_indecision.5.d

	is_triggered_only = yes

	option = {
		name = armour_indecision.5.a
		country_event = armour_indecision.0
		complete_national_focus = GER_morning_of_6_june
	}
}

#Goebbels kinda succeeds
country_event = {
	id = armour_indecision.6
	title = armour_indecision.6.t
	desc = armour_indecision.6.d

	is_triggered_only = yes

	option = {
		name = armour_indecision.6.a
		country_event = armour_indecision.0
		complete_national_focus = GER_morning_of_6_june

		# TODO Remove Goebbels
	}
}

#Goebbels fails
country_event = {
	id = armour_indecision.7
	title = armour_indecision.7.t
	desc = armour_indecision.7.d

	is_triggered_only = yes

	option = {
		name = armour_indecision.7.a
		complete_national_focus = GER_morning_of_6_june
		# TODO Remove Goebbels
	}
}

add_namespace = dday_ger

#Iran in faction
country_event = {
	id = dday_ger.1
	title = dday_ger.1.t
	desc = dday_ger.1.desc

	is_triggered_only = yes

	#Invite them!
	option = {
		name = dday_ger.1.a
		PER = {
			country_event = dday_ger.2
		}
	}

	#We want nothing to do with them!
	option = {
		name = dday_ger.1.b
	}

	#We changed our mind...
	option = {
		name = dday_ger.1.c
		complete_national_focus = GER_secure_iranian_oil
	}
}

#Germany invites us!
country_event = {
	id = dday_ger.2
	title = dday_ger.2.t
	desc = dday_ger.2.desc

	is_triggered_only = yes

	#Yes!
	option = {
		name = dday_ger.2.a

		GER = { add_to_faction = PER }
		if = {
			limit = { GER = { has_war_with = ENG } }
			PER = { add_to_war = { targeted_alliance = GER enemy = ENG hostility_reason = asked_to_join } }
		}

		ai_chance = {
			base = 35
			modifier = {
				PER = { has_opinion = { target = GER value > 50 } }
				add = 30
			}
			modifier = {
				PER = { has_opinion = { target = GER value > 100 } }
				add = 30
			}
		}
	}

	#No!
	option = {
		name = dday_ger.2.b

		#nothing

		ai_chance = {
			base = 60
			modifier = {
				PER = { has_opinion = { target = GER value > 50 } }
				add = -30
			}
		}
	}

	#No so hard we declare war on them!
	option = {
		name = dday_ger.2.b

		ENG = { add_to_faction = PER }
		if = {
			limit = { ENG = { has_war_with = ENG } }
			PER = { add_to_war = { targeted_alliance = ENG enemy = GER hostility_reason = asked_to_join } }
		}

		ai_chance = {
			base = 5
			modifier = {
				PER = { has_opinion = { target = GER value > 50 } }
				factor = 0
			}
		}
	}
}

#Iran response yes
country_event = {
	id = dday_ger.3
	title = dday_ger.3.t
	desc = dday_ger.3.d

	is_triggered_only = yes

	option = {
		name = dday_ger.3.a
	}
}

#Iran response no
country_event = {
	id = dday_ger.4
	title = dday_ger.4.t
	desc = dday_ger.4.d

	is_triggered_only = yes

	option = {
		name = dday_ger.4.a
	}
}

#Iran response very no
country_event = {
	id = dday_ger.5
	title = dday_ger.5.t
	desc = dday_ger.5.d

	is_triggered_only = yes

	option = {
		name = dday_ger.5.a
	}
}

country_event = {
	id = dday_ger.6
	title = dday_ger.6.t
	desc = dday_ger.6.d
	picture = GFX_report_event_midsummer_crisis

	is_triggered_only = yes

	option = { # Diplomatic response
		name = dday_ger.6.a
		ai_chance = { factor = 99 }
		add_political_power = -50
		give_military_access = GER
	}
}

country_event = {
	id = dday_ger.7
	title = dday_ger.7.t
	desc = dday_ger.7.d
	picture = GFX_report_event_german_troops

	is_triggered_only = yes

	option = {
		name = dday_ger.7.a
		ai_chance = { factor = 99 }
		create_wargoal = {
			type = annex_everything
			target = IRQ
		}
	}
}

#Osttruppen
country_event = {
	id = dday_ger.8
	title = dday_ger.8.t
	desc = dday_ger.8.d
	picture = GFX_report_event_german_troops

	is_triggered_only = yes

	option = {
		name = dday_ger.8.a
		set_technology = { osttruppen_battalion = 1 }
	}
}

#Japan calls on Tripartite Pact
country_event = {
	id = dday_ger.9
	title = dday_ger.9.t
	desc = dday_ger.9.d
	picture = GFX_report_event_german_troops

	is_triggered_only = yes

	option = {
		name = dday_ger.9.a

		effect_tooltip = { GER = { declare_war_on = { target = USA type = annex_everything } } }
		news_event = JAP_news_event.30
	}
}

# Wilhelm II dies
# country_event = {
# 	id = wtt_germany.26
# 	title = wtt_germany.26.t
# 	desc = wtt_germany.26.d
# 	picture = GFX_report_event_generic_funeral
#
# 	fire_only_once = yes
#
# 	trigger = {
# 		tag = GER
# 		has_government = neutrality
# 		date > 1939.1.1
# 		has_country_leader = { ruling_only = yes name = "Wilhelm II" }
# 	}
#
# 	mean_time_to_happen = {
# 		days = 1460	# Died in 1941
# 	}
#
# 	#ok I guess
# 	option = {
# 		name = wtt_germany.26.a
# 		kill_country_leader = yes
# 		create_country_leader = {
# 			name = "Wilhelm III"
# 			desc = "POLITICS_WILHELM_III_DESC"
# 			picture = GFX_portrait_ger_wilhelm_iii
# 			expire = "1965.1.1"
# 			ideology = despotism
# 			traits = {
# 			}
# 		}
# 	}
# }


# Operation Felix
country_event = {
	id = dday_ger.101
	title = dday_ger.101.t
	desc = dday_ger.101.d
	picture = GFX_report_event_soldiers_marching

	is_triggered_only = yes

	option = { #start negotiations
		name = dday_ger.101.a
        ai_chance = { factor = 100 }
        SPR = { country_event = { id = dday_ger.102 days = 1 } }
	}

	option = { #force our way through spain
		name = dday_ger.101.b
		trigger = { is_mp = no }
        ai_chance = { factor = 0 }
        declare_war_on = { target = SPR type = annex_everything }
	}
}

# Operation Felix - Spain negotiations
country_event = {
	id = dday_ger.102
	title = dday_ger.102.t
	desc = dday_ger.102.d
	picture = GFX_report_event_soldiers_marching

	is_triggered_only = yes

	option = { #demand morocco, gibraltar and economic support
		name = dday_ger.102.a
        ai_chance = { factor = 60 }
        add_political_power = 50
        add_state_claim = 765
        add_state_claim = 729
        add_state_claim = 734
        GER = { country_event = { id = dday_ger.103 days = 1 } }
        custom_effect_tooltip = dday_ger.102.a.tt
	}

	option = { #morocco and gibraltar
		name = dday_ger.102.b
		trigger = { is_mp = no }
        ai_chance = { factor = 30 }
        add_state_claim = 765
        add_state_claim = 729
        add_state_claim = 734
        GER = { country_event = { id = dday_ger.104 days = 1 } }
        custom_effect_tooltip = dday_ger.102.b.tt
	}

	option = { #just gibraltar
		name = dday_ger.102.c
		trigger = { is_mp = no }
        ai_chance = { factor = 10 }
        add_political_power = -50
        GER = { country_event = { id = dday_ger.105 days = 1 } }
        custom_effect_tooltip = dday_ger.102.c.tt
	}
}

# Operation Felix - German reaction - Spain wants morocco, gibraltar and economic support
country_event = {
	id = dday_ger.103
	title = dday_ger.103.t
	desc = dday_ger.103.d
	picture = GFX_report_event_soldiers_marching

	is_triggered_only = yes

	option = { #sure fine and dandy
		name = dday_ger.103.a
        ai_chance = { factor = 20 }
        set_global_flag = GER_promised_SPR_morocco_gibraltar_economic_support
        SPR = { country_event = { id = dday_ger.106 days = 1 } }
        add_political_power = -50
		add_ideas = GER_economic_support_SPR
		#TODO add spanish food
	}

	option = { #no fuck em, well declare
		name = dday_ger.103.b
		trigger = { is_mp = no }
        ai_chance = { factor = 80 }
        SPR = { country_event = { id = dday_ger.107 days = 1 } }
	}

	option = { #this is too unreasonable, leave it be
		name = dday_ger.103.c
		# trigger = { is_mp = yes }
        ai_chance = { factor = 80 }
        SPR = { country_event = { id = dday_ger.107 days = 1 } }
	}
}

# Operation Felix - German reaction - Spain wants morocco and gibraltar
country_event = {
	id = dday_ger.104
	title = dday_ger.104.t
	desc = dday_ger.104.d
	picture = GFX_report_event_soldiers_marching

	is_triggered_only = yes

	option = { #sure fine and dandy
		name = dday_ger.104.a
        ai_chance = { factor = 60 }
        set_global_flag = GER_promised_SPR_morocco_gibraltar
        SPR = { country_event = { id = dday_ger.106 days = 1 } }
	}

	option = { #no fuck em, well declare
		name = dday_ger.104.b
        ai_chance = { factor = 30 }
        SPR = { country_event = { id = dday_ger.107 days = 1 } }
	}
}

# Operation Felix - German reaction - Spain wants just gibraltar
country_event = {
	id = dday_ger.105
	title = dday_ger.105.t
	desc = dday_ger.105.d
	picture = GFX_report_event_soldiers_marching

	is_triggered_only = yes

	option = { #sure fine and dandy
		name = dday_ger.105.a
        ai_chance = { factor = 90 }
        set_global_flag = GER_promised_SPR_gibraltar
        SPR = { country_event = { id = dday_ger.106 days = 1 } }
	}

	option = { #no fuck em, well declare
		name = dday_ger.105.b
        ai_chance = { factor = 10 }
        SPR = { country_event = { id = dday_ger.107 days = 1 } }
	}
}

# Operation Felix - Spain reaction - Spain given request
country_event = {
	id = dday_ger.106
	title = dday_ger.106.t
	desc = dday_ger.106.d
	picture = GFX_report_event_soldiers_marching

	is_triggered_only = yes

	option = { #nice!
		name = dday_ger.106.a
		set_country_flag = famine_over
		custom_effect_tooltip = famine_wont_increase_tt

        GER = { add_to_faction = SPR }
		GER = { country_event = { id = dday_ger.108 days = 10 } }
	}
}

# Operation Felix - Spain reaction - Spain denied request
country_event = {
	id = dday_ger.107
	title = dday_ger.107.t
	desc = dday_ger.107.d
	picture = GFX_report_event_soldiers_marching

	is_triggered_only = yes

	option = { #angery angery
		name = dday_ger.107.a
		trigger = { is_mp = no }
        hidden_effect = {
            GER = { declare_war_on = { target = SPR type = annex_everything } }
        }
		GER = { country_event = { id = dday_ger.108 days = 10 } }
	}

	option = { #angery angery
		name = dday_ger.107.b
		trigger = { is_mp = yes }
	}
}

# Initiate operation Felix
country_event = {
	id = dday_ger.108
	title = dday_ger.108.t
	desc = dday_ger.108.d
	picture = GFX_report_event_soldiers_marching

	is_triggered_only = yes

	option = { #angery angery
		name = dday_ger.108.a
        GER = { declare_war_on = { target = POR type = annex_everything } }
	}
}


#Africa Stuff

add_namespace = ger_africa

country_event = {
	id = ger_africa.0
	title = ger_africa.0.t
	desc = ger_africa.0.d

	is_triggered_only = yes
	fire_only_once = yes

	trigger = {
		ITA = {
			NOT = {
				controls_state = 745
				controls_state = 747
				controls_state = 757
				controls_state = 749
				controls_state = 874
				controls_state = 744
				controls_state = 428
			}
		}
	}

	option = {
		name = ger_africa.0.a
	}
}
country_event = {
	id = ger_africa.1
	title = ger_africa.1.t
	desc = ger_africa.1.d

	is_triggered_only = yes
	fire_only_once = yes

	trigger = {
		ITA = {
			controls_state = 742
		}
		OR = {
			742 = {
				is_controlled_by_axis = yes
			}
		}

	}

	option = {
		name = ger_africa.1.a
	}
}
country_event = {
	id = ger_africa.2
	title = ger_africa.2.t
	desc = ger_africa.2.d

	is_triggered_only = yes
	fire_only_once = yes

	trigger = {
		ITA = {
			controls_state = 729
		}
		OR = {
			729 = {
				is_controlled_by_axis = yes
			}
		}

	}

	option = {
		name = ger_africa.2.a
	}
}
