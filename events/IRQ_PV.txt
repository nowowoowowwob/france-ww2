﻿###########################
# Iraq event's
###########################

add_namespace = iraq
add_namespace = irq_uprising

#Iraq civil war
country_event = {
    id = irq_uprising.1
    title = irq_uprising.1.t
    desc = irq_uprising.1.desc

    fire_only_once = yes

    trigger = {
        original_tag = IRQ
        date > 1941.3.28
        has_start_date < 1941.6.1
	}
    mean_time_to_happen = {
        days = 1
    }

    option = {
        name = irq_uprising.1.a

        if = {
            limit = {
               check_variable = { IRQ_axis_leaning_variable = 1 }
            }
            set_capital = { state = 568 }
            start_civil_war = {
                ruling_party = neutrality
                ideology = fascism
                size = 0.4
                capital = 625
                states = {
                    625 782 781 780 779 778 777 776 775 774 773 772 178
                }
            }
            hidden_effect = {
                add_popularity = {
                    ideology = neutrality
                    popularity = 0.5
                }
                add_stability = 0.4
                add_war_support = 0.4
                set_equipment_fraction = 0.7
            }
            news_event = global_news.127
        }
        else_if = {
            limit = {
               check_variable = { IRQ_axis_leaning_variable = 2 }
            }
            set_capital = { state = 568 }
            start_civil_war = {
                ruling_party = neutrality
                ideology = fascism
                size = 0.75
                capital = 625
                states = {
                    625 782 781 780 779 778 777 776 775 774 773 772 178 643 37 68 93 431 164
                }
            }
            hidden_effect = {
                add_popularity = {
                    ideology = neutrality
                    popularity = 0.3
                }
                add_stability = 0.2
                add_war_support = 0.2
                set_equipment_fraction = 0.5
            }
            news_event = global_news.127
        }
        else_if = {
            limit = {
               check_variable = { IRQ_axis_leaning_variable = 3 }
            }
            # Support high enough to just flip to axis
            if = {
                limit = {
                    OR = {
                        is_in_faction_with = ENG
                        is_subject_of = ENG
                        has_war_with = GER
                    }
                }

                ENG = { end_puppet = IRQ }

                every_country = {
                    limit = { has_war_with = IRQ }
                    IRQ = { diplomatic_relation = { country = PREV relation = war_relation active = no } }
                    diplomatic_relation = { country = IRQ relation = war_relation active = no }
                }
                leave_faction = yes

                GER = { add_to_faction = IRQ }
                add_to_war = { targeted_alliance = GER enemy = ENG hostility_reason = asked_to_join }
            }
            else = {
                leave_faction = yes
                GER = { add_to_faction = IRQ }
                add_to_war = { targeted_alliance = GER enemy = ENG hostility_reason = asked_to_join }
            }
            news_event = global_news.128
        }
        if = {
            limit = {
                GER = {
                    has_completed_focus = GER_axis_iraq
                }
            }
            GER = { add_to_faction = IRQ }
        }
    }
}

country_event = {
    id = irq_uprising.2
    title = irq_uprising.2.t
    desc = irq_uprising.2.desc

    hidden = yes

    is_triggered_only = yes

    option = {
        name = irq_uprising.2.a

        add_to_variable = { IRQ_axis_leaning_variable = 1 }
    }
}

country_event = {
    id = irq_uprising.3
    title = irq_uprising.3.t
    desc = irq_uprising.3.desc

    hidden = yes

    is_triggered_only = yes

    option = {
        name = irq_uprising.3.a

        subtract_from_variable = { IRQ_axis_leaning_variable = 1 }
    }
}
country_event = {
    id = irq_uprising.4
    title = irq_uprising.4.t
    desc = irq_uprising.4.desc

    hidden = yes

    trigger = {
        fascism > 0.50
    }

    mean_time_to_happen = { days = 1 }
    fire_only_once = yes

    option = {
        name = irq_uprising.4.a

        if = {
            limit = {
               check_variable = { IRQ_axis_leaning_variable = 1 }
            }
            set_variable = { IRQ_axis_leaning_variable = 2 }
        }
        else_if = {
            limit = {
               check_variable = { IRQ_axis_leaning_variable = 2 }
            }
            set_variable = { IRQ_axis_leaning_variable = 3 }
        }
    }
}

country_event = {
    id = irq_uprising.5
    title = irq_uprising.5.t
    desc = irq_uprising.5.desc

    trigger = {
        original_tag = IRQ
        has_global_flag = IRQ_in_axis_promised
        OR = {
            AND = {
                has_government = fascism
                NOT = { has_civil_war = yes }
            }

            all_owned_state = {
                is_controlled_by = GER
            }
        }

        OR = {
            is_in_faction_with = ENG
            is_subject_of = ENG
            has_war_with = GER
        }
    }

    mean_time_to_happen = { days = 1 }
    fire_only_once = yes

    option = {
        name = irq_uprising.5.a

        IRQ = {
            ENG = { end_puppet = IRQ }

            every_country = {
                limit = { has_war_with = IRQ }
                IRQ = { diplomatic_relation = { country = PREV relation = war_relation active = no } }
                diplomatic_relation = { country = IRQ relation = war_relation active = no }
            }
            leave_faction = yes

            GER = { add_to_faction = IRQ }
            add_to_war = { targeted_alliance = GER enemy = ENG hostility_reason = asked_to_join }
        }
        news_event = global_news.129
    }

    option = {
        name = irq_uprising.5.b

        add_political_power = -50
        add_stability = -0.05

        trigger = {
            is_ai = no
        }
    }
}

#
# country_event = {
# 	id = iraq.1
# 	title = iraq.t
# 	desc = iraq.d
#
# 	is_triggered_only = yes
#
# 	option = { # Support democrats
# 		name = iraq.a
# 		trigger = { is_ai = no }
# 		hidden_effect = {
# 			set_capital = { state = 291 }
# 			start_civil_war = {
# 				ruling_party = democratic
# 				ideology = fascism
# 				size = 0.5
# 				capital = 676
# 				states = { 676 675 }
# 			}
# 			random_other_country = {
# 				limit = {
# 					original_tag = IRQ
# 					has_government = fascism
# 				}
# 				load_oob = "IRQ_civil_war_axis"
# 				set_equipment_fraction = 0.7
# 				set_stability = 0.8
# 				add_popularity = { ideology = fascism popularity = 1 }
# 				set_country_flag = IRQ_fascist
# 			}
# 			#news_event = { hours = 6 id = news.63 }
# 			set_global_flag = iraq_civil_war
#
# 			load_oob = "IRQ_civil_war_allies"
# 			set_equipment_fraction = 0.7
# 			set_stability = 0.9
# 			add_popularity = { ideology = democratic popularity = 1 }
# 		}
# 		custom_effect_tooltip = nationalists_chosen
# 	}
# }
