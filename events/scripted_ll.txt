﻿###
	add_namespace = scripted_ll_debug

	country_event = {
		id = scripted_ll_debug.1
		is_triggered_only = yes
		hidden = yes
		option = {
			log = "Testing Array"
			set_country_flag = can_lend_lease_infantry_and_artillery
			set_country_flag = can_lend_lease_tanks
			set_country_flag = can_lend_lease_aircraft
			set_country_flag = can_lend_lease_infantry
			set_country_flag = can_lend_lease_artillery
		}
	}

	country_event = {
		id = scripted_ll_debug.2
		is_triggered_only = yes
		hidden = yes
		option = {
			add_to_variable = { jet_strategic_bomber_level_@var:receiver = 5 }
		}
	}

	country_event = {
		id = scripted_ll_debug.3
		is_triggered_only = yes
		hidden = yes
		option = {
			log = "Receiver: [?receiver.GetName]"
			log = "Var level: [?jet_strategic_bomber_level_ITA]"
		}
	}

	country_event = {
		id = scripted_ll_debug.4
		is_triggered_only = yes
		hidden = yes
		option = {
			set_variables_lend_lease_amount = yes
		}
	}

	country_event = {
		id = scripted_ll_debug.5
		is_triggered_only = yes
		hidden = yes
		option = {
			GER = { send_monthly_lend_lease_effect = yes }
		}
	}

	country_event = {
		id = scripted_ll_debug.6
		is_triggered_only = yes
		hidden = yes
		option = {
			clear_array = GER.lendlease_receiver_array
            add_to_array = { GER.lendlease_receiver_array = ITA }
            add_to_array = { GER.lendlease_receiver_array = POL }
            add_to_array = { GER.lendlease_receiver_array = ENG }
            add_to_array = { GER.lendlease_receiver_array = FRA }
            add_to_array = { GER.lendlease_receiver_array = BEL }
            add_to_array = { GER.lendlease_receiver_array = HOL }
            add_to_array = { GER.lendlease_receiver_array = LUX }
			add_to_array = { GER.lendlease_receiver_array = SWI }
		}
	}

	country_event = {
		id = scripted_ll_debug.7
		is_triggered_only = yes
		hidden = yes
		option = {
			for_each_loop = {
				array = ROOT.lendlease_receiver_array
				index = index_var

				log = "[?var:ROOT.lendlease_receiver_array^index_var.GetName]"
			}
		}
	}
###

###
	add_namespace = scripted_ll

	#Lend Lease report
	country_event = {
		id = scripted_ll.1
		title = scripted_ll.1.t
		desc = scripted_ll.1.d

		is_triggered_only = yes

		trigger = {
			is_lend_leasing_anything_to_receiver = yes
		}

		option = {
			name = scripted_ll.1.a
		}
	}