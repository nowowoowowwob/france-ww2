state={
	id=1071
	name="STATE_1071"
	history={
		owner = USA
		add_core_of = USA
		buildings = {
			infrastructure = 4
			arms_factory = 1
			industrial_complex = 4
			air_base = 6
			anti_air_building = 0
				dockyard = 8
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 4
				arms_factory = 1
				industrial_complex = 6
				air_base = 6
				anti_air_building = 0
				dockyard = 8
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 4
				arms_factory = 3
				industrial_complex = 3
				air_base = 6
				anti_air_building = 0
				dockyard = 8
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 4
				arms_factory = 4
				industrial_complex = 3
				air_base = 6
				anti_air_building = 0
				dockyard = 8
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 4
				arms_factory = 4
				industrial_complex = 2
				air_base = 6
				anti_air_building = 0
				dockyard = 8
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 4
				arms_factory = 5
				industrial_complex = 2
				air_base = 6
				anti_air_building = 0
				dockyard = 8
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}


		1944.12.14 = {
			buildings = {
				infrastructure = 4
				arms_factory = 5
				industrial_complex = 1
				air_base = 6
				anti_air_building = 0
				dockyard = 8
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 4
				arms_factory = 6
				industrial_complex = 1
				air_base = 6
				anti_air_building = 0
				dockyard = 8
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		victory_points = { 8615 5 } #1060
		victory_points = { 8623 5 } #1085
		victory_points = { 8628 5 } #1087
		victory_points = { 8635 5 } #1089
		victory_points = { 8644 5 } #1093
		victory_points = { 8653 5 } #802
		victory_points = { 8637 5 } #1090
		victory_points = { 8626 5 } #1086
		victory_points = { 8621 5 } #1084
		victory_points = { 8643 5 } #1091
		victory_points = { 8627 5 } #1069
		victory_points = { 8625 5 } #1067
		victory_points = { 8618 5 } #1062
		victory_points = { 8619 5 } #1063
		victory_points = { 8620 5 } #1064
		victory_points = { 8622 5 } #1065
		victory_points = { 8624 5 } #1066
		victory_points = { 8630 5 } #1068
		victory_points = { 8632 5 } #1070
		victory_points = { 8633 5 } #1075
		victory_points = { 8639 5 } #1077
		victory_points = { 8638 5 } #1073
		victory_points = { 8652 5 } #803
	}
	provinces={
		8634
	}
	manpower = 1618394

	state_category = metropolis
}
