
state={
	id=533
	name="STATE_533"

	history={
		buildings = {
			infrastructure = 2
			arms_factory = 1
			industrial_complex = 3
			air_base = 2
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 2
				arms_factory = 2
				industrial_complex = 3
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 2
				arms_factory = 2
				industrial_complex = 4
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 2
				arms_factory = 2
				industrial_complex = 4
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 2
				arms_factory = 2
				industrial_complex = 4
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 2
				arms_factory = 2
				industrial_complex = 4
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 2
				arms_factory = 2
				industrial_complex = 4
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 2
				arms_factory = 2
				industrial_complex = 4
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = BUL
		add_core_of = BUL
        victory_points = { 2579 10 }
        victory_points = { 4585 3 }
        victory_points = { 6610 4 }
        victory_points = { 6628 3 }
        victory_points = { 6628 3 }
        victory_points = { 9459 5 }
	}

	provinces={
		118 479 732 1455 2292 2474 2579 2871 3037 4585 6544 6610 6628 6642 6683 9546 9549 9550 9551 9751 9754 9758 9759 9761
	}
	manpower=1821100
	buildings_max_level_factor=1.000
	state_category=town
}
