
state={
	id=725
	name="STATE_725"
	resources={
		steel=28 # was: 36
	}
	history={
		buildings = {
			infrastructure = 2
			arms_factory = 0
			industrial_complex = 0
			air_base = 5
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 0
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 0
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 0
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 0
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 1
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 1
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 1
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = FRA
		add_core_of = ALG
		1941.1.1 = {
			owner = EFR
			controller = EFR

		}
		1942.11.19 = {
			owner = FRA
			controller = FRA

		}
		buildings = {
			7174 = { naval_base = 4 }
		}
        victory_points = { 410 3 }
        victory_points = { 2344 3 }
        victory_points = { 2417 1 }
        victory_points = { 2786 5 }
        victory_points = { 3582 1 }
        victory_points = { 4700 1 }
        victory_points = { 7164 1 }
        victory_points = { 7172 2 }
        victory_points = { 7174 3 }
        victory_points = { 7192 10 }
        victory_points = { 7233 2 }
        victory_points = { 7241 1 }
	}

	provinces={
		356 410 466 503 563 622 1086 1190 1207 1304 1514 1904 1934 2004 2066 2089 2092 2153 2253 2344 2417 2486 2563 2620 2762 2778 2786 2938 2945 2992 3009 3059 3162 3269 3294 3356 3371 3478 3528 3582 3587 3760 3781 3782 3805 4066 4368 4385 4387 4593 4700 4860 7157 7164 7166 7172 7174 7180 7190 7192 7198 7200 7204 7208 7215 7218 7221 7229 7230 7232 7233 7238 7241 7246 7249 7250 7251 7261 7276 7279 7292 7509 7515
	}
	manpower=3089944
    state_category = rural
	buildings_max_level_factor=1.000
}
