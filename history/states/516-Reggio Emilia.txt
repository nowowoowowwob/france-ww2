
state={
	id=516
	name="STATE_516"

	history={
		buildings = {
			445 = {
				naval_base = 2

			}

		}
		buildings = {
			infrastructure = 3
			arms_factory = 1
			industrial_complex = 2
			air_base = 0
			anti_air_building = 0
				dockyard = 1
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 4
				industrial_complex = 3
				air_base = 0
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = ITA
		add_core_of = ITA
		1944.6.6 = {
			add_core_of = KIT

		}
		1944.12.16 = {
			KIT = {
				set_province_controller = 1598
				set_province_controller = 2071

			}

		}
		1945.4.29 = {
			owner = KIT

		}
		victory_points = {
			445 10
		}
		victory_points = {
			2071 10
		}
		victory_points = {
			6463 20
		}
		victory_points = {
			6470 20
		}
		victory_points = {
			6493 20
		}
		victory_points = {
			6597 5
		}
		victory_points = {
			6598 10
		}

	}

	provinces={
		428 445 852 1471 1598 2071 2134 2208 2458 2695 3045 3216 3561 3853 4033 4162 4224 4394 4624 6463 6470 6477 6488 6493 6497 6507 6526 6527 6530 6540 6542 6546 6547 6548 6554 6556 6588 6597 6598 6614 8102 8111 8114 8117 11335 11401
	}
	manpower=1519199
	buildings_max_level_factor=1.000
	state_category=large_city
	local_supplies=0.000
}
