
state={
	id=55
	name="STATE_55"
	resources={
		steel=60.000
		chromium=12.000
		tungsten=55.000
	}

	history={
		buildings = {
			infrastructure = 2
			arms_factory = 0
			industrial_complex = 0
			air_base = 1
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 0
				air_base = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 0
				air_base = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 0
				air_base = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 0
				air_base = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 0
				air_base = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 0
				air_base = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 0
				air_base = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = SOV
		add_core_of = SOV

		victory_points = { 10993 1 } #55
		victory_points = { 4996 1 } #55
		victory_points = { 11023 1 } #55
		victory_points = { 2136 3 } #55
		victory_points = { 10990 1 } #55
		victory_points = { 11001 1 } #55
		victory_points = { 11033 1 } #55
		victory_points = { 5072 1 } #55
		victory_points = { 11066 1 } #55
		victory_points = { 11049 1 } #55
		victory_points = { 504 1 } #55


	}

	provinces={
		86 186 204 357 362 504 670 764 788 1002 1028 1717 2136 2838 4996 5039 5072 5101 10990 10993 10995 10996 10999 11001 11011 11015 11020 11022 11023 11025 11026 11032 11033 11036 11038 11043 11044 11049 11053 11058 11059 11066
	}
	manpower=1601000
	buildings_max_level_factor=1.000
	state_category=rural
	local_supplies=0.000
}
