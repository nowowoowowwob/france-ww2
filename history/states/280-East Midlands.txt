state={
	id=280
	name="STATE_280"
	provinces={
		659 1059 1256 2515 4710 5353
	}
	resources={
		steel=20 # was: 20
	}
	history={
		buildings = {
			infrastructure = 2
			arms_factory = 0
			industrial_complex = 2
			air_base = 5
			anti_air_building = 0
				dockyard = 2
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 3
				air_base = 5
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 4
				air_base = 5
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 4
				air_base = 5
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 4
				air_base = 5
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 4
				air_base = 5
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 4
				air_base = 5
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 4
				air_base = 5
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = ENG
		add_core_of = ENG
		buildings = {
			4710 = { naval_base = 1 }
		}
        victory_points = { 4710 20 }
        victory_points = { 1256 15 }
        victory_points = { 1059 5 }
	}
	manpower=2212021
    state_category = large_city
	buildings_max_level_factor=1.000
}
