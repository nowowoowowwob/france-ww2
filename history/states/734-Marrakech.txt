
state={
	id=734
	name="STATE_734"

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 0
			industrial_complex = 0
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = FRA
		add_core_of = MOR
		1941.1.1 = {
			owner = EFR
			controller = EFR

		}
		1942.11.19 = {
			owner = FRA
			controller = FRA

		}

        victory_points = { 1834 1 }
        victory_points = { 2008 20 }
        victory_points = { 2624 3 }
        victory_points = { 2933 1 }
        victory_points = { 7301 5 }
        victory_points = { 7324 2 }
	}

	provinces={
		18 451 1024 1615 1834 1836 2008 2009 2048 2054 2099 2111 2268 2374 2613 2624 2701 2861 2923 2933 2997 3084 3395 3458 3579 3593 3686 3741 3751 3998 4423 4586 4747 4832 4878 7289 7300 7301 7309 7313 7315 7316 7318 7324 7327 7329 7336 7344 7350 7369 7493 7495 7496 7497 7498 7499 7501 8050 8047 8051
	}
	manpower=1865552
	buildings_max_level_factor=1.000
	state_category=rural
}
