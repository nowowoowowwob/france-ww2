
state={
	id=179
	name="STATE_179"
	resources={
		steel=24.000
	}

	history={
		buildings = {
			infrastructure = 2
			arms_factory = 1
			industrial_complex = 1
			air_base = 3
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 1
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 2
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 2
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 2
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 2
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 2
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 2
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = LAT
		add_core_of = LAT
		1941.1.1 = {
			owner = SOV
			controller = SOV

		}
		1942.11.19 = {
			owner = GOS
			controller = GOS
			add_core_of = GOS
		}

		1944.12.16 = {
			controller = SOV
		}
        victory_points = { 3737 3 }
        victory_points = { 4285 5 }
        victory_points = { 10743 1 }
        victory_points = { 10776 1 }
        victory_points = { 10806 2 }
	}

	provinces={
		640 1497 1703 2459 2510 2976 3737 3941 4285 5179 5185 10704 10727 10743 10745 10761 10762 10775 10776 10798 10801 10806 10815 10835
	}
	manpower=412610
	buildings_max_level_factor=1.000
	state_category=town
	local_supplies=0.000
}
