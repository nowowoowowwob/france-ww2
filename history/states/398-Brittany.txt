
state={
	id=398
	name="STATE_398"

	history={
		buildings = {
			infrastructure = 4
			arms_factory = 1
			industrial_complex = 4
			air_base = 5
			anti_air_building = 0
				dockyard = 2
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 4
				arms_factory = 2
				industrial_complex = 6
				air_base = 5
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 4
				arms_factory = 3
				industrial_complex = 8
				air_base = 5
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 4
				arms_factory = 3
				industrial_complex = 8
				air_base = 5
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 4
				arms_factory = 3
				industrial_complex = 8
				air_base = 5
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 4
				arms_factory = 3
				industrial_complex = 8
				air_base = 5
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 4
				arms_factory = 3
				industrial_complex = 8
				air_base = 5
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 4
				arms_factory = 3
				industrial_complex = 8
				air_base = 5
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = FRA
		add_core_of = FRA
		1941.1.1 = {
			owner = GFR
			controller = GFR
			add_core_of = GFR
			add_core_of = EFR

		}
		1943.1.1 = {
			remove_core_of = EFR

		}
		1944.12.16 = {
			owner = FRA
			controller = FRA
			GFR = {
				set_province_controller = 1689

			}

		}
		buildings = {
			3601 = {
				naval_base = 7

			}
			1689 = {
				naval_base = 3

			}

		}
		victory_points = {
			1689 5
		}
		victory_points = {
			3351 5
		}
		victory_points = {
			3601 20
		}
		victory_points = {
			3609 2
		}
		victory_points = {
			4343 5
		}
		victory_points = {
			4957 3
		}
		victory_points = {
			5731 3
		}
		victory_points = {
			5780 3
		}
		victory_points = {
			5844 1
		}
		victory_points = {
			5869 2
		}
		victory_points = {
			9970 10
		}

	}

	provinces={
		1040 1407 1629 1689 1727 1767 2074 2311 2493 3304 3351 3388 3601 3609 3652 3717 3764 3856 3858 4236 4256 4319 4343 4348 4450 4466 4496 4611 4622 4664 4830 5731 5734 5744 5750 5751 5759 5763 5769 5772 5780 5790 5796 5803 5807 5808 5810 5816 5818 5829 5832 5836 5839 5844 5845 5849 5850 5853 5865 5866 5869 5875 5878 5896 5898 5901 5902 5908 5916 5931 5940 5957 5958 5966 7643 8268 8274 8286 8298 8299 9970 11216
	}
	manpower=2396600
	buildings_max_level_factor=1.000
	state_category=large_city
	local_supplies=0.000
}
