
state={
	id=620
	name="STATE_620"

	history={
		victory_points = { 8863 5 }
		buildings = {
			infrastructure = 3
			arms_factory = 0
			industrial_complex = 1
			air_base = 3
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 2
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 2
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 2
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = SPR
		add_core_of = SPR
		buildings = {
			8863 = {
				naval_base = 3

			}

		}
		victory_points = { 8861 2 }
        victory_points = { 8863 20 }
        victory_points = { 8901 1 }
        victory_points = { 8910 10 }
        victory_points = { 8949 2 }
        victory_points = { 8956 1 }
        victory_points = { 9008 1 }
        victory_points = { 8913 1 }
	}

	provinces={
		8842 8854 8861 8863 8874 8881 8886 8900 8901 8904 8910 8913 8926 8929 8935 8949 8950 8956 8962 8980 9008 9339 9343 9258
	}
	manpower=1679821
	buildings_max_level_factor=1.000
	state_category=city
}
