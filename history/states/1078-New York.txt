state={
	id=1078
	name="STATE_1078"
	resources={
		steel=13 # was: 24.000
	}
	history={
		owner = USA
		add_core_of = USA
		buildings = {
			infrastructure = 4
			arms_factory = 3
			industrial_complex = 20
			air_base = 10
			anti_air_building = 0
				dockyard = 5
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 4
				arms_factory = 4
				industrial_complex = 26
				air_base = 10
				anti_air_building = 0
				dockyard = 5
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 4
				arms_factory = 14
				industrial_complex = 20
				air_base = 10
				anti_air_building = 0
				dockyard = 5
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 4
				arms_factory = 16
				industrial_complex = 14
				air_base = 10
				anti_air_building = 0
				dockyard = 5
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 4
				arms_factory = 20
				industrial_complex = 11
				air_base = 10
				anti_air_building = 0
				dockyard = 5
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 4
				arms_factory = 22
				industrial_complex = 9
				air_base = 10
				anti_air_building = 0
				dockyard = 5
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 4
				arms_factory = 24
				industrial_complex = 7
				air_base = 10
				anti_air_building = 0
				dockyard = 5
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 4
				arms_factory = 26
				industrial_complex = 5
				air_base = 10
				anti_air_building = 0
				dockyard = 5
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
	}
	provinces={
		8642
	}
	state_category = megalopolis
	manpower=12588061
	buildings_max_level_factor=1.000
}
