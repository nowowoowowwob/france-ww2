
state={
	id=286
	name="STATE_286"
	resources={
		oil=3.000
		tungsten=44.000
	}

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 0
			industrial_complex = 1
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 4
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 4
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 4
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 7
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 7
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 7
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 7
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = GER
		add_core_of = GER
		victory_points = {
			1095 5 
		}
		victory_points = {
			1956 3 
		}
		victory_points = {
			3186 30 
		}
		victory_points = {
			3361 4 
		}
		victory_points = {
			3857 10 
		}
		victory_points = {
			4146 10 
		}
		victory_points = {
			4360 10 
		}
		victory_points = {
			4557 5 
		}
		victory_points = {
			5344 5 
		}
		victory_points = {
			5387 3 
		}
		victory_points = {
			5430 3 
		}
		victory_points = {
			5492 10 
		}

	}

	provinces={
		437 1095 1150 1193 1363 1757 1875 1890 1956 2059 2192 2462 2676 3186 3278 3361 3487 3532 3738 3857 4090 4146 4249 4360 4418 4557 4577 4675 4811 4929 5009 5313 5328 5330 5331 5335 5339 5341 5344 5354 5361 5364 5370 5376 5385 5387 5396 5400 5409 5430 5431 5444 5458 5459 5475 5481 5492 8390 8403 8426 8448 8485 8494 11381 
	}
	manpower=1015231
	buildings_max_level_factor=1.000
	state_category=large_city
	local_supplies=0.000
}
