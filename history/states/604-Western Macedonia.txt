
state={
	id=604
	name="STATE_604"

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 1
			industrial_complex = 1
			air_base = 0
			anti_air_building = 0
				dockyard = 1
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = GRE
		add_core_of = GRE
		add_core_of = BUL
		1941.4.1 = {
			owner = GER
			controller = GER

		}

		1944.12.16 = {
			owner = GRE
			controller = GRE
		}
		buildings = {
			4274 = {
				naval_base = 4

			}

		}
		victory_points = { 2788 5 }
        victory_points = { 4274 20 }
        victory_points = { 2312 1 }
		victory_points = { 6797 2 }
	}

	provinces={
		157 505 719 942 1083 1678 1938 2312 2348 2788 4054 4274 4827 6794 6797 6801 6833 6834 9722 9726 9728 9729
	}
	manpower=755700
	buildings_max_level_factor=1.000
	state_category=large_town
}
