
state={
	id=226
	name="STATE_226"
	provinces={
		388 1796 3869 4313 5216 5228 5231 7616
	}
	resources={
		steel=25 # was: 40
	}
	history={
		buildings = {
			infrastructure = 4
			arms_factory = 2
			industrial_complex = 6
			air_base = 4
			anti_air_building = 0
				dockyard = 2
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 4
				arms_factory = 2
				industrial_complex = 7
				air_base = 4
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 4
				arms_factory = 4
				industrial_complex = 7
				air_base = 4
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 4
				arms_factory = 4
				industrial_complex = 7
				air_base = 4
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 4
				arms_factory = 4
				industrial_complex = 7
				air_base = 4
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 4
				arms_factory = 5
				industrial_complex = 7
				air_base = 4
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 4
				arms_factory = 5
				industrial_complex = 7
				air_base = 4
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 4
				arms_factory = 5
				industrial_complex = 8
				air_base = 4
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = DEN
		add_core_of = DEN
		buildings = {
			1796 = { naval_base = 5 }
			7616 = { naval_base = 2 }
		}
        victory_points = { 1796 30 }
        victory_points = { 7616 1 }
        victory_points = { 5228 2 }
        victory_points = { 4313 3 }
	}
	manpower=2034114
    state_category = large_city
	buildings_max_level_factor=1.000
}
