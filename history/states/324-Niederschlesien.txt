
state={
	id=324
	name="STATE_324"
	resources={
		steel=80.000
		aluminium=26.000
	}

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 1
			industrial_complex = 1
			air_base = 6
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 3
				air_base = 6
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 3
				air_base = 6
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 3
				air_base = 6
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 3
				air_base = 6
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 3
				air_base = 6
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 3
				air_base = 6
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 3
				air_base = 6
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = GER
		add_core_of = GER
		1945.3.20 = {
			SOV = {
				set_province_controller = 11211
				set_province_controller = 11210
				set_province_controller = 3571
				set_province_controller = 8400
				set_province_controller = 2041
				set_province_controller = 5505
				set_province_controller = 5476
				set_province_controller = 1991
				set_province_controller = 5490
				set_province_controller = 4762
				set_province_controller = 5462
				set_province_controller = 5439
				set_province_controller = 4370
				set_province_controller = 5480
				set_province_controller = 1185
				set_province_controller = 5457
				set_province_controller = 5438
				set_province_controller = 8386
				set_province_controller = 11215
				set_province_controller = 11374

			}

		}
		victory_points = {
			2041 3
		}
		victory_points = {
			5548 20
		}
		victory_points = {
			4251 2
		}
		victory_points = {
			4762 5
		}
		victory_points = {
			5438 4
		}
		victory_points = {
			5480 4
		}
		victory_points = {
			5490 3
		}
		victory_points = {
			5574 2
		}
		victory_points = {
			5600 2
		}
		victory_points = {
			5583 1
		}

	}

	provinces={
		727 790 1185 1221 1268 1701 1991 2003 2041 2727 3571 3898 4055 4251 4370 4762 5438 5439 5457 5462 5476 5480 5490 5505 5511 5533 5544 5547 5548 5583 5600 8378 8379 8386 8400 11209 11210 11211 11215 11374
	}
	manpower=2136908
	buildings_max_level_factor=1.000
	state_category=large_city
	local_supplies=0.000
}
