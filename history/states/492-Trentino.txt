
state={
	id=492
	name="STATE_492"
	resources={
		aluminium=22.000
	}

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 0
			industrial_complex = 0
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}


		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = ITA
		add_core_of = ITA
		1944.6.6 = {
			owner = GER
			controller = GER
			add_core_of = KIT

		}

        victory_points = { 3347 5 }
	}

	provinces={
		1859 1937 2090 3336 3347 3625 3885 4997 6239 6245 6255 6256 6269 6275 6298 6338 10017
	}
	manpower=370739
	buildings_max_level_factor=1.000
	state_category=rural
}
