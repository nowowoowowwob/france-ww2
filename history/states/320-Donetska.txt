
state={
	id=320
	name="STATE_320"

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 0
			industrial_complex = 0
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = SOV
		add_core_of = SOV

		victory_points = { 10267 10 } #320

		1942.11.19 = {
			owner = GMO
			controller = GMO
			add_core_of = GMO
		}
		1944.6.6 = {
			owner = SOV

		}

		victory_points = { 2826 3 } #320
		victory_points = { 10219 1 } #320
		victory_points = { 5558 10 } #320
		victory_points = { 4175 3 } #320
		victory_points = { 10265 1 } #320
		victory_points = { 331 3 } #320
	}

	provinces={
		43 331 2826 4175 5558 10219 10225 10230 10242 10260 10265 10267 10294
	}
	manpower=3500000
	buildings_max_level_factor=1.000
	state_category=large_city
	local_supplies=0.000
}
