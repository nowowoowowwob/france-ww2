
state={
	id=176
	name="STATE_176"

	history={
		buildings = {
			infrastructure = 2
			arms_factory = 0
			industrial_complex = 0
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = SOV
		add_core_of = SOV

		victory_points = { 4606 10 } #176
		victory_points = { 10668 1 } #176
		victory_points = { 471 1 } #176
		victory_points = { 10608 3 } #176
		victory_points = { 317 1 } #176
		victory_points = { 10627 1 } #176
		victory_points = { 10653 1 } #176
		victory_points = { 10746 1 } #176
		victory_points = { 10742 1 } #176
		victory_points = { 10800 1 } #176
		victory_points = { 10757 1 } #176
	}

	provinces={
		317 443 471 783 844 4606 10608 10614 10627 10630 10636 10640 10653 10668 10669 10673 10674 10685 10690 10693 10701 10711 10715 10726 10731 10732 10740 10741 10742 10746 10753 10754 10757 10767 10777 10786 10800 10807
	}
	manpower=1648000
	buildings_max_level_factor=1.000
	state_category=rural
	local_supplies=0.000
}
