
state={
	id=491
	name="STATE_491"
	resources={
		chromium=20.000
		tungsten=24.000
	}

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 1
			industrial_complex = 3
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = YUG
		add_core_of = YUG
		add_core_of = CRO
		1941.4.1 = {
			owner = CRO
			controller = CRO

		}
		victory_points = {
			2596 2 
		}
		victory_points = {
			3323 3 
		}
		victory_points = {
			4798 3 
		}
		victory_points = {
			6285 20 
		}
		victory_points = {
			6296 5 
		}
		victory_points = {
			6326 3 
		}
		victory_points = {
			6361 5 
		}

	}

	provinces={
		394 396 1653 1688 1749 2080 2188 2439 2558 2569 2900 3139 3323 3545 3588 4025 4048 4058 4582 4637 4798 4979 6259 6264 6285 6294 6296 6326 6347 6352 6361 6375 6383 6410 9783 9791 11346 
	}
	manpower=1274640
	buildings_max_level_factor=1.000
	state_category=large_town
	local_supplies=0.000
}
