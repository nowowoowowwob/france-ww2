
state={
	id=528
	name="STATE_528"
	resources={
		aluminium=15.000
	}

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 0
			industrial_complex = 1
			air_base = 2
			anti_air_building = 0
				dockyard = 1
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 3
				air_base = 2
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 2
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 2
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 2
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 2
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 2
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 2
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = ITA
		add_core_of = ITA
		1944.6.6 = {
			add_core_of = KIT

		}
		1944.12.16 = {
			owner = KIT
			ITA = {
				set_province_controller = 6529
				set_province_controller = 6536
				set_province_controller = 6522
				set_province_controller = 4725
				set_province_controller = 4756
				set_province_controller = 3991
			}
		}
		buildings = {
			2753 = {
				naval_base = 4

			}

		}

        victory_points = { 2322 5 }
        victory_points = { 2612 10 }
        victory_points = { 2753 15 }
        victory_points = { 3016 5 }
        victory_points = { 6529 20 }
	}

	provinces={
		1192 1929 2322 2612 2753 3016 3184 3350 3449 3595 3991 4022 4725 4756 4887 4923 6522 6529 6536 6551 6561 6571 6572 6573 6584 6608 6609 6621
	}
	manpower=1848818
	buildings_max_level_factor=1.000
	state_category=large_city
}
