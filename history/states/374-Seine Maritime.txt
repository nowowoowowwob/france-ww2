
state={
	id=374
	name="STATE_374"

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 1
			industrial_complex = 3
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = FRA
		add_core_of = FRA
		1941.1.1 = {
			owner = GFR
			controller = GFR
			add_core_of = GFR
			add_core_of = EFR

		}
		1943.1.1 = {
			remove_core_of = EFR

		}
		1944.12.16 = {
			owner = FRA
			controller = FRA

		}
		buildings = {
			5676 = {
				naval_base = 4

			}
			5624 = {
				naval_base = 3

			}

		}
		victory_points = {
			4951 20 
		}
		victory_points = {
			5624 10 
		}
		victory_points = {
			5676 10 
		}
		victory_points = {
			5644 1 
		}

	}

	provinces={
		1260 2274 3273 3640 4040 4180 4214 4784 4951 4954 4957 5624 5644 5656 5666 5670 5676 5688 5691 5696 5703 11217 
	}
	manpower=932840
	buildings_max_level_factor=1.000
	state_category=town
	local_supplies=0.000
}
