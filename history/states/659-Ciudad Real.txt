
state={
	id=659
	name="STATE_659"

	history={
		buildings = {
			infrastructure = 2
			arms_factory = 0
			industrial_complex = 0
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = SPR
		add_core_of = SPR

		victory_points = { 8902 2 } #659
		victory_points = { 8871 8 } #659
		victory_points = { 8876 3 } #659
		victory_points = { 8889 2 } #659
		victory_points = { 8837 3 } #659
		victory_points = { 8835 1 } #659
		victory_points = { 8879 1 } #659
	}

	provinces={
		8825 8832 8835 8836 8837 8840 8841 8847 8848 8859 8865 8867 8868 8870 8871 8872 8876 8878 8882 8883 8887 8888 8889 8891 8903 8908 9229 9230 9231 9232 9338 9340
	}
	manpower=526903
	buildings_max_level_factor=1.000
	state_category=large_town
}
