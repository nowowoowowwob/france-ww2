
state={
	id=339
	name="STATE_339"
	resources={
		steel=21.000
		chromium=5.000
		tungsten=5.000
	}

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 1
			industrial_complex = 1
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = GER
		add_core_of = GER
		victory_points = {
			5510 20 
		}
		victory_points = {
			2610 5 
		}
		victory_points = {
			3473 10 
		}
		victory_points = {
			4046 5 
		}
		victory_points = {
			4538 1 
		}
		victory_points = {
			5534 3 
		}
		victory_points = {
			5546 30 
		}
		victory_points = {
			5575 5 
		}

	}

	provinces={
		840 983 1446 1528 2173 2610 2830 3057 3251 3372 3473 3476 4046 4538 4618 4643 5510 5515 5520 5534 5535 5539 5546 5575 8363 8369 8373 8387 8394 11373 
	}
	manpower=4474060
	buildings_max_level_factor=1.000
	state_category=large_city
	local_supplies=0.000
}
