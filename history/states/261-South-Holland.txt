
state={
	id=261
	name="STATE_261"

	resources={
		rubber=407
		aluminium = 28
		oil=27
	}

	history={
		buildings = {
			infrastructure = 4
			arms_factory = 2
			industrial_complex = 2
			air_base = 10
			anti_air_building = 2
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
			2042 = {
				bunker = 3

			}

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 4
				arms_factory = 2
				industrial_complex = 4
				air_base = 10
				anti_air_building = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 4
				arms_factory = 2
				industrial_complex = 5
				air_base = 10
				anti_air_building = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

			add_resource = {
				type = rubber
				amount = -407
				state = 261
			}
			add_resource = {
				type = aluminium
				amount = -28
				state = 261
			}
			add_resource = {
				type = oil
				amount = -27
				state = 261
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 4
				arms_factory = 2
				industrial_complex = 5
				air_base = 10
				anti_air_building = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 4
				arms_factory = 2
				industrial_complex = 5
				air_base = 10
				anti_air_building = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 4
				arms_factory = 2
				industrial_complex = 5
				air_base = 10
				anti_air_building = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 4
				arms_factory = 2
				industrial_complex = 5
				air_base = 10
				anti_air_building = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 4
				arms_factory = 2
				industrial_complex = 5
				air_base = 10
				anti_air_building = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = HOL
		add_core_of = HOL
		1941.6.22 = {
			owner = GNL
			controller = GNL
			add_core_of = GNL

		}
		buildings = {
			4685 = {
				naval_base = 3

			}

		}
		victory_points = {
			4685 20
		}
		victory_points = {
			2193 20
		}
		victory_points = {
			11223 5
		}

	}

	provinces={
		595 2042 2193 4275 4685 4975 5417 5435 5443 5446 5452 8430 8432 11223
	}
	manpower=2105684
	buildings_max_level_factor=1.000
	state_category=large_city
	local_supplies=0.000
}
