
state={
	id=368
	name="STATE_368"

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 2
			industrial_complex = 1
			air_base = 3
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 1
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 1
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 1
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 1
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 1
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 1
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 1
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = SOV
		add_core_of = SOV
		1942.11.19 = {
			owner = ROM

		}
		1944.6.6 = {
			owner = SOV
			controller = SOV

		}
        victory_points = { 10164 1 }
        victory_points = { 10169 2 }
	}

	provinces={
		726 730 3240 10149 10151 10164 10169 10172
	}
	manpower=890000
	buildings_max_level_factor=1.000
	state_category=large_town
	local_supplies=0.000
}
