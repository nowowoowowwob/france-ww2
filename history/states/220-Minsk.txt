
state={
	id=220
	name="STATE_220"

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 4
			industrial_complex = 2
			air_base = 8
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 6
				industrial_complex = 3
				air_base = 8
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 7
				industrial_complex = 4
				air_base = 8
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 7
				industrial_complex = 4
				air_base = 8
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 7
				industrial_complex = 4
				air_base = 8
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 7
				industrial_complex = 4
				air_base = 8
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 7
				industrial_complex = 4
				air_base = 8
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 9
				industrial_complex = 5
				air_base = 8
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = SOV
		add_core_of = SOV
		1942.11.19 = {
			owner = GOS
			controller = GOS
			add_core_of = GOS
		}
		1944.8.15 = {
			owner = SOV
			controller = SOV

		}
		victory_points = {
			10546 20
		}
        victory_points = { 10421 10 }
        victory_points = { 5251 2 }
        victory_points = { 10578 1 }
        victory_points = { 10462 2 }

	}

	provinces={
		1590 2266 3250 3352 5251 10421 10436 10462 10478 10490 10534 10546 10578
	}
	manpower=564231
	buildings_max_level_factor=1.000
	state_category=large_city
	local_supplies=0.000
}
