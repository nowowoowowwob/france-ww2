state={
	id=885
	name="STATE_885"
	provinces={
		9679
	}
	history={
		buildings = {
			infrastructure = 3
		}
        owner = AUS

		1939.1.1 = {
			owner = GER
		}
    }
    state_category = river_crossing
    manpower=1
	buildings_max_level_factor=1.000
}
