
state={
	id=583
	name="STATE_583"
	resources={
		chromium=98.000
		aluminium=22.000
	}

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 1
			industrial_complex = 1
			air_base = 2
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 1
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = YUG
		add_core_of = YUG
		add_core_of = BUL
		1941.4.1 = {
			owner = BUL
			controller = BUL

		}
		1944.12.16 = {
			owner = YUG
			controller = YUG

		}
		victory_points = {
			98 2 
		}
		victory_points = {
			1622 1 
		}
		victory_points = {
			2164 20 
		}
		victory_points = {
			3633 2 
		}
		victory_points = {
			4075 5 
		}
		victory_points = {
			6764 3 
		}
		victory_points = {
			9545 5 
		}

	}

	provinces={
		98 127 1079 1622 2164 2387 2536 3282 3284 3498 3633 3828 4075 4093 4123 4186 4473 4607 4761 6701 6726 6729 6731 6732 6755 6763 6764 6779 6787 9545 9730 9733 9736 9738 9743 11324 
	}
	manpower=75000
	buildings_max_level_factor=1.000
	state_category=town
	local_supplies=0.000
}
