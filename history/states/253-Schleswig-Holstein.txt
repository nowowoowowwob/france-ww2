
state={
	id=253
	name="STATE_253"

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 1
			industrial_complex = 1
			air_base = 0
			anti_air_building = 0
				dockyard = 2
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 3
				air_base = 0
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 3
				air_base = 0
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 3
				air_base = 0
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 3
				air_base = 0
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 3
				air_base = 0
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}


		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 3
				air_base = 0
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 3
				air_base = 0
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = GER
		add_core_of = GER
		buildings = {
			5274 = {
				naval_base = 3

			}
			2692 = {
				naval_base = 1

			}

		}

        victory_points = { 2692 10 }
        victory_points = { 5305 20 }
        victory_points = { 10010 10 }
        victory_points = { 5263 2 }
        victory_points = { 7623 1 }
        victory_points = { 10011 5 }
	}

	provinces={
		1321 1792 1794 2234 2692 3054 3291 3718 3886 3968 3969 4452 4584 4623 4748 4910 5256 5263 5264 5272 5274 5278 5280 5284 5292 5295 5303 5305 7623 7626 7627 7628 7629 8499 10010 10011
	}
	manpower=2111297
	buildings_max_level_factor=1.000
	state_category=large_town
}
