
state={
	id=42
	name="STATE_42"
	provinces={
		8186
	}

	history={
		buildings = {
			infrastructure = 3
		}
		owner = FRA

		1940.6.22 = {
			owner = EFR
		}

		1942.11.19 = {
			owner = GFR
		}
		1944.12.16 = {
			owner = FRA
		}
	}
	manpower=1
	state_category = river_crossing
	buildings_max_level_factor=1.000
}
