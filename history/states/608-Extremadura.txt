
state={
	id=608
	name="STATE_608"
	resources={
		tungsten=6
	}

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 0
			industrial_complex = 1
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = SPR
		add_core_of = SPR
		victory_points = {
			9234 4 
		}
		victory_points = {
			8986 2 
		}
		victory_points = {
			8967 1 
		}
		victory_points = {
			8925 7 
		}
		victory_points = {
			8902 20 
		}
		victory_points = {
			8893 3 
		}
		victory_points = {
			8920 1 
		}
		victory_points = {
			8855 2 
		}
		victory_points = {
			8816 1 
		}
		victory_points = {
			8877 2 
		}

	}

	provinces={
		8816 8831 8833 8844 8846 8849 8855 8857 8858 8864 8866 8869 8873 8877 8880 8893 8895 8896 8898 8899 8902 8907 8909 8915 8920 8925 8932 8933 8939 8943 8947 8948 8958 8963 8964 8966 8967 8971 8979 8986 9000 9005 9017 9234 9235 9237 9334 9342 9348 9350 9357 11320 11321 
	}
	manpower=1417652
	buildings_max_level_factor=1.000
	state_category=town
	local_supplies=0.000
}
