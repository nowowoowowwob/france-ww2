
state={
	id=590
	name="STATE_590"

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 2
			industrial_complex = 0
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = ITA
		add_core_of = ITA
		1944.6.6 = {
			add_core_of = KIT
			KIT = {
				set_province_controller = 955
				set_province_controller = 4581
				set_province_controller = 3263
				set_province_controller = 6724
				set_province_controller = 6717
				set_province_controller = 4220
				set_province_controller = 6738
				set_province_controller = 6742
				set_province_controller = 3340
				set_province_controller = 6775
				set_province_controller = 6770
				set_province_controller = 6758
			}
		}
		1944.6.23 = {
			owner = KIT

		}

        victory_points = { 567 1 }
        victory_points = { 1464 2 }
        victory_points = { 6687 1 }
        victory_points = { 6717 5 }
	}

	provinces={
		567 648 955 1464 1519 1811 1935 2943 3207 3228 3263 3288 3340 3420 4050 4220 4241 4581 6687 6691 6693 6710 6712 6717 6722 6724 6730 6734 6737 6738 6742 6750 6758 6770 6775 8076 8079 8081
	}
	manpower=1201536
	buildings_max_level_factor=1.000
	state_category=large_city
}
