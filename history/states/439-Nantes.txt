
state={
	id=439
	name="STATE_439"

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 0
			industrial_complex = 0
			air_base = 2
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		buildings = {
			3502 = {
				naval_base = 3

			}
		}
		owner = FRA
		add_core_of = FRA
		1941.1.1 = {
			owner = GFR
			controller = GFR
			add_core_of = GFR
			add_core_of = EFR

		}
		1943.1.1 = {
			remove_core_of = EFR

		}
		1944.12.16 = {
			owner = FRA
			controller = FRA
			GFR = {
				set_province_controller = 3502

			}

		}
		victory_points = {
			2454 3
		}
		victory_points = {
			3293 2
		}
		victory_points = {
			3502 3
		}
		victory_points = {
			4132 15
		}
		victory_points = {
			4566 20
		}

	}

	provinces={
		1481 1967 1986 2454 2681 3293 3442 3502 3992 4132 4566 4783 4813 4857 4943 5920 5944 5971 5976 5988 5996 6009 6010 6018 6020 11221
	}
	manpower=1388400
	buildings_max_level_factor=1.000
	state_category=rural
	local_supplies=0.000
}
