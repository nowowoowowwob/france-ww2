
state={
	id=5
	name="STATE_5"

	history={
		buildings = {
			infrastructure = 1
			arms_factory = 0
			industrial_complex = 1
			air_base = 2
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 1
				arms_factory = 0
				industrial_complex = 1
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 1
				arms_factory = 1
				industrial_complex = 1
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 1
				arms_factory = 1
				industrial_complex = 1
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 1
				arms_factory = 1
				industrial_complex = 1
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 1
				arms_factory = 1
				industrial_complex = 1
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 1
				arms_factory = 1
				industrial_complex = 1
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 1
				arms_factory = 1
				industrial_complex = 1
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = SOV
		add_core_of = SOV
		victory_points = { 5012 5 } #5
		victory_points = { 3494 1 } #5
		victory_points = { 993 1 } #5
		victory_points = { 1177 1 } #5
		victory_points = { 596 1 } #5

		1942.11.19 = {
			FIN = {
				set_province_controller = 690
			}
		}

		1944.12.16 = {
			controller = SOV
		}
	}

	provinces={
		94 113 596 690 993 1177 1187 1396 1421 1468 2584 2790 3494 4677 5012 5016 5017 7905 7906
	}
	manpower=275751
	buildings_max_level_factor=1.000
	state_category=rural
	local_supplies=0.000
}
