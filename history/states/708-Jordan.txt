
state={
	id=708
	name="STATE_708"

	history={
		buildings = {
			infrastructure = 2
			arms_factory = 0
			industrial_complex = 1
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = ENG
		add_core_of = JOR

        victory_points = { 142 1 }
        victory_points = { 924 1 }
        victory_points = { 2282 20 }
        victory_points = { 2356 1 }
        victory_points = { 3094 3 }
        victory_points = { 4151 5 }
        victory_points = { 4315 10 }
        victory_points = { 4395 1 }
        victory_points = { 7343 2 }
	}

	provinces={
		142 171 207 270 323 529 562 830 924 1038 1235 1319 1364 1375 1384 1385 1534 1697 1784 2165 2256 2282 2356 2509 2557 2591 2625 2664 2672 2680 2697 2721 2742 2876 3094 3136 3299 3312 3354 3612 3618 3638 4151 4295 4309 4315 4363 4395 4481 4493 4547 4661 4848 4866 4911 7084 7119 7133 7179 7187 7191 7202 7203 7217 7224 7240 7242 7262 7267 7280 7285 7287 7290 7296 7305 7311 7343 7363 7607 7612
	}
	manpower=932000
    state_category = rural
	buildings_max_level_factor=1.000
}
