
state={
	id=515
	name="STATE_515"
	resources={
		steel=42.000
		aluminium=42.000
	}

	history={
		buildings = {
			9966 = {
				naval_base = 4

			}

		}
		buildings = {
			infrastructure = 3
			arms_factory = 0
			industrial_complex = 0
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = FRA
		add_core_of = FRA
		1941.1.1 = {
			owner = EFR
			controller = EFR
			add_core_of = EFR
			add_core_of = GFR

		}
		1942.11.19 = {
			owner = ITA
			controller = ITA
			remove_core_of = EFR

		}
		1944.6.6 = {
			owner = GFR
			controller = GFR

		}

		1944.12.16 = {
			owner = FRA
			controller = FRA
		}

		1944.12.16 = {
			owner = FRA
			controller = FRA
		}
		victory_points = {
			2779 1
		}
		victory_points = {
			6657 2
		}

	}

	provinces={
		93 488 1072 1790 1920 2779 3128 3161 3974 4006 4520 4629 4711 6518 6534 6541 6553 6618 6634 6657 6663 6667 9966
	}
	manpower=173300
	buildings_max_level_factor=1.000
	state_category=rural
	local_supplies=0.000
}
