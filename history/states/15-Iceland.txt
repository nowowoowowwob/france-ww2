state={
	id=15
	name="STATE_15"
	provinces={
		11 197 361 772 1013 1285 1512 2592 5018
	}
	history = {
		owner = DEN
		add_core_of = ICE
		1940.5.1 = {
			owner = ENG
			controller = ENG
		}
		1941.7.1 = {
			owner = USA
			controller = USA
		}
		1944.6.1 = {
			owner = ICE
			controller = ICE
		}

		buildings = {
			1285 ={
				naval_base = 3
			}
			air_base = 1
		}
        victory_points = { 361 10 }
        victory_points = { 5018 1 }
	}
	manpower=115870
    state_category = rural
	buildings_max_level_factor=1.000
}
