
state={
	id=122
	name="STATE_122"
	provinces={
		8613
	}

	history={
		buildings = {
			infrastructure = 4
		}
		owner = HOL

		1940.6.22 = {
			owner = GNL
		}
		1945.3.20 = {
			controller = HOL
		}
	}
	manpower=1
	state_category = river_crossing
	buildings_max_level_factor=1.000
}
