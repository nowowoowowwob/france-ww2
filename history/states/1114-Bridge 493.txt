state={
	id=1114
	name="STATE_1114"
	provinces={
		11254
	}

	history={
		buildings = {
			infrastructure = 3
		}
		owner = BEL

		1940.6.1 = {
			owner = GBF
		}

		1944.12.16 = {
			owner = BEL
			controller = BEL
		}
	}
	manpower=1
	state_category = river_crossing
	buildings_max_level_factor=1.000
}
