
state={
	id=569
	name="STATE_569"

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 1
			industrial_complex = 0
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = ITA
		add_core_of = ITA
		1944.6.6 = {
			add_core_of = KIT

		}
		1944.7.1 = {
			owner = KIT

		}
		buildings = {

		}

        victory_points = { 299 10 }
        victory_points = { 4298 2 }
        victory_points = { 4337 5 }
        victory_points = { 6695 1 }
        victory_points = { 6716 5 }
        victory_points = { 6739 1 }
	}

	provinces={
		299 1330 1429 1850 3424 3481 3812 3834 3921 4298 4337 4696 4716 4754 6695 6700 6706 6708 6716 6733 6739 8078 9959
	}
	manpower=722544
	buildings_max_level_factor=1.000
	state_category=large_city
}
