state={
	id=1086
	name="STATE_1086"

	resources={
		aluminium=12 # was: 18
	}
	history={
		owner = USA
		add_core_of = USA
		buildings = {
			infrastructure = 4
			arms_factory = 0
			industrial_complex = 5
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 4
				arms_factory = 1
				industrial_complex = 10
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 4
				arms_factory = 5
				industrial_complex = 6
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 4
				arms_factory = 5
				industrial_complex = 6
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 4
				arms_factory = 5
				industrial_complex = 6
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 4
				arms_factory = 5
				industrial_complex = 6
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}


		1944.12.14 = {
			buildings = {
				infrastructure = 4
				arms_factory = 5
				industrial_complex = 6
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 4
				arms_factory = 5
				industrial_complex = 6
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
	}
	provinces={
		8626
	}
	manpower = 3629365

	state_category = large_city
	buildings_max_level_factor=1.000
	local_supplies=7.0
}
