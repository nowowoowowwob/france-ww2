
state={
	id=273
	name="STATE_273"

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 0
			industrial_complex = 0
			air_base = 0
			anti_air_building = 0
				dockyard = 2
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}


		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = GER
		add_core_of = GER

        victory_points = { 1787 2 }
        victory_points = { 1914 1 }
        victory_points = { 2063 1 }
        victory_points = { 4444 1 }
        victory_points = { 4642 10 }
        victory_points = { 5287 1 }
        victory_points = { 5293 10 }
	}

	provinces={
		950 1591 1787 1914 2063 2084 3267 4283 4444 4642 5287 5293 5304 5316 7624 7630 8492 8498 10009
	}
	manpower=1006221
	buildings_max_level_factor=1.000
	state_category=town
}
