state={
	id=329
	name="STATE_329"
	provinces={
		891 1087 1090 2318 3455 4659 5472
	}
	resources={
		steel=49 # was: 48
	}
	history={
		buildings = {
			infrastructure = 3
			arms_factory = 1
			industrial_complex = 2
			air_base = 6
			anti_air_building = 0
				dockyard = 3
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 6
				anti_air_building = 0
				dockyard = 3
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 6
				anti_air_building = 0
				dockyard = 3
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 6
				anti_air_building = 0
				dockyard = 3
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 6
				anti_air_building = 0
				dockyard = 3
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 6
				anti_air_building = 0
				dockyard = 3
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 6
				anti_air_building = 0
				dockyard = 3
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 6
				anti_air_building = 0
				dockyard = 3
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = ENG
		add_core_of = ENG
		buildings = {
			3455 = { naval_base = 4 }
			4659 = { naval_base = 5 }
		}
        victory_points = { 3455 15 }
        victory_points = { 4659 10 }
	}
	manpower=1221013
    state_category = large_city
	buildings_max_level_factor=1.000
}
