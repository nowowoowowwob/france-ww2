
state={
	id=98
	name="STATE_98"

	history={
		buildings = {
			infrastructure = 2
			arms_factory = 1
			industrial_complex = 0
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = SOV
		add_core_of = SOV

		victory_points = { 3380 10 } #98
		victory_points = { 643 5 } #98
		victory_points = { 10894 1 } #98
		victory_points = { 10960 1 } #98
		victory_points = { 225 1 } #98
		victory_points = { 54 1 } #98

	}

	provinces={
		54 225 291 643 702 735 3380 10828 10859 10872 10894 10903 10915 10937 10938 10943 10948 10960 10964 10974 10978
	}
	manpower=1000000
	buildings_max_level_factor=1.000
	state_category=rural
	local_supplies=0.000
}
