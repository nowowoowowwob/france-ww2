
state={
	id=304
	name="STATE_304"

	history={
		buildings = {
			infrastructure = 4
			arms_factory = 3
			industrial_complex = 3
			air_base = 8
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 4
				arms_factory = 7
				industrial_complex = 5
				air_base = 8
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 4
				arms_factory = 10
				industrial_complex = 6
				air_base = 8
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 4
				arms_factory = 10
				industrial_complex = 6
				air_base = 8
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 4
				arms_factory = 12
				industrial_complex = 7
				air_base = 8
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 4
				arms_factory = 12
				industrial_complex = 7
				air_base = 8
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 4
				arms_factory = 14
				industrial_complex = 8
				air_base = 8
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 4
				arms_factory = 14
				industrial_complex = 8
				air_base = 8
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = SOV
		add_core_of = SOV
		1942.11.19 = {
			owner = GUK
			controller = GUK
			add_core_of = GUK
		}
		1944.6.6 = {
			owner = SOV
			controller = SOV

		}
		victory_points = {
			10327 10
		}
		victory_points = {
			10319 20
		}
		victory_points = {
			10322 10
		}
		victory_points = { 10306 10 } #304
		victory_points = { 10301 3 } #304
		victory_points = { 3466 1 } #304
		victory_points = { 1700 3 } #304
		victory_points = { 92 3 } #304
		victory_points = { 10233 1 } #304
		victory_points = { 10276 10 } #304
		victory_points = { 1403 1 } #304


	}

	provinces={
		92 178 814 1403 1700 3466 3995 10233 10248 10252 10254 10275 10276 10282 10301 10306 10319 10322 10325 10327
	}
	manpower=1832652
	buildings_max_level_factor=1.000
	state_category=large_city
	local_supplies=0.000
}
