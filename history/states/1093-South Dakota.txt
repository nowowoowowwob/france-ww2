state={
	id=1093
	name="STATE_1093"
	history={
		owner = USA
		add_core_of = USA
		buildings = {
			infrastructure = 1
			arms_factory = 0
			industrial_complex = 1
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 1
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 1
				arms_factory = 1
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 1
				arms_factory = 1
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 1
				arms_factory = 1
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 1
				arms_factory = 1
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}


		1944.12.14 = {
			buildings = {
				infrastructure = 1
				arms_factory = 1
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 1
				arms_factory = 1
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
	}
	provinces={
		8644
	}
	manpower = 69149
	state_category = rural
	buildings_max_level_factor=1.000
	local_supplies=0.000
}
