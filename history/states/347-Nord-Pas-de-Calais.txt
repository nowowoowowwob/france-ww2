
state={
	id=347
	name="STATE_347"
	resources={
		steel=76.000
		aluminium=14.000
	}

	history={
		buildings = {
			infrastructure = 4
			arms_factory = 0
			industrial_complex = 1
			air_base = 10
			anti_air_building = 0
				dockyard = 2
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 4
				arms_factory = 1
				industrial_complex = 3
				air_base = 10
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 4
				arms_factory = 1
				industrial_complex = 3
				air_base = 10
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 4
				arms_factory = 1
				industrial_complex = 3
				air_base = 10
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 4
				arms_factory = 1
				industrial_complex = 3
				air_base = 10
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 4
				arms_factory = 1
				industrial_complex = 3
				air_base = 10
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 4
				arms_factory = 1
				industrial_complex = 3
				air_base = 10
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 4
				arms_factory = 1
				industrial_complex = 3
				air_base = 10
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = FRA
		add_core_of = FRA
		1941.6.22 = {
			owner = GER
			controller = GER

		}
		1941.6.22 = {
			owner = GBF
			controller = GBF
			add_core_of = GBF
			add_core_of = GFR
			add_core_of = EFR

		}
		1943.1.1 = {
			remove_core_of = EFR

		}
		1944.12.16 = {
			GBF = {
				set_province_controller = 2689
			}
			FRA = {
				set_province_controller = 1418
				set_province_controller = 1565
				set_province_controller = 1847
				set_province_controller = 2098
				set_province_controller = 2798
				set_province_controller = 3081
				set_province_controller = 3152
				set_province_controller = 3429
				set_province_controller = 3435
				set_province_controller = 3759
				set_province_controller = 3975
				set_province_controller = 4115
				set_province_controller = 4156
				set_province_controller = 4426
				set_province_controller = 4470
				set_province_controller = 4641
				set_province_controller = 4721
				set_province_controller = 4792
				set_province_controller = 4797
				set_province_controller = 4899
				set_province_controller = 5554
				set_province_controller = 5573
				set_province_controller = 5576
				set_province_controller = 5584
				set_province_controller = 5597
				set_province_controller = 5601
				set_province_controller = 5603
				set_province_controller = 5608
				set_province_controller = 5615
				set_province_controller = 5634
				set_province_controller = 9885
				set_province_controller = 11218
				set_province_controller = 11219
			}
		}
		buildings = {
			2689 = {
				naval_base = 6

			}
			1565 = {
				naval_base = 4

			}

		}
		victory_points = {
			1565 20
		}
		victory_points = {
			2689 10
		}
		victory_points = {
			5584 10
		}
		victory_points = {
			3152 3
		}
		victory_points = {
			4899 20
		}
		victory_points = {
			5597 5
		}
		victory_points = {
			5601 5
		}
		victory_points = {
			5615 5
		}
		victory_points = {
			11218 10
		}

	}

	provinces={
		1418 1565 1847 2098 2689 2798 3081 3152 3429 3435 3759 3975 4115 4156 4426 4470 4641 4721 4792 4797 4899 5554 5573 5576 5584 5597 5601 5603 5608 5615 5634 9885 11218 11219
	}
	manpower=892920
	buildings_max_level_factor=1.000
	state_category=large_city
	local_supplies=0.000
}
