
state={
	id=741
	name="STATE_741"

	history={
		buildings = {
			infrastructure = 1
			arms_factory = 0
			industrial_complex = 0
			air_base = 6
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 1
				arms_factory = 0
				industrial_complex = 0
				air_base = 6
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 1
				arms_factory = 0
				industrial_complex = 0
				air_base = 6
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 1
				arms_factory = 0
				industrial_complex = 0
				air_base = 6
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 1
				arms_factory = 0
				industrial_complex = 0
				air_base = 6
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 1
				arms_factory = 0
				industrial_complex = 0
				air_base = 6
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 1
				arms_factory = 0
				industrial_complex = 0
				air_base = 6
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 1
				arms_factory = 0
				industrial_complex = 0
				air_base = 6
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = EGY
		add_core_of = EGY
		buildings = {
			9449 = {
				naval_base = 4

			}
			9452 = {
				naval_base = 7

			}
			3963 = {
				naval_base = 1

			}

		}
		victory_points = {
			9449 20 
		}
		victory_points = {
			7371 15 
		}
		victory_points = {
			1777 1 
		}
		victory_points = {
			2431 10 
		}
		victory_points = {
			3632 10 
		}
		victory_points = {
			4837 10 
		}
		victory_points = {
			9449 10 
		}
		victory_points = {
			9452 20 
		}
		victory_points = {
			2232 1 
		}
		victory_points = {
			2937 5 
		}
		victory_points = {
			3891 3 
		}
		victory_points = {
			3963 15 
		}
		victory_points = {
			4768 5 
		}
		victory_points = {
			7312 3 
		}

	}

	provinces={
		417 432 1016 2232 2431 2586 2712 2937 2980 3199 3396 3474 3632 3891 3913 3963 4510 4537 4708 4768 4837 7312 7325 7326 7331 7333 7338 7340 7347 7354 7361 7371 7379 8512 8514 8519 8523 8526 9449 9452 9465 9469 9470 9471 9472 9476 11384 11385 
	}
	manpower=3859239
	buildings_max_level_factor=1.000
	state_category=large_city
	local_supplies=0.000
}
