
state={
	id=574
	name="STATE_574"

	history={
		buildings = {
			infrastructure = 2
			arms_factory = 0
			industrial_complex = 1
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = YUG
		add_core_of = YUG
		add_core_of = ALB
		1941.4.1 = {
			owner = ALB
			controller = ALB

		}

        victory_points = { 3830 10 }
        victory_points = { 6678 1 }
        victory_points = { 6688 3 }
	}

	provinces={
		828 3355 3568 3830 4406 6678 6685 6688 9547 9745
	}
	manpower=897365
	buildings_max_level_factor=1.000
	state_category=rural
}
