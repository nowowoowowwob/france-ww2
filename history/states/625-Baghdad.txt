
state={
	id=625
	name="STATE_625"
	resources={
		oil=60.000
	}

	history={
		buildings = {
			infrastructure = 1
			arms_factory = 3
			industrial_complex = 8
			air_base = 4
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 1
				arms_factory = 5
				industrial_complex = 11
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 1
				arms_factory = 7
				industrial_complex = 12
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 1
				arms_factory = 8
				industrial_complex = 13
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 1
				arms_factory = 8
				industrial_complex = 13
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 1
				arms_factory = 9
				industrial_complex = 14
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 1
				arms_factory = 9
				industrial_complex = 15
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 1
				arms_factory = 10
				industrial_complex = 15
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = IRQ
		add_core_of = IRQ
		buildings = {
			infrastructure = 3
			arms_factory = 3
			industrial_complex = 4
			air_base = 4
			synthetic_refinery = 3
			fuel_silo = 3

		}
		victory_points = {
			722 1 
		}
		victory_points = {
			1684 5 
		}
		victory_points = {
			2186 1 
		}
		victory_points = {
			3419 3 
		}
		victory_points = {
			3523 2 
		}
		victory_points = {
			4764 1 
		}
		victory_points = {
			6988 1 
		}
		victory_points = {
			9456 20 
		}

	}

	provinces={
		374 722 733 911 1000 1042 1070 1091 1203 1315 1336 1389 1394 1684 1865 2076 2144 2186 2306 2401 2520 2589 2653 2698 2749 2787 2859 3124 3129 3141 3203 3419 3523 3572 3685 3703 3790 4020 4028 4124 4335 4425 4508 4514 4667 4764 4780 4880 4976 5078 6830 6859 6880 6883 6887 6893 6903 6909 6913 6916 6921 6926 6933 6937 6945 6946 6949 6959 6966 6971 6975 6986 6988 7542 7544 7585 8066 8542 8543 8548 8551 8558 8560 8564 8570 8572 8577 8580 8614 9456 9457 11387 11390 11391 11392 11393 11394 
	}
	manpower=2044165
	buildings_max_level_factor=1.000
	state_category=metropolis
	local_supplies=0.000
}
