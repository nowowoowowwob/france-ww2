
state={
	id=424
	name="STATE_424"

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 2
			industrial_complex = 3
			air_base = 2
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 4
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 6
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 6
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 6
				air_base = 2
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = HUN
		add_core_of = HUN

		1944.12.16 = {
			controller = SOV
		}
        victory_points = { 1440 20 }
        victory_points = { 5913 1 }
	}

	provinces={
		1440 4674 5913 5981
	}
	manpower=1306917
	buildings_max_level_factor=1.000
	state_category=town
}
