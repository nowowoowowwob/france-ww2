
state={
	id=520
	name="STATE_520"

	history={
		buildings = {
			infrastructure = 4
			arms_factory = 2
			industrial_complex = 4
			air_base = 5
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 4
				arms_factory = 3
				industrial_complex = 6
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 4
				arms_factory = 4
				industrial_complex = 7
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 4
				arms_factory = 4
				industrial_complex = 7
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 4
				arms_factory = 5
				industrial_complex = 8
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 4
				arms_factory = 5
				industrial_complex = 9
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}


		1944.12.14 = {
			buildings = {
				infrastructure = 4
				arms_factory = 5
				industrial_complex = 10
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 4
				arms_factory = 5
				industrial_complex = 11
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = YUG
		add_core_of = YUG
		add_core_of = SER
		1941.4.1 = {
			owner = SER
			controller = SER

		}

		1944.12.16 = {
			owner = YUG
			controller = YUG
		}

        victory_points = { 2719 4 }
        victory_points = { 3377 5 }
        victory_points = { 3953 3 }
        victory_points = { 6442 5 }
        victory_points = { 6452 5 }
        victory_points = { 9968 25 }
	}

	provinces={
		1008 1671 1814 2482 2719 2732 3014 3377 3439 3953 4017 4047 4935 6442 6452 6453 6495 6509 6537 6595 6620 9926 9945 9968
	}
	manpower=1864700
	buildings_max_level_factor=1.000
	state_category=city
}
