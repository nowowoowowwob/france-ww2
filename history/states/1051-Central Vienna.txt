state={
	id=1051
	name="STATE_1051"
	provinces={
		9963
	}
	history = {
		buildings = {
			infrastructure = 4
			arms_factory = 1
			industrial_complex = 2
			air_base = 6
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 4
				arms_factory = 1
				industrial_complex = 2
				air_base = 6
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 4
				arms_factory = 1
				industrial_complex = 2
				air_base = 6
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 4
				arms_factory = 1
				industrial_complex = 2
				air_base = 6
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 4
				arms_factory = 1
				industrial_complex = 2
				air_base = 6
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 4
				arms_factory = 1
				industrial_complex = 3
				air_base = 6
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}


		1944.12.14 = {
			buildings = {
				infrastructure = 4
				arms_factory = 1
				industrial_complex = 3
				air_base = 6
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 4
				arms_factory = 1
				industrial_complex = 3
				air_base = 6
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = AUS
		add_core_of = AUS
		add_core_of = GER
		1939.1.1 = {
			owner = GER
			controller = GER
		}
		victory_points = { 9963 30 }
	}
	manpower=103487
	state_category=town
	buildings_max_level_factor=1.000
}
