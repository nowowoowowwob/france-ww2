
state={
	id=729
	name="STATE_729"
	resources={
		chromium=12.000
	}

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 0
			industrial_complex = 1
			air_base = 4
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = FRA
		add_core_of = MOR
		1941.1.1 = {
			owner = EFR
			controller = EFR

		}
		1942.11.19 = {
			owner = FRA
			controller = FRA

		}
		buildings = {
			1117 = {
				naval_base = 6

			}
			7231 = {
				naval_base = 4

			}

		}
		victory_points = {
			7211 10 
		}
		victory_points = {
			1117 20 
		}
		victory_points = {
			278 3 
		}
		victory_points = {
			826 1 
		}
		victory_points = {
			988 5 
		}
		victory_points = {
			1093 1 
		}
		victory_points = {
			1146 3 
		}
		victory_points = {
			1574 10 
		}
		victory_points = {
			1931 2 
		}
		victory_points = {
			3629 1 
		}
		victory_points = {
			4083 1 
		}
		victory_points = {
			4139 1 
		}
		victory_points = {
			4831 1 
		}
		victory_points = {
			7211 5 
		}
		victory_points = {
			7231 10 
		}
		victory_points = {
			7243 20 
		}
		victory_points = {
			7263 1 
		}

	}

	provinces={
		62 278 518 673 774 819 821 826 837 988 992 1019 1036 1041 1093 1109 1110 1117 1146 1296 1431 1533 1574 1575 1618 1640 1804 1931 1979 1995 2031 2132 2166 2183 2236 2370 2398 2475 2513 2815 2836 2864 2890 3056 3101 3105 3106 3629 3649 3668 3763 4081 4083 4139 4591 4609 4610 4620 4796 4831 4922 7205 7211 7213 7231 7237 7243 7245 7258 7259 7260 7263 7264 7265 7273 7281 7283 7291 7293 7294 7299 7302 7306 7500 7506 7507 7510 7513 8048 8054 8055 8058 8060 
	}
	manpower=3197719
	buildings_max_level_factor=1.000
	state_category=rural
	local_supplies=0.000
}
