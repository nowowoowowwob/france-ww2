
state={
	id=594
	name="STATE_594"

	history={
		buildings = {
			infrastructure = 2
			arms_factory = 0
			industrial_complex = 0
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = FRA
		add_core_of = SYR
		1940.6.22 = {
			owner = EFR

		}
		1941.7.12 = {
			owner = FRA

		}
		victory_points = {
			1930 1
		}
		victory_points = {
			4265 2
		}
		victory_points = {
			1379 2
		}
		victory_points = {
			6873 2
		}

	}

	provinces={
		427 554 829 1269 1305 1323 1348 1356 1361 1379 1461 1769 1830 1833 1930 1960 2097 2291 2295 2300 2334 2497 2644 2777 2874 2954 2977 3088 3146 3173 3246 3317 3415 3433 3450 3639 3809 3837 4084 4161 4265 4349 4782 4873 4959 6719 6752 6790 6802 6803 6814 6815 6823 6832 6840 6846 6857 6873 6877 6886 6890 6900 6906 6910 6919 6925 6941 6956 6958 6970 6983 6998 7000 7020 7045 7186 8457 8556 8568 8583 8584 8586 11395
	}
	manpower=324110
	buildings_max_level_factor=1.000
	state_category=rural
	local_supplies=0.000
}
