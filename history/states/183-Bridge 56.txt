
state={
	id=183
	name="STATE_183"
	provinces={
		8427
	}

	history={
		buildings = {
			infrastructure = 3
		}
		owner = HOL

		1940.6.22 = {
			owner = GNL
		}
		1944.12.16 = {
			controller = HOL
		}
	}
	manpower=1
	state_category = river_crossing
	buildings_max_level_factor=1.000
}
