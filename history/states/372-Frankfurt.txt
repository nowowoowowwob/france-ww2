
state={
	id=372
	name="STATE_372"

	history={
		buildings = {
			infrastructure = 4
			arms_factory = 2
			industrial_complex = 3
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 4
				arms_factory = 3
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 4
				arms_factory = 3
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 4
				arms_factory = 5
				industrial_complex = 6
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 4
				arms_factory = 5
				industrial_complex = 6
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 4
				arms_factory = 5
				industrial_complex = 6
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 4
				arms_factory = 5
				industrial_complex = 6
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 4
				arms_factory = 3
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = GER
		add_core_of = GER
		set_demilitarized_zone = yes
		1937.1.1 = {
			set_demilitarized_zone = no

		}
		victory_points = {
			2200 40 
		}
		victory_points = {
			2303 10 
		}
		victory_points = {
			4689 10 
		}
		victory_points = {
			5671 5 
		}

	}

	provinces={
		775 2200 2303 3602 3770 4689 4714 4942 5607 5671 5699 5706 11231 
	}
	manpower=605547
	buildings_max_level_factor=1.000
	state_category=city
	local_supplies=0.000
}
