
state={
	id=205
	name="STATE_205"
	resources={
		steel=24.000
	}

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 1
			industrial_complex = 4
			air_base = 4
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 7
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 8
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 9
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 9
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 9
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 9
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 9
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = LIT
		add_core_of = LIT
		1941.1.1 = {
			owner = SOV

		}
		1942.11.19 = {
			owner = GOS
			controller = GOS
			add_core_of = GOS
		}
		1944.12.16 = {
			owner = SOV

		}
		victory_points = { 11170 20 }
        victory_points = { 539 3 }
        victory_points = { 3580 2 }
        victory_points = { 5218 1 }
	}

	provinces={
		144 539 1901 2055 2812 2903 3392 3580 4562 5193 5208 5218 5234 5249 10566 10582 10583 11170
	}
	manpower=879534
	buildings_max_level_factor=1.000
	state_category=large_city
	local_supplies=0.000
}
