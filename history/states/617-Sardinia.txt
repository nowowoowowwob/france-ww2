
state={
	id=617
	name="STATE_617"
	provinces={
		301 757 769 822 951 981 1007 1932 2467 2849 2920 2934 3170 3414 3714 4078 4504 4736 4741 4787 4882 4950 6805 6820 6831 6837 6842 6844 6849 6855 6882 6892 6896 6911 6914 6930 6942 6947
	}
	history = {
		owner = ITA
		add_core_of = ITA

		buildings = {
			6947 = {
				naval_base = 2
			}
		}
		buildings = {
			infrastructure = 2
			arms_factory = 0
			industrial_complex = 1
			air_base = 5
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 2
				arms_factory = 2
				industrial_complex = 2
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 2
				arms_factory = 2
				industrial_complex = 2
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 2
				arms_factory = 2
				industrial_complex = 2
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 2
				arms_factory = 2
				industrial_complex = 3
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 2
				arms_factory = 2
				industrial_complex = 3
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 2
				arms_factory = 2
				industrial_complex = 3
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 2
				arms_factory = 2
				industrial_complex = 3
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}

		1944.6.6 = {
			add_core_of = KIT
		}

		1944.6.6 = {
			owner = KIT
			controller = KIT
		}
        victory_points = { 3714 1 }
        victory_points = { 4741 1 }
        victory_points = { 6805 1 }
        victory_points = { 6831 3 }
        victory_points = { 6837 3 }
        victory_points = { 6896 1 }
        victory_points = { 6947 5 }
	}
	manpower=1034206
    state_category = town
	buildings_max_level_factor=1.000
}
