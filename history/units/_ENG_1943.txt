﻿division_template = {
	name = "Infantry Division"			# 1st - 5th divisions = fully equipped, others = reserve divisions
	division_names_group = ENG_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

        infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

        infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }

		artillery_brigade = { x = 3 y = 0 }
		artillery_brigade = { x = 3 y = 1 }
		artillery_brigade = { x = 3 y = 2 }
	}
	support = {
       	engineer = { x = 0 y = 0 }
       	artillery = { x = 0 y = 1 }
		maintenance_company = { x = 0 y = 2 }
		anti_tank = { x = 0 y = 3 }
		signal_company = { x = 1 y = 0 }
        field_hospital = { x = 1 y = 1 }
	}
}
division_template = {
	name = "Infantry Regiment"
	division_names_group = ENG_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		artillery_brigade = { x = 1 y = 0 }
	}
}
division_template = {
	name = "Marine Division" 		# 1st, 2nd Marine Brigades, precursors to 1st, 2nd USMC Divisions
	division_names_group = ENG_MAR_01
	regiments = {
		marine = { x = 0 y = 0 }
		marine = { x = 0 y = 1 }

		marine = { x = 1 y = 0 }
		marine = { x = 1 y = 1 }

		marine = { x = 2 y = 0 }
		marine = { x = 2 y = 1 }

		artillery_brigade = { x = 3 y = 0 }
		artillery_brigade = { x = 3 y = 1 }
		artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		anti_air = { x = 0 y = 0 }
		anti_tank = { x = 0 y = 1 }
		recon = { x = 0 y = 2 }
	}
	priority = 2
}
division_template = {
	name = "Armoured Reconnaissance Brigade"
	division_names_group = ENG_INF_01

	regiments = {
		motorized = { x = 0 y = 0 }
		motorized = { x = 0 y = 1 }
		motorized = { x = 1 y = 0 }
		motorized = { x = 1 y = 1 }
	}
	support = {
        light_tank_recon = { x = 0 y = 0 }
		maintenance_company = { x = 0 y = 1 }
		signal_company = { x = 0 y = 2 }
	}
}
division_template = {
	name = "Motor Division"
	division_names_group = ENG_INF_01

	regiments = {
		motorized = { x = 0 y = 0 }
		motorized = { x = 0 y = 1 }
		motorized = { x = 0 y = 2 }

        motorized = { x = 1 y = 0 }
		motorized = { x = 1 y = 1 }
		motorized = { x = 1 y = 2 }

		mot_artillery_brigade = { x = 2 y = 0 }
		mot_artillery_brigade = { x = 2 y = 1 }
	}
	support = {
       	engineer = { x = 0 y = 0 }
       	artillery = { x = 0 y = 1 }
		maintenance_company = { x = 0 y = 2 }
		anti_tank = { x = 0 y = 3 }
		signal_company = { x = 1 y = 0 }
        field_hospital = { x = 1 y = 1 }
	}
}
division_template = {
	name = "Armour Division" 				# 1st Armoured Division, 7th Armoured 'Desert
	division_names_group = ENG_ARM_01

	regiments = {
		light_armor = { x = 0 y = 0 }		# Cruiser Tanks Mks I, II
	    light_armor = { x = 0 y = 1 }
	    light_armor = { x = 0 y = 2 }
		medium_armor = { x = 1 y = 0 }
	    medium_armor = { x = 1 y = 1 }
		medium_armor = { x = 1 y = 2 }
		mechanized = { x = 2 y = 0 }
	    mechanized = { x = 2 y = 1 }
	}
	support = {
       	engineer = { x = 0 y = 0 }
		field_hospital = { x = 0 y = 1 }
		signal_company = { x = 0 y = 2 }
		maintenance_company = { x = 0 y = 3 }
		anti_air = { x = 1 y = 1 }
	}
}
division_template = {
	name = "Cavalry Brigade"
	division_names_group = ENG_CAV_02

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
	}
	support = {
        recon = { x = 0 y = 0 }
		anti_air = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Armoured Brigade"
	division_names_group = ENG_ARM_02
	regiments = {
		heavy_armor = { x = 0 y = 0 }
		heavy_armor = { x = 0 y = 1 }

		mechanized = { x = 1 y = 0 } #Added, nonexistent
	}
}
division_template = {
	name = "Colonial Garrison"
	division_names_group = ENG_COL_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		artillery_brigade = { x = 1 y = 0 }
	}
	support = {
       	engineer = { x = 0 y = 0 }
		anti_air = { x = 0 y = 1 }
		signal_company = { x = 0 y = 2 }
	}
	priority = 0
}

division_template = {
	name = "Airborne Division"
	division_names_group = ENG_PAR_01

	regiments = {
		paratrooper = { x = 0 y = 0 }
		paratrooper = { x = 0 y = 1 }
		paratrooper = { x = 0 y = 2 }

		paratrooper = { x = 1 y = 0 }
		paratrooper = { x = 1 y = 1 }
		paratrooper = { x = 1 y = 2 }

		paratrooper = { x = 2 y = 0 }
		paratrooper = { x = 2 y = 1 }
		paratrooper = { x = 2 y = 2 }

		paradrop_artillery_brigade = { x = 3 y = 0 }
		paradrop_artillery_brigade = { x = 3 y = 1 }
		paradrop_artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		maintenance_company = { x = 0 y = 0 }
		anti_air = { x = 0 y = 1 }
		engineer = { x = 0 y = 2 }
		signal_company = { x = 0 y = 3 }
	}
}


units = {
	# Ex-County Divisions
		division= {
			name = "76. Infantry Division"
			location = 3839
			division_template = "Infantry Division"
			start_equipment_factor = 0.8
			start_experience_factor = 0.1
		}
		division= {
			name = "77. Infantry Division"
			location = 3501
			division_template = "Infantry Division"
			start_equipment_factor = 0.8
			start_experience_factor = 0.1
		}
	#
	# Colonial Office #
		division= {
			name = "Cyprus Colonial Garrison"
			location = 7090
			division_template = "Colonial Garrison"
			start_equipment_factor = 0.7
			start_experience_factor = 0.1
		}
		division= {
			name = "Gibraltar Colonial Garrison"
			location = 8672
			division_template = "Colonial Garrison"
			start_equipment_factor = 0.7
			start_experience_factor = 0.1
		}
		division= {
			name = "Malta Colonial Garrison"
			location = 7736
			division_template = "Colonial Garrison"
			start_equipment_factor = 0.7
			start_experience_factor = 0.1
		}
		division= {
			name = "Newfoundland Colonial Garrison"
			location = 8661
			division_template = "Colonial Garrison"
			start_equipment_factor = 0.7
			start_experience_factor = 0.1
		}
		division= {
			name = "Palestine Colonial Garrison"
			location = 3430
			division_template = "Colonial Garrison"
			start_equipment_factor = 0.7
			start_experience_factor = 0.1
		}
	#
	# War Office #
		# Northern Command #
			# Northumbrian Area #
				division= {
					name = "25. Tank Brigade"
					location = 3951
					division_template = "Armoured Brigade"
					start_equipment_factor = 0.9
					start_experience_factor = 0.3
				}
			#
			# West Riding Area #
				division= {
					name = "46. Infantry Division"
					location = 2123
					division_template = "Infantry Division"
					start_equipment_factor = 0.9
					start_experience_factor = 0.3
				}
			#
		#
		# Eastern Command #
			# East Anglia Area #
				division= { #Disbanded after 1943.12.14
					name = "54. Infantry Division"
					location = 3839
					division_template = "Infantry Division"
					start_equipment_factor = 0.9
					start_experience_factor = 0.3
				}
			#
		#
		# Western Command #
			# Welsh Area #
				division= {
					name = "38. Infantry Division"
					location = 3662
					division_template = "Infantry Division"
					start_equipment_factor = 0.9
					start_experience_factor = 0.3
				}
				division= {
					name = "53. Infantry Division"
					location = 2133
					division_template = "Infantry Division"
					start_equipment_factor = 0.9
					start_experience_factor = 0.3
				}
			#
			# West Lancashire Area #
				division= {
					name = "59. Motor Division"
					location = 239
					division_template = "Motor Division"
					start_equipment_factor = 0.9
					start_experience_factor = 0.3
				}
			#
		#
		# Southern Command #
			# South Western Area #
				division= {
					name = "43. Infantry Division"
					location = 4659
					division_template = "Infantry Division"
					start_equipment_factor = 0.9
					start_experience_factor = 0.3
				}
				division= {
					name = "45. Infantry Division"
					location = 4659
					division_template = "Infantry Division"
					start_equipment_factor = 0.9
					start_experience_factor = 0.3
				}
			#
			# South Midland Area #
				division= {
					name = "61. Infantry Division"
					location = 1059
					division_template = "Infantry Division"
					start_equipment_factor = 0.9
					start_experience_factor = 0.3
				}
			#
		#
		# Scottish Command #
			# Highland Area #
				division= {
					name = "Scottish Cavalry Brigade"
					location = 1279
					division_template = "Cavalry Brigade"
					start_equipment_factor = 0.9
					start_experience_factor = 0.3
				}
			#
			# Lowland Area #
				division= {
					name = "15. Infantry Division"
					location = 11013
					division_template = "Infantry Division"
					start_equipment_factor = 0.9
					start_experience_factor = 0.3
				}
				division= {
					name = "52. Infantry Division"
					location = 11190
					division_template = "Infantry Division"
					start_equipment_factor = 0.9
					start_experience_factor = 0.3
				}
				division= {
					name = "Scottish Cavalry Brigade"
					location = 11013
					division_template = "Cavalry Brigade"
					start_equipment_factor = 0.9
					start_experience_factor = 0.3
				}
				division= {
					name = "Scottish Infantry Regiment"
					location = 11190
					division_template = "Infantry Regiment"
					start_equipment_factor = 0.9
					start_experience_factor = 0.3
				}
			#
		#
		# Northern Ireland District #
			division= {
				name = "Irish Infantry Regiment"
				location = 7502
				division_template = "Infantry Regiment"
				start_equipment_factor = 0.9
				start_experience_factor = 0.3
			}
		#
		# London District #
			division= {
				name = "56. London Infantry Division"
				location = 3430
				division_template = "Infantry Division"
				start_equipment_factor = 0.9
				start_experience_factor = 0.3
			}
			division= {
				name = "2. London Motor Division"
				location = 2527
				division_template = "Motor Division"
				start_equipment_factor = 0.9
				start_experience_factor = 0.3
			}
			division= {
				name = "London Infantry Regiment"
				location = 2527
				division_template = "Infantry Regiment"
				start_equipment_factor = 0.9
				start_experience_factor = 0.3
			}
			division= {
				name = "London Infantry Regiment"
				location = 2527
				division_template = "Infantry Regiment"
				start_equipment_factor = 0.9
				start_experience_factor = 0.3
			}
		#
		# Middle East Command #
			# Egypt #
                division= {
                    name = "7. Armoured Division"
                    location = 4413
                    division_template = "Armour Division"
                    start_equipment_factor = 0.7
                    start_experience_factor = 0.3
                }
                division= { #Disbanded in 1943.8
					name = "Royal Marine Division"
					location = 2527
					division_template = "Marine Division"
					start_equipment_factor = 0.9
					start_experience_factor = 0.3
				}
                division= {
                    name = "Egyptian Colonial Garrison"
                    location = 9435
                    division_template = "Colonial Garrison"
                    start_equipment_factor = 0.6
                    start_experience_factor = 0.1
                }
                division= {
                    name = "Egyptian Colonial Garrison"
                    location = 9452
                    division_template = "Colonial Garrison"
                    start_equipment_factor = 0.6
                    start_experience_factor = 0.1
                }
                division= {
                    name = "70. Infantry Division"
                    location = 3341
                    division_template = "Infantry Division"
                    start_equipment_factor = 0.7
                    start_experience_factor = 0.5
                }
                division= {
                    name = "2. New Zealand Infantry Division"
                    location = 9437
                    division_template = "Infantry Division"
                    start_equipment_factor = 0.7
                    start_experience_factor = 0.5
                }
                division= {
                    name = "10. Armoured Division"
                    location = 3963
                    division_template = "Armour Division"
                    start_equipment_factor = 0.7
                    start_experience_factor = 0.5
                }
			#
			# Palestine #
                division= {
                    name = "Jerusalem Colonial Garrison"
                    location = 7252
                    division_template = "Colonial Garrison"
                    start_equipment_factor = 1
                    start_experience_factor = 0.1
                }
                division= {
                    name = "Palestine Colonial Garrison"
                    location = 3862
                    division_template = "Colonial Garrison"
                    start_equipment_factor = 1
                    start_experience_factor = 0.1
                }
			#
		#

		# North West Expeditionary Force (Norway) #
			division= {
				name = "49. Infantry Division"
				location = 5442
				division_template = "Infantry Division"
				start_equipment_factor = 0.9
				start_experience_factor = 0.3
			}
		#

		# British Expeditionary Force (BEF) #
			# Corps 1 #
				division= {
					name = "1. Infantry Division"
					location = 3501
					division_template = "Infantry Division"
					start_equipment_factor = 0.5
					start_experience_factor = 0.5
				}
				division= {
					name = "48. Infantry Division"
					location = 3455
					division_template = "Infantry Division"
					start_equipment_factor = 0.5
					start_experience_factor = 0.5
				}
			#
			# Corps 2 #
				division= {
					name = "3. Infantry Division"
					location = 4659
					division_template = "Infantry Division"
					start_equipment_factor = 0.5
					start_experience_factor = 0.5
				}
				division= {
					name = "4. Infantry Division"
					location = 5442
					division_template = "Infantry Division"
					start_equipment_factor = 0.5
					start_experience_factor = 0.5
				}
				division= {
					name = "50. Motor Division"
					location = 3092
					division_template = "Motor Division"
					start_equipment_factor = 0.5
					start_experience_factor = 0.5
				}
			#
			# Corps 3 #
			#
			division= {
				name = "1. Armoured Division"
				location = 9442
				division_template = "Armour Division"
				start_equipment_factor = 0.9
				start_experience_factor = 0.3
			}
			division= {
				name = "6. Armoured Division"
				location = 2128
				division_template = "Armour Division"
				start_equipment_factor = 0.9
				start_experience_factor = 0.2
			}
			division= {#Disbanded 1944.7.31
				name = "9. Armoured Division"
				location = 3754
				division_template = "Armour Division"
				start_equipment_factor = 0.9
				start_experience_factor = 0.2
			}
			division= {
				name = "11. Armoured Division"
				location = 3501
				division_template = "Armour Division"
				start_equipment_factor = 0.9
				start_experience_factor = 0.2
			}
			division= {
				name = "Guards Armoured Division"
				location = 5397
				division_template = "Armour Division"
				start_equipment_factor = 0.9
				start_experience_factor = 0.2
			}
			division= {
				name = "42. Armoured Division"
				location = 2784
				division_template = "Armour Division"
				start_equipment_factor = 0.9
				start_experience_factor = 0.2
			}
			division= {
				name = "79. Armoured Division"
				location = 591
				division_template = "Armour Division"
				start_equipment_factor = 0.7
				start_experience_factor = 0.2
			}
		# Operation Husky divs #
            division= {
                name = "5. Infantry Division"
                location = 7127
                division_template = "Infantry Division"
                start_equipment_factor = 1
                start_experience_factor = 0.5
            }
            division= {
                name = "50. Infantry Division"
                location = 11240
                division_template = "Infantry Division"
                start_equipment_factor = 0.8
                start_experience_factor = 0.5
            }
            division= {
                name = "51. Infantry Division"
                location = 7139
                division_template = "Infantry Division"
                start_equipment_factor = 0.8
                start_experience_factor = 0.3
            }
            division= {
                name = "78. Infantry Division"
                location = 7125
                division_template = "Infantry Division"
                start_equipment_factor = 1
                start_experience_factor = 0.5
            }
			division= { 
				name = "Royal Marine Division"
				location = 7148
				division_template = "Marine Division"
				start_equipment_factor = 0.9
				start_experience_factor = 0.3
			}
			division= {
				name = "1. Airborne Division"
				location = 2331
				division_template = "Airborne Division"
				start_equipment_factor = 0.9
				start_experience_factor = 0.3
			}
			division= {
				name = "8. Armoured Division"
				location = 7127
				division_template = "Armour Division"
				start_equipment_factor = 0.5
				start_experience_factor = 0.4
			}
}


air_wings = {
	### RAF Fighter Command ###
	325 = {
		### No. 11 Fighter Command, Middlesex area -- Hawker Demons, Furys, Gloster Gauntlets
		fighter_equipment_0 = {
			owner = "ENG"
			amount = 216
		}
		### No. 11 Fighter Command, Essex area -- Hawker Demons, Furys, Gloster Gauntlets
		fighter_equipment_0 = {
			owner = "ENG"
			amount = 144
		}
		### No. 12 Fighter Command, Nottingham area -- Hawker Demons, Furys, Gloster Gauntlets
		fighter_equipment_0 = {
			owner = "ENG"
			amount = 96
		}
		### No. 13 Fighter Command, Newcastle -- Hawker Demons, Furys, Gloster Gauntlets
		fighter_equipment_0 = {
			owner = "ENG"
			amount = 72
		}
		### No. 1 Bomber Command, East Anglia area -- Fairey Gordons, Battles
		CAS_equipment_1 = {
			owner = "ENG"
			amount = 64
		}
		### No. 2 Bomber Command, East Anglia area -- HP Heyford, Harrows, AW Whitleys
		tac_bomber_equipment_0 = {
			owner = "ENG"
			amount = 48
		}
		### No. 3 Bomber Command, Suffolk area -- HP Heyford, Harrows, AW Whitleys
		tac_bomber_equipment_0 = {
			owner = "ENG"
			amount = 64
		}
		### No. 4 Bomber Command, York area -- HP Heyford, Harrows, AW Whitleys
		tac_bomber_equipment_0 = {
			owner = "ENG"
			amount = 80
		}
		### No. 5 Bomber Command, Lincoln area -- Fairey Gordons, Battles
		CAS_equipment_1 = {
			owner = "ENG"
			amount = 64
		}
		### No. 16 Coastal Command -- Vickers Vildebeests
		nav_bomber_equipment_1 = {
			owner = "ENG"
			amount = 24
		}
	}

	### RAF Middle East and Africa
	325 = {
		### AHQ Egypt
		fighter_equipment_0 = {
			owner = "ENG"
			amount = 72
		}
		tac_bomber_equipment_0 = {
			owner = "ENG"
			amount = 48
		}
		CAS_equipment_1 = {
			owner = "ENG"
			amount = 64
		}
		### RAF Palestine & Iraq
		CAS_equipment_1 = {
			owner = "ENG"
			amount = 32
		}
		### RAF Aden
		fighter_equipment_0 = {
			owner = "ENG"
			amount = 24
		}
		CAS_equipment_1 = {
			owner = "ENG"
			amount = 16
		}
		### RAF Far East
		nav_bomber_equipment_1 = {
			owner = "ENG"
			amount = 24
		}
		CAS_equipment_1 = {
			owner = "ENG"
			amount = 64
		}
	}
}

	#########################
	## STARTING PRODUCTION ##
	#########################
instant_effect = {


	add_equipment_production = {
		equipment = {
			type = infantry_equipment_1
			creator = "ENG"
		}
		requested_factories = 3
		progress = 0.1
		efficiency = 50
	}
	add_equipment_production = {
		equipment = {
			type = artillery_equipment_1
			creator = "ENG"
		}
		requested_factories = 2
		progress = 0.1
		efficiency = 50
	}

	add_equipment_production = {
		equipment = {
			type = support_equipment_1
			creator = "ENG"
		}
		requested_factories = 1
		progress = 0.3
		efficiency = 50
	}

	add_equipment_production = {
		equipment = {
			type = light_tank_chassis_1
			creator = "ENG"
			version_name = "Light Tank Mk. VI"
		}
		requested_factories = 1
		progress = 0.4
		efficiency = 50
	}

	add_equipment_production = {
		equipment = {
			type = fighter_equipment_0
			creator = "ENG"
		}
		requested_factories = 1
		progress = 0.15
		efficiency = 50
	}

	add_equipment_production = {
		equipment = {
			type = tac_bomber_equipment_0
			creator = "ENG"
		}
		requested_factories = 1
		progress = 0.1
		efficiency = 50
	}
}
	#####################
