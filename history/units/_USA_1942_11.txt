﻿start_equipment_factor = 0.1
##### Division Templates #####
division_template = {
	name = "Infantry Division"  	# Standard formation for frontline divisions

	division_names_group = USA_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }

		artillery_brigade = { x = 3 y = 0 }
		artillery_brigade = { x = 3 y = 1 }
		artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		artillery = { x = 0 y = 0 }  # US Hvy Field Arty consisted of 2 Bns, 155mm howitzers
		engineer = { x = 0 y = 1 }   # US Eng Rgt consisted of 2 Bns
		field_hospital = { x = 0 y = 2 }
		anti_tank = { x = 0 y = 3 }
		armored_car_recon = { x = 1 y = 0 }
		signal_company = { x = 1 y = 1 }
	}
}
division_template = {
	name = "Cavalry Division"  	# 1st Cavalry Division only

	division_names_group = USA_CAV_01

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		cavalry = { x = 1 y = 2 }
		cavalry = { x = 2 y = 0 }
		cavalry = { x = 2 y = 1 }

		mot_artillery_brigade = { x = 3 y = 0 }
		mot_artillery_brigade = { x = 3 y = 1 }
		mot_artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		light_tank_recon = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }   # US Eng Rgt consisted of 2 Bns
		field_hospital = { x = 0 y = 2 }
		signal_company = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Armored Division"

	division_names_group = USA_ARM_01

	regiments = {
		light_armor = { x = 0 y = 0 }
		medium_armor = { x = 0 y = 1 }
		medium_armor = { x = 0 y = 2 }

		light_armor = { x = 1 y = 0 }
		medium_armor = { x = 1 y = 1 }
		medium_armor = { x = 1 y = 2 }

		mechanized = { x = 2 y = 0 }
		mechanized = { x = 2 y = 1 }
		mechanized = { x = 2 y = 2 }
	}
	support = {
		light_tank_recon = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }   # US Eng Rgt consisted of 2 Bns
		field_hospital = { x = 0 y = 2 }
		signal_company = { x = 0 y = 3 }
		artillery = { x = 1 y = 0 }
		maintenance_company = { x = 1 y = 1 }
	}
}

division_template = {
	name = "Airborne Division"
	division_names_group = USA_PAR_01

	regiments = {
		paratrooper = { x = 0 y = 0 }
		paratrooper = { x = 0 y = 1 }
		paratrooper = { x = 0 y = 2 }

		paratrooper = { x = 1 y = 0 }
		paratrooper = { x = 1 y = 1 }
		paratrooper = { x = 1 y = 2 }

		paratrooper = { x = 2 y = 0 }
		paratrooper = { x = 2 y = 1 }
		paratrooper = { x = 2 y = 2 }

		paradrop_artillery_brigade = { x = 3 y = 0 }
		paradrop_artillery_brigade = { x = 3 y = 1 }
		paradrop_artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		maintenance_company = { x = 0 y = 0 }
		anti_air = { x = 0 y = 1 }
		engineer = { x = 0 y = 2 }
		signal_company = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Marine Brigade" 		# 1st, 2nd Marine Brigades, precursors to 1st, 2nd USMC Divisions

	division_names_group = USA_MAR_01

	regiments = {
		marine = { x = 0 y = 0 }
		marine = { x = 0 y = 1 }
		marine = { x = 1 y = 0 }
		marine = { x = 1 y = 1 }
	}
	support = {
		engineer = { x = 0 y = 0 }
	}
	priority = 2
}
division_template = {
	name = "Garrison Brigade"  		# used for island defense (older equipment, lower experience)

	division_names_group = USA_GAR_01		# Brigade level irregular unit, so no default names

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }
	}
	priority = 0
}


###### OOB ######
units = {
	###North Africa###
		division= {
			division_name = {
				is_name_ordered = yes
				name_order = 1
			}
			location = 7277
			division_template = "Infantry Division"
			start_experience_factor = 0.4
			start_equipment_factor = 0.8
		}
		division= {
			division_name = {
				is_name_ordered = yes
				name_order = 3
			}
			location = 7257
			division_template = "Infantry Division"
			start_experience_factor = 0.4
			start_equipment_factor = 0.8
		}
		division= {
			division_name = {
				is_name_ordered = yes
				name_order = 9
			}
			location = 809
			division_template = "Infantry Division"
			start_experience_factor = 0.4
			start_equipment_factor = 0.8
		}
		division= {
			division_name = {
				is_name_ordered = yes
				name_order = 34
			}
			location = 7269
			division_template = "Infantry Division"
			start_experience_factor = 0.4
			start_equipment_factor = 0.8
		}
		division= {
			division_name = {
				is_name_ordered = yes
				name_order = 1
			}
			location = 7298
			division_template = "Armored Division"
			start_experience_factor = 0.4
			start_equipment_factor = 0.8
		}
	#
	# Army 1 #
		# Corps 1 #
			division= {
				division_name = {
					is_name_ordered = yes
					name_order = 8
				}
				location = 7502
				division_template = "Infantry Division"
				start_experience_factor = 0.2
				start_equipment_factor = 0.6
			}
			division= {
				division_name = {
					is_name_ordered = yes
					name_order = 30
				}
				location = 8634
				division_template = "Infantry Division"
				start_experience_factor = 0.2
			}
		#
		# Corps 2 #
			division= {
				division_name = {
					is_name_ordered = yes
					name_order = 28
				}
				location = 8634
				division_template = "Infantry Division"
				start_experience_factor = 0.2
			}
			division= {
				division_name = {
					is_name_ordered = yes
					name_order = 29
				}
				location = 11190
				division_template = "Infantry Division"
				start_experience_factor = 0.2
			}
			division= {
				division_name = {
					is_name_ordered = yes
					name_order = 44
				}
				location = 8634
				division_template = "Infantry Division"
				start_experience_factor = 0.2
			}
		#
		# Corps 6 #
			division= {
				division_name = {
					is_name_ordered = yes
					name_order = 26
				}
				location = 8634
				division_template = "Infantry Division"
				start_experience_factor = 0.2
			}
		#
	#
	# Army 2 #
		# Corps 7 #
			division= {
				division_name = {
					is_name_ordered = yes
					name_order = 35
				}
				location = 8634
				division_template = "Infantry Division"
				start_experience_factor = 0.2
			}
		#
		division= { #disbanded 1944
			division_name = {
				is_name_ordered = yes
				name_order = 2
			}
			location = 8634
			division_template = "Cavalry Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.6
		}
		division= {
			division_name = {
				is_name_ordered = yes
				name_order = 5
			}
			location = 1285
			division_template = "Infantry Division"
			start_experience_factor = 0.2
		}
	#
	# Army 3 #
		# Corps 4 #
			division= {
				division_name = {
					is_name_ordered = yes
					name_order = 4
				}
				location = 8634
				division_template = "Infantry Division"
				start_experience_factor = 0.2
			}
		#
		# Corps 8 #
			division= {
				division_name = {
					is_name_ordered = yes
					name_order = 2
				}
				location = 8634
				division_template = "Infantry Division"
				start_experience_factor = 0.2
			}
			division= { #1943 North Africa
				division_name = {
					is_name_ordered = yes
					name_order = 36
				}
				location = 8634
				division_template = "Infantry Division"
				start_experience_factor = 0.2
			}
			division= { # 1943 Sicily
				division_name = {
					is_name_ordered = yes
					name_order = 45
				}
				location = 8634
				division_template = "Infantry Division"
				start_experience_factor = 0.2
			}
		#
	#
	# Army Armor #
		# Corps Armor 1 #
			division= { #1943 Husky
				division_name = {
					is_name_ordered = yes
					name_order = 2
				}
				location = 7137
				division_template = "Armored Division"
				start_experience_factor = 0.2
			}
		#
		division= {#1944 Somerset
			division_name = {
				is_name_ordered = yes
				name_order = 3
			}
			location = 8634
			division_template = "Armored Division"
			start_experience_factor = 0.2
		}
		division= { #1944 England
			division_name = {
				is_name_ordered = yes
				name_order = 4
			}
			location = 8634
			division_template = "Armored Division"
			start_experience_factor = 0.2
		}
	#

	division= { #1943 Sicily
		division_name = {
			is_name_ordered = yes
			name_order = 45
		}
		location = 8634
		division_template = "Infantry Division"
		start_experience_factor = 0.2
	}

	# 1942 #
		division= { #1943 Sicily
			division_name = {
				is_name_ordered = yes
				name_order = 82
			}
			location = 8634
			division_template = "Airborne Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= {
			division_name = {
				is_name_ordered = yes
				name_order = 101
			}
			location = 8634
			division_template = "Airborne Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}

		division= { # 1944 UK
			division_name = {
				is_name_ordered = yes
				name_order = 5
			}
			location = 8634
			division_template = "Armored Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { # 1944 UK
			division_name = {
				is_name_ordered = yes
				name_order = 6
			}
			location = 8634
			division_template = "Armored Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { #1944 UK
			division_name = {
				is_name_ordered = yes
				name_order = 7
			}
			location = 8634
			division_template = "Armored Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { #1944.12 UK
			division_name = {
				is_name_ordered = yes
				name_order = 8
			}
			location = 8634
			division_template = "Armored Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { #1944.6 England
			division_name = {
				is_name_ordered = yes
				name_order = 9
			}
			location = 8634
			division_template = "Armored Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { #1944.12 France
			division_name = {
				is_name_ordered = yes
				name_order = 10
			}
			location = 8634
			division_template = "Armored Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { #1944.12 Normandy
			division_name = {
				is_name_ordered = yes
				name_order = 11
			}
			location = 8634
			division_template = "Armored Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { #1944.12 Normandy
			division_name = {
				is_name_ordered = yes
				name_order = 12
			}
			location = 8634
			division_template = "Armored Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { # 1945 France
			division_name = {
				is_name_ordered = yes
				name_order = 13
			}
			location = 8634
			division_template = "Armored Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}

		division= { #1944.12 England
			division_name = {
				is_name_ordered = yes
				name_order = 76
			}
			location = 8634
			division_template = "Infantry Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { #1944.12 Roetgen Germany
			division_name = {
				is_name_ordered = yes
				name_order = 78
			}
			location = 8634
			division_template = "Infantry Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { # England 1944
			division_name = {
				is_name_ordered = yes
				name_order = 79
			}
			location = 8634
			division_template = "Infantry Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { # England 1944
			division_name = {
				is_name_ordered = yes
				name_order = 80
			}
			location = 8634
			division_template = "Infantry Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { # 1944.6 Wales
			division_name = {
				is_name_ordered = yes
				name_order = 83
			}
			location = 8634
			division_template = "Infantry Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { # 1944.12 Verdenne
			division_name = {
				is_name_ordered = yes
				name_order = 84
			}
			location = 8634
			division_template = "Infantry Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { #1944 Rome
			division_name = {
				is_name_ordered = yes
				name_order = 85
			}
			location = 8634
			division_template = "Infantry Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { #1944 Rome
			division_name = {
				is_name_ordered = yes
				name_order = 88
			}
			location = 8634
			division_template = "Infantry Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { #1945 Echternach
			division_name = {
				is_name_ordered = yes
				name_order = 89
			}
			location = 8634
			division_template = "Infantry Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { # 1944 England
			division_name = {
				is_name_ordered = yes
				name_order = 90
			}
			location = 8634
			division_template = "Infantry Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { #1944 Rome
			division_name = {
				is_name_ordered = yes
				name_order = 91
			}
			location = 8634
			division_template = "Infantry Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { #1944 Sicily
			division_name = {
				is_name_ordered = yes
				name_order = 92
			}
			location = 8634
			division_template = "Infantry Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { # 1944 England
			division_name = {
				is_name_ordered = yes
				name_order = 94
			}
			location = 8634
			division_template = "Infantry Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { #1944.12 Saarlautern
			division_name = {
				is_name_ordered = yes
				name_order = 95
			}
			location = 8634
			division_template = "Infantry Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { #1944.12 Elsenborn
			division_name = {
				is_name_ordered = yes
				name_order = 99
			}
			location = 8634
			division_template = "Infantry Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { # 1944.12 Bitche
			division_name = {
				is_name_ordered = yes
				name_order = 100
			}
			location = 8634
			division_template = "Infantry Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { # 1944.12 Wurm
			division_name = {
				is_name_ordered = yes
				name_order = 102
			}
			location = 8634
			division_template = "Infantry Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { #1944.12 Sarreguemines
			division_name = {
				is_name_ordered = yes
				name_order = 103
			}
			location = 8634
			division_template = "Infantry Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
		division= { #1944.12 Duren Germany
			division_name = {
				is_name_ordered = yes
				name_order = 104
			}
			location = 8634
			division_template = "Infantry Division"
			start_experience_factor = 0.1
			start_equipment_factor = 0.8
		}
	#
}


air_wings = {
	1071 = { 															# Virginia
		### 2nd USAAF Wing (CO: Krogstad) ###
		heavy_fighter_equipment_1 =  { owner = "USA" amount = 168 }		# 1st Pursuit Group -- P-35s
				# 8th Pursuit Group -- P-35s
		strat_bomber_equipment_1 = { owner = "USA" amount = 36 }		# 9th Bombardment Group -- B-17s
		tac_bomber_equipment_0 = { owner = "USA" amount = 48 }			# 2nd Bombardment Group -- B-18s
		### USN Patrol Wings ###
		nav_bomber_equipment_1 =  { owner = "USA" amount = 36 }			# Naval Patrol Wing 5 -- PBY-1
		cv_fighter_equipment_1 =  { owner = "USA" amount = 12 }			# 1st/I Marine Air Wing -- F3F Grumman
		cv_CAS_equipment_1 =  { owner = "USA" amount = 12 }				# 1st/II Marine Air Wing -- SB2U Vindicator
		cv_nav_bomber_equipment_1 =  { owner = "USA" amount = 12 }		# 1st/II Marine Air Wing -- TBD Devastator												# Louisiana
		### 3rd USAAF Wing (CO: Martin) ###
		CAS_equipment_2 = { owner = "USA" amount = 63 }					# 3rd Attack Group -- A-20s
		### 19th Composite Wing (CO: Dargue) ###
		fighter_equipment_0 =  { owner = "USA" amount = 48 }			# 16th Pursuit Group -- P-26As
		tac_bomber_equipment_0 = { owner = "USA" amount = 36 }			# 6th/I Composite Group -- B-18s
		CAS_equipment_1 = { owner = "USA" amount = 21 }					# 6th/II Composite Group -- A-17s
		### USN Patrol Wings ###
		nav_bomber_equipment_1 =  { owner = "USA" amount = 36 }			# Naval Patrol Wing 3 -- PBY-1
		### USN Patrol Wings ###
		nav_bomber_equipment_1 =  { owner = "USA" amount = 30 }			# Naval Patrol Wing 4 -- PBY-1
	}
}



#########################
## STARTING PRODUCTION ##
#########################
instant_effect = {

	add_equipment_production = {
		equipment = {
			type = infantry_equipment_1
			creator = "USA"
		}
		requested_factories = 6
		progress = 0.1
		efficiency = 100
	}

	add_equipment_production = {
		equipment = {
			type = support_equipment_1
			creator = "USA"
		}
		requested_factories = 2
		progress = 0.3
		efficiency = 100
	}

	add_equipment_production = {
		equipment = {
			type = artillery_equipment_1
			creator = "USA"
		}
		requested_factories = 1
		progress = 0.3
		efficiency = 100
	}

	add_equipment_production = {
		equipment = {
			type = cv_fighter_equipment_1
			creator = "USA"
		}
		requested_factories = 1
		progress = 0.15
		efficiency = 100
	}
}
#####################
