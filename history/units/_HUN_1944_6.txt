﻿division_template = {
	name = "Könnyű Gyalogsági Hadosztály"				# Infantry Brigade
	division_names_group = HUN_LTINF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		artillery_brigade = { x = 2 y = 0 }
		artillery_brigade = { x = 2 y = 1 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		signal_company = { x = 0 y = 1 }
		anti_air = { x = 0 y = 2 }
       	field_hospital = { x = 0 y = 3 }
       	recon = { x = 1 y = 0 }
	}
}
division_template = {
	name = "Gyalogsági Hadosztály"				# Infantry Brigade
	division_names_group = HUN_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }

		artillery_brigade = { x = 3 y = 0 }
		artillery_brigade = { x = 3 y = 1 }
		artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		signal_company = { x = 0 y = 1 }
		anti_air = { x = 0 y = 2 }
       	field_hospital = { x = 0 y = 3 }
       	recon = { x = 1 y = 0 }
	}
}

division_template = {
	name = "Huszár Hadosztály" 					# Cavalry Brigade
	division_names_group = HUN_CAV_01
	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }

		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		cavalry = { x = 1 y = 2 }

		bicycle_battalion = { x = 2 y = 0 }
		mot_artillery_brigade = { x = 2 y = 1 }
		mot_artillery_brigade = { x = 2 y = 2 }

		medium_armor = { x = 3 y = 0 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		signal_company = { x = 0 y = 1 }
		anti_air = { x = 0 y = 2 }
       	field_hospital = { x = 0 y = 3 }
		maintenance_company = { x = 1 y = 0 }
		light_tank_recon = { x = 1 y = 1 }
		artillery = { x = 1 y = 2 }
	}
}

division_template = {
	name = "Páncélos Hadosztály" 			# Motorized Brigade

	division_names_group = HUN_ARM_01

	regiments = {
		motorized = { x = 0 y = 0 }
		motorized = { x = 0 y = 1 }
		motorized = { x = 0 y = 2 }

		medium_armor = { x = 1 y = 0 }
		medium_armor = { x = 1 y = 1 }
		medium_armor = { x = 1 y = 2 }

		mot_artillery_brigade = { x = 2 y = 0 }
		mot_artillery_brigade = { x = 2 y = 1 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		signal_company = { x = 0 y = 1 }
		light_sp_anti_air_brigade = { x = 0 y = 2 }
		light_tank_recon = { x = 0 y = 3 }
		maintenance_company = { x = 1 y = 0 }
		anti_tank = { x = 1 y = 1 }
       	field_hospital = { x = 1 y = 2 }
	}
}

division_template = {
	name = "Hegymászó Dandár"
	division_names_group = HUN_MTN_01

	regiments = {
		mountaineers = { x = 0 y = 0 }
	    mountaineers = { x = 0 y = 1 }
	    mountaineers = { x = 0 y = 2 }
		mountaineers = { x = 1 y = 0 }
	    mountaineers = { x = 1 y = 1 }

		artillery_brigade = { x = 2 y = 0 }
		artillery_brigade = { x = 2 y = 1 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		signal_company = { x = 0 y = 1 }
		anti_tank = { x = 0 y = 2 }
		field_hospital = { x = 0 y = 3 }
		recon = { x = 1 y = 0 }
	}
}

units = {
	# Field Replacement Army #
		division= {
			name = "2. Field Replacement Division"
			location = 1813
			division_template = "Gyalogsági Hadosztály"
			start_experience_factor = 0.2
			start_equipment_factor = 0.6
		}
		division= {
			name = "3. Field Replacement Division"
			location = 1813
			division_template = "Gyalogsági Hadosztály"
			start_experience_factor = 0.2
			start_equipment_factor = 0.6
		}
		division= {
			name = "4. Field Replacement Division"
			location = 1813
			division_template = "Gyalogsági Hadosztály"
			start_experience_factor = 0.2
			start_equipment_factor = 0.6
		}
		division= {
			name = "5. Field Replacement Division"
			location = 1813
			division_template = "Gyalogsági Hadosztály"
			start_experience_factor = 0.2
			start_equipment_factor = 0.6
		}
		division= {
			name = "6. Field Replacement Division"
			location = 1813
			division_template = "Gyalogsági Hadosztály"
			start_experience_factor = 0.2
			start_equipment_factor = 0.6
		}
		division= {
			name = "2. Hegymászó Csere Dandár"
			location = 9567
			division_template = "Hegymászó Dandár"
			start_experience_factor = 0.2
			start_equipment_factor = 0.5
		}
		division= {
			name = "1. Hegymászó Csere Dandár"
			location = 9567
			division_template = "Hegymászó Dandár"
			start_experience_factor = 0.2
			start_equipment_factor = 0.5
		}
	#
	# VKF (General Staff) #
		division= {
			name = "1. Páncélos Hadosztály"
			location = 553
			division_template = "Páncélos Hadosztály"
			start_experience_factor = 0.4
			start_equipment_factor = 0.8
		}
		division= {
			name = "6. Gyalogsági Hadosztály"
			location = 1506
			division_template = "Gyalogsági Hadosztály"
			start_experience_factor = 0.4
			start_equipment_factor = 0.8
		}
		division= {
			name = "7. Gyalogsági Hadosztály"
			location = 96
			division_template = "Gyalogsági Hadosztály"
			start_experience_factor = 0.4
			start_equipment_factor = 0.8
		}
		division= {
			name = "1. Huszár Hadosztály"
			location = 1440
			division_template = "Huszár Hadosztály"
			start_experience_factor = 0.4
			start_equipment_factor = 0.8
		}
		division= {
			name = "10. Gyalogsági Hadosztály"
			location = 999
			division_template = "Gyalogsági Hadosztály"
			start_experience_factor = 0.4
			start_equipment_factor = 0.8
		}
		division= {
			name = "13. Gyalogsági Hadosztály"
			location = 9536
			division_template = "Gyalogsági Hadosztály"
			start_experience_factor = 0.4
			start_equipment_factor = 0.8
		}
	#
	# Corps 3 (VKF) #
		division= {
			name = "2. Páncélos Hadosztály"
			location = 9458
			division_template = "Páncélos Hadosztály"
			start_experience_factor = 0.4
			start_equipment_factor = 0.8
		}
	#

	# Corps 6 (VFK) #
		division= {
			name = "18. Gyalogsági Hadosztály"
			location = 9534
			division_template = "Gyalogsági Hadosztály"
			start_experience_factor = 0.2
			start_equipment_factor = 0.6
		}
		division= {
			name = "19. Gyalogsági Hadosztály"
			location = 9534
			division_template = "Gyalogsági Hadosztály"
			start_experience_factor = 0.2
			start_equipment_factor = 0.6
		}
		division= {
			name = "27. Könnyű Gyalogsági Hadosztály"
			location = 631
			division_template = "Könnyű Gyalogsági Hadosztály"
			start_experience_factor = 0.4
			start_equipment_factor = 0.6
		}
		division= {
			name = "2. Hegymászó Dandár"
			location = 631
			division_template = "Hegymászó Dandár"
			start_experience_factor = 0.4
			start_equipment_factor = 0.5
		}
		division= {
			name = "1. Hegymászó Dandár"
			location = 631
			division_template = "Hegymászó Dandár"
			start_experience_factor = 0.4
			start_equipment_factor = 0.5
		}
	#

	# Corps 7 (VFK) #
		division= {
			name = "16. Könnyű Gyalogsági Hadosztály"
			location = 9534
			division_template = "Könnyű Gyalogsági Hadosztály"
			start_experience_factor = 0.4
			start_equipment_factor = 0.8
		}
		division= {
			name = "66. Határőr"
			location = 9534
			division_template = "Könnyű Gyalogsági Hadosztály"
			start_experience_factor = 0.3
			start_equipment_factor = 0.6
		}
	#
	# Reserve Corps 2 (Eastern front, Heeresgruppe Mitte Army 2) #
		division= {
			name = "5. Gyalogsági Hadosztály"
			location = 5357
			division_template = "Gyalogsági Hadosztály"
			start_experience_factor = 0.4
			start_equipment_factor = 0.6
		}
		division= {
			name = "12. Gyalogsági Hadosztály"
			location = 5357
			division_template = "Gyalogsági Hadosztály"
			start_experience_factor = 0.4
			start_equipment_factor = 0.6
		}
		division= {
			name = "23. Gyalogsági Hadosztály"
			location = 5357
			division_template = "Gyalogsági Hadosztály"
			start_experience_factor = 0.4
			start_equipment_factor = 0.6
		}
		division= {
			name = "9. Gyalogsági Hadosztály"
			location = 5357
			division_template = "Gyalogsági Hadosztály"
			start_experience_factor = 0.4
			start_equipment_factor = 0.6
		}
	#
	# Corps 9 (Heeresgruppe Nordukraine) #
		division= {
			name = "24. Könnyű Gyalogsági Hadosztály"
			location = 9507
			division_template = "Könnyű Gyalogsági Hadosztály"
			start_experience_factor = 0.2
			start_equipment_factor = 0.6
		}
		division= {
			name = "25. Gyalogsági Hadosztály"
			location = 9507
			division_template = "Gyalogsági Hadosztály"
			start_experience_factor = 0.2
			start_equipment_factor = 0.6
		}
		division= {
			name = "20. Gyalogsági Hadosztály"
			location = 631
			division_template = "Gyalogsági Hadosztály"
			start_experience_factor = 0.4
			start_equipment_factor = 0.6
		}
	#

	# Székler Command #
		division= {
			name = "9. Határőr"
			location = 9565
			division_template = "Hegymászó Dandár"
			start_experience_factor = 0.3
			start_equipment_factor = 0.8
		}
		division= {
			name = "68, 69. Határőr"
			location = 3831
			division_template = "Könnyű Gyalogsági Hadosztály"
			start_experience_factor = 0.3
			start_equipment_factor = 0.8
		}
	#
}

### Magyar Légiero ###
air_wings = {
	43 = {
		fighter_equipment_0 = { owner = "HUN" amount = 36 }		# 1. Légi Ezred -- Fiat CR.32 fighters
		fighter_equipment_1 = { owner = "HUN" amount = 36 }		# 2. Légi Ezred -- Fiat CR.42 fighters
	}
	154 = {
		tac_bomber_equipment_1 = { owner = "HUN" amount = 54 }			# 3. Légi Ezred -- Ju-86K, WM-16 Budapest bombers
	}
}

### Starting Production ###
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_1
			creator = "HUN"
		}
		requested_factories = 1
		progress = 0.82
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = artillery_equipment_1
			creator = "HUN"
		}
		requested_factories = 1
		progress = 0.5
		efficiency = 50
	}
	add_equipment_production = {
		equipment = {
			type = support_equipment_1
			creator = "HUN"
		}
		requested_factories = 1
		progress = 0.6
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = motorized_equipment_1
			creator = "HUN"
		}
		requested_factories = 1
		progress = 0.4
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = CAS_equipment_2
			creator = "HUN"
		}
		requested_factories = 1
		progress = 0.47
		efficiency = 100
	}
}
