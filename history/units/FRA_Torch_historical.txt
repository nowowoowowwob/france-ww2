﻿###### OOB ######
division_template = {
	name = "Partisans"

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
	}
	priority = 1
}


units = {
	#CASABLANCA
	division= {
		name = "Partisans"
		location = 7137
		division_template = "Partisans"
		start_experience_factor = 0.1
	}
	division= {
		name = "Partisans"
		location = 1158
		division_template = "Partisans"
		start_experience_factor = 0.1
	}
	division= {
		name = "Partisans"
		location = 7175
		division_template = "Partisans"
		start_experience_factor = 0.1
	}
	division= {
		name = "Partisans"
		location = 7175
		division_template = "Partisans"
		start_experience_factor = 0.1
	}
	division= {
		name = "Partisans"
		location = 7137
		division_template = "Partisans"
		start_experience_factor = 0.1
	}
	division= {
		name = "Partisans"
		location = 7174
		division_template = "Partisans"
		start_experience_factor = 0.1
	}
	division= {
		name = "Partisans"
		location = 7192
		division_template = "Partisans"
		start_experience_factor = 0.1
	}
	division= {
		name = "Partisans"
		location = 7233
		division_template = "Partisans"
		start_experience_factor = 0.1
	}
	division= {
		name = "Partisans"
		location = 4203
		division_template = "Partisans"
		start_experience_factor = 0.1
	}
	division= {
		name = "Partisans"
		location = 7253
		division_template = "Partisans"
		start_experience_factor = 0.1
	}

	#ORAN
	division= {
		name = "Partisans"
		location = 3485
		division_template = "Partisans"
		start_experience_factor = 0.1
	}
	division= {
		name = "Partisans"
		location = 7228
		division_template = "Partisans"
		start_experience_factor = 0.1
	}
	division= {
		name = "Partisans"
		location = 2123
		division_template = "Partisans"
		start_experience_factor = 0.1
	}
	division= {
		name = "Partisans"
		location = 2123
		division_template = "Partisans"
		start_experience_factor = 0.1
	}
	division= {
		name = "Partisans"
		location = 7112
		division_template = "Partisans"
		start_experience_factor = 0.1
	}
}
#####################
