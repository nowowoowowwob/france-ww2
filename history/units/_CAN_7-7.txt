units = {
	# 4 Canadian infantry
		division= {
			name = "4. Infantry Regiment, 2. Canadian Infantry Division"
			location = 10150
			division_template = "British Infantry Regiment"
			start_experience_factor = 0.3
		}
		division= {
			name = "5. Infantry Regiment, 2. Canadian Infantry Division"
			location = 10150
			division_template = "British Infantry Regiment"
			start_experience_factor = 0.3
		}
		division= {
			name = "6. Infantry Regiment, 2. Canadian Infantry Division"
			location = 10150
			division_template = "British Infantry Regiment"
			start_experience_factor = 0.3
		}
	#
}
