﻿division_template = {
	name = "Pekhotna Divizija"			# Infantry Division

	division_names_group = BUL_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }

		artillery_brigade = { x = 3 y = 0 }
		artillery_brigade = { x = 3 y = 1 }
		artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		artillery = { x = 0 y = 1 }
		signal_company = { x = 0 y = 2 }
		recon = { x = 0 y = 3 }
		anti_air = { x = 1 y = 0 }
	}
}

division_template = {
	name = "Konna Divizija" 			# Cavalry Division

	division_names_group = BUL_CAV_01

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		motorized = { x = 2 y = 0 }
		motorized = { x = 2 y = 1 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		artillery = { x =0 y = 1 }
		recon = { x = 0 y = 2 }
	}
}

units = {
	# Corps 1 #
		division= {
			division_name = {
				is_name_ordered = yes
				name_order = 1
			}
			location = 11174
			division_template = "Pekhotna Divizija"
			start_equipment_factor = 0.7
			start_experience_factor = 0.3
		}
		division= {
			division_name = {
				is_name_ordered = yes
				name_order = 7
			}
			location = 4314
			division_template = "Pekhotna Divizija"
			start_equipment_factor = 0.7
			start_experience_factor = 0.3
		}
	#
	# Corps 2 #
		division= {
			division_name = {
				is_name_ordered = yes
				name_order = 2
			}
			location = 2579
			division_template = "Pekhotna Divizija"
			start_equipment_factor = 0.7
			start_experience_factor = 0.3
		}
		division= {
			division_name = {
				is_name_ordered = yes
				name_order = 8
			}
			location = 6628
			division_template = "Pekhotna Divizija"
			start_equipment_factor = 0.7
			start_experience_factor = 0.3
		}
		division= {
			division_name = {
				is_name_ordered = yes
				name_order = 10
			}
			location = 2543
			division_template = "Pekhotna Divizija"
			start_equipment_factor = 0.7
			start_experience_factor = 0.3
		}
	#
	# Corps 3 #
		division= {
			division_name = {
				is_name_ordered = yes
				name_order = 3
			}
			location = 248
			division_template = "Pekhotna Divizija"
			start_equipment_factor = 0.7
			start_experience_factor = 0.3
		}
		division= {
			division_name = {
				is_name_ordered = yes
				name_order = 4
			}
			location = 9967
			division_template = "Pekhotna Divizija"
			start_equipment_factor = 0.7
			start_experience_factor = 0.3
		}
	#
	# Corps 4 #
		division= {
			division_name = {
				is_name_ordered = yes
				name_order = 5
			}
			location = 384
			division_template = "Pekhotna Divizija"
			start_equipment_factor = 0.7
			start_experience_factor = 0.3
		}
		division= {
			division_name = {
				is_name_ordered = yes
				name_order = 6
			}
			location = 4179
			division_template = "Pekhotna Divizija"
			start_equipment_factor = 0.7
			start_experience_factor = 0.3
		}
		division= {
			division_name = {
				is_name_ordered = yes
				name_order = 9
			}
			location = 4069
			division_template = "Pekhotna Divizija"
			start_equipment_factor = 0.7
			start_experience_factor = 0.3
		}
	#
	# Corps 5 #
		division= {
			division_name = {
				is_name_ordered = yes
				name_order = 14
			}
			location = 2164
			division_template = "Pekhotna Divizija"
			start_equipment_factor = 0.7
			start_experience_factor = 0.4
		}
		division= {
			division_name = {
				is_name_ordered = yes
				name_order = 15
			}
			location = 4607
			division_template = "Pekhotna Divizija"
			start_equipment_factor = 0.7
			start_experience_factor = 0.4
		}
	#
	division= {
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 11174
		division_template = "Konna Divizija"
		start_equipment_factor = 0.8
		start_experience_factor = 0.2
	}
	division= {
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 1916
		division_template = "Konna Divizija"
		start_equipment_factor = 0.8
		start_experience_factor = 0.2
	}
	division= {
		division_name = {
			is_name_ordered = yes
			name_order = 16
		}
		location = 9486
		division_template = "Pekhotna Divizija"
		start_equipment_factor = 0.7
		start_experience_factor = 0.4
	}
}

air_wings = {
	### Vuzdushnite voiski -- Sofia (early purchases of GER Ar 65, He 51 fighters)
	529 = {
		# Vuzdushnite voiski na Negovo Velichestvo
		fighter_equipment_0 =  {
			owner = "BUL"
			creator = "GER"
			amount = 48
		}
	}
}

### Starting Production ###
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_1
			creator = "BUL"
		}
		requested_factories = 1
		progress = 0.49
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = support_equipment_1
			creator = "BUL"
		}
		requested_factories = 1
		progress = 0.73
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = motorized_equipment_1
			creator = "BUL"
		}
		requested_factories = 1
		progress = 0.4
		efficiency = 100
	}
}
