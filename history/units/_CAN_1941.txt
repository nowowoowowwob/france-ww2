﻿division_template = {
	name = "Infantry Division"			# For 1st, 2nd Canadian Infantry Divisions
										# Frontline divisions, most experienced, best-equipped
	division_names_group = CAN_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
        infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
        infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }

		artillery_brigade = { x = 3 y = 0 }
		artillery_brigade = { x = 3 y = 1 }
		artillery_brigade = { x = 3 y = 2 }
	}
	support = {
       	engineer = { x = 0 y = 0 }
		signal_company = { x = 0 y = 1 }
       	artillery = { x = 0 y = 2 }
	}
}
division_template = {
	name = "District Militia"			# For largest Military District Forces (2 brigade forces)
										# Note: all forces at reserve levels in 1939, low experience and WWI-era equipment

	division_names_group = CAN_GAR_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		artillery_brigade = { x = 2 y = 0 }
		artillery_brigade = { x = 2 y = 1 }
	}
	support = {
       	engineer = { x = 0 y = 0 }
		signal_company = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Cavalry Militia"
	# Note: all forces at reserve levels in 1936

	division_names_group = CAN_CAV_01

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
        infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		artillery_brigade = { x = 2 y = 0 }
		artillery_brigade = { x = 2 y = 1 }
		artillery_brigade = { x = 2 y = 2 }
	}
}

division_template = {
	name = "Armoured Division"

	division_names_group = CAN_ARM_01

	regiments = {
		light_armor = { x = 0 y = 0 }
		light_armor = { x = 0 y = 1 }
		light_armor = { x = 0 y = 2 }

		medium_armor = { x = 1 y = 0 }
		medium_armor = { x = 1 y = 1 }
		medium_armor = { x = 1 y = 2 }

		motorized = { x = 2 y = 0 }
		motorized = { x = 2 y = 1 }
	}
	support = {
       	engineer = { x = 0 y = 0 }
       	artillery = { x = 0 y = 1 }
		anti_air = { x = 0 y = 2 }
		anti_tank = { x = 0 y = 3 }
		armored_car_recon = { x = 1 y = 0 }
	}
}


units = {
	##### Royal Canadian Army #####
	### Canadian Active Service Force (CO: Crerar) ###
	division= {
		#name = "1st Canadian Infantry Division"			# CO: McNaughton
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 1
		}
		location = 2767  # Halifax
		division_template = "Infantry Division"
		start_experience_factor = 0.3
	}
	division= {
		#name = "2nd Canadian Infantry Division"			# CO: Odium
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 2
		}
		location = 8641 # Toronto
		division_template = "Infantry Division"			# Still forming and under-strength with reserve equipment
		start_experience_factor = 0.3
	}
	division= {
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
		location = 1285 # Iceland, D-day after
		division_template = "Infantry Division"
		start_experience_factor = 0.3
	}
	division= {
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
		location = 8641 #
		division_template = "Infantry Division" # Tank division from 1942
		start_experience_factor = 0.3
		start_equipment_factor = 0.7
	}
	division= {
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
		location = 8641 #
		division_template = "Armoured Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7
	}
	division= {
		name = "1st Canadian Armoured Brigade"
		location = 591 #
		division_template = "Armoured Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.8
	}


	## 1st Military District ##
	division= {
		#name = "1st District Militia"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 1
		}
		location = 8641 # London ONT
		division_template = "District Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.6

	}
	division= {
		#name = "4th District Cavalry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 2
		}
		location = 8641 # Toronto
		division_template = "Cavalry Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.6

	}

	## 3rd Military District ##
	division= {
		#name = "3rd District Militia"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 3
		}
		location = 8654 # Kingston ONT
		division_template = "District Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.6

	}
	division= {
		#name = "4th District Cavalry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 4
		}
		location = 8655 # Montreal
		division_template = "Cavalry Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.6

	}

	## 4th Military District ##
	division= {
		#name = "5th District Militia"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 5
		}
		location = 8655 # Montreal
		division_template = "District Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.6

	}

	## 5th Military District ##
	division= {
		#name = "5th District Militia"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 5
		}
		location = 8656 # Quebec City
		division_template = "District Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.6

	}

	## 6th Military District ##
	division= {
		#name = "6th District Militia"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 6
		}
		location = 8657  # Halifax
		division_template = "District Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.6

	}

	## 7th Military District ##
	division= {
		#name = "7th District Militia"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 7
		}
		location = 8658 # St Johns
		division_template = "District Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.6

	}

	division= {
		#name = "10th District Cavalry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 10
		}
		location = 8659  # Winnipeg
		division_template = "Cavalry Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.6

	}
}

air_wings = {
	### Royal Canadian Air Force (RCAF)(CO: ) ###
	1096 = {																# Ontario
		# RCAF Station Trenton (ONT)#
		fighter_equipment_0 =  { owner = "CAN" amount = 24 }			# No. 3 Fighter Squadron -- Wapitis														# Vancouver
		# RCAF Station Vancouver (BC) #
		fighter_equipment_1 =  { owner = "CAN" amount = 24 }			# No. 1 Fighter Squadron -- Hurricanes
		nav_bomber_equipment_1 =  { owner = "CAN" amount = 12 }			# No. 6 Coastal Squadron -- Sharks
	}
}

##### Starting Production #####
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_1
			creator = "CAN"
		}
		requested_factories = 1
		progress = 0.39
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = support_equipment_1
			creator = "CAN"
		}
		requested_factories = 1
		progress = 0.45
		efficiency = 100
	}
}
