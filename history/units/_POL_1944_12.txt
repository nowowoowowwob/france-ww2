﻿##### Division Templates #####
division_template = {
	name = "Infantry Division" #Represents French infantry, but is actually polish
	division_names_group = POL_INF_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }

		artillery_brigade = { x = 3 y = 0 }
		artillery_brigade = { x = 3 y = 1 }
		artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		artillery = { x = 0 y = 0 } 	# Heavy Arty Regiment had 2x 155mm battalions
		engineer = { x = 0 y = 1 }
		anti_tank = { x = 0 y = 2 }
		signal_company = { x = 0 y = 3 }
       	field_hospital = { x = 1 y = 0 }
	}
}
division_template = {
	name = "Dywizja Pancerna"
	division_names_group = POL_ARM_01

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }

		motorized = { x = 1 y = 0 }#Mechanized in 1944+
		motorized = { x = 1 y = 1 }

		light_armor = { x = 2 y = 0 }#Medium armour in 1944+
		light_armor = { x = 2 y = 1 }

		mot_artillery_brigade = { x = 3 y = 0 }
		mot_artillery_brigade = { x = 3 y = 1 }
	}
	support = {
		recon = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		signal_company = { x = 0 y = 2 }
		anti_tank = { x = 0 y = 3 }
		anti_air = { x = 1 y = 0 }
	}
}
division_template = {
	name = "Brygada Pancerna"
	division_names_group = POL_ARM_BRIGADE_01

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }

		motorized = { x = 1 y = 0 }

		light_armor = { x = 2 y = 0 }#Medium armour in 1944+
		light_armor = { x = 2 y = 1 }

		mot_artillery_brigade = { x = 3 y = 0 }
	}
	support = {
		recon = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Brygada Piechoty Górskiej"
	division_names_group = POL_MNT_01

	regiments = {
		mountaineers = { x = 0 y = 0 }
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }
	}
	support = {
		engineer = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Brygada Spadochronowa"  	# Standard formation for frontline divisions

	division_names_group = POL_PAR_01

	regiments = {
		paratrooper = { x = 0 y = 0 }
		paratrooper = { x = 0 y = 1 }
		paratrooper = { x = 0 y = 2 }
	}
	support = {
		artillery = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		anti_tank = { x = 0 y = 2 }
		signal_company = { x = 0 y = 3 }
	}
}

units = {
	# Eastern #
		division= {
			name = "5. Infantry Division"
			location = 3350
			division_template = "Infantry Division"
			start_equipment_factor = 1
			start_experience_factor = 0.3
		}
		division= {
			name = "6. Infantry Division"
			location = 6561
			division_template = "Infantry Division"
			start_equipment_factor = 1
			start_experience_factor = 0.3
		}
		division= {
			name = "7. Infantry Division"
			location = 4022
			division_template = "Infantry Division"
			start_equipment_factor = 1
			start_experience_factor = 0.3
		}
		division= {
			name = "2. Warsaw Armored Brigade"
			location = 6571
			division_template = "Brygada Pancerna"
			start_equipment_factor = 1
			start_experience_factor = 0.3
		}
	#

	# African #
		division= {
			name = "3. Carpathian Infantry Division"
			location = 3449
			division_template = "Infantry Division"
			start_equipment_factor = 1
			start_experience_factor = 0.3
		}
	#

	# Western #
		division= { #Saw no combat
			name = "4. Infantry Division"
			location = 11190
			division_template = "Infantry Division"
			start_equipment_factor = 1
			start_experience_factor = 0.3
		}
		division= { #Saw no combat
			name = "2. Armoured Grenadier Division"
			location = 11190
			division_template = "Infantry Division"
			start_equipment_factor = 0.7
			start_experience_factor = 0.3
		}
		division= {#1944.12 Nijmegen
			name = "1. Independent Parachute Brigade"
			location = 4530
			division_template = "Brygada Spadochronowa"
			start_equipment_factor = 1
			start_experience_factor = 0.3
		}
		division= {#1944.12 Moerdijk, 1945 Emmen/Coevorden
			name = "1. Armoured Division"
			location = 5460
			division_template = "Dywizja Pancerna"
			start_equipment_factor = 1
			start_experience_factor = 0.3
		}
	#
}
