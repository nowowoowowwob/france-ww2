﻿division_template = {
	name = "Division d'Infanterie"		# Same for "Active" and "Reserve" Infantry Divisions
	division_names_group = BEL_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }

		artillery_brigade = { x = 3 y = 0 }
		artillery_brigade = { x = 3 y = 1 }
		artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		signal_company = { x = 0 y = 1 }
		anti_tank = { x = 0 y = 2 }
		anti_air = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Division de Cavalerie"  	# Cavalry Division
	division_names_group = BEL_CAV_01

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }

		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		cavalry = { x = 1 y = 2 }

		cavalry = { x = 2 y = 0 }
		cavalry = { x = 2 y = 1 }
		cavalry = { x = 2 y = 2 }

		mot_artillery_brigade = { x = 3 y = 0 }
		mot_artillery_brigade = { x = 3 y = 1 }
		mot_artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		signal_company = { x = 0 y = 1 }
		anti_tank = { x = 0 y = 2 }
		anti_air = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Division Chasseurs"
	division_names_group = BEL_INF_01

	regiments = {
		bicycle_battalion = { x = 0 y = 0 }
		bicycle_battalion = { x = 0 y = 1 }
		bicycle_battalion = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }

		mot_artillery_brigade = { x = 3 y = 0 }
		mot_artillery_brigade = { x = 3 y = 1 }
		mot_artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		signal_company = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		anti_tank = { x = 0 y = 2 }
		anti_air = { x = 0 y = 3 }
	}
}

division_template = {
	name = "Cavalerie Motorisée"  		# Motorized Cavalry Brigade
	division_names_group = BEL_MOT_01
										# Note: motorized cavalry. Most sources have them truck-equipped.
	regiments = {
		motorized = { x = 0 y = 0 }
		motorized = { x = 0 y = 1 }
		motorized = { x = 1 y = 0 }
		motorized = { x = 1 y = 1 }
	}
	support = {
		anti_tank = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Brigade Motorisée"
	division_names_group = BEL_MOT_01
	regiments = {
		motorized = { x = 0 y = 0 }
		motorized = { x = 0 y = 1 }
		motorized = { x = 0 y = 2 }
	}
	support = {
		artillery = { x = 0 y = 0 }
		field_hospital = { x = 0 y = 1 }
		armored_car_recon = { x = 0 y = 2 }
	}
}

units = {
	division= {
		name = "Brigade Piron"
		location = 5545
		division_template = "Division d'Infanterie"
		start_experience_factor = 0.4
		start_equipment_factor = 0.8
	}
}
