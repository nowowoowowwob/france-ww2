units = {
	# 9th infantry
		division= {
			name = "39. Infantry Regiment, 9. Infantry Division"
			location = 683
			division_template = "American Infantry Regiment"
			start_experience_factor = 0.3
		}
		division= {
			name = "47. Infantry Regiment, 9. Infantry Division"
			location = 683
			division_template = "American Infantry Regiment"
			start_experience_factor = 0.3
		}
		division= {
			name = "60. Infantry Regiment, 9. Infantry Division"
			location = 683
			division_template = "American Infantry Regiment"
			start_experience_factor = 0.3
		}
	#
	# 30th infantry
		division= {
			name = "117. Infantry Regiment, 30. Infantry Division"
			location = 683
			division_template = "American Infantry Regiment"
			start_experience_factor = 0.3
		}
		division= {
			name = "119. Infantry Regiment, 30. Infantry Division"
			location = 683
			division_template = "American Infantry Regiment"
			start_experience_factor = 0.3
		}
		division= {
			name = "120. Infantry Regiment, 30. Infantry Division"
			location = 683
			division_template = "American Infantry Regiment"
			start_experience_factor = 0.3
		}
	#
}
