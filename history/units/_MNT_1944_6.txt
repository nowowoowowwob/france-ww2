﻿division_template = {
	name = "Partisans"
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
	}
	support = {
		engineer = { x = 0 y = 0 }
	}
}

units = {
	division = {
		name = "Chetniks"
		location = 4776
		division_template = "Partisans"
		force_equipment_variants = { infantry_equipment_1 = { owner = "ITA" } }
		start_experience_factor = 0.6
		start_equipment_factor = 0.4
	}
	division = {
		name = "Chetniks"
		location = 1984
		division_template = "Partisans"
		force_equipment_variants = { infantry_equipment_1 = { owner = "ITA" } }
		start_experience_factor = 0.6
		start_equipment_factor = 0.4
	}
	division = {
		name = "Chetniks"
		location = 3276
		division_template = "Partisans"
		force_equipment_variants = { infantry_equipment_1 = { owner = "ITA" } }
		start_experience_factor = 0.6
		start_equipment_factor = 0.4
	}
	division = {
		name = "Chetniks"
		location = 3036
		division_template = "Partisans"
		force_equipment_variants = { infantry_equipment_1 = { owner = "ITA" } }
		start_experience_factor = 0.6
		start_equipment_factor = 0.4
	}
}
