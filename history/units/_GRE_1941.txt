﻿division_template = {
	name = "Merarchía Pezikoú" 				# Triangular Infantry Division
	division_names_group = GRE_INF_01
	regiments = {
		mountaineers = { x = 0 y = 0 }
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 0 y = 2 }

		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }
		mountaineers = { x = 1 y = 2 }

		mountaineers = { x = 2 y = 0 }
		mountaineers = { x = 2 y = 1 }
		mountaineers = { x = 2 y = 2 }

		artillery_brigade = { x = 3 y = 0 }
		artillery_brigade = { x = 3 y = 1 }
		artillery_brigade = { x = 3 y = 2 }
	}
	support = {
       	field_hospital = { x = 0 y = 0 }
	}
}
division_template = {
	name = "Merarchía Ippikoú" 			# Cavalry Division
	division_names_group = GRE_CAV_01

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }

		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		cavalry = { x = 1 y = 2 }

		cavalry = { x = 2 y = 0 }
		cavalry = { x = 2 y = 1 }
		cavalry = { x = 2 y = 2 }

		mot_artillery_brigade = { x = 3 y = 0 }
	}
	support = {
		mot_recon = { x = 0 y = 0 }      # Motorized recon inf bn
       	military_police = { x = 0 y = 1 }
		engineer = { x = 0 y = 2 }
	}
}
division_template = {
	name = "Greek Brigade"
	division_names_group = GRE_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		artillery_brigade = { x = 1 y = 0 }
	}
	support = {
		armored_car_recon = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		field_hospital = { x = 0 y = 2 }
	}
}

units = {
	division = {
		name = "1. Greek Brigade"
		location = 10021
		division_template = "Greek Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.6
	}
}
