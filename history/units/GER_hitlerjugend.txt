units = {
	##### PFU Hitlerjugend #####
	division= {
		name = "SS-Hitlerjugend"
		location = 8469
		division_template = "Panzer-Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.1
	}
}
