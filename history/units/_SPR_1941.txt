﻿### Copy of 1940 ###
##### Division Templates #####
division_template = {
	name = "División de Infantería" 		# Used for both regular infantry divisions and larger garrison divisions
	division_names_group = SPR_INF_01
	# Note: Spanish divisions were 2x brigades of 2x2 rgts each, + support
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }

		artillery_brigade = { x = 3 y = 0 }
		artillery_brigade = { x = 3 y = 1 }
		artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		artillery = { x = 0 y = 0 }  # Spanish Arty Rgt/Bge had 2 Bn 105mm arty
		engineer = { x = 0 y = 1 }   # Eng Bn
       	field_hospital = { x = 0 y = 2 }
		recon = { x = 0 y = 3 }
		logistics_company = { x = 1 y = 0 }
	}
}
division_template = {
	name = "División de Caballería"  		# Only one Cavalry Division (3x bge of 2x2 Rgts)
	division_names_group = SPR_CAV_01

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		motorized = { x = 2 y = 0 }
		motorized = { x = 2 y = 1 }
		mot_artillery_brigade = { x = 2 y = 2 }
		motorized = { x = 3 y = 0 }
		motorized = { x = 3 y = 1 }
		mot_artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		artillery = { x = 0 y = 0 }  # Spanish Arty Rgt/Bge had 2 Bn 105mm arty
		engineer = { x = 0 y = 1 }   # Eng Bn
		recon = { x = 0 y = 2 }      # Recon consisted of motorcycles and ACs
       	field_hospital = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Brigada Montaña"  		# Mountain Brigades were 2x2 Rgts + support
	division_names_group = SPR_MNT_01
	regiments = {
		mountaineers = { x = 0 y = 0 }
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }

		artillery_brigade = { x = 2 y = 0 }
		artillery_brigade = { x = 2 y = 1 }
	}
	support = {
		engineer = { x = 0 y = 0 }
	}
}
division_template = {
	name = "Brigada de Infantería" 		# Infantry Brigade - Used for smaller garrison units
	division_names_group = SPR_GAR_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		artillery_brigade = { x = 2 y = 0 }
		artillery_brigade = { x = 2 y = 1 }
	}
	support = {
		artillery = { x = 0 y = 0 }  # Spanish Arty Rgt/Bge had 2 Bn 105mm arty
		engineer = { x = 0 y = 1 }   # Eng Bn
       	field_hospital = { x = 0 y = 2 }
		recon = { x = 0 y = 3 }
		logistics_company = { x = 1 y = 0 }
	}
}
division_template = {
	name = "División Blindada" 		# Infantry Brigade - Used for smaller garrison units
	division_names_group = SPR_ARM_01

	regiments = {
		light_armor = { x = 0 y = 0 }
		light_armor = { x = 0 y = 1 }
		light_armor = { x = 0 y = 2 }

		motorized = { x = 1 y = 0 }
		motorized = { x = 1 y = 1 }
		motorized = { x = 1 y = 2 }

		mot_artillery_brigade = { x = 2 y = 0 }
		mot_artillery_brigade = { x = 2 y = 1 }
	}
	support = {
		artillery = { x = 0 y = 0 }  # Spanish Arty Rgt/Bge had 2 Bn 105mm arty
		engineer = { x = 0 y = 1 }   # Eng Bn
       	field_hospital = { x = 0 y = 2 }
		light_tank_recon = { x = 0 y = 3 }
		logistics_company = { x = 1 y = 0 }
		anti_tank = { x = 1 y = 1 }
		signal_company = { x = 1 y = 2 }
	}
}

###### OOB ######
units = {
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 11
		}
		location = 8991
		division_template = "División de Caballería"
		start_experience_factor = 0.4
		start_equipment_factor = 0.65
	}
	# Corps 1 #
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 11
			}
			location = 8991
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 12
			}
			location = 9385
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 13
			}
			location = 9386
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
	#
	# Corps 2 #
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 21
			}
			location = 8688
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 22
			}
			location = 8686
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 23
			}
			location = 8677
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
	#
	# Corps 3 #
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 31
			}
			location = 8748
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 32
			}
			location = 8863
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
	#
	# Corps 4 #
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 41
			}
			location = 9056
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 42
			}
			location = 9025
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 43
			}
			location = 9090
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
	#
	# Corps 5 #
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 51
			}
			location = 9068
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 52
			}
			location = 9087
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
	#
	# Corps 6 #
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 61
			}
			location = 2463
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 62
			}
			location = 568
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
	#
	# Corps 7 #
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 71
			}
			location = 9166
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 72
			}
			location = 9011
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
	#
	# Corps 8 #
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 81
			}
			location = 193
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 82
			}
			location = 1225
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
	#
	# Corps 9 #
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 91
			}
			location = 7199
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 92
			}
			location = 2423
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 93
			}
			location = 1313
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 2
			}
			location = 1313
			division_template = "Brigada de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
	#
	# Corps 10 #
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 101
			}
			location = 8956
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 102
			}
			location = 8910
			division_template = "División de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}

		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 1
			}
			location = 9009
			division_template = "Brigada de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
	#
	# Corps Balearics #

		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 36
			}
			location = 8850
			division_template = "Brigada de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}

		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 60
			}
			location = 8800
			division_template = "Brigada de Infantería"
			start_experience_factor = 0.2
			start_equipment_factor = 0.65
		}
	#
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 8991
		division_template = "División Blindada"
		start_experience_factor = 0.2
		start_equipment_factor = 0.65
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 8991
		division_template = "División Blindada"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5
	}
}

### Air Wings
air_wings = {
	# Arma de Aviación -- Madrid (Getafe)
	600 = {
		# Grupo n.1 de Caza FARE 'Alas Rojas'
		fighter_equipment_0 = {
			owner = "SPR"
			amount = 36
		}
		# Arma de Aviación -- Sevilla (and Barcelona)
		# Grupo n.3 de Caza FARE 'Malraux'
		fighter_equipment_0 = {
			owner = "SPR"
			amount = 24
		}
	}
	# Aeronáutica Naval -- Murcia
	635 = {
		nav_bomber_equipment_1 = {
			owner = "SPR"
			amount = 27
		}
	}
}


instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_1
			creator = "SPR"
		}
		requested_factories = 2
		progress = 0.42
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = support_equipment_1
			creator = "SPR"
		}
		requested_factories = 1
		progress = 0.24
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = artillery_equipment_1
			creator = "SPR"
		}
		requested_factories = 1
		progress = 0.24
		efficiency = 100
	}
}
