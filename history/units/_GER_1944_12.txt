﻿division_template = {
	name = "Serbian Volunteer Corps"
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }
	}
}

units = {
	#Incorporated by Waffen-SS
	division = {
		name = "Serbian Volunteer Corps"
		location = 2129
		division_template = "Serbian Volunteer Corps"
		force_equipment_variants = { infantry_equipment_1 = { owner = "ITA" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.7
	}
}


instant_effect = {

}
