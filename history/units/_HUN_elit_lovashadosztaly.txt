﻿division_template = {
	name = "Elit Lovashadosztály"

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }

		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		cavalry = { x = 1 y = 2 }

		cavalry = { x = 2 y = 0 }
		cavalry = { x = 2 y = 1 }
		cavalry = { x = 2 y = 2 }

		medium_armor = { x = 3 y = 0 }
		medium_armor = { x = 3 y = 1 }
		medium_armor = { x = 3 y = 2 }
	}
	support = {
        recon = { x = 0 y = 0 }
	    engineer = { x = 0 y = 1 }
        artillery = { x = 0 y = 2 }
	}
}

units = {
	##### 1. Lovashadosztály #####
	division= {
		name = "1. Lovashadosztály"
		location = 9660		# Budapest
		division_template = "Elit Lovashadosztály"
		start_experience_factor = 0.6
		start_equipment_factor = 0.9
	}
}
