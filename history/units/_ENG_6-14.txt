units = {
	# 15 infantry
		division= {
			name = "44. Infantry Brigade, 15. Infantry Division"
			location = 10150
			division_template = "British Artillery Infantry Brigade"
			start_experience_factor = 0.3
		}
		division= {
			name = "46. Infantry Brigade, 15. Infantry Division"
			location = 10150
			division_template = "British Artillery Infantry Brigade"
			start_experience_factor = 0.3
		}
		division= {
			name = "227. Infantry Brigade, 15. Infantry Division"
			location = 10150
			division_template = "British Artillery Infantry Brigade"
			start_experience_factor = 0.3
		}
	#
}
