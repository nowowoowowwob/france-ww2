﻿##### Division Templates #####
division_template = {
	name = "Jalkaväkidivisioona"		# Infantry Division

	division_names_group = FIN_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }

		artillery_brigade = { x = 3 y = 0 }
		artillery_brigade = { x = 3 y = 1 }
		artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		signal_company = { x = 0 y = 1 }
       	field_hospital = { x = 0 y = 2 }
		anti_tank = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Jalkaväkiprikaatti"		# Infantry Brigade

	division_names_group = FIN_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		artillery_brigade = { x = 3 y = 0 }
		artillery_brigade = { x = 3 y = 1 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		signal_company = { x = 0 y = 1 }
       	field_hospital = { x = 0 y = 2 }
		anti_tank = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Erillinen Prikaati" 	# Separate Brigade - Represents groups of separate battalions (Erillinen Pataljoona) and various garrison units

	division_names_group = FIN_GAR_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }
	}
	priority = 0
}
division_template = {
	name = "Ratsuväkiprikaati"  		# Cavalry Brigade

	division_names_group = FIN_CAV_01

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 1 y = 0 }
	}
	support = {
		recon = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Jääkäriprikaati"					# Jaeger brigade, armoured
	division_names_group = FIN_CAV_01

	regiments = {
		bicycle_battalion = { x = 0 y = 0 }
		bicycle_battalion = { x = 0 y = 1 }
		bicycle_battalion = { x = 0 y = 2 }

		light_armor = { x = 1 y = 0 }
	}
	support = {
		signal_company = { x = 0 y = 0 }
		anti_tank = { x = 0 y = 1 }
	}
}


##### OOB #####
units = {
	# Army Karelia #
		# Corps 6 #
			division = {
				division_name = {
					is_name_ordered = yes
					name_order = 5
				}
				location = 1404
				division_template = "Jalkaväkidivisioona"
				start_experience_factor = 0.3
				start_equipment_factor = 0.8
			}
			division = {
				division_name = {
					is_name_ordered = yes
					name_order = 11
				}
				location = 5067
				division_template = "Jalkaväkidivisioona"
				start_experience_factor = 0.3
				start_equipment_factor = 0.8
			}
		#
		# Corps 7 #
			division = {
				division_name = {
					is_name_ordered = yes
					name_order = 1
				}
				location = 676
				division_template = "Jalkaväkidivisioona"
				start_experience_factor = 0.3
				start_equipment_factor = 0.8
			}
			division = {
				division_name = {
					is_name_ordered = yes
					name_order = 7
				}
				location = 1561
				division_template = "Jalkaväkidivisioona"
				start_experience_factor = 0.3
				start_equipment_factor = 0.8
			}
			division = {
				division_name = {
					is_name_ordered = yes
					name_order = 19
				}
				location = 5041
				division_template = "Jalkaväkidivisioona"
				start_experience_factor = 0.3
				start_equipment_factor = 0.8
			}
		#
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 2
			}
			location = 65
			division_template = "Erillinen Prikaati"
			start_experience_factor = 0.3
			start_equipment_factor = 0.8
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 1
			}
			location = 65
			division_template = "Ratsuväkiprikaati"
			start_experience_factor = 0.3
			start_equipment_factor = 0.8
		}
	#

	# Corps 2 #
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 2
			}
			location = 1737
			division_template = "Jalkaväkidivisioona"
			start_experience_factor = 0.3
			start_equipment_factor = 0.8
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 15
			}
			location = 11144
			division_template = "Jalkaväkidivisioona"
			start_experience_factor = 0.3
			start_equipment_factor = 0.8
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 18
			}
			location = 257
			division_template = "Jalkaväkidivisioona"
			start_experience_factor = 0.3
			start_equipment_factor = 0.8
		}
	#

	# Corps 3 #
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 3
			}
			location = 2862
			division_template = "Jalkaväkidivisioona"
			start_experience_factor = 0.3
			start_equipment_factor = 0.8
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 6
			}
			location = 2441
			division_template = "Jalkaväkidivisioona"
			start_experience_factor = 0.3
			start_equipment_factor = 0.8
		}
	#
	# Corps 4 #
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 4
			}
			location = 5026
			division_template = "Jalkaväkidivisioona"
			start_experience_factor = 0.3
			start_equipment_factor = 0.8
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 8
			}
			location = 949
			division_template = "Jalkaväkidivisioona"
			start_experience_factor = 0.3
			start_equipment_factor = 0.8
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 10
			}
			location = 4371
			division_template = "Jalkaväkidivisioona"
			start_experience_factor = 0.3
			start_equipment_factor = 0.8
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 12
			}
			location = 5020
			division_template = "Jalkaväkidivisioona"
			start_experience_factor = 0.3
			start_equipment_factor = 0.8
		}
	#

	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 14
		}
		location = 1071
		division_template = "Jalkaväkidivisioona"
		start_experience_factor = 0.3
		start_equipment_factor = 0.8
	}

	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 17
		}
		location = 506
		division_template = "Jalkaväkidivisioona"
		start_experience_factor = 0.3
		start_equipment_factor = 0.8
	}
	division = {
		#name = "1. Divisioona"
		name = "Jääkäriprikaati"
		location = 4849
		division_template = "Jalkaväkidivisioona"
		start_experience_factor = 0.3
		start_equipment_factor = 0.8
	}
}

air_wings = {
	84 = { 														# Helsinki
		CAS_equipment_1 = { owner = "FIN" amount = 28 }				# 1, 4 Bomber Groups -- Fokker C.Xs, Blenheim Is
		fighter_equipment_0 = { owner = "FIN" amount = 45 }			# 2 Fighter Group -- Fokker D.XXIs, Bristol Bulldogs
	}
}

### Starting Production ###
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_1
			creator = "FIN"
		}
		requested_factories = 1
		progress = 0.66
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = support_equipment_1
			creator = "FIN"
		}
		requested_factories = 1
		progress = 0.35
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = fighter_equipment_0
			creator = "FIN"
		}
		requested_factories = 1
		progress = 0.89
		efficiency = 100
	}
}
