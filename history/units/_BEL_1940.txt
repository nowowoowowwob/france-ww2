﻿division_template = {
	name = "Division d'Infanterie"		# Same for "Active" and "Reserve" Infantry Divisions
	division_names_group = BEL_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }

		artillery_brigade = { x = 3 y = 0 }
		artillery_brigade = { x = 3 y = 1 }
		artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		signal_company = { x = 0 y = 1 }
		anti_tank = { x = 0 y = 2 }
		anti_air = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Division de Cavalerie"  	# Cavalry Division
	division_names_group = BEL_CAV_01

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }

		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		cavalry = { x = 1 y = 2 }

		cavalry = { x = 2 y = 0 }
		cavalry = { x = 2 y = 1 }
		cavalry = { x = 2 y = 2 }

		mot_artillery_brigade = { x = 3 y = 0 }
		mot_artillery_brigade = { x = 3 y = 1 }
		mot_artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		signal_company = { x = 0 y = 1 }
		anti_tank = { x = 0 y = 2 }
		anti_air = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Division Chasseurs"
	division_names_group = BEL_INF_01

	regiments = {
		bicycle_battalion = { x = 0 y = 0 }
		bicycle_battalion = { x = 0 y = 1 }
		bicycle_battalion = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }

		mot_artillery_brigade = { x = 3 y = 0 }
		mot_artillery_brigade = { x = 3 y = 1 }
		mot_artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		signal_company = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		anti_tank = { x = 0 y = 2 }
		anti_air = { x = 0 y = 3 }
	}
}

division_template = {
	name = "Cavalerie Motorisée"  		# Motorized Cavalry Brigade
	division_names_group = BEL_MOT_01
										# Note: motorized cavalry. Most sources have them truck-equipped.
	regiments = {
		motorized = { x = 0 y = 0 }
		motorized = { x = 0 y = 1 }
		motorized = { x = 1 y = 0 }
		motorized = { x = 1 y = 1 }
	}
	support = {
		anti_tank = { x = 0 y = 0 }
	}
}

### OOB ###
units = {
	##### L'Armée Belge #####
	# I Corps d'Armée
		division= {	# "4ème Division d'Infanterie"
			division_name = {
					is_name_ordered = yes
					name_order = 4
			}
			location = 2940  # Hoeselt
			division_template = "Division d'Infanterie"
			start_experience_factor = 0.15
			start_equipment_factor = 0.7
		}
		division= {	# "7ème Division d'Infanterie"
			division_name = {
					is_name_ordered = yes
					name_order = 7
			}
			location = 1579  # Hoeselt
			division_template = "Division d'Infanterie"
			start_experience_factor = 0.15
			start_equipment_factor = 0.7
		}
	#

	# II Corps d'Armée
		division= {	# "6ème Division d'Infanterie"
			division_name = {
					is_name_ordered = yes
					name_order = 6
			}
			location = 5545  # Leuven
			division_template = "Division d'Infanterie"
			start_experience_factor = 0.15
			start_equipment_factor = 0.7
		}
		division= {	# "9ème Division d'Infanterie"
			division_name = {
					is_name_ordered = yes
					name_order = 9
			}
			location = 5555  # Leuven
			division_template = "Division d'Infanterie"
			start_experience_factor = 0.15
			start_equipment_factor = 0.7
		}
	#

	# III Corps d'Armée
		division= {	# "2ème Division d'Infanterie"
			division_name = {
					is_name_ordered = yes
					name_order = 2
			}
			location = 5605  # Liege
			division_template = "Division d'Infanterie"
			start_experience_factor = 0.15
			start_equipment_factor = 0.7
		}
		division= {	# "3ème Division d'Infanterie"
			division_name = {
					is_name_ordered = yes
					name_order = 3
			}
			location = 4743  # Liege
			division_template = "Division d'Infanterie"
			start_experience_factor = 0.15
			start_equipment_factor = 0.7
		}

	# IV Corps d'Armée
		division= {	# "12ème Division d'Infanterie"
			division_name = {
					is_name_ordered = yes
					name_order = 12
			}
			location = 2242  #Brussels
			division_template = "Division d'Infanterie"
			start_experience_factor = 0.15
			start_equipment_factor = 0.7
		}
		division= {	# "15ème Division d'Infanterie"
			division_name = {
					is_name_ordered = yes
					name_order = 15
			}
			location = 8392  #Brussels
			division_template = "Division d'Infanterie"
			start_experience_factor = 0.15
			start_equipment_factor = 0.7
		}
		division= {	# "18ème Division d'Infanterie"
			division_name = {
					is_name_ordered = yes
					name_order = 18
			}
			location = 4726  #Brussels
			division_template = "Division d'Infanterie"
			start_experience_factor = 0.15
			start_equipment_factor = 0.7
		}
	#

	# V Corps d'Armée
		division= {	# "13ème Division d'Infanterie"
			division_name = {
					is_name_ordered = yes
					name_order = 13
			}
			location = 8410  #Brussels
			division_template = "Division d'Infanterie"
			start_experience_factor = 0.15
			start_equipment_factor = 0.7
		}
		division= {	# "17ème Division d'Infanterie"
			division_name = {
					is_name_ordered = yes
					name_order = 17
			}
			location = 5514  #Brussels
			division_template = "Division d'Infanterie"
			start_experience_factor = 0.15
			start_equipment_factor = 0.7
		}
	#

	# VI Corps d'Armée
		division= {	# "5ème Division d'Infanterie"
			division_name = {
					is_name_ordered = yes
					name_order = 5
			}
			location = 2088  #Brussels
			division_template = "Division d'Infanterie"
			start_experience_factor = 0.15
			start_equipment_factor = 0.7
		}
		division= {	# "10ème Division d'Infanterie"
			division_name = {
					is_name_ordered = yes
					name_order = 10
			}
			location = 5550  #Brussels
			division_template = "Division d'Infanterie"
			start_experience_factor = 0.15
			start_equipment_factor = 0.7
		}
	#

	# VII Corps d'Armée
		division= {	# "8ème Division d'Infanterie"
			division_name = {
					is_name_ordered = yes
					name_order = 8
			}
			location = 5596  #Brussels
			division_template = "Division d'Infanterie"
			start_experience_factor = 0.15
			start_equipment_factor = 0.7
		}
		division= {
			name = "2. Chasseurs Ardennais"
			location = 5588  #Brussels
			division_template = "Division Chasseurs"
			start_experience_factor = 0.3
			start_equipment_factor = 0.9
		}
	#

	# Corps d'Cavalerie
		division= {	# "1ère Division d'Infanterie"
			division_name = {
					is_name_ordered = yes
					name_order = 1
			}
			location = 5631  # Liege
			division_template = "Division d'Infanterie"
			start_experience_factor = 0.15
			start_equipment_factor = 0.7
		}
		division= {	# "14ère Division d'Infanterie"
			division_name = {
					is_name_ordered = yes
					name_order = 14
			}
			location = 3558  # Liege
			division_template = "Division d'Infanterie"
			start_experience_factor = 0.15
			start_equipment_factor = 0.7
		}
		division= {	# "2ème Division de Cavalerie"
			division_name = {
					is_name_ordered = yes
					name_order = 2
			}
			location = 2734  # Leuven
			division_template = "Division de Cavalerie"
			start_experience_factor = 0.15
			start_equipment_factor = 0.7
		}
		division= {
			name = "Brigade de Cavalerie Motorisée"
			location = 3467  # Leuven
			division_template = "Cavalerie Motorisée"
			start_experience_factor = 0.15
			start_equipment_factor = 0.7
		}
	#

	division= {	# "11ème Division d'Infanterie"
		division_name = {
				is_name_ordered = yes
				name_order = 11
		}
		location = 5528  # Hoeselt
		division_template = "Division d'Infanterie"
		start_experience_factor = 0.15
		start_equipment_factor = 0.7
	}
	division= {	# "16ème Division d'Infanterie"
		division_name = {
				is_name_ordered = yes
				name_order = 16
		}
		location = 5527  # Hoeselt
		division_template = "Division d'Infanterie"
		start_experience_factor = 0.15
		start_equipment_factor = 0.7
	}
}

air_wings = {
	### Aéronautique Militaire Belge -- Brussels (Nivelle)
	204 = {
		# 2e Régiment d'Aéronautique
		# Consisted of 50 operational Gladiator Is, Hurricanes, CR.42s in 1940
		# Only had Fairey Fox recon/l bombers that were also used as fighters in 1936, along with trainers
		fighter_equipment_0 =  {
			owner = "BEL"
			amount = 20
		}
	}
}

### Starting Production ###
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_1
			creator = "BEL"
		}
		requested_factories = 2
		progress = 0.52
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = support_equipment_1
			creator = "BEL"
		}
		requested_factories = 1
		progress = 0.85
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = fighter_equipment_0
			creator = "BEL"
		}
		requested_factories = 1
		progress = 0.85
		efficiency = 100
	}
}
