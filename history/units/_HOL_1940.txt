﻿division_template = {
	name = "Infanterie Divisie"
	division_names_group = HOL_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }

		artillery_brigade = { x = 3 y = 0 }
		artillery_brigade = { x = 3 y = 1 }
		artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		signal_company = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		anti_tank = { x = 0 y = 2 }
		anti_air = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Infanterie Brigade"
	division_names_group = HOL_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		artillery_brigade = { x = 3 y = 0 }
		artillery_brigade = { x = 3 y = 1 }
	}
	support = {
		signal_company = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		anti_tank = { x = 0 y = 2 }
		anti_air = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Peel Divisievak"
	division_names_group = HOL_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		artillery_brigade = { x = 1 y = 0 }
	}
	support = {
		signal_company = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Lichte Divisie"
	division_names_group = HOL_INF_01

	regiments = {
		bicycle_battalion = { x = 0 y = 0 }
		bicycle_battalion = { x = 0 y = 1 }
		bicycle_battalion = { x = 0 y = 2 }

		bicycle_battalion = { x = 1 y = 0 }
		bicycle_battalion = { x = 1 y = 1 }
		bicycle_battalion = { x = 1 y = 2 }

		bicycle_battalion = { x = 2 y = 0 }
		bicycle_battalion = { x = 2 y = 1 }
		bicycle_battalion = { x = 2 y = 2 }

		mot_artillery_brigade = { x = 3 y = 0 }
		mot_artillery_brigade = { x = 3 y = 1 }
		mot_artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		signal_company = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		anti_tank = { x = 0 y = 2 }
		anti_air = { x = 0 y = 3 }
		maintenance_company = { x = 1 y = 0 }
	}
}

division_template = {
	name = "Mariniersdivisie"		# Marines
	division_names_group = HOL_MAR_01

	regiments = {
		marine = { x = 0 y = 0 }
		marine = { x = 0 y = 1 }
		marine = { x = 1 y = 0 }
		marine = { x = 1 y = 1 }
	}
	priority = 2
}

units = {
	division= {	# "1e Divisie"
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 2193  # Amsterdam
		division_template = "Infanterie Divisie"
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}
	division= {	# "2e Divisie"
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 2691  # Rotterdam
		division_template = "Infanterie Divisie"
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}
	division= {	# "3e Divisie"
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
		location = 4685 # The Hague
		division_template = "Infanterie Divisie"
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}
	division= {	# "4e Divisie"
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
		location = 8434  # Rotterdam
		division_template = "Infanterie Divisie"
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}
	division= {	# "5e Divisie"
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
		location = 5452  # Venlo
		division_template = "Infanterie Divisie"
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}
	division= {	# "6e Divisie"
		division_name = {
			is_name_ordered = yes
			name_order = 6
		}
		location = 4530  # Maastricht
		division_template = "Infanterie Divisie"
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}
	division= {	# "7e Divisie"
		division_name = {
			is_name_ordered = yes
			name_order = 7
		}
		location = 5426  # Gronigen
		division_template = "Infanterie Divisie"
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}
	division= {	# "8e Divisie"
		division_name = {
			is_name_ordered = yes
			name_order = 8
		}
		location = 5426  # Gronigen
		division_template = "Infanterie Divisie"
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}

	division= {	# "Lichte divisie"
		name = "Lichte Divisie"
		location = 4975  # Gronigen
		division_template = "Lichte Divisie"
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}

	division= {
		name = "Peel Divisie Vak Schaijk"
		location = 4262  # Gronigen
		division_template = "Peel Divisievak"
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}
	division= {
		name = "Peel Divisie Vak Erp"
		location = 8395  # Gronigen
		division_template = "Peel Divisievak"
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}
	division= {
		name = "Peel Divisie Vak Bakel"
		location = 5498  # Gronigen
		division_template = "Peel Divisievak"
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}
	division= {
		name = "Peel Divisie Vak Asten"
		location = 1791  # Gronigen
		division_template = "Peel Divisievak"
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}
	division= {
		name = "Peel Divisie Vak Weert"
		location = 625  # Gronigen
		division_template = "Peel Divisievak"
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}

	division= {
		name = "Infanterie Brigade A"
		location = 2013  # Gronigen
		division_template = "Infanterie Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}
	division= {
		name = "Infanterie Brigade B"
		location = 1275  # Gronigen
		division_template = "Infanterie Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}
	division= {
		name = "Infanterie Brigade C"
		location = 4530  # Gronigen
		division_template = "Infanterie Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}
}

### Air Wings
air_wings = {
	# Luchtvaartafdeling -- NONE
	# Note: All European aircraft were built by Fokker or ordered from USA starting in 1938

	# Militaire Luchtvaart van het Koninklijk Nederlands-Indisch Leger (ML-KNIL) -- Celebes
	# Note: ML-KNIL would increase in size during 1937-40 with USA models:
	#	Curtis P-36 Hawks (H-75), CW-21 Interceptors, Brewster Buffaloes, Martin B-10s (WH-1--WH-3s)
	308 = {
		fighter_equipment_0 = {		# Curtis P-6
			owner = "HOL"
			amount = 12
		}
	}
}

### Starting Production ###
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_1
			creator = "HOL"
		}
		requested_factories = 1
		progress = 0.88
		efficiency = 100
	}
}
