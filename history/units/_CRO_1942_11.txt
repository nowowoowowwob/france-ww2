﻿### Copied from Yugoslavia ###
division_template = {
	name = "Pesadijska Divizija"		# Infantry Division

	division_names_group = CRO_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		artillery = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Jaeger Brigade"		# Infantry Division

	division_names_group = CRO_INF_01

	regiments = {
		jaeger = { x = 0 y = 0 }
		jaeger = { x = 0 y = 1 }
		jaeger = { x = 1 y = 0 }
		jaeger = { x = 1 y = 1 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		artillery = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Alpine Odred"			# Mountain Brigade

	division_names_group = CRO_MTN_01

	regiments = {
		mountaineers = { x = 0 y = 0 }
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }
	}
}
division_template = {
	name = "Konjièka Divizija" 			# Cavalry Division

	division_names_group = CRO_CAV_01

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }
		motorized = { x = 1 y = 0 }
		motorized = { x = 1 y = 1 }
	}
	support = {
		engineer = { x = 0 y = 0 }
	}
}
division_template = {
	name = "Bataljon Bornih Kola" 			# Tank Battalion

	division_names_group = CRO_ARM_01

	regiments = {
		light_armor = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Partisans"
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
	}
	support = {
		engineer = { x = 0 y = 0 }
	}
}

units = {
	division = {
		name = "Sava Divisional Region"
		location = 6285
		division_template = "Pesadijska Divizija"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7
	}
	division = {
		name = "Osijek Divisional Region"
		location = 6326
		division_template = "Pesadijska Divizija"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7
	}
	division = {
		name = "Bosnia Divisional Region"
		location = 4121
		division_template = "Pesadijska Divizija"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7
	}
	division = {
		name = "Vrbas Divisional Region"
		location = 3040
		division_template = "Pesadijska Divizija"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7
	}
	division = {
		name = "Jadran Divisional Region"
		location = 2228
		division_template = "Pesadijska Divizija"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7
	}
	division = {
		name = "Independent Divisional Unit"
		location = 2228
		division_template = "Konjièka Divizija"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7
	}
	division = {
		name = "1. Mountain Brigade"
		location = 6638
		division_template = "Alpine Odred"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7
	}
	division = {
		name = "2. Mountain Brigade"
		location = 6456
		division_template = "Alpine Odred"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7
	}
	division = {
		name = "3. Mountain Brigade"
		location = 956
		division_template = "Alpine Odred"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7
	}
	division = {
		name = "4. Mountain Brigade"
		location = 3315
		division_template = "Alpine Odred"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7
	}

	division = {
		name = "Chetniks"
		location = 6296
		division_template = "Partisans"
		force_equipment_variants = { infantry_equipment_1 = { owner = "ITA" } }
		start_experience_factor = 0.6
		start_equipment_factor = 0.8
	}
	division = {
		name = "Chetniks"
		location = 6458
		division_template = "Partisans"
		force_equipment_variants = { infantry_equipment_1 = { owner = "ITA" } }
		start_experience_factor = 0.6
		start_equipment_factor = 0.8
	}
	division = {
		name = "Chetniks"
		location = 3315
		division_template = "Partisans"
		force_equipment_variants = { infantry_equipment_1 = { owner = "ITA" } }
		start_experience_factor = 0.6
		start_equipment_factor = 0.8
	}
	division = {
		name = "Chetniks"
		location = 1888
		division_template = "Partisans"
		force_equipment_variants = { infantry_equipment_1 = { owner = "ITA" } }
		start_experience_factor = 0.6
		start_equipment_factor = 0.8
	}
}
