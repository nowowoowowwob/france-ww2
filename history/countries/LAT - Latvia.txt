﻿#############################################################################################
##################################### LAT History File ######################################
#############################################################################################

#########################################################################################
####################################### Politics ########################################
#########################################################################################
	set_politics = {
		ruling_party = neutrality
		last_election = "1931.10.3"
		election_frequency = 36
		elections_allowed = no
	}
	set_popularities = {
		democratic = 74
		fascism = 6
		communism = 8
		neutrality = 12
	}

	recruit_character = LAT_karlis_ulmanis
	recruit_character = LAT_fricis_menders
	recruit_character = LAT_gustavs_celminsh
	recruit_character = LAT_vilis_lacis
#########################################################################################

#########################################################################################
####################################### Characters ######################################
#########################################################################################
	recruit_character = LAT_martinsh_penikis
	recruit_character = LAT_hermanis_buks
	recruit_character = LAT_janis_berzins
	recruit_character = LAT_ludvigs_bolsteins
	recruit_character = LAT_teodors_zvejnieks
	recruit_character = LAT_kornelijs_veidnieks
	recruit_character = LAT_rudolfs_kandis
	recruit_character = LAT_konstantins_cakste
	recruit_character = LAT_arturs_silgailis
	recruit_character = LAT_fricis_celmins
	recruit_character = LAT_otto_hellmich
	recruit_character = LAT_rudolfs_kocins
	recruit_character = LAT_arturs_sprogis
	recruit_character = LAT_tomas_spade
	recruit_character = LAT_vilhelms_munters
	recruit_character = LAT_janis_kaminskis
	recruit_character = LAT_janis_balodis
	recruit_character = LAT_fricis_virsaitis
	recruit_character = LAT_andrejs_krustinsh
	recruit_character = LAT_zhanis_bahs
	recruit_character = LAT_krisjanis_berkis
#########################################################################################

#########################################################################################
################################ Equipment and Research #################################
#########################################################################################
	set_convoys = 10
	set_research_slots = 2
	capital = 1097
	set_stability = 0.5
	set_war_support = 0.5

	1936.1.1 = {
		set_technology = {
			infantry_weapons = 1
				basic_train = 1
			infantry_weapons1 = 1
			early_fighter = 1
		}

		set_technology = {
			early_ship_hull_submarine = 1
		}
	}
1936.1.1 = {
	set_technology = {
		# Infantry
        infantry_weapons = 1
		infantry_weapons1 = 1
        armored_car1 = 1
        motorised_infantry = 1

		# Support
		tech_support = 1
		tech_engineers = 1

		#
		basic_train = 1
		gwtank_chassis = 1

		# Artillery
		gw_artillery = 1

		# Ships
		early_ship_hull_light = 1
		basic_torpedo = 1
		early_ship_hull_submarine = 1
		basic_battery = 1

		# Planes
		early_fighter = 1
		early_bomber = 1

		# Industry
		fuel_silos = 1

		# Doctrines Land
		trench_warfare = 1

		# Doctrine Sea
		fleet_in_being = 1
		battlefleet_concentration = 1

		# Doctrine Air
		force_rotation = 1
	}
}
1940.5.10 = {
	set_technology = {
		# Inf
        support_weapons = 1
        infantry_weapons2 = 1
        paratroopers = 1
        marines = 1

		# support
		tech_mountaineers = 1
		tech_special_forces = 1
		tech_flamethrower = 1
		tech_recon = 1
		tech_military_police = 1
		tech_maintenance_company = 1
		tech_field_hospital = 1
		tech_logistics_company = 1
		tech_signal_company = 1

		# Mechanized
		railway_gun = 1
		basic_light_tank_chassis = 1
		basic_medium_tank_chassis = 1
		basic_heavy_tank_chassis = 1
		armor_tech_1 = 1
		engine_tech_1 = 1

		# Artillery
		interwar_artillery = 1
		artillery1 = 1
		interwar_antiair = 1
		interwar_antitank = 1
		antiair1 = 1
		antitank1 = 1

		# Ships
		basic_ship_hull_light = 1
		smoke_generator = 1
		basic_depth_charges = 1
		sonar = 1
		early_ship_hull_cruiser = 1

		# Planes
		improved_airplane_launcher = 1
		early_ship_hull_heavy = 1
		basic_secondary_battery = 1
		mtg_transport = 1
		fighter1 = 1
		fighter2 = 1
		CAS1 = 1
		naval_bomber1 = 1
		tactical_bomber1 = 1
		scout_plane1 = 1

		# Industry
		electronic_mechanical_engineering = 1
		radio = 1
		radio_detection = 1
		excavation1 = 1
		construction1 = 1
		construction2 = 1
		basic_machine_tools = 1
		improved_machine_tools = 1
		dispersed_industry = 1
		dispersed_industry2 = 1

		# Doctrines Land
		grand_battle_plan = 1

		# Doctrine Sea
		subsidiary_carrier_role = 1

		# Doctrine Air
		fighter_baiting = 1
	}
}
1941.6.21 = {
	set_technology = {
		# Infantry
        support_weapons2 = 1
        improved_infantry_weapons = 1
        mechanised_infantry = 1
		improved_special_forces = 1

		# Support
		tech_engineers2 = 1
		tech_recon2 = 1

		# Mechanized
		wartime_train = 1
		armored_train = 1
		improved_light_tank_chassis = 1
		armor_tech_2 = 1
		engine_tech_2 = 1

		#Artillery
		artillery2 = 1
		antiair2 = 1
		antitank2 = 1

		# Ships
		improved_sonar = 1
		basic_ship_hull_cruiser = 1
		basic_cruiser_armor_scheme = 1
		basic_ship_hull_heavy = 1
		early_ship_hull_carrier = 1
		magnetic_detonator = 1
		basic_light_battery = 1
		basic_medium_battery = 1
		basic_heavy_battery = 1

		# Planes
		CAS2 = 1
		naval_bomber2 = 1
		strategic_bomber1 = 1

		# Industry
		mechanical_computing = 1
		decimetric_radar = 1
		basic_fire_control_system = 1
		fuel_refining = 1
		synth_oil_experiments = 1
		advanced_machine_tools = 1
		construction3 = 1
		dispersed_industry3 = 1

		# Doctrines Land
		prepared_defense = 1

		# Doctrine Sea
		hunter_killer_groups = 1

		# Doctrine Air
		low_echelon_support = 1
	}
}
1942.11.19 = {
	set_technology = {
		#Infantry
        support_weapons3 = 1
        improved_infantry_weapons_2 = 1
        infantry_at = 1
        paratroopers2 = 1
        marines2 = 1

		#Support
		tech_mountaineers2 = 1
		survival_training = 1
		tech_flamethrower2 = 1
		tech_military_police2 = 1
		tech_maintenance_company2 = 1
		tech_field_hospital2 = 1
		tech_logistics_company2 = 1
		tech_signal_company2 = 1

		# Mechanized
		improved_medium_tank_chassis = 1
		improved_heavy_tank_chassis = 1
		armor_tech_3 = 1
		engine_tech_3 = 1

		#Artillery
		rocket_artillery = 1

		#Ships
		improved_depth_charges = 1
		basic_heavy_armor_scheme = 1
		basic_ship_hull_submarine = 1
		basic_light_shell = 1
		basic_medium_shell = 1
		basic_heavy_shell = 1
		improved_secondary_battery = 1
		damage_control_1 = 1
		fire_control_methods_1 = 1
		tactical_bomber2 = 1

		#Planes
		heavy_fighter1 = 1

		#Industry
		construction4 = 1
		computing_machine = 1
		excavation2 = 1
		fuel_refining2 = 1
		dispersed_industry4 = 1

		# Doctrines Land
		grand_assault = 1

		# Doctrine Sea
		floating_fortress = 1

		# Doctrine Air
		dispersed_fighting = 1
	}
}
1943.7.11 = {
	set_technology = {
		#Infantry
        support_weapons4 = 1
        advanced_infantry_weapons = 1
        armored_car2 = 1
		jaegers = 1
		shocktroops = 1
		desertinfantry_at = 1

		#Support
		tech_engineers3_1 = 1
		tech_recon3 = 1

		#Mechanized
		advanced_light_tank_chassis = 1

		#Artillery
		artillery3 = 1
		antiair3 = 1
		antitank3 = 1

		#Ships
		improved_ship_hull_light = 1
		improved_cruiser_armor_scheme = 1
		improved_heavy_armor_scheme = 1
		improved_ship_torpedo_launcher = 1
		improved_ship_hull_submarine = 1
		basic_submarine_snorkel = 1
		improved_light_battery = 1
		improved_medium_battery = 1
		improved_heavy_battery = 1
		dp_secondary_battery = 1
		mtg_landing_craft = 1

		#Planes
		fighter3 = 1
		CAS3 = 1
		naval_bomber3 = 1
		tactical_bomber3 = 1
		strategic_bomber2 = 1
		scout_plane2 = 1

		#Industry
		improved_computing_machine = 1
		improved_decimetric_radar = 1
		improved_fire_control_system = 1
		assembly_line_production = 1
		oil_processing = 1
		rubber_processing = 1

		# Doctrines Land
		infantry_offensive = 1

		# Doctrine Sea
		convoy_sailing = 1

		# Doctrine Air
		operational_destruction = 1
	}
}
1944.6.6 = {
	set_technology = {
		#Infantry

		#Support
        tech_military_police3 = 1
		tech_maintenance_company3 = 1
		tech_field_hospital3 = 1
		tech_logistics_company3 = 1
		tech_signal_company3 = 1

		#Mechanized
		armor_tech_4 = 1
		engine_tech_4 = 1

		#Artillery
		artillery4 = 1
		rocket_artillery2 = 1
		antiair4 = 1
		antitank4 = 1
		infantry_at2 = 1

		#Ships
		improved_ship_hull_cruiser = 1
		improved_ship_hull_heavy = 1
		basic_ship_hull_carrier = 1
		electric_torpedo = 1
		advanced_ship_torpedo_launcher = 1
		improved_light_shell = 1
		improved_medium_shell = 1
		improved_heavy_shell = 1
		damage_control_2 = 1
		fire_control_methods_2 = 1

		#Planes
		fighter4 = 1
		CAS4 = 1
		naval_bomber4 = 1
		heavy_fighter2 = 1
		strategic_bomber3 = 1

		#Industry
		advanced_computing_machine = 1
		construction5 = 1
		excavation3 = 1
		fuel_refining3 = 1
		dispersed_industry5 = 1

		# Doctrines Land
		armored_operations = 1

		# Doctrine Sea
		convoy_escorts = 1

		# Doctrine Air
		fighter_veteran_initiative = 1
	}
}
1944.12.16 = {
	set_technology = {
		#Infantry
        night_vision = 1
        advanced_infantry_weapons2 = 1
        armored_car3 = 1
        paratroopers3 = 1
        marines3 = 1
		jaegers2 = 1
		shocktroops2 = 1
		desertinfantry_at2 = 1
		elite_forces = 1

		#Arty
		artillery5 = 1

		#Support
		tech_mountaineers3 = 1
		tech_flamethrower3 = 1

		#Mechanized
		advanced_medium_tank_chassis = 1
		advanced_heavy_tank_chassis = 1

		#Industry
		streamlined_line = 1
		centimetric_radar = 1

		# Doctrines Land
		infiltration_assault = 1

		# Doctrine Sea
		escort_carriers = 1
		submarine_operations = 1

		# Doctrine Air
		naval_strike_torpedo_tactics_oi = 1
	}
}
1945.1.1 = {
	set_technology = {
		#Infantry

		#Support
		tech_engineers4 = 1
		tech_recon4 = 1

		#Mechanized
		semi_modern_light_tank_chassis = 1

		#Artillery
		artillery_a = 1
		rocket_artillery3 = 1
		antiair5 = 1
		antitank5 = 1
		infantry_at_a = 1

		#Ships
		damage_control_3 = 1
		fire_control_methods_3 = 1

		#Planes
		tactical_bomber4 = 1
		strategic_bomber4 = 1

		#Industry
		improved_centimetric_radar = 1
		excavation4 = 1

		# Doctrines Land
		night_assault_tactics = 1

		# Doctrine Sea
		integrated_convoy_defence = 1
		undersea_blockade = 1

		# Doctrine Air
		cas_veteran_initiative = 1
	    tactical_bomber4 = 1
		strategic_bomber4 = 1

		# Doctrines Land
		night_assault_tactics = 1

		# Doctrine Sea
		integrated_convoy_defence = 1
		undersea_blockade = 1

		# Doctrine Air
		cas_veteran_initiative = 1
	}
}

#########################################################################################

#########################################################################################
######################################## Ideas ##########################################
#########################################################################################
	1936.1.1 = {
		add_ideas = {
			limited_conscription
		}
	}
	1940.5.10 = {
		add_ideas = {

		}
	}
	1941.6.21 = {
		add_ideas = {

		}
	}
	1942.11.19 = {
		add_ideas = {

		}
	}
	1943.7.11 = {
		add_ideas = {

		}
	}
	1944.6.6 = {
		add_ideas = {

		}
	}

	1944.12.16 = {
		add_ideas = {

		}
	}
	1945.1.1 = {
		add_ideas = {

		}
	}
#########################################################################################

#########################################################################################
######################################## Focuses ########################################
#########################################################################################
	1940.5.10 = {
		complete_national_focus = army_effort
		unlock_national_focus = equipment_effort
		unlock_national_focus = motorization_effort
		complete_national_focus = aviation_effort
		unlock_national_focus = industrial_effort
		complete_national_focus = production_effort
		complete_national_focus = production_effort_2
		complete_national_focus = construction_effort
		complete_national_focus = construction_effort_2
		complete_national_focus = construction_effort_3
		complete_national_focus = infrastructure_effort
		complete_national_focus = infrastructure_effort_2
		complete_national_focus = extra_tech_slot
		complete_national_focus = motorization_effort
		unlock_national_focus = doctrine_effort
		unlock_national_focus = doctrine_effort_2
		unlock_national_focus = equipment_effort
		unlock_national_focus = equipment_effort_2
		unlock_national_focus = fighter_focus
		unlock_national_focus = aviation_effort_2
		complete_national_focus = political_effort
		complete_national_focus = liberty_ethos
		complete_national_focus = neutrality_focus
	}

	1941.6.22 = {
		complete_national_focus = production_effort_3
		complete_national_focus = equipment_effort_3
		unlock_national_focus = CAS_effort
		unlock_national_focus = mechanization_effort
	}

	1942.11.19 = {
		complete_national_focus = secret_weapons
		complete_national_focus = deterrence
		complete_national_focus = why_we_fight
		complete_national_focus = technology_sharing
		unlock_national_focus = nuclear_effort
		unlock_national_focus = NAV_effort
		unlock_national_focus = armor_effort
	}
	1943.7.11 = {
		unlock_national_focus = special_forces
		unlock_national_focus = rocket_effort
	}
#########################################################################################

#########################################################################################
####################################### Settings ########################################
#########################################################################################
	### Settings
		set_country_flag = wants_major_news_events
		set_country_flag = wants_medium_news_events
		set_country_flag = wants_minor_news_events

		set_country_flag = wants_fallen_city_newsevents

		set_global_flag = player_wants_axis_resistance
		set_global_flag = player_wants_allies_resistance
	###

	### Bridges ###
		add_dynamic_modifier = { modifier = blowing_bridge_dynamic_modifier }
		add_dynamic_modifier = { modifier = repairing_bridge_dynamic_modifier }
	###
#########################################################################################

#########################################################################################
################################### Equipment Variants ##################################
#########################################################################################
	if = {
		limit = { has_dlc = "Man the Guns" }
		# Submarines #
		create_equipment_variant = {
			name = "Ronis Class"
			type = ship_hull_submarine_1
			name_group = LAT_SS_HISTORICAL
			parent_version = 0
			modules = {
				fixed_ship_torpedo_slot = ship_torpedo_sub_1
				fixed_ship_engine_slot = sub_ship_engine_1
				rear_1_custom_slot = empty
			}
		}
}
#########################################################################################

#########################################################################################
######################################## OOBs ###########################################
#########################################################################################
	oob = "_LAT_1936"
		set_naval_oob = "naval_mtg_LAT_1936"

	1940.1.1 = {
		oob = "_LAT_1940"
		set_naval_oob = "naval_mtg_LAT_1940"
	}

	1941.6.21 = {
		oob = "_LAT_1941"
		set_naval_oob = "naval_mtg_LAT_1941"
	}

	1942.11.19 = {
		oob = "_LAT_1942_11"
		set_naval_oob = "naval_mtg_LAT_1942_11"
	}

	1943.1.1 = {
		oob = "_LAT_1943"
		set_naval_oob = "naval_mtg_LAT_1943"
	}

	1944.6.1 = {
		oob = "_LAT_1944_6"
		set_naval_oob = "naval_mtg_LAT_1944_6"
	}



	1944.12.1 = {
		oob = "_LAT_1944_12"
		set_naval_oob = "naval_mtg_LAT_1944_12"
	}

	1945.1.1 = {
		oob = "_LAT_1945"
		set_naval_oob = "naval_mtg_LAT_1945"
	}
#########################################################################################
