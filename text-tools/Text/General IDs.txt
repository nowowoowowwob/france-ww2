#######################################
### List of General and Admiral IDs ###
#######################################

### Continents ###
	Europe: 0-4999
	Eastern Europe: 5000-7499
	America and British Isles: 7500-9999
	Scandinavia: 10000-12499
	Middle East 15000 - 17499
###

### Countries ###
	### Europe 0-4999 ###
		# Germany 0-199 #
			1 - Gerd von Rundstedt
			2 - Günther von Kluge
			3 - Friedrich Paulus
			4 - Erwin Rommel
			5 - Erich von Manstein
			6 - Heinz Guderian
			7 - Wilhelm List
			8 - Ewald von Kleist
			9 - Fedor von Bock
			10 - Walter Model
			11 - Maximilian von Weichs
			13 - Wilhelm Ritter von Leeb
			14 - Georg-Hans Reinhardt
			15 - Walter Krüger
			16 - Kurt Student
			17 - Erwin von Witzleben
			18 - Hermann Hoth
			19 - Johannes Blaskowitz
			20 - Paul Hausser
			21 - Friedrich Schulz
			22 - Georg von Küchler
			23 - Alfred Jodl
			24 - Sepp Dietrich
			25 - Hasso von Manteuffel
			26 - Karl Dönitz
			27 - Erich Raeder
			28 - Alfred Saalwächter"
			29 - Hermann Boehm"
			30 - Wilhelm Marschall"
			31 - Günther Lütjens"
			32 - Albert Kesselring
			33 - Gotthard Heinrici
			34 - Ernst-Eberhard Hell
			35 - Felix Steiner

		# France 200-399 #
			200 - Jean de Lattre de Tassigny
			201 - Alphonse Juin
			202 - Maurice Gamelin
			203 - Maxime Weygand
			204 - Alphonse Georges
			205 - Antoine-Marie-Benoît Besson
			206 - Charles De Gaulle
			207 - Philippe Leclerc
			208 - Henri Giraud
			209 - Charles Huntziger
			210 - Gaston-Henri Billotte
			211 - Henry Freydenberg
			212 - René Olry
			213 - François Darlan
			214 - Jean-Marie Charles Abrial
			215 - Jean-Pierre Esteva
			216 - René-Émile Godfroy
			217 - Jean de Laborde
		# Italy 400-599 #
			400 - Pietro Badoglio
			401 - Emilio De Bono
			402 - Ugo Cavallero
			403 - Giovanni Messe
			404 - Italo Balbo
			405 - Sebastiano Visconti Prasca
			406 - Junio Valerio Borghese 'Black Prince'
			407 - Italo Gariboldi
			408 - Ubaldo Soddu
			409 - Rodolfo Graziani
			410 - Inigo Campioni
			411 - Alberto Da Zara
			412 - Giuseppe Fioravanzo
			413 - Carlo Bergamini
			414 - Angelo Iachino
		# Albania 600-699 #
			600 - Xhemal Aranitasi
		# Austria 700-799 #
			700 - Karl Eglseer
		# Belgium 800-899 #
			800 - M.C.L. Keyaerts
			801 - Georges Timmermans
		# Denmark 900-999 #
			900 - Wilhelm Wain Prior"
		# Greece 1000-1099 #
			1000 - Alexander Papagos
			1001 - Ioannis Pitsikas
		# Netherlands 1100-1199 #
			1100 - Izaak Reijnders
			1101 - Petrus Wilhelmus Best
			1102 - Godfried van Voorst tot Voorst
			1103 - Johannes Theodorus Furstner
			1104 - Conrad Helfrich
			1105 - Karel Doorman
		# Portugal 1200-1299 #

		# Spain 1300-1399 #
			1301 - José Asensio Torrado
			1302 - Vicente Rojo Lluch
			1303 - José Miaja
			1304 - Valentín González
			1305 - Juan Modesto
			1306 - Antonio Cordón García
			1307 - János Gálicz
			1308 - Francisco Franco
			1309 - Mohamed Meziane
			1310 - Gonzalo Queipo de Llano
			1311 - Emilio Mola
			1312 - Juan Yagüe
			1313 - Agustín Muñoz Grandes
			1314 - José Enrique Varela
			1315 - Miguel Cabanellas
			1316 - José Millán Astray
			1317 - Miguel Buiza Fernández-Palacios
			1318 - Luis Carrero Blanco
		# Switzerland 1400-1499 #
			1400 - Henri Guisan
		# Slovakia 1500-1599 #

		# Czechoslovakia 1600-1699 #
			1600 - Vojtěch Luža
			1601 - Josef Šnejdárek
			1602 - Richard Tesařík
			1603 - Sergej Vojcechovský

		# Vichy France 1700-1799 #


	### Eastern Europe 5000-7499 ###
		# Soviet Union 5000-5199 #
			5000 - Aleksandr Vasilevsky
			5001 - Boris Shaposhnikov
			5002 - Nikolai Vatutin
			5003 - Ivan Konev
			5004 - Semyon Timoshenko
			5005 - Kliment Voroshilov
			5006 - Mikhail Tukhachevsky
			5007 - Semyon Budyonny
			5008 - Ivan Chernyakhovsky
			5009 - Leonid Govorov
			5010 - Filipp Golikov
			5011 - Andrey Yeryomenko
			5012 - Georgiy Zakharov
			5013 - Yakov Cherevichenko
			5014 - Max Reyter
			5015 - Nikandr Chibisov
			5016 - Vasily Kuznetsov
			5017 - Andrey Vlasov
			5018 - Ivan Fedyuninsky
			5019 - Maksim Purkayev
			5020 - Kuzma Galitsky
			5021 - Markian Popov
			5022 - Nikolai Berzarin
			5023 - Alexander Gorbatov
			5024 - Kirill Moskalenko
			5025 - Dmitry Lelyushenko
			5026 - Andrei Grechko
			5027 - Rodion Malinovsky
			5028 - Filipp Oktyabrskiy
			5029 - Sergey Gorshkov
			5030 - Gordey Levchenko
			5031 - Arseniy Golovko
			5032 - Vladimir Kasatonov
			5033 - Kirill Meretskov
			5034 - Grigory Kulik
		# Bulgaria 5200-5299 #
			5200 - Vasil Tenev Boydev
			5201 - Georgi Nikolov Popov
		# Estonia 5300-5399 #

		# Hungary 5400-5499 #
			5400 - Géza Lakatos
			5401 - Iván Hindy
			5402 - Béla Miklós
			5403 - Gábor Faragho
			5404 - Dezsö László
			5405 - Gusztáv Jány
			5406 - Zoltán Szügyi
			5407 - Ferenc Farkas
			5408 - Marcel Stomm
			5409 - Lajos Csatay
			5410 - Károly Beregfy
			5411 - Lajos Veress
			5412 - István Bata
			5413 - Joseph von Habsburg
			5414 - Ferenc Feketehalmy-Czeydner
			5415 - Erwin von Witzleben
			5416 - Wilhelm Ritter von Leeb
			5417 - Mihajlo Lukić
			5418 - Rafael Boban
			5419 - Vladimir Laxa
			5420 - János Legeza
			5421 - László Hollósy-Kuthy
			5422 - Antal Vattay
			5423 - István Kozma
			5424 - Miklós Horthy
			5425 - Kálmán Hardy
			5426 - Olaf Richárd Wulf
			5427 - Marijan Polić
			5428 - Georg von Trapp
		# Latvia 5500-5599 #

		# Lithuania 5600-5699 #

		# Poland 5700-5799 #
			5700 - Władysław Sikorski
			5701 - Władysław Bortnowski
			5702 - Stanisław Kopański
			5703 - Władysław Anders
			5704 - Roman Abraham
			5705 - Wincenty Kowalski
		# Romania 5800-5899 #
			5800 - Petre Dumitrescu
			5801 - Ion Antonescu
			5802 - Ioan Mihail Racovita
		# Yugoslavia 5900-5999 #
			5900 - Vladimir Čukavac
			5901 - Ivan Gošnjak
			5902 - Danilo Kalafatović
			5903 - Milutin Nedić
			5904 - Petar Kosić
			5905 - Josef Depre
			5906 - Marijan Polić

	### America and British Isles 7500-9999 ###
		# United Kingdom 7500-7699 #

		# USA 7700-7899 #
			7700 - George S. Patton
			7701 - Omar Bradley
			7702 - Dwight D. Eisenhower
			7703 - Geoffrey Keyes
			7704 - Lucian Truscott
			7705 - Mark Clark
			7706 - Douglas MacArthur
			7707 - Walter Krueger
			7708 - Lloyd Fredendall
			7709 - Leslie McNair
			7710 - Jonathan Wainwright
			7711 - Russell Hartle
			7712 - Leonard Gerow
			7713 - Edward Brooks
			7714 - Clarence Huebner
			7715 - Alexander Patch
			7716 - William Simpson
			7717 - J. Lawton Collins
			7718 - Courtney Hodges
			7719 - Joseph Stilwell
			7720 - Oscar Griswold
			7721 - Maurice Rose
			7722 - Ernest King
			7723 - Chester W. Nimitz
			7724 - William Halsey, Jr.
			7725 - Frank Jack Fletcher
			7726 - Raymond A. Spruance
			7727 - Harold Rainsford Stark
			7728 - Arleigh Burke
			7729 - Charles M. Cooke, Jr.
		# Canada 7900-7999 #
			7900 - Thomas Victor Anderson
			7901 - Charles Foulkes
			7902 - John Meredith Rockingham
			7903 - Bert Hoffmeister
			7904 - Leonard W. Murray
			7905 - H.T. Baillie-Grohman
			7906 - James D. Prentice
			7907 - Harry DeWolf
		# Ireland 8000-8099 #
			8000 - Michael Costello

	### Scandinavia 10000-12499 ###
		# Finland 10000-10099 # Yes I know # #

		# Norway 10100-10199 #

		# Sweden 10200-10299 #
			10200 - Folke Högberg
			10201 - Erik af Edholm
			10202 - Claes Lindström
	### Africa 12500-14999 ###
		# Egypt 12500-12599 #

	### Middle East 15000 - 17499 ###

		# Iraq 15000-15099 #

		# Iran 15100-15199 #

		# Turkey 15200-15299 #

	### Multiplayer 17500 - 17XXX ###
		# Multiplayer 1 17500-17599 #
	         17500 - Alan Brooke
	         17501 - Bernard Montgomery
	         17502 - Neil Ritchie
	         17503 - Alan Cunningham
	         17504 - Richard O'Connor
	         17505 - George Giffard
	         17506 - Harold Alexander
	         17507 - Henry Wilson
	         17508 - Oliver Leese
	         17509 - Miles Dempsey
	         17510 - Brian Horrocks
	         17511 - George S. Patton
	         17512 - Omar Bradley
	         17513 - Dwight D. Eisenhower
	         17514 - Geoffrey Keyes
	         17515 - Lucian Truscott
	         17516 - Mark Clark
	         17517 - Leslie McNair
	         17518 - Leonard Gerow
	         17519 - Edward Brooks
	         17520 - Clarence Huebner
	         17521 - Alexander Patch
	         17522 - William Simpson
	         17523 - J. Lawton Collins
	         17524 - Courtney Hodges
	         17525 - Maurice Rose
	         17526 - Jean de Lattre de Tassigny
	         17527 - Alphonse Juin
	         17528 - Alphonse Georges
	         17529 - Charles De Gaulle
	         17530 - Henri Giraud
	         17531 - Maxime Weygand
	         17532 - Maurice Gamelin
	    # Multiplayer 2 17600-17699 #
	         17600 - Alan Brooke
	         17601 - Bernard Montgomery
	         17602 - Neil Ritchie
	         17603 - Alan Cunningham
	         17604 - Richard O'Connor
	         17605 - George Giffard
	         17606 - Harold Alexander
	         17607 - Henry Wilson
	         17608 - Oliver Leese
	         17609 - Miles Dempsey
	         17610 - Brian Horrocks
	         17611 - George S. Patton
	         17612 - Omar Bradley
	         17613 - Dwight D. Eisenhower
	         17614 - Geoffrey Keyes
	         17615 - Lucian Truscott
	         17616 - Mark Clark
	         17617 - Leslie McNair
	         17618 - Leonard Gerow
	         17619 - Edward Brooks
	         17620 - Clarence Huebner
	         17621 - Alexander Patch
	         17622 - William Simpson
	         17623 - J. Lawton Collins
	         17624 - Courtney Hodges
	         17625 - Maurice Rose
	         17626 - Jean de Lattre de Tassigny
	         17627 - Alphonse Juin
	         17628 - Alphonse Georges
	         17629 - Charles De Gaulle
	         17630 - Henri Giraud
	         17631 - Maxime Weygand
	         17632 - Maurice Gamelin
	    # Multiplayer 3 17700-17799 #
	         17700 - Alan Brooke
	         17701 - Bernard Montgomery
	         17702 - Neil Ritchie
	         17703 - Alan Cunningham
	         17704 - Richard O'Connor
	         17705 - George Giffard
	         17706 - Harold Alexander
	         17707 - Henry Wilson
	         17708 - Oliver Leese
	         17709 - Miles Dempsey
	         17710 - Brian Horrocks
	         17711 - George S. Patton
	         17712 - Omar Bradley
	         17713 - Dwight D. Eisenhower
	         17714 - Geoffrey Keyes
	         17715 - Lucian Truscott
	         17716 - Mark Clark
	         17717 - Leslie McNair
	         17718 - Leonard Gerow
	         17719 - Edward Brooks
	         17720 - Clarence Huebner
	         17721 - Alexander Patch
	         17722 - William Simpson
	         17723 - J. Lawton Collins
	         17724 - Courtney Hodges
	         17725 - Maurice Rose
	         17726 - Jean de Lattre de Tassigny
	         17727 - Alphonse Juin
	         17728 - Alphonse Georges
	         17729 - Charles De Gaulle
	         17730 - Henri Giraud
	         17731 - Maxime Weygand
	         17732 - Maurice Gamelin
	    # Multiplayer 4 17800-17899 #
	         17800 - Alan Brooke
	         17801 - Bernard Montgomery
	         17802 - Neil Ritchie
	         17803 - Alan Cunningham
	         17804 - Richard O'Connor
	         17805 - George Giffard
	         17806 - Harold Alexander
	         17807 - Henry Wilson
	         17808 - Oliver Leese
	         17809 - Miles Dempsey
	         17810 - Brian Horrocks
	         17811 - George S. Patton
	         17812 - Omar Bradley
	         17813 - Dwight D. Eisenhower
	         17814 - Geoffrey Keyes
	         17815 - Lucian Truscott
	         17816 - Mark Clark
	         17817 - Leslie McNair
	         17818 - Leonard Gerow
	         17819 - Edward Brooks
	         17820 - Clarence Huebner
	         17821 - Alexander Patch
	         17822 - William Simpson
	         17823 - J. Lawton Collins
	         17824 - Courtney Hodges
	         17825 - Maurice Rose
	         17826 - Jean de Lattre de Tassigny
	         17827 - Alphonse Juin
	         17828 - Alphonse Georges
	         17829 - Charles De Gaulle
	         17830 - Henri Giraud
	         17831 - Maxime Weygand
	         17832 - Maurice Gamelin
