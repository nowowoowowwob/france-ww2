######################################
############## Infantry ##############
######################################

	########### Support weapons ###########
		support_weapons
		support_weapons2
		support_weapons3
		support_weapons4
		night_vision
		night_vision2

	############# Infantry ###############
		infantry_weapons
		infantry_weapons1
		infantry_weapons2
		improved_infantry_weapons
		improved_infantry_weapons_2
		advanced_infantry_weapons
		advanced_infantry_weapons2
		advanced_infantry_weapons_a
		advanced_infantry_weapons_b

		########## Anti-tank weapons #########
		infantry_at
		infantry_at2
		infantry_at_a
		infantry_at_b

	############ Armored Car #############
		armored_car1
		armored_car2
		armored_car3
		armored_car_at_upgrade

	############# Mechanised #############
		motorised_infantry
		motorized_rocket_unit
		mechanised_infantry
		mechanised_infantry2
		mechanised_infantry3
		mechanised_infantry4
		mechanised_infantry5

		######## Amphibious Mechanised #######
		amphibious_mechanized_infantry
		amphibious_mechanized_infantry_2

	############ Paratroopers ############
		paratroopers
		paratroopers2
		paratroopers3
		paratroopers4

	############## Marines ###############
		marines
		marines2
		marines3
		marines4

	############ Mountaineers ############
		tech_mountaineers
		tech_mountaineers2
		tech_mountaineers3
		tech_mountaineers4

	############### Jaegers ##############
		jaegers
		jaegers2
		jaegers3
		jaegers4

	############# Shocktroops ############
		shocktroops
		shocktroops2
		shocktroops3
		shocktroops4

	########### Desert Infantry ##########
		desertinfantry_at
		desertinfantry_at2
		desertinfantry_at3
		desertinfantry_at4

	########### Special Forces ###########
		tech_special_forces
		advanced_special_forces
		improved_special_forces
		extreme_environment_training
		survival_training
		elite_forces

######################################
############### Support ##############
######################################
	############# Engineer ###############
		tech_support
		tech_engineers
		tech_engineers2
		tech_engineers3 - tech_engineers3_1
		tech_engineers4
		tech_engineers5

	############# Sappers ###############
		tech_sappers
		tech_sappers2
		tech_sappers3
		tech_sappers4

	########### Flamethrower ############
		tech_flamethrower
		tech_flamethrower2 - tech_flamethrower2_1
		tech_flamethrower3
		tech_flamethrower4

	############### Recon ###############
		tech_recon
		tech_recon2
		tech_recon3
		tech_recon4
		tech_recon5

		######### Military Police ###########
		tech_military_police
		tech_military_police2
		tech_military_police3
		tech_military_police4
		tech_military_police5

	########### Maintenance #############
		tech_maintenance_company
		tech_maintenance_company2
		tech_maintenance_company3
		tech_maintenance_company4
		tech_maintenance_company5

		########## Field Hospital ###########
		tech_field_hospital
		tech_field_hospital2
		tech_field_hospital3
		tech_field_hospital4
		tech_field_hospital5

	############ Logistics ##############
		tech_logistics_company
		tech_logistics_company2
		tech_logistics_company3
		tech_logistics_company4
		tech_logistics_company5

		########## Signal Company ###########
		tech_signal_company
		tech_signal_company2
		tech_signal_company3
		tech_signal_company4
		tech_signal_company5

	############### Trains ###############
		basic_train
		railway_gun
		wartime_train
		armored_train

######################################
################ Armour ##############
######################################
	############ Light Armour ############
		gwtank_chassis
		basic_light_tank_chassis
		improved_light_tank_chassis
		advanced_light_tank_chassis
		semi_modern_light_tank_chassis
		modern_light_tank_chassis

		########## Amphibious Armour #########
		amphibious_tank_chassis
		amphibious_drive
		improved_amphibious_tank_chassis

	############ Medium Armour ###########
		basic_medium_tank_chassis
		improved_medium_tank_chassis
		advanced_medium_tank_chassis
		semi_modern_medium_tank_chassis
		modern_medium_tank_chassis

	############ Heavy Armour ############
		basic_heavy_tank_chassis
		improved_heavy_tank_chassis
		advanced_heavy_tank_chassis
		semi_modern_heavy_tank_chassis
		modern_heavy_tank_chassis

		########## Superheavy Armour #########
		super_heavy_tank_chassis
		improved_super_heavy_tank_chassis

	############### Armour ###############
		armor_tech_1
		armor_tech_2
		armor_tech_3
		armor_tech_4

	############### Engine ###############
		engine_tech_1
		engine_tech_2
		engine_tech_3
		engine_tech_4

######################################
############## Artillery #############
	######################################
		############# Artillery ##############
		gw_artillery
		interwar_artillery
		artillery1
		artillery2
		artillery3
		artillery4
		artillery5
		artillery_a
		artillery_b
		artillery_c
		artillery_d
		artillery_e

		########## Rocket Artillery ##########
		rocket_artillery
		rocket_artillery2
		rocket_artillery3
		rocket_artillery4
		rocket_artillery_a
		rocket_artillery_b
		rocket_artillery_c

	############## Anti-air ##############
		antiair1
		antiair2
		antiair3
		antiair4
		antiair5
		antiair_a
		antiair_b
		antiair_c
		antiair_d
		antiair_e

	############## Anti-tank ##############
		antitank1
		antitank2
		antitank3
		antitank4
		antitank5
		antitank_a
		antitank_b
		antitank_c
		antitank_d
		antitank_e

######################################
################ Navy ################
######################################
	############# Light Ships ############
		early_ship_hull_light
		basic_ship_hull_light
		improved_ship_hull_light
		advanced_ship_hull_light
		smoke_generator
		basic_depth_charges
		improved_depth_charges
		advanced_depth_charges
		modern_depth_charges
		sonar
		improved_sonar

	############## Cruisers ##############
		early_ship_hull_cruiser
		basic_ship_hull_cruiser
		improved_ship_hull_cruiser
		advanced_ship_hull_cruiser
		improved_airplane_launcher
		basic_cruiser_armor_scheme
		improved_cruiser_armor_scheme
		advanced_cruiser_armor_scheme

	############# Heavy Ships ############
		early_ship_hull_heavy
		basic_ship_hull_heavy
		improved_ship_hull_heavy
		advanced_ship_hull_heavy
		ship_hull_super_heavy
		basic_heavy_armor_scheme
		improved_heavy_armor_scheme

	############## Carriers ##############
		early_ship_hull_carrier
		basic_ship_hull_carrier
		improved_ship_hull_carrier
		advanced_ship_hull_carrier

	############# Submarines #############
		basic_torpedo
		magnetic_detonator
		electric_torpedo
		homing_torpedo
		improved_ship_torpedo_launcher
		advanced_ship_torpedo_launcher
		modern_ship_torpedo_launcher
		early_ship_hull_submarine
		basic_ship_hull_submarine
		improved_ship_hull_submarine
		advanced_ship_hull_submarine
		basic_submarine_snorkel
		improved_submarine_snorkel

	############# Armaments ##############
		basic_battery
		basic_light_battery
		improved_light_battery
		advanced_light_battery
		basic_light_shell
		improved_light_shell
		basic_medium_battery
		improved_medium_battery
		advanced_medium_battery
		basic_medium_shell
		improved_medium_shell
		basic_heavy_battery
		improved_heavy_battery
		advanced_heavy_battery
		basic_heavy_shell
		improved_heavy_shell
		basic_secondary_battery
		improved_secondary_battery
		dp_secondary_battery

	############## Armaments #############
		damage_control_1
		damage_control_2
		damage_control_3

	############ Fire Control ############
		fire_control_methods_1
		fire_control_methods_2
		fire_control_methods_3

	############# Transports #############
		mtg_transport
		mtg_landing_craft
		mtg_tank_landing_craft

######################################
############## Aircraft ##############
######################################
############## Fighters ##############
		early_fighter
		cv_early_fighter
		fighter1
		cv_fighter1
		fighter2
		cv_fighter2
		fighter3
		cv_fighter3
		fighter4
		cv_fighter4
		fighter5
		cv_fighter5
		fighter6
		cv_fighter6
		jet_fighter1
		jet_fighter2
		jet_fighter3

	################ CAS ################
		CAS1
		cv_CAS1
		CAS2
		cv_CAS2
		CAS3
		cv_CAS3
		CAS4
		cv_CAS4
		CAS5
		cv_CAS5
		CAS6
		cv_CAS6

	########### Naval Bombers ###########
		naval_bomber1
		cv_naval_bomber1
		naval_bomber2
		cv_naval_bomber2
		naval_bomber3
		cv_naval_bomber3
		naval_bomber4
		cv_naval_bomber4
		naval_bomber5
		cv_naval_bomber5
		naval_bomber6
		cv_naval_bomber6

		########## Tactical Bombers ##########
		early_bomber
		tactical_bomber1
		tactical_bomber2
		tactical_bomber3
		tactical_bomber4
		tactical_bomber5
		tactical_bomber6
		jet_tactical_bomber1
		jet_tactical_bomber2
		jet_tactical_bomber3

	########### Heavy Fighters ###########
		heavy_fighter1
		heavy_fighter2
		heavy_fighter3
		heavy_fighter4
		heavy_fighter5
		heavy_fighter6

		######### Strategic Bombers ##########
		strategic_bomber1
		strategic_bomber2
		strategic_bomber3
		strategic_bomber4
		strategic_bomber5
		strategic_bomber6

	############ Scout Planes ############
		scout_plane1
		scout_plane2

######################################
############## Industry ##############
######################################
		# Electronic Mechanical Engineering ##
		electronic_mechanical_engineering
		mechanical_computing
		computing_machine
		improved_computing_machine
		advanced_computing_machine
		future_computing_machine
		future_computing_machine_2

	############### Radio ################
		radio
		radio_detection
		decimetric_radar
		improved_decimetric_radar
		centimetric_radar
		improved_centimetric_radar
		advanced_centimetric_radar
		future_radar
		future_radar_2

	############ Fire Control ############
		basic_fire_control_system
		improved_fire_control_system
		advanced_fire_control_system

	############ Jet Engines #############
		experimental_rockets
		rocket_engines
		improved_rocket_engines
		advanced_rocket_engines
		jet_engines

	############### Nukes ################
		atomic_research
		nuclear_reactor
		combined_nuclear_effort
		advanced_moderators
		nukes

######################################
############## Industry ##############
######################################
	########### Machine Tools ############
		basic_machine_tools
		improved_machine_tools
		advanced_machine_tools
		assembly_line_production
		flexible_line
		flexible_line_a
		streamlined_line
		streamlined_line_a
		streamlined_line_b

	############ Conversion #############
		improved_equipment_conversion
		advanced_equipment_conversion
		modern_equipment_conversion

	########### Concentrated ############
		concentrated_industry
		concentrated_industry2
		concentrated_industry3
		concentrated_industry4
		concentrated_industry5
		concentrated_industry_a
		concentrated_industry_b

	############ Dispersed #############
		dispersed_industry
		dispersed_industry2
		dispersed_industry3
		dispersed_industry4
		dispersed_industry5
		dispersed_industry_a
		dispersed_industry_b

		########## Fuel Refining ###########
		fuel_silos
		fuel_refining
		fuel_refining2
		fuel_refining3
		fuel_refining4
		fuel_refining5
		fuel_refining6
		fuel_refining7

		########## Synthetic Oil ###########
		synth_oil_experiments
		oil_processing
		improved_oil_processing
		advanced_oil_processing
		modern_oil_processing
		oil_processing_6
		oil_processing_7

		######## Synthetic Rubber #########
		rubber_processing
		improved_rubber_processing
		advanced_rubber_processing
		modern_rubber_processing
		rubber_plant_a
		rubber_plant_b

		########## Construction ###########
		construction1
		construction2
		construction3
		construction4
		construction5
		construction_a
		construction_b

	########### Excavation ############
		excavation1
		excavation2
		excavation3
		excavation4
		excavation5
		excavation_a
		excavation_b

	######################################
		############ Land Doctrine ###########
		######################################
		########## Mobile Warfare ############
		mobile_warfare
		delay
		elastic_defence
		mobile_infantry 			- armored_spearhead
		mass_motorization 			- schwerpunk
		mechanised_offensive 		- blitzkrieg
		kampfgruppe
		volkssturm 					- firebrigades
		nd_conscription 			- backhand_blow
		werwolf_guerillas 			- modern_blitzkrieg
		MW_3rd_Left_1 				- MW_3rd_Right_1
		MW_3rd_Left_2 				- MW_3rd_Right_2
		MW_3rd_Left_3 				- MW_3rd_Right_3

		######## Superior Firepower #########
		superior_firepower
		sup_delay
		mobile_defence
		dispersed_support			- intergrated_support
		overwhelming_firepower		- regimental_combat_teams
		sup_mechanized_offensive
		concentrated_fire_plans		- centralized_fire_control
		combined_arms				- forward_observers
		tactical_control			- advanced_firebases
		air_land_battle				- shock_and_awe
		observer_balloons
		tactical_smoke_deployment
		concentration_of_firepower 	- rear_echelon_disruption

		######## Grand Battleplan #########
		trench_warfare
		grand_battle_plan
		prepared_defense
		grand_assault
		grand_mechanized_offensive 	- infantry_offensive
		assault_concentration 		- armored_operations
		branch_interoperation 		- infiltration_assault
		assault_breakthrough 		- night_assault_tactics
		central_planning 			- attritional_containment
		c3i_theory 					- infiltration_in_depth
		hedgerow_combat
		truck_convoy_systems		- advanced_deception_strategy
		broad_front_strategy		- integrated_resistance_fighters

		######### Mass Assault ##########
		mass_assault
		pocket_defence
		defence_in_depth
		large_front_operations 		- peoples_army
		deep_operations				- human_infantry_offensive
		operational_concentration	- x
		vast_offensives				- large_front_offensive
		breakthrough_priority		- human_wave_offensive
		mechanized_wave				- x
		continuous_offensive		- guerilla_warfare
		anti_tank_ditches
		localised_counter_attacks
		eternal_meat_grinder

	######################################
		########### Naval Doctrine ###########
		######################################

	########### Fleet in Being ###########
		fleet_in_being
		# Fleet in Being Left #
		battlefleet_concentration
		subsidiary_carrier_role
		hunter_killer_groups
		floating_fortress
		floating_airfield # shared
		grand_battlefleet # shared

		# Fleet in Being Center #
		convoy_sailing
		convoy_escorts
		escort_carriers
		integrated_convoy_defence

		# Fleet in Being Right #
		submarine_operations
		undersea_blockade
		convoy_interdiction
		submarine_offensive

		######### Trade Interdiction #########
		trade_interdiction
		# Trade interdiction left #
		raider_patrols
		capital_ship_raiders
		battlefleet_concentration_ti
		floating_fortress_ti
		floating_airfield_ti # shared

		# Trade interdiction center #
		carrier_operations
		convoy_sailing_ti
		subsidiary_carrier_role_ti
		naval_air_operations

		# Trade interdiction right #
		convoy_interdiction_ti
		unrestricted_submarine_warfare
		wolfpacks
		advanced_submarine_warfare
		combined_operations_raiding

	############ Base Strike ############
		base_strike
		# Base Strike left #
		convoy_escorts_bs
		escort_patrols
		convoy_sailing_bs
		integrated_convoy_defence_bs

		# Base Strike center #
		submarine_operations_bs
		undersea_blockade_bs
		convoy_interdiction_bs
		submarine_offensive_bs

		# Base Strike right #
		carrier_primacy
		carrier_task_forces
		floating_airfield_bs
		massed_strikes
		floating_fortress_bs
		carrier_battlegroups

	######################################
		############ Air Doctrine ############
		######################################

		####### Strategic Destruction ########
		air_superiority
		infrastructure_destruction
		home_defence
		naval_strike_tactics
		fighter_sweeps 				- dogfighting_experience
		multialtitude_flying
		logistical_bombing
		night_bombing				- day_bombing
		massed_bomber_formations	- air_offense
		flying_fortress
		offensive_formations
		mass_destruction

		######## Battlefield Support #########
		formation_flying
		dive_bombing
		direct_ground_support
		formation_fighting			- fighter_ace_initiative
		hunt_and_destroy
		combat_unit_destruction
		battlefield_support
		keypoint_bombing
		ground_support_integration
		naval_strike_torpedo_tactics
		strategic_destruction
		forward_interception

		####### Operational Integrity ########
		force_rotation
		fighter_baiting
		low_echelon_support
		dispersed_fighting
		operational_destruction
		fighter_veteran_initiative
		naval_strike_torpedo_tactics_oi
		cas_veteran_initiative
		carousel_bombing
		infiltration_bombing
		air_skirmish
		high_level_bombing
