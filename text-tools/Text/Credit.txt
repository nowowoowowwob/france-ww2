Primo Victoria was not built in one day, and it was not built alone. The following people directly assisted on the mod.

[h1]Coders[/h1]
MisterJay
Rudy
Agemo
ucicuyh

[h1]GFX[/h1]
Alfred Rosenberg
Birran

[h1]VPs[/h1]
JandJTV
Sirvanyx🇧🇪

[h1]Event Writing[/h1]
AceAdamGaming
moonite
Netgearghost
Illuminutter
Sir Dankus
Snake007
Watt


If permission is given by the Primo Victoria team to use (certain parts of) the mod, this EXCLUDES the following. Permission will have to be asked separately.

The following people indirectly assisted on the mod or provided resources:

[h1]Map[/h1]
Edouard Saladier, for the use of his France map

[h1]Code[/h1]
BlackICE for various things
Cakefound for their "¡Viva Espana! - Spain Reimagined" mod
Herman Lindqvist and SwordOfWrynn for their "Special Forces" mod

[h1]GFX[/h1]
Indyclone
Brammeke
The people who share their art in the Coop
