1944:
    Panzer II:
        Wespe: 144

    Panzer 38(t):
        marder 3: 138
        grille: 346
        jagdpanzer 38(t) hetzer: 1687

    Panzer III:
        Stug III F/G: 3849
        StuH 42: 903

    Panzer IV:
        Panzer IV F2-J: 3126
        Stug IV: 1006
        Jagdpanzer IV: 769
        Jagdpanzer IV/70: 767
        Sturmpanzer IV: 215
        Hornisse: 133
        Hummel: 289
        Möbelwagen: 205
        Wirbelwind: 100
        Ostwind: 15

    Panzer V:
        Panther: 3777
        Jagdpanther: 226

    Panzer VI:
        Tiger I: 623
        Sturmtiger: 18
        Tiger II: 377
        Jagdtiger: 51
